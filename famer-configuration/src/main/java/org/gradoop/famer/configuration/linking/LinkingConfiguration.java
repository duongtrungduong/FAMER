/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.linking;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.StandAloneConfiguration;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.famer.configuration.linking.blocking.BlockingConfiguration;
import org.gradoop.famer.configuration.linking.selection.SelectionConfiguration;
import org.gradoop.famer.configuration.linking.similarity.SimilarityConfiguration;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponent;
import org.gradoop.famer.linking.linking.Linker;
import org.gradoop.famer.linking.linking.dataStructures.LinkerComponent;
import org.gradoop.famer.linking.selection.dataStructures.SelectionComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Class to parse and run the JSON configuration for linking.
 */
public class LinkingConfiguration implements StandAloneConfiguration {

  /**
   * The linker instance to run the linking
   */
  private Linker linker;

  /**
   * The structure that keeps the required input parameters as well as required helper methods needed for
   * {@link Linker}
   */
  private LinkerComponent linkerComponent;

  /**
   * The list of blocking components needed for linking. Blocking is the 1st step of linking
   */
  private List<BlockingComponent> blockingComponents;

  /**
   * The list of similarity components needed for linking. Similarity computation is the 2nd step of linking
   */
  private List<SimilarityComponent> similarityComponents;

  /**
   * The selection component needed for linking. Selection computation is the 3rd step of linking
   */
  private SelectionComponent selectionComponent;

  public Linker getLinker() {
    return linker;
  }

  public LinkerComponent getLinkerComponent() {
    return linkerComponent;
  }

  public List<BlockingComponent> getBlockingComponents() {
    return blockingComponents;
  }

  public List<SimilarityComponent> getSimilarityComponents() {
    return similarityComponents;
  }

  public SelectionComponent getSelectionComponent() {
    return selectionComponent;
  }

  /**
   * Parses and checks the linking JSON configuration file and tries to build the linking components.
   * A {@link FamerConfigException} will be thrown for any inconsistency.
   *
   * @param configFilePath Path to the json configuration file for linking
   * @return The instance of {@link LinkingConfiguration} with the initialized components
   */
  @Override
  public LinkingConfiguration checkConfigAndBuildComponents(String configFilePath) {
    JSONObject linkingJsonConfig;
    try {
      String jsonString =
        new String(Files.readAllBytes(Paths.get(configFilePath)), StandardCharsets.UTF_8);
      linkingJsonConfig = new JSONObject(jsonString);
    } catch (Exception ex) {
      throw getLinkingConfigException("Could not parse JSON configuration file.", ex);
    }

    return checkConfigAndBuildComponents(linkingJsonConfig);
  }

  /**
   * Parses and checks the linking JSON configuration object and tries to build the linking components.
   * A {@link FamerConfigException} will be thrown for any inconsistency.
   *
   * @param configJSON The json configuration object for linking
   * @return The instance of {@link LinkingConfiguration} with the initialized components
   */
  @Override
  public LinkingConfiguration checkConfigAndBuildComponents(JSONObject configJSON) {
    boolean keepCurrentEdges = extractKeepCurrentEdges(configJSON);
    boolean recomputeSimilarityForCurrentEdges = extractRecomputeSimilarityForCurrentEdges(configJSON);
    boolean tagNeighborCluster = extractTagNeighborCluster(configJSON);
    boolean tagNewLink = extractTagNewLink(configJSON);
    // build blocking components
    blockingComponents =
      new BlockingConfiguration().checkConfigAndBuildComponents(configJSON).getBlockingComponents();
    // build similarity components
    similarityComponents =
      new SimilarityConfiguration().checkConfigAndBuildComponents(configJSON).getSimilarityComponents();
    // build selection component
    selectionComponent =
      new SelectionConfiguration().checkConfigAndBuildComponents(configJSON).getSelectionComponent();
    // build linker component
    linkerComponent =
      new LinkerComponent(blockingComponents, similarityComponents, selectionComponent, keepCurrentEdges,
        recomputeSimilarityForCurrentEdges, tagNeighborCluster, tagNewLink);
    // build linker, all other components are successfully initialized here
    linker = new Linker(linkerComponent);

    return this;
  }

  /**
   * Runs the full linking process with blocking, similarity computation, similarity value selection and
   * aggregation
   *
   * @param input The {@link LogicalGraph} to run the full linking process on
   * @return The linked {@link LogicalGraph}
   */
  public LogicalGraph runLinking(LogicalGraph input) {
    checkForInitializedLinker();
    return linker.executeLinking(input);
  }

  /**
   * Runs the full linking process with blocking, similarity computation, similarity value selection and
   * aggregation
   *
   * @param input The {@link GraphCollection} to run the full linking process on
   * @return The linked {@link LogicalGraph}
   */
  public LogicalGraph runLinking(GraphCollection input) {
    checkForInitializedLinker();
    return linker.executeLinking(input);
  }

  /**
   * Checks whether the {@link #linker} was initialized, throws an exception if not
   */
  private void checkForInitializedLinker() {
    if (linker == null) {
      throw getLinkingConfigException(
        "No linker has been created yet.\n" + "\nRun checkConfigAndBuildComponents() first.");
    }
  }

  /**
   * Retrieves the value for the extractKeepCurrentEdges parameter from the linking json configuration
   *
   * @param linkingJsonConfig The json configuration object for linking
   * @return The value for the extractKeepCurrentEdges parameter
   */
  private boolean extractKeepCurrentEdges(JSONObject linkingJsonConfig) {
    try {
      return linkingJsonConfig.getBoolean("keepCurrentEdges");
    } catch (Exception ex) {
      throw getLinkingConfigException(
        "Could not found or parse boolean value for keepCurrentEdges in linking configuration", ex);
    }
  }

  /**
   * Retrieves the value for the recomputeSimilarityForCurrentEdges parameter from the linking json
   * configuration
   *
   * @param linkingJsonConfig The json configuration object for linking
   * @return The value for the recomputeSimilarityForCurrentEdges parameter
   */
  private boolean extractRecomputeSimilarityForCurrentEdges(JSONObject linkingJsonConfig) {
    try {
      return linkingJsonConfig.getBoolean("recomputeSimilarityForCurrentEdges");
    } catch (Exception ex) {
      throw getLinkingConfigException("Could not found or parse boolean value for " +
        "recomputeSimilarityForCurrentEdges in linking configuration", ex);
    }
  }


  /**
   * Retrieves the value for the tagNeighborCluster parameter from the linking json configuration. Related
   * to famer incremental module.
   *
   * @param linkingJsonConfig The json configuration object for linking
   * @return The value for the extractTagNeighborCluster parameter
   */
  private boolean extractTagNeighborCluster(JSONObject linkingJsonConfig) {
    try {
      if (linkingJsonConfig.has("tagNeighborCluster")) {
        return linkingJsonConfig.getBoolean("tagNeighborCluster");
      }
    } catch (Exception ex) {
      throw getLinkingConfigException(
        "Could not found or parse boolean value for tagNeighborCluster in linking configuration", ex);
    }
    return false;
  }


  /**
   * Retrieves the value for the tagNewLink parameter from the linking json configuration. Related
   * to famer incremental module.
   *
   * @param linkingJsonConfig The json configuration object for linking
   * @return The value for the extractTagNewNewLink parameter
   */
  private boolean extractTagNewLink(JSONObject linkingJsonConfig) {
    try {
      if (linkingJsonConfig.has("tagNewLink")) {
        return linkingJsonConfig.getBoolean("tagNewLink");
      }
    } catch (Exception ex) {
      throw getLinkingConfigException(
        "Could not found or parse boolean value for tagNewLink in linking configuration", ex);
    }
    return false;
  }

  /**
   * Creates a {@link FamerConfigException} for linking configuration
   *
   * @param message the detail message
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getLinkingConfigException(String message) {
    message = "Linking configuration failed:\n" + message;
    return new FamerConfigException(message);
  }

  /**
   * Creates a {@link FamerConfigException} for linking configuration
   *
   * @param message the detail message
   * @param cause   the cause
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getLinkingConfigException(String message, Throwable cause) {
    message = "Linking configuration failed:\n" + message;
    return new FamerConfigException(message, cause);
  }
}
