/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.incremental;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.StandAloneConfiguration;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.famer.incremental.parallelIncremental.AbstractParallelIncremental;
import org.gradoop.famer.incremental.parallelIncremental.common.dataStructures.IncrementalRepairingMethod;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Class to parse the JSON configuration for incremental repairing and build all needed objects.
 */
public class IncrementalConfiguration implements StandAloneConfiguration {

  /**
   * Incremental repairing algorithm that will be used in run
   */
  private AbstractParallelIncremental incrementalRepairAlgorithm;

  public AbstractParallelIncremental getIncrementalRepairAlgorithm() {
    return incrementalRepairAlgorithm;
  }

  /**
   * Parses and checks the incremental repairing JSON configuration file and tries to build the incremental
   * repairing components. A {@link FamerConfigException} will be thrown for any inconsistency.
   *
   * @param configFilePath Path to the json configuration file for incremental repairing
   * @return The instance of {@link IncrementalConfiguration} with the initialized components
   */
  @Override
  public IncrementalConfiguration checkConfigAndBuildComponents(String configFilePath) {
    JSONObject incrementalJsonConfig;
    try {
      String jsonString =
        new String(Files.readAllBytes(Paths.get(configFilePath)), StandardCharsets.UTF_8);
      incrementalJsonConfig = new JSONObject(jsonString);
    } catch (Exception ex) {
      throw getIncrementalConfigException("Could not parse JSON configuration file.", ex);
    }

    return checkConfigAndBuildComponents(incrementalJsonConfig);
  }

  /**
   * Parses and checks the incremental repairing JSON configuration object and tries to build the repairing
   * components. A {@link FamerConfigException} will be thrown for any inconsistency.
   *
   * @param configJSON The json configuration object for incremental repairing
   * @return The instance of {@link IncrementalConfiguration} with the initialized components
   */
  @Override
  public IncrementalConfiguration checkConfigAndBuildComponents(JSONObject configJSON) {
    try {
      incrementalRepairAlgorithm = instantiateRepairingAlgorithm(configJSON);
    } catch (Exception ex) {
      throw getIncrementalConfigException("Could not parse incremental repair json object", ex);
    }
    return this;
  }

  /**
   * Runs the incremental repairing on the given graph. Assumes that
   *
   * @param graph The {@link LogicalGraph} to run the incremental repairing on
   * @return The fully clustered {@link LogicalGraph}
   * @link #repairAlgorithm} is already set,
   * throws an exception if not.
   */
  public LogicalGraph runIncrementalRepairing(LogicalGraph graph) {
    if (incrementalRepairAlgorithm == null) {
      throw getIncrementalConfigException("No incremental repairing algorithm has been created yet." +
        "\nRun checkConfigAndBuildComponents() first.");
    }
    graph = incrementalRepairAlgorithm.execute(graph);
    return graph;
  }


  /**
   * Creates a class instance for the incremental repairing algorithm. Uses the full class names for the
   * parallel incremental repairing algorithms defined in {@link IncrementalRepairingMethod} to get the class
   * instances via reflections.
   *
   * @param configJSON The incremental repairing json config object
   * @return An instance of {@link AbstractParallelIncremental}
   */
  AbstractParallelIncremental instantiateRepairingAlgorithm(JSONObject configJSON) {
    try {
      IncrementalRepairingMethod repairingMethod =
        IncrementalRepairingMethod.valueOf(configJSON.getString("repairingMethod"));

      Class<?> incrementalRepairingClass = Class.forName(repairingMethod.getFullClassName());

      return (AbstractParallelIncremental) incrementalRepairingClass.getDeclaredConstructor(JSONObject.class)
        .newInstance(configJSON);
    } catch (Exception ex) {
      throw getIncrementalConfigException("Could not instantiate incremental repairing algorithm.", ex);
    }
  }

  /**
   * Creates a {@link FamerConfigException} for incremental repairing configuration
   *
   * @param message the detail message
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getIncrementalConfigException(String message) {
    message = "Incremental configuration failed:\n" + message;
    return new FamerConfigException(message);
  }

  /**
   * Creates a {@link FamerConfigException} for incremental repairing configuration
   *
   * @param message the detail message
   * @param cause   the cause
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getIncrementalConfigException(String message, Throwable cause) {
    message = "Incremental configuration failed:\n" + message;
    return new FamerConfigException(message, cause);
  }
}
