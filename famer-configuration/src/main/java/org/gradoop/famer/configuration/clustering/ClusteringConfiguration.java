/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.clustering;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringMethod;
import org.gradoop.famer.clustering.postprocessing.AbstractClusterPostprocessing;
import org.gradoop.famer.clustering.postprocessing.dataStructures.PostprocessingMethod;
import org.gradoop.famer.configuration.StandAloneConfiguration;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Class to parse the JSON configuration for clustering and cluster post-processing and build all needed
 * objects.
 */
public class ClusteringConfiguration implements StandAloneConfiguration {

  /**
   * Clustering algorithm that will be used in run
   */
  private AbstractParallelClustering clusteringAlgorithm;

  /**
   * Postprocessing algorithm that will be run after clustering
   */
  private AbstractClusterPostprocessing postprocessingAlgorithm;

  public AbstractParallelClustering getClusteringAlgorithm() {
    return clusteringAlgorithm;
  }

  public AbstractClusterPostprocessing getPostprocessingAlgorithm() {
    return postprocessingAlgorithm;
  }

  /**
   * Parses and checks the clustering JSON configuration file and tries to build the clustering components.
   * A {@link FamerConfigException} will be thrown for any inconsistency.
   *
   * @param configFilePath Path to the json configuration file for clustering and cluster
   *                       post-processing
   * @return The instance of {@link ClusteringConfiguration} with the initialized components
   */
  @Override
  public ClusteringConfiguration checkConfigAndBuildComponents(String configFilePath) {
    JSONObject clusteringJsonConfig;
    try {
      String jsonString =
        new String(Files.readAllBytes(Paths.get(configFilePath)), StandardCharsets.UTF_8);
      clusteringJsonConfig = new JSONObject(jsonString);
    } catch (Exception ex) {
      throw getClusteringConfigException("Could not parse JSON configuration file.", ex);
    }

    return checkConfigAndBuildComponents(clusteringJsonConfig);
  }

  /**
   * Parses and checks the clustering JSON configuration object and tries to build the clustering
   * components.
   * A {@link FamerConfigException} will be thrown for any inconsistency.
   *
   * @param configJSON The json configuration object for clustering and cluster post-processing
   * @return The instance of {@link ClusteringConfiguration} with the initialized components
   */
  @Override
  public ClusteringConfiguration checkConfigAndBuildComponents(JSONObject configJSON) {
    if (configJSON.opt("clustering") != null) {
      try {
        JSONObject clusteringObject = configJSON.getJSONObject("clustering");
        clusteringAlgorithm = instantiateClusteringAlgorithm(clusteringObject);
      } catch (Exception ex) {
        throw getClusteringConfigException("Could not parse clustering json object", ex);
      }
    }

    if (configJSON.opt("postprocessing") != null) {
      try {
        JSONObject postprocessingObject = configJSON.getJSONObject("postprocessing");
        postprocessingAlgorithm = instantiatePostprocessingAlgorithm(postprocessingObject);
      } catch (Exception ex) {
        throw getClusteringConfigException("Could not parse postprocessing json object", ex);
      }
    }

    return this;
  }

  /**
   * Runs the clustering and the cluster postprocessing on the given graph. Assumes that
   * {@link #clusteringAlgorithm} and (optional) {@link #postprocessingAlgorithm} is already set,
   * throws an exception if not.
   *
   * @param graph The {@link LogicalGraph} to run the clustering on
   * @return The clustered {@link LogicalGraph}
   */
  public LogicalGraph runClustering(LogicalGraph graph) {
    if ((clusteringAlgorithm == null) && (postprocessingAlgorithm == null)) {
      throw getClusteringConfigException("No clustering or postprocessing algorithm has been created yet." +
        "\nRun checkConfigAndBuildComponents() first.");
    }
    if (clusteringAlgorithm != null) {
      graph = clusteringAlgorithm.execute(graph);
    }

    if (postprocessingAlgorithm != null) {
      graph = postprocessingAlgorithm.execute(graph);
    }

    return graph;
  }


  /**
   * Creates a class instance for the clustering algorithm. Uses the full class names for the parallel
   * clustering algorithms defined in {@link ClusteringMethod} to get the class instances via reflections.
   *
   * @param clusteringObject The clustering json config object
   * @return An instance of {@link AbstractParallelClustering}
   */
  AbstractParallelClustering instantiateClusteringAlgorithm(JSONObject clusteringObject) {
    try {
      ClusteringMethod clusteringMethod =
        ClusteringMethod.valueOf(clusteringObject.getString("clusteringMethod"));

      if (clusteringMethod.equals(ClusteringMethod.NONE)) {
        return null;
      } else {
        Class<?> clusteringClass = Class.forName(clusteringMethod.getFullClassName());

        return (AbstractParallelClustering) clusteringClass.getDeclaredConstructor(JSONObject.class)
          .newInstance(clusteringObject);
      }
    } catch (Exception ex) {
      throw getClusteringConfigException("Could not instantiate clustering algorithm.", ex);
    }
  }

  /**
   * Creates a class instance for the postprocessing algorithm. Uses the full class names for the
   * postprocessing algorithms defined in {@link PostprocessingMethod} to get the class instances via
   * reflections.
   *
   * @param postprocessingObject The postprocessing json config object
   * @return An instance of {@link AbstractClusterPostprocessing}
   */
  AbstractClusterPostprocessing instantiatePostprocessingAlgorithm(JSONObject postprocessingObject) {
    try {
      PostprocessingMethod postprocessingMethod =
        PostprocessingMethod.valueOf(postprocessingObject.getString("postprocessingMethod"));

      Class<?> postprocessingClass = Class.forName(postprocessingMethod.getFullClassName());

      return (AbstractClusterPostprocessing) postprocessingClass.getDeclaredConstructor(JSONObject.class)
        .newInstance(postprocessingObject);
    } catch (Exception ex) {
      throw getClusteringConfigException("Could not instantiate cluster postprocessing algorithm.", ex);
    }
  }

  /**
   * Creates a {@link FamerConfigException} for clustering configuration
   *
   * @param message the detail message
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getClusteringConfigException(String message) {
    message = "Clustering configuration failed:\n" + message;
    return new FamerConfigException(message);
  }

  /**
   * Creates a {@link FamerConfigException} for clustering configuration
   *
   * @param message the detail message
   * @param cause   the cause
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getClusteringConfigException(String message, Throwable cause) {
    message = "Clustering configuration failed:\n" + message;
    return new FamerConfigException(message, cause);
  }
}
