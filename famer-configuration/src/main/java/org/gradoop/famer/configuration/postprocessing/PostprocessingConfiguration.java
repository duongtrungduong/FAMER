/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.postprocessing;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.core.fs.Path;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.FamerConfiguration;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.famer.postprocessing.io.gradoop.GradoopGraphWriter;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * Class to parse the JSON configuration for post-processing like evaluation and result writing and build all
 * needed objects.
 */
public class PostprocessingConfiguration {

  /**
   * Available postprocessing tasks
   */
  private enum Task {
    /**
     * Write a Gradoop {@link LogicalGraph} or {@link GraphCollection} as CSV to disk
     */
    WRITE_GRAPH,
    /**
     * Write a Flink {@link DataSet} as CSV to disk
     */
    WRITE_DS,
    /**
     * Compute and write quality measures for a similarity graph or a clustering
     */
    QUALITY,
    /**
     * Compute and write graph statistics
     */
    GRAPH_STATISTICS
  }

  /**
   * Parses and checks the postprocessing JSON configuration object. A {@link FamerConfigException} will be
   * thrown for any inconsistency. The {@link FamerConfiguration.DataType} to run the postprocessing on
   * is set with the default value {@code DataType.LOGICAL_GRAPH}.
   *
   * @param postprocessingJsonConfig The json configuration object for linking
   */
  public void checkPostprocessingConfig(JSONArray postprocessingJsonConfig) throws JSONException {
    checkPostprocessingConfig(postprocessingJsonConfig, FamerConfiguration.DataType.LOGICAL_GRAPH);
  }

  /**
   * Parses and checks the postprocessing JSON configuration object. A {@link FamerConfigException} will be
   * thrown for any inconsistency.
   *
   * @param postprocessingTasks The json configuration object for postprocessing
   * @param dataType            {@link FamerConfiguration.DataType} to run the postprocessing on
   */
  public void checkPostprocessingConfig(JSONArray postprocessingTasks,
    FamerConfiguration.DataType dataType) throws JSONException {
    // check array content
    if ((postprocessingTasks == null) || (postprocessingTasks.length() == 0)) {
      throw getPostprocessingConfigException("Could not find any postprocessing tasks");
    }
    // validate single tasks
    for (int i = 0; i < postprocessingTasks.length(); i++) {
      JSONObject taskObject = postprocessingTasks.getJSONObject(i);
      Task task;
      try {
        task = Task.valueOf(taskObject.getString("task"));
      } catch (Exception ex) {
        throw getPostprocessingConfigException("Unknown or missing postprocessing task name", ex);
      }
      switch (task) {
      case WRITE_GRAPH:
        if (!dataType.equals(FamerConfiguration.DataType.LOGICAL_GRAPH) &&
          !dataType.equals(FamerConfiguration.DataType.GRAPH_COLLECTION)) {
          throw getPostprocessingConfigException(
            "Incompatible input data type for WRITE_GRAPH: " + "must be LOGICAL_GRAPH or GRAPH_COLLECTION");
        }
        validateDataPath(taskObject.getString("path"));
        if (!taskObject.optString("overwrite").equals("")) {
          try {
            taskObject.getBoolean("overwrite");
          } catch (Exception ex) {
            throw getPostprocessingConfigException("Could not parse overwrite value for WRITE_GRAPH", ex);
          }
        }
        break;
      case WRITE_DS:
        // TODO: implement
        break;
      case QUALITY:
        // TODO: implement
        break;
      case GRAPH_STATISTICS:
        // TODO: implement
        break;
      default:
        break;
      }
    }
  }

  /**
   * Runs the post-processing.
   *
   * @param postprocessingTasks The json configuration object for postprocessing
   * @param inputData           The data to run the postprocessing on
   */
  public void runPostprocessing(JSONArray postprocessingTasks, Object inputData) throws JSONException {
    for (int i = 0; i < postprocessingTasks.length(); i++) {
      JSONObject taskObject = postprocessingTasks.getJSONObject(i);
      Task task = Task.valueOf(taskObject.getString("task"));
      switch (task) {
      case WRITE_GRAPH:
        boolean overwrite = true;
        if (taskObject.opt("overwrite") != null) {
          overwrite = taskObject.getBoolean("overwrite");
        }
        String path = taskObject.getString("path");
        try {
          if (inputData instanceof LogicalGraph) {
            new GradoopGraphWriter().writeLogicalGraphAsCSV((LogicalGraph) inputData, path, overwrite);
          } else if (inputData instanceof GraphCollection) {
            new GradoopGraphWriter().writeGraphCollectionAsCSV((GraphCollection) inputData, path, overwrite);
          }
        } catch (Exception ex) {
          throw getPostprocessingConfigException("There was an error while writing the graph data", ex);
        }
        break;
      case WRITE_DS:
        // TODO: implement
        break;
      case QUALITY:
        // TODO: implement
        break;
      case GRAPH_STATISTICS:
        // TODO: implement
        break;
      default:
        break;
      }
    }
  }

  /**
   * Checks if the given path value could be parsed and if the path exist
   *
   * @param path The path value
   */
  private void validateDataPath(String path) {
    try {
      Path fsPath = new Path(path);
      FileSystem fileSystem = fsPath.getFileSystem();
      if (!fileSystem.exists(fsPath)) {
        throw getPostprocessingConfigException("The given path or directory does not exist");
      }
    } catch (Exception ex) {
      throw getPostprocessingConfigException(
        "The path value could not be found or parsed or the path does " + "not exist", ex);
    }
  }

  /**
   * Creates a {@link FamerConfigException} for postprocessing configuration
   *
   * @param message the detail message
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getPostprocessingConfigException(String message) {
    message = "Postprocessing configuration failed:\n" + message;
    return new FamerConfigException(message);
  }

  /**
   * Creates a {@link FamerConfigException} for postprocessing configuration
   *
   * @param message the detail message
   * @param cause   the cause
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getPostprocessingConfigException(String message, Throwable cause) {
    message = "Postprocessing configuration failed:\n" + message;
    return new FamerConfigException(message, cause);
  }
}
