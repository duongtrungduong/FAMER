/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration;

import org.codehaus.jettison.json.JSONObject;

/**
 * Interface for all configuration classes which are callable as single tasks and provide an instance
 * with the corresponding JSON-based config data checked and all needed components initialized.
 */
public interface StandAloneConfiguration {

  /**
   * Parses and checks the config from the given file path and builds the configuration task components.
   *
   * @param configFilePath The path to the JSON config file
   * @return The instance of the configuration class with its initialized components
   */
  StandAloneConfiguration checkConfigAndBuildComponents(String configFilePath);

  /**
   * Parses and checks the config from the given JSON object and builds the configuration task components.
   *
   * @param configJSON The JSON config object
   * @return The instance of the configuration class with its initialized components
   */
  StandAloneConfiguration checkConfigAndBuildComponents(JSONObject configJSON);
}
