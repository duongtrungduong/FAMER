/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.linking.blocking;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.StandAloneConfiguration;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.famer.linking.blocking.keyGeneration.BlockingKeyGenerator;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.KeyGeneratorComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponentBaseConfig;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class to parse the JSON configuration for blocking and build all needed objects.
 */
public class BlockingConfiguration implements StandAloneConfiguration {

  /**
   * The list of {@link BlockingComponent}s needed for linking
   */
  private List<BlockingComponent> blockingComponents;

  public List<BlockingComponent> getBlockingComponents() {
    return blockingComponents;
  }

  /**
   * Parses and checks the linking JSON configuration file and tries to build the blocking components.
   * A {@link FamerConfigException} will be thrown for any inconsistency.
   *
   * @param configFilePath Path to the json configuration file for linking
   * @return The instance of {@link BlockingConfiguration} with the initialized components
   */
  @Override
  public BlockingConfiguration checkConfigAndBuildComponents(String configFilePath) {
    JSONObject linkingJsonConfig;
    try {
      String jsonString =
        new String(Files.readAllBytes(Paths.get(configFilePath)), StandardCharsets.UTF_8);
      linkingJsonConfig = new JSONObject(jsonString);
    } catch (Exception ex) {
      throw getBlockingConfigException("Could not parse JSON configuration file.", ex);
    }

    return checkConfigAndBuildComponents(linkingJsonConfig);
  }

  /**
   * Parses and checks the linking JSON configuration object and tries to build the blocking components.
   * A {@link FamerConfigException} will be thrown for any inconsistency.
   *
   * @param configJSON The json configuration object for linking
   * @return The instance of {@link BlockingConfiguration} with the initialized components
   */
  @Override
  public BlockingConfiguration checkConfigAndBuildComponents(JSONObject configJSON) {
    if (configJSON.optJSONArray("blockingComponents") == null) {
      throw getBlockingConfigException("No blockingComponents found in linking configuration");
    }
    if (configJSON.optJSONArray("blockingComponents").length() == 0) {
      throw getBlockingConfigException("Array of blockingComponents is empty");
    }

    List<BlockingComponent> tmpBlockingComponents = new ArrayList<>();

    JSONArray blockingJson = configJSON.optJSONArray("blockingComponents");

    Map<String, Set<String>> graphPairs = buildPairs(configJSON, "sourceGraph", "targetGraph");
    Map<String, Set<String>> categoryPairs = buildPairs(configJSON, "sourceLabel", "targetLabel");

    for (int i = 0; i < blockingJson.length(); i++) {
      JSONObject blockingJsonObject = blockingJson.optJSONObject(i);

      if (blockingJsonObject.optJSONObject("keyGenerationComponent") == null) {
        throw getBlockingConfigException("No keyGenerationComponent found in blockingComponent");
      }
      KeyGeneratorComponent keyGeneratorComponent =
        buildKeyGenerationComponent(blockingJsonObject.optJSONObject("keyGenerationComponent"));

      BlockingKeyGenerator blockingKeyGenerator = new BlockingKeyGenerator(keyGeneratorComponent);

      BlockingComponentBaseConfig blockingComponentBaseConfig =
        new BlockingComponentBaseConfig(blockingKeyGenerator, graphPairs, categoryPairs);

      try {
        String blockingMethod = blockingJsonObject.getString("blockingMethod");

        Class<?> blockingComponentClass =
          Class.forName("org.gradoop.famer.linking.blocking.methods.dataStructures." + blockingMethod);
        Class<?>[] classArgs = {BlockingComponentBaseConfig.class, JSONObject.class};

        BlockingComponent component =
          (BlockingComponent) blockingComponentClass.getDeclaredConstructor(classArgs)
            .newInstance(blockingComponentBaseConfig, blockingJsonObject);

        tmpBlockingComponents.add(component);
      } catch (Exception ex) {
        throw getBlockingConfigException("Could not instantiate blocking component.", ex);
      }
    }

    blockingComponents = tmpBlockingComponents;

    return this;
  }

  /**
   * Creates a mapping of the allowed pairs specified by 'similarityComponents' object in configuration
   * file.
   * Take the value of the given source element and maps it to a set of associated values of given target
   * elements.
   *
   * @param linkingJsonConfig The json configuration object for linking
   * @param sourceElement     The key for the source element of the mapping.
   * @param targetElement     The key for the target value the source value is associated.
   * @return The mapping of all source values and there associated target set.
   */
  Map<String, Set<String>> buildPairs(JSONObject linkingJsonConfig, String sourceElement,
    String targetElement) {
    if (linkingJsonConfig.optJSONArray("similarityComponents") == null) {
      throw getBlockingConfigException("No similarityComponents found in linking configuration");
    }
    if (linkingJsonConfig.optJSONArray("similarityComponents").length() == 0) {
      throw getBlockingConfigException("Array of similarityComponents is empty");
    }

    Map<String, Set<String>> pairs = new HashMap<>();
    JSONArray similarityComponents = linkingJsonConfig.optJSONArray("similarityComponents");

    for (int i = 0; i < similarityComponents.length(); i++) {
      JSONObject component = similarityComponents.optJSONObject(i);
      String sourceJson = component.optString(sourceElement);
      String targetJson = component.optString(targetElement);

      if (sourceJson.equals("")) {
        throw getBlockingConfigException("No " + sourceElement + " found in similarityComponent.");
      }
      if (targetJson.equals("")) {
        throw getBlockingConfigException("No " + sourceElement + " found in similarityComponent.");
      }
      String source = sourceJson;
      String target = targetJson;

      Set<String> targetSet = pairs.get(source);
      if (targetSet != null) {
        targetSet.add(target);
      } else {
        targetSet = new HashSet<>();
        targetSet.add(target);
      }
      pairs.put(source, targetSet);
    }

    return pairs;
  }

  /**
   * Creates a {@link KeyGeneratorComponent} for the associated key generation method specified in the given
   * json object.
   *
   * @param keyGenConfig The json object that specify the key generation method.
   * @return A {@link KeyGeneratorComponent} for the given method.
   */
  KeyGeneratorComponent buildKeyGenerationComponent(JSONObject keyGenConfig) {
    try {
      String keyGenMethod = keyGenConfig.getString("keyGenerationMethod");

      Class<?> keyGenComponentClass =
        Class.forName("org.gradoop.famer.linking.blocking.keyGeneration.dataStructures." + keyGenMethod);

      return (KeyGeneratorComponent) keyGenComponentClass.getDeclaredConstructor(JSONObject.class)
        .newInstance(keyGenConfig);
    } catch (Exception ex) {
      throw getBlockingConfigException("Could not instantiate key generation component.", ex);
    }
  }

  /**
   * Creates a {@link FamerConfigException} for blocking configuration
   *
   * @param message the detail message
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getBlockingConfigException(String message) {
    message = "Blocking configuration failed:\n" + message;
    return new FamerConfigException(message);
  }

  /**
   * Creates a {@link FamerConfigException} for blocking configuration
   *
   * @param message the detail message
   * @param cause   the cause
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getBlockingConfigException(String message, Throwable cause) {
    message = "Blocking configuration failed:\n" + message;
    return new FamerConfigException(message, cause);
  }
}
