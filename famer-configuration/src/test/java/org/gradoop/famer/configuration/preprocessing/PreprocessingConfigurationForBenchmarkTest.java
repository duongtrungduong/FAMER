/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.preprocessing;

import org.apache.flink.api.java.DataSet;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * JUnit test for {@link PreprocessingConfiguration} - Tests for Benchmark data
 */
public class PreprocessingConfigurationForBenchmarkTest extends GradoopFlinkTestBase {

  @Test
  public void testRunPreprocessingForBenchmarkAbtBuyReturnGraphCollection() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/abtBuy").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"GRAPH_COLLECTION\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"ABT_BUY\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof GraphCollection);
  }

  @Test
  public void testRunPreprocessingForBenchmarkAbtBuyReturnPerfectMapping() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/abtBuy").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"PERFECT_MAPPING\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"ABT_BUY\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof DataSet);
  }

  @Test
  public void testRunPreprocessingForBenchmarkAmazonGoogleReturnGraphCollection() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/amazonGoogle").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"GRAPH_COLLECTION\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"AMAZON_GOOGLE\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof GraphCollection);
  }

  @Test
  public void testRunPreprocessingForBenchmarkAmazonGoogleReturnPerfectMapping() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/amazonGoogle").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"PERFECT_MAPPING\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"AMAZON_GOOGLE\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof DataSet);
  }

  @Test
  public void testRunPreprocessingForBenchmarkDblpAcmReturnGraphCollection() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/dblpAcm").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"GRAPH_COLLECTION\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"DBLP_ACM\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof GraphCollection);
  }

  @Test
  public void testRunPreprocessingForBenchmarkDblpAcmReturnPerfectMapping() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/dblpAcm").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"PERFECT_MAPPING\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"DBLP_ACM\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof DataSet);
  }

  @Test
  public void testRunPreprocessingForBenchmarkDblpScholarReturnGraphCollection() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/dblpScholar").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"GRAPH_COLLECTION\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"DBLP_SCHOLAR\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof GraphCollection);
  }

  @Test
  public void testRunPreprocessingForBenchmarkDblpScholarReturnPerfectMapping() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/dblpScholar").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"PERFECT_MAPPING\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"DBLP_SCHOLAR\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof DataSet);
  }

  @Test
  public void testRunPreprocessingForBenchmarkAffiliationsReturnGraphCollection() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/affiliations").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"GRAPH_COLLECTION\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"AFFILIATIONS\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof GraphCollection);
  }

  @Test
  public void testRunPreprocessingForBenchmarkAffiliationsReturnPerfectMapping() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/affiliations").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"PERFECT_MAPPING\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"AFFILIATIONS\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof DataSet);
  }

  @Test
  public void testRunPreprocessingForBenchmarkGeographicReturnGraphCollection() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/geographicSettlements").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"GRAPH_COLLECTION\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"GEOGRAPHIC\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof GraphCollection);
  }

  @Test
  public void testRunPreprocessingForBenchmarkGeographicReturnPerfectMapping() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/geographicSettlements").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"PERFECT_MAPPING\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"GEOGRAPHIC\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof DataSet);
  }

  @Test
  public void testRunPreprocessingForBenchmarkGeographicReturnClusteringMapping() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/geographicSettlements").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"PERFECT_CLUSTERING\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"GEOGRAPHIC\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof DataSet);
  }

  @Test
  public void testRunPreprocessingForBenchmarkNcVoters5PartyReturnGraphCollection() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/northCarolinaVoters/5Party").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"GRAPH_COLLECTION\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"NC_VOTERS\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof GraphCollection);
  }

  @Test
  public void testRunPreprocessingForBenchmarkNcVoters5PartyReturnPerfectMapping() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/northCarolinaVoters/5Party").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"PERFECT_MAPPING\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"NC_VOTERS\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof DataSet);
  }

  @Test
  public void testRunPreprocessingForBenchmarkMusicbrainzReturnGraphCollection() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/musicbrainz").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"GRAPH_COLLECTION\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"MUSICBRAINZ\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof GraphCollection);
  }

  @Test
  public void testRunPreprocessingForBenchmarkMusicbrainzReturnPerfectMapping() throws Exception {
    String path = getClass().getClassLoader().getResource("benchmarks/musicbrainz").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"PERFECT_MAPPING\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"MUSICBRAINZ\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof DataSet);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPreprocessingConfigurationForBenchmarkWithUnknownName() throws JSONException {
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"GRAPH_COLLECTION\",\n" +
      "\"name\":\"A\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    new PreprocessingConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPreprocessingConfigurationForBenchmarkWithWrongReturnType() throws JSONException {
    String path = getClass().getClassLoader().getResource("benchmarks/abtBuy").getPath();
    String preConfig = "{\n" +
      "\"task\":\"BENCHMARK\",\n" +
      "\"return\":\"PERFECT_CLUSTERING\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"name\":\"ABT_BUY\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    new PreprocessingConfiguration().checkConfigAndBuildComponents(configJson);
  }
}
