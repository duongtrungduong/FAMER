/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.incremental;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.ConnectedComponents;
import org.gradoop.famer.incremental.parallelIncremental.AbstractParallelIncremental;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.EarlyMaxBoth;
import org.gradoop.famer.incremental.parallelIncremental.ndr.NDR;
import org.gradoop.famer.incremental.parallelIncremental.ndr.OneDR;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * JUnit tests for incremental repairing algorithm instantiation in {@link IncrementalConfiguration}.
 */
public class IncrementalAlgorithmInstantiationTest extends GradoopFlinkTestBase {

  private final IncrementalConfiguration incrementalConfiguration = new IncrementalConfiguration();

  private JSONObject clusteringConfig;

  @Before
  public void setUp() throws JSONException {
    String clusteringJson = "{ " +
      " \"clusteringMethod\":\"CONNECTED_COMPONENTS\"," +
      " \"clusteringOutputType\":\"GRAPH\"," +
      " \"maxIteration\":\"MAX_VALUE\"" +
      "}";

    clusteringConfig = new JSONObject(clusteringJson);
  }

  @Test
  public void testBuildRepairingAlgorithmForMBM() throws JSONException {
    String json = "{\n" +
      " \"repairingMethod\":\"MBM\",\n" +
      " \"delta\":0.0,\n" +
      " \"clusterIdPrefix\":\"mbm\",\n" +
      " \"isSCRemoving\":\"false\",\n" +
      " \"clustering\":" + clusteringConfig +
      "}";
    JSONObject incrementalConfigJson = new JSONObject(json);

    AbstractParallelIncremental repairingAlgorithm =
      incrementalConfiguration.instantiateRepairingAlgorithm(incrementalConfigJson);

    assertNotNull(repairingAlgorithm);
    assertTrue(repairingAlgorithm instanceof EarlyMaxBoth);
    assertEquals(0.0, ((EarlyMaxBoth) repairingAlgorithm).getDelta(), 0.0);
    assertFalse(((EarlyMaxBoth) repairingAlgorithm).isSCRemoving());
    assertEquals("mbm", ((EarlyMaxBoth) repairingAlgorithm).getPreClusteringPrefix());
    assertTrue(((EarlyMaxBoth) repairingAlgorithm).getClusteringAlgorithm() instanceof ConnectedComponents);
  }

  @Test
  public void testBuildRepairingAlgorithmForNDR() throws JSONException {
    String json = "{\n" +
      " \"repairingMethod\":\"NDR\",\n" +
      " \"clusterIdPrefix\":\"ndr\",\n" +
      " \"depth\": 2,\n" +
      " \"clustering\":" + clusteringConfig +
      "}";
    JSONObject incrementalConfigJson = new JSONObject(json);

    AbstractParallelIncremental repairingAlgorithm =
      incrementalConfiguration.instantiateRepairingAlgorithm(incrementalConfigJson);

    assertNotNull(repairingAlgorithm);
    assertTrue(repairingAlgorithm instanceof NDR);
    assertEquals(2, ((NDR) repairingAlgorithm).getDepth());
    assertEquals("ndr", ((NDR) repairingAlgorithm).getClusterIdPrefix());
    assertTrue(((NDR) repairingAlgorithm).getClusteringAlgorithm() instanceof ConnectedComponents);
  }

  @Test
  public void testBuildRepairingAlgorithmForOneDR() throws JSONException {
    String json = "{\n" +
      " \"repairingMethod\":\"ONE_DR\",\n" +
      " \"clusterIdPrefix\":\"1dr\",\n" +
      " \"clustering\":" + clusteringConfig +
      "}";
    JSONObject incrementalConfigJson = new JSONObject(json);

    AbstractParallelIncremental repairingAlgorithm =
      incrementalConfiguration.instantiateRepairingAlgorithm(incrementalConfigJson);

    assertNotNull(repairingAlgorithm);
    assertTrue(repairingAlgorithm instanceof OneDR);
    assertEquals("1dr", ((OneDR) repairingAlgorithm).getClusterIdPrefix());
    assertTrue(((OneDR) repairingAlgorithm).getClusteringAlgorithm() instanceof ConnectedComponents);
  }


}
