/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.clustering;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import static org.gradoop.famer.clustering.common.PropertyNames.SIM_VALUE;
import static org.junit.Assert.assertNotNull;

/**
 * JUnit tests for {@link ClusteringConfiguration}.
 */
public class ClusteringConfigurationTest extends GradoopFlinkTestBase {

  private static final String CLUSTERING_CONFIG = "{\n" +
    "   \"clustering\":{" +
    "     \"clusteringMethod\":\"CENTER\",\n" +
    "     \"prioritySelection\":\"MIN\",\n" +
    "     \"isEdgesBiDirected\":false,\n" +
    "     \"clusteringOutputType\":\"GRAPH\",\n" +
    "     \"maxIteration\":\"MAX_VALUE\"\n" +
    "   },\n" +
    "   \"postprocessing\":{" +
    "     \"postprocessingMethod\":\"OVERLAP_RESOLVE_NO_MERGE\",\n" +
    "     \"delta\":\"0.5\",\n" +
    "     \"runPhase2\":false\n" +
    "   }\n" +
    "}";

  private LogicalGraph graph;

  @Before
  public void setUpGraph() throws Exception {
    String graphString = "g[" +
      "(v0 {id:0})" +
      "(v1 {id:1})" +
      "(v0)-[e0 {" + SIM_VALUE + ":0.9}]->(v1)" +
      "]";
    graph = getLoaderFromString(graphString).getLogicalGraphByVariable("g");
  }

  @Test
  public void testCheckConfigAndBuildComponentsForJsonConfig() throws Exception {
    JSONObject configJson = new JSONObject(CLUSTERING_CONFIG);


    ClusteringConfiguration clusteringConfiguration =
      new ClusteringConfiguration().checkConfigAndBuildComponents(configJson);

    assertNotNull(clusteringConfiguration.getClusteringAlgorithm());
    assertNotNull(clusteringConfiguration.getPostprocessingAlgorithm());
  }

  @Test
  public void testCheckConfigAndBuildComponentsForConfigFile() throws Exception {
    File tempFile = File.createTempFile("clusteringConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(CLUSTERING_CONFIG);
    bw.close();

    ClusteringConfiguration clusteringConfiguration =
      new ClusteringConfiguration().checkConfigAndBuildComponents(tempFile.getPath());

    assertNotNull(clusteringConfiguration.getClusteringAlgorithm());
    assertNotNull(clusteringConfiguration.getPostprocessingAlgorithm());
  }

  @Test
  public void testRunClusteringAndPostprocessing() throws JSONException {
    JSONObject configJson = new JSONObject(CLUSTERING_CONFIG);

    ClusteringConfiguration clusteringConfigSpy = Mockito.spy(new ClusteringConfiguration());

    clusteringConfigSpy.checkConfigAndBuildComponents(configJson).runClustering(graph);

    // verify method calls
    Mockito.verify(clusteringConfigSpy, Mockito.times(1))
      .checkConfigAndBuildComponents(Mockito.any(JSONObject.class));
    Mockito.verify(clusteringConfigSpy, Mockito.times(1)).runClustering(Mockito.any(LogicalGraph.class));
    Mockito.verify(clusteringConfigSpy, Mockito.times(1))
      .instantiateClusteringAlgorithm(Mockito.any(JSONObject.class));
    Mockito.verify(clusteringConfigSpy, Mockito.times(1))
      .instantiatePostprocessingAlgorithm(Mockito.any(JSONObject.class));
    Mockito.verifyNoMoreInteractions(clusteringConfigSpy);
  }

  @Test
  public void testRunClustering() throws JSONException {
    String json = "{\n" +
      "   \"clustering\":{" +
      "     \"clusteringMethod\":\"CENTER\",\n" +
      "     \"prioritySelection\":\"MIN\",\n" +
      "     \"isEdgesBiDirected\":false,\n" +
      "     \"clusteringOutputType\":\"GRAPH\",\n" +
      "     \"maxIteration\":\"MAX_VALUE\"\n" +
      "   }\n" +
      "}";
    JSONObject configJson = new JSONObject(json);
    ClusteringConfiguration clusteringConfigSpy = Mockito.spy(new ClusteringConfiguration());

    clusteringConfigSpy.checkConfigAndBuildComponents(configJson).runClustering(graph);

    // verify method calls
    Mockito.verify(clusteringConfigSpy, Mockito.times(1))
      .checkConfigAndBuildComponents(Mockito.any(JSONObject.class));
    Mockito.verify(clusteringConfigSpy, Mockito.times(1)).runClustering(Mockito.any(LogicalGraph.class));
    Mockito.verify(clusteringConfigSpy, Mockito.times(1))
      .instantiateClusteringAlgorithm(Mockito.any(JSONObject.class));
    Mockito.verifyNoMoreInteractions(clusteringConfigSpy);
  }

  @Test
  public void testRunPostprocessing() throws JSONException {
    String json = "{\n" +
      "   \"postprocessing\":{" +
      "     \"postprocessingMethod\":\"OVERLAP_RESOLVE_NO_MERGE\",\n" +
      "     \"delta\":\"0.5\",\n" +
      "     \"runPhase2\":false\n" +
      "   }\n" +
      "}";
    JSONObject configJson = new JSONObject(json);
    ClusteringConfiguration clusteringConfigSpy = Mockito.spy(new ClusteringConfiguration());

    clusteringConfigSpy.checkConfigAndBuildComponents(configJson).runClustering(graph);

    // verify method calls
    Mockito.verify(clusteringConfigSpy, Mockito.times(1))
      .checkConfigAndBuildComponents(Mockito.any(JSONObject.class));
    Mockito.verify(clusteringConfigSpy, Mockito.times(1)).runClustering(Mockito.any(LogicalGraph.class));
    Mockito.verify(clusteringConfigSpy, Mockito.times(1))
      .instantiatePostprocessingAlgorithm(Mockito.any(JSONObject.class));
    Mockito.verifyNoMoreInteractions(clusteringConfigSpy);
  }

  @Test(expected = FamerConfigException.class)
  public void testRunClusteringWithoutCheckAndBuild() throws Exception {
    ClusteringConfiguration clusteringConfiguration = new ClusteringConfiguration();
    clusteringConfiguration.runClustering(graph);
  }
}
