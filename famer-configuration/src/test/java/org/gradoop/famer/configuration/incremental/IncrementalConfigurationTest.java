/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.incremental;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.EarlyMaxBoth;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import static org.gradoop.famer.clustering.common.PropertyNames.GRAPH_LABEL;
import static org.gradoop.famer.clustering.common.PropertyNames.SIM_VALUE;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * JUnit tests for {@link IncrementalConfiguration}.
 */
public class IncrementalConfigurationTest extends GradoopFlinkTestBase {

  private LogicalGraph graph;

  private static final String INCREMENTAL_CONFIG = "{\n" +
    " \"repairingMethod\":\"MBM\",\n" +
    " \"delta\":0.0,\n" +
    " \"clusterIdPrefix\":\"mbm\",\n" +
    " \"isSCRemoving\":false,\n" +
    " \"clustering\":{\n" +
    "   \"clusteringMethod\":\"CONNECTED_COMPONENTS\",\n" +
    "   \"clusteringOutputType\":\"GRAPH\",\n" +
    "   \"maxIteration\":\"MAX_VALUE\"\n" +
    " }\n" +
    "}";

  @Before
  public void setUpGraph() throws Exception {
    String graphString = "g[" +
      "(v0 {id:0, " + GRAPH_LABEL + ":\"src0\"})" +
      "(v1 {id:1, " + GRAPH_LABEL + ":\"src1\"})" +
      "(v0)-[e0 {" + SIM_VALUE + ":0.9}]->(v1)" +
      "]";
    graph = getLoaderFromString(graphString).getLogicalGraphByVariable("g");
  }

  @Test
  public void testCheckConfigAndBuildComponentsForJsonConfig() throws Exception {
    JSONObject configJson = new JSONObject(INCREMENTAL_CONFIG);

    IncrementalConfiguration incrementalConfiguration =
      new IncrementalConfiguration().checkConfigAndBuildComponents(configJson);

    assertNotNull(incrementalConfiguration.getIncrementalRepairAlgorithm());
    assertTrue(incrementalConfiguration.getIncrementalRepairAlgorithm() instanceof EarlyMaxBoth);
  }

  @Test
  public void testCheckConfigAndBuildComponentsForConfigFile() throws Exception {
    File tempFile = File.createTempFile("incrementalConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(INCREMENTAL_CONFIG);
    bw.close();

    IncrementalConfiguration incrementalConfiguration =
      new IncrementalConfiguration().checkConfigAndBuildComponents(tempFile.getPath());

    assertNotNull(incrementalConfiguration.getIncrementalRepairAlgorithm());
    assertTrue(incrementalConfiguration.getIncrementalRepairAlgorithm() instanceof EarlyMaxBoth);
  }

  @Test
  public void testRunIncrementalRepair() throws Exception {
    JSONObject configJson = new JSONObject(INCREMENTAL_CONFIG);

    IncrementalConfiguration incrementalConfigSpy = Mockito.spy(new IncrementalConfiguration());

    incrementalConfigSpy.checkConfigAndBuildComponents(configJson).runIncrementalRepairing(graph);

    // verify method calls
    Mockito.verify(incrementalConfigSpy, Mockito.times(1))
      .checkConfigAndBuildComponents(Mockito.any(JSONObject.class));
    Mockito.verify(incrementalConfigSpy, Mockito.times(1))
      .instantiateRepairingAlgorithm(Mockito.any(JSONObject.class));
    Mockito.verify(incrementalConfigSpy, Mockito.times(1))
      .runIncrementalRepairing(Mockito.any(LogicalGraph.class));
    Mockito.verifyNoMoreInteractions(incrementalConfigSpy);
  }

  @Test(expected = FamerConfigException.class)
  public void testRunIncrementalRepairWithoutCheckAndBuild() throws Exception {
    IncrementalConfiguration incrementalConfiguration = new IncrementalConfiguration();
    incrementalConfiguration.runIncrementalRepairing(graph);
  }
}
