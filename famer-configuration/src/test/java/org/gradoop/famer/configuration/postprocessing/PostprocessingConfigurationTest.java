/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.postprocessing;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.gradoop.famer.configuration.FamerConfiguration;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;

import static org.junit.Assert.assertTrue;

/**
 * JUnit tests for {@link PostprocessingConfiguration}
 */
public class PostprocessingConfigurationTest extends GradoopFlinkTestBase {

  @Test(expected = FamerConfigException.class)
  public void testCheckPostprocessingConfigurationWithEmptyTasks() throws JSONException {
    String postConfig = "[]";
    JSONArray configJson = new JSONArray(postConfig);

    new PostprocessingConfiguration().checkPostprocessingConfig(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPostprocessingConfigurationForUnknownTask() throws JSONException {
    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"A\"\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    new PostprocessingConfiguration().checkPostprocessingConfig(configJson);
  }

  @Test
  public void testRunPostprocessingForWriteGraph() throws IOException, JSONException {
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\"})" +
      "(bob:Person {id:2, name:\"Bob\"})" +
      "(alice)-[]->(bob)" +
      "]";
    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    String tempOutDir = Files.createTempDirectory("postprocessingOutput").toString();

    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"WRITE_GRAPH\",\n" +
      "    \"path\":\"" + tempOutDir.replace("\\", "\\\\") + "\"\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    new PostprocessingConfiguration().runPostprocessing(configJson, inputGraph);

    File dir = new File(tempOutDir);
    assertTrue(dir.exists());
    assertTrue(Objects.requireNonNull(dir.list()).length > 0);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPostprocessingConfigurationForWriteGraphWithIncompatibleType() throws IOException, JSONException {
    String tempOutDir = Files.createTempDirectory("postprocessingOutput").toString();

    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"WRITE_GRAPH\",\n" +
      "    \"path\":\"" + tempOutDir + "\"\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    new PostprocessingConfiguration().checkPostprocessingConfig(configJson, FamerConfiguration.DataType.PERFECT_MAPPING);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPostprocessingConfigurationForWriteGraphWithInvalidPath() throws IOException, JSONException {
    String tempOutDir = Files.createTempDirectory("postprocessingOutput").toString() + "/unknown";

    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"WRITE_GRAPH\",\n" +
      "    \"path\":\"" + tempOutDir + "\"\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    new PostprocessingConfiguration().checkPostprocessingConfig(configJson);
  }
}
