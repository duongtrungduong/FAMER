/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration;

import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.util.Objects;

import static org.junit.Assert.assertTrue;

/**
 * JUnit test for {@link FamerConfiguration}
 */
public class FamerConfigurationTest extends GradoopFlinkTestBase {

  @Test
  public void testRunFamerConfigurationWithGraphCollection() throws Exception {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_graph_collection_2").getPath();
    String tempOutDir = Files.createTempDirectory("famerOutput").toString();

    String config = "[\n" +
      "  [\n" +
      "    {\n" +
      "      \"task\":\"PREPROCESSING\",\n" +
      "      \"config\":{\n" +
      "        \"task\":\"READ\",\n" +
      "        \"return\":\"GRAPH_COLLECTION\",\n" +
      "        \"path\":\"" + path + "\"\n" +
      "      }\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"LINKING\",\n" +
      "      \"config\":{\n" +
      "        \"keepCurrentEdges\":\"true\",\n" +
      "        \"recomputeSimilarityForCurrentEdges\":\"false\",\n" +
      "        \"blockingComponents\":[\n" +
      "          {\n" +
      "            \"blockingMethod\":\"CartesianProductComponent\",\n" +
      "            \"keyGenerationComponent\":{\n" +
      "              \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "              \"attribute\":\"name\"\n" +
      "            }\n" +
      "          }\n" +
      "        ],\n" +
      "        \"similarityComponents\":[\n" +
      "          {\n" +
      "            \"id\":\"simName\",\n" +
      "            \"sourceGraph\":\"*\",\n" +
      "            \"targetGraph\":\"*\",\n" +
      "            \"sourceLabel\":\"A\",\n" +
      "            \"targetLabel\":\"B\",\n" +
      "            \"sourceAttribute\":\"name\",\n" +
      "            \"targetAttribute\":\"name\",\n" +
      "            \"weight\":\"1\",\n" +
      "            \"similarityMethod\":\"TruncateBeginComponent\",\n" +
      "            \"length\":\"2\"\n" +
      "          }\n" +
      "        ],\n" +
      "        \"selectionComponent\":{}\n" +
      "      }\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"CLUSTERING\",\n" +
      "      \"config\":{\n" +
      "        \"clustering\":{\n" +
      "          \"clusteringMethod\":\"CENTER\",\n" +
      "          \"prioritySelection\":\"MIN\",\n" +
      "          \"isEdgesBiDirected\":false,\n" +
      "          \"clusteringOutputType\":\"GRAPH\",\n" +
      "          \"maxIteration\":\"MAX_VALUE\"\n" +
      "        }\n" +
      "      }\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"POSTPROCESSING\",\n" +
      "      \"config\":[\n" +
      "        {\n" +
      "          \"task\":\"WRITE_GRAPH\",\n" +
      "          \"path\":\"" + tempOutDir.replace("\\", "\\\\") + "\"\n" +
      "          \n" +
      "        }\n" +
      "      ]\n" +
      "    }\n" +
      "  ]\n" +
      "]";

    File tempFile = File.createTempFile("famerConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(config);
    bw.close();

    FamerConfiguration.main(new String[]{tempFile.getPath()});

    // check if result graph was written
    File dir = new File(tempOutDir);
    assertTrue(dir.exists());
    assertTrue(Objects.requireNonNull(dir.list()).length > 0);
  }

  @Test
  public void testRunFamerConfigurationWithLogicalGraph() throws Exception {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_2").getPath();
    String tempOutDir = Files.createTempDirectory("famerOutput").toString();

    String config = "[\n" +
      "  [\n" +
      "    {\n" +
      "      \"task\":\"PREPROCESSING\",\n" +
      "      \"config\":{\n" +
      "        \"task\":\"READ\",\n" +
      "        \"return\":\"LOGICAL_GRAPH\",\n" +
      "        \"path\":\"" + path + "\",\n" +
      "        \"checkGraphLabel\":{\n" +
      "          \"valueIfNotExists\":\"source1\",\n" +
      "          \"overwrite\":\"true\"\n" +
      "        }\n" +
      "      }\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"LINKING\",\n" +
      "      \"config\":{\n" +
      "        \"keepCurrentEdges\":\"true\",\n" +
      "        \"recomputeSimilarityForCurrentEdges\":\"false\",\n" +
      "        \"blockingComponents\":[\n" +
      "          {\n" +
      "            \"blockingMethod\":\"CartesianProductComponent\",\n" +
      "            \"keyGenerationComponent\":{\n" +
      "              \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "              \"attribute\":\"name\"\n" +
      "            }\n" +
      "          }\n" +
      "        ],\n" +
      "        \"similarityComponents\":[\n" +
      "          {\n" +
      "            \"id\":\"simName\",\n" +
      "            \"sourceGraph\":\"*\",\n" +
      "            \"targetGraph\":\"*\",\n" +
      "            \"sourceLabel\":\"C\",\n" +
      "            \"targetLabel\":\"D\",\n" +
      "            \"sourceAttribute\":\"name\",\n" +
      "            \"targetAttribute\":\"name\",\n" +
      "            \"weight\":\"1\",\n" +
      "            \"similarityMethod\":\"TruncateBeginComponent\",\n" +
      "            \"length\":\"2\"\n" +
      "          }\n" +
      "        ],\n" +
      "        \"selectionComponent\":{}\n" +
      "      }\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"CLUSTERING\",\n" +
      "      \"config\":{\n" +
      "        \"clustering\":{\n" +
      "          \"clusteringMethod\":\"CENTER\",\n" +
      "          \"prioritySelection\":\"MIN\",\n" +
      "          \"isEdgesBiDirected\":false,\n" +
      "          \"clusteringOutputType\":\"GRAPH\",\n" +
      "          \"maxIteration\":\"MAX_VALUE\"\n" +
      "        }\n" +
      "      }\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"POSTPROCESSING\",\n" +
      "      \"config\":[\n" +
      "        {\n" +
      "          \"task\":\"WRITE_GRAPH\",\n" +
      "          \"path\":\"" + tempOutDir.replace("\\", "\\\\") + "\"\n" +
      "          \n" +
      "        }\n" +
      "      ]\n" +
      "    }\n" +
      "  ]\n" +
      "]";

    File tempFile = File.createTempFile("famerConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(config);
    bw.close();

    FamerConfiguration.main(new String[]{tempFile.getPath()});

    // check if result graph was written
    File dir = new File(tempOutDir);
    assertTrue(dir.exists());
    assertTrue(Objects.requireNonNull(dir.list()).length > 0);
  }

  @Test
  public void testRunFamerConfigurationWithIncrementalRepairing() throws Exception {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_2").getPath();
    String tempOutDir = Files.createTempDirectory("famerOutput").toString();

    String config = "[\n" +
      "  [\n" +
      "    {\n" +
      "      \"task\":\"PREPROCESSING\",\n" +
      "      \"config\":{\n" +
      "        \"task\":\"READ\",\n" +
      "        \"return\":\"LOGICAL_GRAPH\",\n" +
      "        \"path\":\"" + path + "\",\n" +
      "        \"checkGraphLabel\":{\n" +
      "          \"valueIfNotExists\":\"source1\",\n" +
      "          \"overwrite\":\"true\"\n" +
      "        }\n" +
      "      }\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"LINKING\",\n" +
      "      \"config\":{\n" +
      "        \"keepCurrentEdges\":\"true\",\n" +
      "        \"recomputeSimilarityForCurrentEdges\":\"false\",\n" +
      "        \"blockingComponents\":[\n" +
      "          {\n" +
      "            \"blockingMethod\":\"CartesianProductComponent\",\n" +
      "            \"keyGenerationComponent\":{\n" +
      "              \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "              \"attribute\":\"name\"\n" +
      "            }\n" +
      "          }\n" +
      "        ],\n" +
      "        \"similarityComponents\":[\n" +
      "          {\n" +
      "            \"id\":\"simName\",\n" +
      "            \"sourceGraph\":\"*\",\n" +
      "            \"targetGraph\":\"*\",\n" +
      "            \"sourceLabel\":\"C\",\n" +
      "            \"targetLabel\":\"D\",\n" +
      "            \"sourceAttribute\":\"name\",\n" +
      "            \"targetAttribute\":\"name\",\n" +
      "            \"weight\":\"1\",\n" +
      "            \"similarityMethod\":\"TruncateBeginComponent\",\n" +
      "            \"length\":\"2\"\n" +
      "          }\n" +
      "        ],\n" +
      "        \"selectionComponent\":{},\n" +
      "        \"tagNewLink\":\"true\"," +
      "      }\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"INCREMENTAL_REPAIRING\",\n" +
      "      \"config\":{\n" +
      "        \"repairingMethod\":\"MBM\",\n" +
      "        \"delta\":0.0,\n" +
      "        \"clusterIdPrefix\":\"mbm\",\n" +
      "        \"isSCRemoving\":false,\n" +
      "        \"clustering\":{\n" +
      "          \"clusteringMethod\":\"CONNECTED_COMPONENTS\",\n" +
      "          \"clusteringOutputType\":\"GRAPH\",\n" +
      "          \"maxIteration\":\"MAX_VALUE\"\n" +
      "        }\n" +
      "      }\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"POSTPROCESSING\",\n" +
      "      \"config\":[\n" +
      "        {\n" +
      "          \"task\":\"WRITE_GRAPH\",\n" +
      "          \"path\":\"" + tempOutDir.replace("\\", "\\\\") + "\"\n" +
      "          \n" +
      "        }\n" +
      "      ]\n" +
      "    }\n" +
      "  ]\n" +
      "]";

    File tempFile = File.createTempFile("famerConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(config);
    bw.close();

    FamerConfiguration.main(new String[]{tempFile.getPath()});

    // check if result graph was written
    File dir = new File(tempOutDir);
    assertTrue(dir.exists());
    assertTrue(Objects.requireNonNull(dir.list()).length > 0);
  }

  @Test(expected = FamerConfigException.class)
  public void testRunFamerConfigurationWithMissingPreprocessing() throws Exception {
    String config = "[\n" +
      "  [\n" +
      "    {\n" +
      "      \"task\":\"LINKING\",\n" +
      "      \"config\":{}\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"CLUSTERING\",\n" +
      "      \"config\":{}\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"POSTPROCESSING\",\n" +
      "      \"config\":[]\n" +
      "    }\n" +
      "  ]\n" +
      "]";

    File tempFile = File.createTempFile("famerConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(config);
    bw.close();

    FamerConfiguration.main(new String[]{tempFile.getPath()});
  }

  @Test(expected = FamerConfigException.class)
  public void testRunFamerConfigurationWithMissingPostprocessing() throws Exception {
    String config = "[\n" +
      "  [\n" +
      "    {\n" +
      "      \"task\":\"PREPROCESSING\",\n" +
      "      \"config\":{}\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"LINKING\",\n" +
      "      \"config\":{}\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"CLUSTERING\",\n" +
      "      \"config\":{}\n" +
      "    }\n" +
      "  ]\n" +
      "]";

    File tempFile = File.createTempFile("famerConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(config);
    bw.close();

    FamerConfiguration.main(new String[]{tempFile.getPath()});
  }

  @Test(expected = FamerConfigException.class)
  public void testRunFamerConfigurationWithIncompatibleReturnType() throws Exception {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_1").getPath();

    String config = "[\n" +
      "  [\n" +
      "    {\n" +
      "      \"task\":\"PREPROCESSING\",\n" +
      "      \"config\":{\n" +
      "        \"task\":\"BENCHMARK\",\n" +
      "        \"return\":\"PERFECT_MAPPING\",\n" +
      "        \"path\":\"" + path + "\",\n" +
      "        \"name\":\"MUSICBRAINZ\"" +
      "      }\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"LINKING\",\n" +
      "      \"config\":{}\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"CLUSTERING\",\n" +
      "      \"config\":{}\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"POSTPROCESSING\",\n" +
      "      \"config\":[]\n" +
      "    }\n" +
      "  ]\n" +
      "]";

    File tempFile = File.createTempFile("famerConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(config);
    bw.close();

    FamerConfiguration.main(new String[]{tempFile.getPath()});
  }

  @Test(expected = FamerConfigException.class)
  public void testRunFamerConfigurationWithIncompatibleTaskOrder() throws Exception {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_1").getPath();

    String config = "[\n" +
      "  [\n" +
      "    {\n" +
      "      \"task\":\"PREPROCESSING\",\n" +
      "      \"config\":{\n" +
      "        \"task\":\"READ\",\n" +
      "        \"return\":\"LOGICAL_GRAPH\",\n" +
      "        \"path\":\"" + path + "\"\n" +
      "      }\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"CLUSTERING\",\n" +
      "      \"config\":{\n" +
      "        \"clustering\":{\n" +
      "          \"clusteringMethod\":\"CENTER\",\n" +
      "          \"prioritySelection\":\"MIN\",\n" +
      "          \"isEdgesBiDirected\":false,\n" +
      "          \"clusteringOutputType\":\"GRAPH\",\n" +
      "          \"maxIteration\":\"MAX_VALUE\"\n" +
      "        }\n" +
      "      }\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"LINKING\",\n" +
      "      \"config\":{}\n" +
      "    },\n" +
      "    {\n" +
      "      \"task\":\"POSTPROCESSING\",\n" +
      "      \"config\":[]\n" +
      "    }\n" +
      "  ]\n" +
      "]";

    File tempFile = File.createTempFile("famerConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(config);
    bw.close();

    FamerConfiguration.main(new String[]{tempFile.getPath()});
  }
}
