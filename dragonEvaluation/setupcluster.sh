#!/bin/bash
#SBATCH --time=02:00:00
#SBATCH --output=output33
#SBATCH --nodes=12
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=24
#SBATCH --exclusive

export FLINK_REST_PORT=2512
export FLINK_JOBMANAGER_RPC_PORT=7123
export FLINK_JOBMANAGER_HEAP_SIZE="112640m"
export FLINK_TASKMANAGER_HEAP_SIZE="112640m"
export FLINK_TASKMANAGER_NETWORK_MEMORY_MIN="40960m"
export FLINK_TASKMANAGER_NETWORK_MEMORY_MAX="40960m"


function echodt {
	dt=$(date '+%d.%m.%Y %H:%M:%S');
   	echo -e "\e[32m[$dt]\e[0m $@" 
}

function get_nodelist {
   	scontrol show hostname $SLURM_NODELIST
}

function clean_cluster {
	srun rm -rf $FLINK_HOME
}

function stop_cluster {
	srun /bin/bash -c '[ -f "$FLINK_HOME/bin/jobmanager.sh" ] && $FLINK_HOME/bin/jobmanager.sh stop-all'
	srun /bin/bash -c '[ -f "$FLINK_HOME/bin/taskmanager.sh" ] && $FLINK_HOME/bin/taskmanager.sh stop-all'
}

function start_cluster {
	ssh $1 "/bin/bash $FLINK_HOME/bin/start-cluster.sh"
}

function configure_cluster {
	MASTER_NODE=$1
	shift
	SLAVE_NODES=($@)
	sed "s@jobmanager.rpc.address.pattern@$MASTER_NODE@; \
		s@taskmanager.numberOfTaskSlots.pattern@$(nproc)@; \
		s@jobmanager.rpc.port.pattern@$FLINK_JOBMANAGER_RPC_PORT@; \
		s@rest.port.pattern@$FLINK_REST_PORT@; \
		s@jobmanager.heap.size.pattern@$FLINK_JOBMANAGER_HEAP_SIZE@; \
		s@taskmanager.heap.size.pattern@$FLINK_TASKMANAGER_HEAP_SIZE@; \
		s@env.java.home.pattern@$JAVA_HOME@; \
		s@taskmanager.network.memory.min.pattern@$FLINK_TASKMANAGER_NETWORK_MEMORY_MIN@; \
		s@taskmanager.network.memory.max.pattern@$FLINK_TASKMANAGER_NETWORK_MEMORY_MAX@;" \
		"$FLINK_TEMPLATE/conf/flink-conf.yaml.tmp" > "$FLINK_TEMPLATE/conf/flink-conf.yaml"
	echo "$MASTER_NODE:$FLINK_REST_PORT" > $FLINK_TEMPLATE/conf/masters
	printf '%s\n' "${SLAVE_NODES[@]}" > $FLINK_TEMPLATE/conf/slaves
	srun rm -rf $FLINK_HOME
	srun mkdir -p $FLINK_HOME
	srun cp -Trf $FLINK_TEMPLATE $FLINK_HOME
}

USAGE="Usage: setupcluster.sh (start [num_slaves]|stop|clean)"
STARTSTOP=$1
NUM_SLAVES=$2

if [[ $STARTSTOP != "start" ]] && [[ $STARTSTOP != "stop" ]] && [[ $STARTSTOP != "clean" ]]; then
	echo $USAGE
	exit 1
fi

if [[ $STARTSTOP == "start" ]]; then
	echodt "Job id: $SLURM_JOB_ID" 
	echodt "Cluster uses $SLURM_JOB_NUM_NODES following nodes: $SLURM_NODELIST" 
	NODES=( $(get_nodelist) )
	if [ ${#NODES[@]} -lt 2 ];then
		echo "Not enough nodes to start cluster. Minimum number of nodes is 2." 
		exit 1
	fi
	if [ -z "$NUM_SLAVES" ]; then
		NUM_SLAVES=$((${#NODES[@]} - 1))
	fi
	if [ $((${#NODES[@]} - 1)) -lt $NUM_SLAVES ];then
		echo "Not enough salve nodes." 
		exit 1
	fi
	export MASTER_NODE=(${NODES[@]:0:1})
	export SLAVE_NODES=(${NODES[@]:1:NUM_SLAVES})
	echodt "Master node: $MASTER_NODE"
	echodt "Slave nodes: ${SLAVE_NODES[@]}"
	echodt "Stop all flink instances if running..."
	# stop_cluster
	echodt "Configure flink cluster on $SLURM_NODELIST..."
	configure_cluster $MASTER_NODE ${SLAVE_NODES[@]}
	echodt "Start cluster on $SLURM_NODELIST..."
	start_cluster $MASTER_NODE
	echo "${MASTER_NODE}.sc.uni-leipzig.de" > /tmp/drasca.masternode
	echodt "Setup cluster done!"
fi

if [[ $STARTSTOP == "stop" ]]; then
	echodt "Stop all flink instances on the cluster..."
	stop_cluster
fi

if [[ $STARTSTOP == "clean" ]]; then
	echodt "Remove flink directories at $FLINK_HOME on the cluster..."
	clean_cluster
fi
