#!/bin/bash
#SBATCH --time=02:00:00
#SBATCH --output=output9-vote
#SBATCH --nodes=9
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=24
#SBATCH --exclusive
#SBATCH --partition galaxy-job

export JAVA_HOME="/usr/lib/jvm/java-1.8.0-openjdk"
export FLINK_HOME="/tmp/dragon/flink-1.7.2"
export FLINK_TEMPLATE="$PWD/flink-1.7.2-template"
export JOB_CONF_DIR="$PWD/dragonEvaluation/evaluationConfiguration"
export EVALUATOR_CLASS="org.gradoop.famer.linking.similarityMeasuring.dragonEvaluation.DragonEvaluator"
export EVALUATOR_JAR="$PWD/dragon-linking.jar"

function echodt {
    dt=$(date '+%d.%m.%Y %H:%M:%S');
    echo -e "\e[32m[$dt]\e[0m $@" 
}


echodt "Start jobs"
for NUM_SLAVE in 8 4 2 1
do
    . "$PWD"/setupcluster.sh start $NUM_SLAVE
    sleep 45
    for JOB_CONF_FILE in "$JOB_CONF_DIR"/*
    do
        echodt "Current num slaves: $NUM_SLAVE"
        echodt "Current job config: $JOB_CONF_FILE"
        ssh $MASTER_NODE "$FLINK_HOME/bin/flink run -c $EVALUATOR_CLASS $EVALUATOR_JAR $JOB_CONF_FILE"
        sleep 30
    done
    echodt "Stop and clean cluster!"
    . "$PWD"/setupcluster.sh stop
    . "$PWD"/setupcluster.sh clean
    sleep 30
done
echodt "Jobs done!"
