/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.standardBlocking.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;

/**
 * Computes the size of each group of {@code Tuple3<BlockingKey, PartitionId, Count>}, grouped by
 * BlockingKey. The size is aggregated by adding all Count values in this group.
 * Returns a {@code Tuple2<BlockingKey, AggregatedCount>}.
 */
public class BlockSizeComputer implements
  GroupReduceFunction<Tuple3<String, Integer, Long>, Tuple2<String, Long>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple2<String, Long> reuseTuple = new Tuple2<>();

  @Override
  public void reduce(Iterable<Tuple3<String, Integer, Long>> group,
    Collector<Tuple2<String, Long>> out) throws Exception {
    String blockingKey = "";
    long count = 0L;
    for (Tuple3<String, Integer, Long> groupItem : group) {
      blockingKey = groupItem.f0;
      count += groupItem.f2;
    }
    reuseTuple.f0 = blockingKey;
    reuseTuple.f1 = count;
    out.collect(reuseTuple);
  }
}
