/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.common;

/**
 * Util class holding common methods for all packages in famer-linking module
 */
public class Utils {

  /**
   * Cleans the string attribute value by reducing more than one spaces to 1 space, set characters to lower
   * case and removes leading and trailing whitespace.
   *
   * @param attributeValue the input attribute value to be cleaned
   *
   * @return cleaned attribute value
   */
  public static String cleanAttributeValue(String attributeValue) {
    return attributeValue.toLowerCase().replaceAll("\\s+", " ").trim();
  }
}
