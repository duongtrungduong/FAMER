/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;

/**
 * Computes the similarity degree of two strings using the Truncate-Begin method
 */
public class TruncateBegin implements SimilarityComputation<String> {

  /**
   * The length parameter
   */
  private final int length;

  /**
   * Creates an instance of TruncateEnd
   *
   * @param length The length parameter
   */
  public TruncateBegin(int length) {
    this.length = length;
  }

  @Override
  public double computeSimilarity(String str1, String str2) throws Exception {
    if (str1 == null) {
      throw new NullPointerException("str1 must not be null");
    }
    if (str2 == null) {
      throw new NullPointerException("str2 must not be null");
    }
    double simDegree = 0.0;
    if (str1.length() < length || str2.length() < length) {
      if (str1.equals(str2)) {
        simDegree = 1.0;
      }
    } else if (str1.substring(0, length).equals(str2.substring(0, length))) {
      simDegree = 1.0;
    }

    return simDegree;
  }

  @Override
  public String parsePropertyValue(PropertyValue value) {
    return getString(value);
  }
}
