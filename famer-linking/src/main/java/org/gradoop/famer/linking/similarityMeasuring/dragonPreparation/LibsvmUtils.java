package org.gradoop.famer.linking.similarityMeasuring.dragonPreparation;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.io.TextOutputFormat.TextFormatter;
import org.apache.flink.core.fs.FileSystem.WriteMode;
import org.gradoop.famer.trainingModule.common.dataStructures.CommonInstance;

import java.net.URI;
import java.util.Arrays;
import java.util.Objects;
import java.util.Vector;


public class LibsvmUtils {
    private static final String INDEX_VALUE_DELIMITER = ":";
    private static final String ELEMENT_DELIMITER = " ";
    private static final String ELEMENT_DELIMITER_REGEX = "\\s+";

    public static void writeAsLibsvm(DataSet<CommonInstance> instances, URI fileUri, boolean overWrite) {
        WriteMode writeMode = overWrite ? WriteMode.OVERWRITE : WriteMode.NO_OVERWRITE;
        instances.writeAsFormattedText(fileUri.toString(), writeMode, (TextFormatter<CommonInstance>) instance -> {
            StringBuilder sb = new StringBuilder();
            sb.append(instance.getLabel()).append(ELEMENT_DELIMITER);
            for (int i = 0; i < instance.getFeatures().size(); i++) {
                Double feature = instance.getFeatures().get(i);
                if (Double.compare(feature, 0d) != 0) {
                    sb.append(i + 1).append(INDEX_VALUE_DELIMITER).append(feature).append(ELEMENT_DELIMITER);
                }
            }
            return sb.toString();
        });
    }

    public static DataSet<CommonInstance> readFromLibsvm(ExecutionEnvironment env, URI fileUri) throws Exception {
        DataSet<String> inputRows = env.readTextFile(fileUri.toString());
        int featureSize = inputRows.map((MapFunction<String, Integer>) inputRow -> {
            String[] data = inputRow.split(ELEMENT_DELIMITER_REGEX);
            int max = 0;
            for (int i = 1; i < data.length; i++) {
                int size = Integer.parseInt(data[i].split(INDEX_VALUE_DELIMITER)[0]);
                if (size > max) max = size;
            }
            return max;
        }).returns(new TypeHint<Integer>() {
        }).distinct().collect().stream().max(Integer::compareTo).orElse(0);
        return readFromLibsvm(env, fileUri, featureSize);
    }

    public static DataSet<CommonInstance> readFromLibsvm(ExecutionEnvironment env, URI fileUri, int featureSize) throws Exception {
        DataSet<String> inputRows = env.readTextFile(fileUri.toString());
        return inputRows.map((MapFunction<String, CommonInstance>) inputRow -> {
            String[] data = inputRow.split(ELEMENT_DELIMITER);
            if (data.length == 0) return null;
            double label = Double.parseDouble(data[0]);
            double[] featureArray = new double[featureSize];
            for (int i = 1; i < data.length; i++) {
                String[] tokens = data[i].split(INDEX_VALUE_DELIMITER);
                if (tokens.length <= 1) return null;
                int index = Integer.parseInt(tokens[0]) - 1;
                double value = Double.parseDouble(tokens[1]);
                featureArray[index] = value;
            }
            return new CommonInstance(new Vector<>(Arrays.asList(ArrayUtils.toObject(featureArray))), label, 0d);
        }).filter(Objects::nonNull);
    }
}