/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.keyGeneration.methods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Splits the attribute value on the given {@link #tokenizer} into a list of generated keys. If the
 * attribute value does not contain the tokenizer then the attribute value itself is returned as a
 * single key.
 */
public class WordTokenizer implements KeyGenerator {

  /**
   * The string used as the splitter on the attribute value
   */
  private final String tokenizer;

  /**
   * Creates an instance of PrefixLength
   *
   * @param tokenizer The string used as the splitter on the attribute value
   **/
  public WordTokenizer(String tokenizer) {
    this.tokenizer = tokenizer;
  }

  @Override
  public List<String> generateKey(String attributeValue) {
    return new ArrayList<>(Arrays.asList(attributeValue.split(tokenizer)));
  }

  @Override
  public boolean returnsMultipleKeys() {
    return true;
  }
}
