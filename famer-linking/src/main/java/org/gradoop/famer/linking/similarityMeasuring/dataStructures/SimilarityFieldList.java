/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import java.util.ArrayList;
import java.util.List;

/**
 * The structure that keeps the list of {@link SimilarityField}s for a vertex pair as well as the required
 * methods
 */
public class SimilarityFieldList {

  /**
   * The list of {@link SimilarityField}s for a vertex pair
   */
  private final List<SimilarityField> similarityFields;

  /**
   * Creates an instance of SimilarityFieldList
   */
  public SimilarityFieldList() {
    similarityFields = new ArrayList<>();
  }

  /**
   * Returns the list of {@link SimilarityField}s for a vertex pair
   *
   * @return list of {@link SimilarityField}s for a vertex pair
   */
  public List<SimilarityField> getSimilarityFields() {
    return similarityFields;
  }

  /**
   * Retrieves a SimilarityField by its id
   *
   * @param id The unique identifier of a SimilarityField
   *
   * @return Returns null, if the SimilarityField does not exist
   */
  public SimilarityField getSimilarityField(String id) {
    for (SimilarityField similarityField : similarityFields) {
      if (similarityField.getId().equals(id)) {
        return similarityField;
      }
    }
    return null;
  }

  /**
   * Adds a SimilarityField to the list of {@link SimilarityField}s
   *
   * @param similarityField The similarityField that is going to be added to the list
   */
  public void add(SimilarityField similarityField) {
    similarityFields.add(similarityField);
  }

  /**
   * Checks whether the similarity field list is empty
   *
   * @return true, if the similarity field list has no elements
   */
  public boolean isEmpty() {
    return similarityFields.isEmpty();
  }
}
