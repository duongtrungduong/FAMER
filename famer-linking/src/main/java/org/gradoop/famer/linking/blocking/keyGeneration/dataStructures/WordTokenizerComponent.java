/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.keyGeneration.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.blocking.keyGeneration.methods.KeyGenerator;
import org.gradoop.famer.linking.blocking.keyGeneration.methods.WordTokenizer;

/**
 * Key generator component which uses {@link WordTokenizer}.
 */
public class WordTokenizerComponent extends KeyGeneratorComponent {

  /**
   * String used as splitter on the attribute value
   */
  private final String tokenizer;

  /**
   * Creates an instance of WordTokenizerComponent
   *
   * @param attribute The property / attribute name for generating the blocking keys
   * @param tokenizer String used as splitter on the attribute value
   */
  public WordTokenizerComponent(String attribute, String tokenizer) {
    super(attribute);
    this.tokenizer = tokenizer;
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The key generation component json config part
   */
  public WordTokenizerComponent(JSONObject jsonConfig) {
    super(jsonConfig);
    try {
      this.tokenizer = jsonConfig.getString("tokenizer");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("WordTokenizerComponent: value for tokenizer could not " +
        "be found or parsed to int", ex);
    }
  }

  public String getTokenizer() {
    return tokenizer;
  }

  @Override
  public KeyGenerator buildKeyGenerator() {
    return new WordTokenizer(tokenizer);
  }
}
