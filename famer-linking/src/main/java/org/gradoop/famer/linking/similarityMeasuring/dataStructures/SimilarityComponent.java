/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;

import java.io.Serializable;

/**
 * Abstract base class for all similarity component methods
 */
public abstract class SimilarityComponent implements Serializable {

  /**
   * The unique identifier of the component object
   */
  private String componentId;

  /**
   * The attribute/property title of the source graph
   */
  private String sourceAttribute;

  /**
   * The attribute/property title of the target graph
   */
  private String targetAttribute;

  /**
   * The graph label of the source graph which specifies the DATA SOURCE of an entity and its corresponding
   * value is stored as property in each vertex
   */
  private String sourceGraph;

  /**
   * The graph label of the target graph which specifies the DATA SOURCE of an entity and its corresponding
   * value is stored as property in each vertex
   */
  private String targetGraph;

  /**
   * The label of the source graph
   */
  private String sourceLabel;

  /**
   * The label of the target graph
   */
  private String targetLabel;

  /**
   * The effective coefficient of the similarity value
   */
  private double weight;

  /**
   * Creates an instance of SimilarityComponent from the given json object.
   *
   * @param jsonConfig The json object that contains the configuration.
   */
  public SimilarityComponent(JSONObject jsonConfig) {
    if (jsonConfig.opt("id") == null) {
      throw new UnsupportedOperationException("No id found in similarity component.");
    }
    this.componentId = jsonConfig.optString("id");

    if (jsonConfig.opt("sourceGraph") == null) {
      throw new UnsupportedOperationException("No sourceGraph found in similarity component.");
    }
    this.sourceGraph = jsonConfig.optString("sourceGraph");

    if (jsonConfig.opt("targetGraph") == null) {
      throw new UnsupportedOperationException("No targetGraph found in similarity component.");
    }
    this.targetGraph = jsonConfig.optString("targetGraph");

    if (jsonConfig.opt("sourceLabel") == null) {
      throw new UnsupportedOperationException("No sourceLabel found in similarity component.");
    }
    this.sourceLabel = jsonConfig.optString("sourceLabel");

    if (jsonConfig.opt("targetLabel") == null) {
      throw new UnsupportedOperationException("No targetLabel found in similarity component.");
    }
    this.targetLabel = jsonConfig.optString("targetLabel");

    if (jsonConfig.opt("sourceAttribute") == null) {
      throw new UnsupportedOperationException("No sourceAttribute found in similarity component.");
    }
    this.sourceAttribute = jsonConfig.optString("sourceAttribute");

    if (jsonConfig.opt("targetAttribute") == null) {
      throw new UnsupportedOperationException("No targetAttribute found in similarity component.");
    }
    this.targetAttribute = jsonConfig.optString("targetAttribute");

    try {
      this.weight = jsonConfig.getDouble("weight");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Value for weight in similarity component could not be found " +
        "or parsed to double.", ex);
    }
  }

  /**
   * Creates an instance of SimilarityComponent from the given {@link SimilarityComponentBaseConfig}.
   *
   * @param baseConfig The base configuration for the similarity component
   */
  public SimilarityComponent(SimilarityComponentBaseConfig baseConfig) {
    this.componentId = baseConfig.getComponentId();
    this.sourceAttribute = baseConfig.getSourceAttribute();
    this.targetAttribute = baseConfig.getTargetAttribute();
    this.sourceGraph = baseConfig.getSourceGraph();
    this.targetGraph = baseConfig.getTargetGraph();
    this.sourceLabel = baseConfig.getSourceLabel();
    this.targetLabel = baseConfig.getTargetLabel();
    this.weight = baseConfig.getWeight();
  }

  public String getComponentId() {
    return componentId;
  }

  public void setComponentId(String componentId) {
    this.componentId = componentId;
  }

  public String getSourceAttribute() {
    return sourceAttribute;
  }

  public void setSourceAttribute(String sourceAttribute) {
    this.sourceAttribute = sourceAttribute;
  }

  public String getTargetAttribute() {
    return targetAttribute;
  }

  public void setTargetAttribute(String targetAttribute) {
    this.targetAttribute = targetAttribute;
  }

  public String getSourceGraph() {
    return sourceGraph;
  }

  public void setSourceGraph(String sourceGraph) {
    this.sourceGraph = sourceGraph;
  }

  public String getTargetGraph() {
    return targetGraph;
  }

  public void setTargetGraph(String targetGraph) {
    this.targetGraph = targetGraph;
  }

  public String getSourceLabel() {
    return sourceLabel;
  }

  public void setSourceLabel(String sourceLabel) {
    this.sourceLabel = sourceLabel;
  }

  public String getTargetLabel() {
    return targetLabel;
  }

  public void setTargetLabel(String targetLabel) {
    this.targetLabel = targetLabel;
  }

  public double getWeight() {
    return weight;
  }

  public void setWeight(double weight) {
    this.weight = weight;
  }

  /**
   * The abstract method that must be implemented by all similarity computation components
   *
   * @return The similarity computation method
   *
   * @throws Exception Thrown on errors while building the similarity component
   */
  public abstract SimilarityComputation buildSimilarityComputation() throws Exception;
}
