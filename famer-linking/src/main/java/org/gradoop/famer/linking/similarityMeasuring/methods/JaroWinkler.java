/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;

/**
 * Computes the similarity degree of two strings using the JaroWinkler method from
 * {@link info.debatty.java.stringsimilarity}
 */
public class JaroWinkler implements SimilarityComputation<String> {

  /**
   * The threshold
   */
  private double threshold;

  /**
   * Creates an instance of JaroWinkler with default library threshold (currently 0.7)
   */
  public JaroWinkler() {
    this(0d);
  }

  /**
   * Creates an instance of JaroWinkler with the given threshold. If the threshold is set to {@code 0}, the
   * default library threshold (currently 0.7) is used.
   *
   * @param threshold The threshold
   */
  public JaroWinkler(double threshold) {
    this.threshold = threshold;
  }

  @Override
  public double computeSimilarity(String str1, String str2) throws Exception {
    if (str1 == null) {
      throw new NullPointerException("str1 must not be null");
    }
    if (str2 == null) {
      throw new NullPointerException("str2 must not be null");
    }
    double simDegree;
    info.debatty.java.stringsimilarity.JaroWinkler jaroWinkler;
    if (threshold == 0d) {
      jaroWinkler = new info.debatty.java.stringsimilarity.JaroWinkler();
    } else {
      jaroWinkler = new info.debatty.java.stringsimilarity.JaroWinkler(threshold);
    }
    simDegree = jaroWinkler.similarity(str1, str2);

    return simDegree;
  }

  @Override
  public String parsePropertyValue(PropertyValue value) {
    return getString(value);
  }
}
