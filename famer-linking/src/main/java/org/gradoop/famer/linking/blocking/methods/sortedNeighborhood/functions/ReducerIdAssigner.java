/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.sortedNeighborhood.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Assigns each vertex to a reducer based on its global position, number of all vertices and the number
 * of reducers.
 */
@FunctionAnnotation.ForwardedFields("f0;f1")
public class ReducerIdAssigner implements
  MapFunction<Tuple4<EPGMVertex, String, Long, Long>, Tuple3<EPGMVertex, String, Integer>> {

  /**
   * The number of reducers (usually parallelism degree)
   */
  private final int reducerNo;

  /**
   * The windowSize
   */
  private final int windowSize;

  /**
   * Reduce object instantiation
   */
  private final Tuple3<EPGMVertex, String, Integer> reuseTuple;

  /**
   * Creates an instance of ReducerIdAssigner
   *
   * @param reducerNo  The number of reduces (usually parallelism degree)
   * @param windowSize The size of window
   */
  public ReducerIdAssigner(int reducerNo, int windowSize) {
    this.reducerNo = reducerNo;
    this.windowSize = windowSize;
    this.reuseTuple = new Tuple3<>();
  }

  @Override
  public Tuple3<EPGMVertex, String, Integer> map(Tuple4<EPGMVertex, String, Long, Long> in) throws Exception {
    int vertexNoPerReducer = (int) Math.ceil((double) in.f3 / reducerNo);
    // the number of vertices on a reducer must not be less than window size, o.w. we will loose some
    // comparisons due to wrong replication
    if (vertexNoPerReducer < windowSize) {
      vertexNoPerReducer = windowSize;
    }
    int reducerId = (int) Math.ceil((double) in.f2 / vertexNoPerReducer);
    reuseTuple.f0 = in.f0;
    reuseTuple.f1 = in.f1;
    reuseTuple.f2 = reducerId;
    return reuseTuple;
  }
}
