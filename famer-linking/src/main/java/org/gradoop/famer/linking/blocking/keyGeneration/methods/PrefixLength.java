/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.keyGeneration.methods;

import java.util.ArrayList;
import java.util.List;

/**
 * Generates a key from the n initial letters of the attribute value, with n given in {@link #prefixLength}.
 */
public class PrefixLength implements KeyGenerator {

  /**
   * The length of the blocking key
   */
  private final int prefixLength;

  /**
   * Creates an instance of PrefixLength
   *
   * @param prefixLength The length of the blocking key
   */
  public PrefixLength(int prefixLength) {
    this.prefixLength = prefixLength;
  }

  @Override
  public List<String> generateKey(String attributeValue) {
    if (prefixLength <= 0) {
      throw new IllegalArgumentException("Invalid prefix length: " + prefixLength);
    }

    List<String> keys = new ArrayList<>();
    if (attributeValue.length() <= prefixLength) {
      keys.add(attributeValue);
    } else {
      keys.add(attributeValue.substring(0, prefixLength));
    }
    return keys;
  }

  @Override
  public boolean returnsMultipleKeys() {
    return false;
  }
}
