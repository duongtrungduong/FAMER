package org.gradoop.famer.linking.similarityMeasuring.dragonEvaluation;

import org.apache.commons.io.FileUtils;
import org.apache.flink.api.common.functions.FlatJoinFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.utils.DataSetUtils;
import org.apache.flink.configuration.ConfigConstants;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.RestOptions;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMGraphHead;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.linking.Linker;
import org.gradoop.famer.linking.linking.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.famer.linking.similarityMeasuring.SimilarityMeasurer;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponentBaseConfig;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityField;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityFieldList;
import org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.DragonTrainer;
import org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.LibsvmUtils;
import org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.dataStructures.EnrichedDragonComponent;
import org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions.GoldGraphCreator;
import org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions.TrainDataCreator;
import org.gradoop.famer.linking.similarityMeasuring.dragonReport.DragonRESTReport;
import org.gradoop.famer.trainingModule.classifier.dataStructures.ClassifierComponent;
import org.gradoop.famer.trainingModule.classifier.dataStructures.ClassifierComponentBaseConfig;
import org.gradoop.famer.trainingModule.classifier.dataStructures.DragonClassifierComponent;
import org.gradoop.famer.trainingModule.classifier.dataStructures.EnsembleDragonClassifierComponent;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FMeasureFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FitnessFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FitnessFunction.FitnessFunctionType;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.GiniIndexFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.ErrorEstimatePruner;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.FMeasurePruner;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.NonePruner;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.PruningFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.PruningFunction.PruningMethod;
import org.gradoop.famer.trainingModule.classifier.methods.Classifier;
import org.gradoop.famer.trainingModule.classifier.methods.DragonClassifier;
import org.gradoop.famer.trainingModule.classifier.methods.EnsembleDragonClassifier;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;
import org.gradoop.famer.trainingModule.common.dataStructures.CommonInstance;
import org.gradoop.famer.trainingModule.common.dataStructures.TrainTestDataSet;
import org.gradoop.famer.trainingModule.common.functions.ClassBalancer;
import org.gradoop.famer.trainingModule.common.functions.ClassBalancer.ClassBalanceMethod;
import org.gradoop.famer.trainingModule.common.functions.TrainTestSplitter;
import org.gradoop.flink.io.impl.csv.CSVDataSink;
import org.gradoop.flink.io.impl.csv.CSVDataSource;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.util.GradoopFlinkConfig;

import java.io.File;
import java.io.Serializable;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.apache.flink.core.fs.FileSystem.WriteMode.OVERWRITE;
import static org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.dataStructures.EnrichedDragonComponent.CLASS_NAME;
import static org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.dataStructures.EnrichedDragonComponent.CLASS_VALUES;
import static org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions.GoldGraphCreator.*;
import static org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions.JsonComponentHelper.getSimilarityComponentAsJson;
import static org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions.JsonComponentHelper.getSimilarityComponentFromJson;
import static org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance.TRUE;

public class DragonEvaluator {
    public enum EvaluationComponent {
        GOLD_GRAPH_CREATOR,
        TRAIN_DATA_CREATOR,
        DRAGON_TRAIN_PREDICT,
        DRAGON_TRAINING_PIPELINE,
        AUROC,
        ENSEMBLE_TRAIN_PREDICT
    }

    public enum ClassifierEvaluationMethod {
        HOLD_OUT,
        K_FOLD
    }

    public static String evaluateGoldGraphCreator(ExecutionEnvironment env, URI inputGraphsCsvUri, URI groundTruthUri,
            URI goldGraphCsvUri) throws Exception {
        GradoopFlinkConfig gradoopFlinkConfig = GradoopFlinkConfig.createConfig(env);
        CSVDataSource inputGraphsSource = new CSVDataSource(inputGraphsCsvUri.toString(), gradoopFlinkConfig);
        GraphCollection inputGraphs = inputGraphsSource.getGraphCollection();
        LogicalGraph goldGraph = GoldGraphCreator.getGoldGraph(inputGraphs, groundTruthUri, gradoopFlinkConfig);

        List<String> graphLabels = goldGraph.getVertices()
                .map(m -> m.getPropertyValue(Linker.GRAPH_LABEL).getString()).distinct().collect();
        long numVertices = goldGraph.getVertices().count();
        long numEdges = goldGraph.getEdges().count();
        long numPositiveExamples = goldGraph.getEdges()
                .filter(f -> f.getPropertyValue(GoldGraphCreator.IS_MATCH_PROPERTY_KEY).getBoolean()).count();
        long numNegativeExamples = goldGraph.getEdges()
                .filter(f -> !f.getPropertyValue(GoldGraphCreator.IS_MATCH_PROPERTY_KEY).getBoolean()).count();

        CSVDataSink goldDataSink = new CSVDataSink(goldGraphCsvUri.toString(), gradoopFlinkConfig);
        goldGraph.writeTo(goldDataSink, true);
        env.execute();
        return "evaluation.type:\t" + "gold_graph_creator" + "\n" +
                "evaluation.graphLabels:\t" + graphLabels + "\n" +
                "evaluation.numVertices:\t" + numVertices + "\n" +
                "evaluation.numEdges:\t" + numEdges + "\n" +
                "evaluation.numPositiveExamples:\t" + numPositiveExamples + "\n" +
                "evaluation.numNegativeExamples:\t" + numNegativeExamples;
    }

    public static String evaluateTrainDataCreator(ExecutionEnvironment env, URI goldGraphCsvUri,
            List<SimilarityComponent> trainingSimilarityComponents, URI trainDataSetUri, int scaleSampleFactor) throws Exception {
        GradoopFlinkConfig gradoopFlinkConfig = GradoopFlinkConfig.createConfig(env);
        LogicalGraph goldGraph = new CSVDataSource(goldGraphCsvUri.toString(), gradoopFlinkConfig).getLogicalGraph();

        DataSet<BinaryInstance> trainDataSet = TrainDataCreator.getTrainingInstances(goldGraph, trainingSimilarityComponents);

        long numPositiveExamples = trainDataSet.filter(BinaryInstance::isTrue).count();
        long numNegativeExamples = trainDataSet.filter(f -> !f.isTrue()).count();
        List<JSONObject> similarityConfigs = new ArrayList<>();
        for (SimilarityComponent sc : trainingSimilarityComponents) {
            similarityConfigs.add(getSimilarityComponentAsJson(sc));
        }

        LibsvmUtils.writeAsLibsvm(trainDataSet.map(m->m), trainDataSetUri,true);
        env.execute();

        if (scaleSampleFactor > 1) {
            DataSet<BinaryInstance>  sampledDataSet = DataSetUtils.sample(trainDataSet, true, scaleSampleFactor);
            URI sampledOutputUri = URI.create(trainDataSetUri.toString() + "-x" + scaleSampleFactor);
            LibsvmUtils.writeAsLibsvm(sampledDataSet.map(m->m), sampledOutputUri, true);
            env.execute();
        }

        return "evaluation.type:\t" + "train_data_creator" + "\n" +
                "evaluation.numPositiveExamples:\t" + numPositiveExamples + "\n" +
                "evaluation.numNegativeExamples:\t" + numNegativeExamples + "\n" +
                "evaluation.featureSize:\t" + trainingSimilarityComponents.size() + "\n" +
                "evaluation.similarityComponents:\t" + similarityConfigs;
    }

    public static void evaluateAUROC(ExecutionEnvironment env, URI goldGraphCsvUri,
            EnrichedDragonComponent dragonComponent, double fraction, URI aurocCsvUri) throws Exception {
        GradoopFlinkConfig gradoopFlinkConfig = GradoopFlinkConfig.createConfig(env);

        LogicalGraph goldGraph = new CSVDataSource(goldGraphCsvUri.toString(), gradoopFlinkConfig).getLogicalGraph();
        Tuple2<LogicalGraph, LogicalGraph> trainTestGraph = graphSplitter(goldGraph, fraction, gradoopFlinkConfig);
        LogicalGraph trainGraph = trainTestGraph.f0;
        LogicalGraph testGraph = trainTestGraph.f1;

        DragonTrainer.trainDragon(dragonComponent, trainGraph);
        System.out.println(dragonComponent.getDragonClassifier().getDragonTree());

        DataSet<Tuple2<EPGMVertex, EPGMVertex>> testVertices = TrainDataCreator.getPositiveAndNegativeVertexPairs(testGraph);
        List<SimilarityComponent> similarityComponents = Collections.singletonList(dragonComponent);
        testVertices.flatMap(new SimilarityMeasurer(similarityComponents))
                .map(new AurocDataGenerator())
                .withBroadcastSet(testGraph.getEdges(), "GOLD_EDGES")
                .filter(Objects::nonNull)
                .writeAsText(aurocCsvUri.toString(), OVERWRITE)
                .setParallelism(1);
        env.execute();
    }

    private static class AurocDataGenerator extends RichMapFunction<Tuple3<EPGMVertex, EPGMVertex,
            SimilarityFieldList>, String> {

        private Map<Tuple2<GradoopId, GradoopId>, Boolean> vertexIdPairsAndLabel;

        @Override
        public void open(Configuration parameters) {
            List<EPGMEdge> goldEdges = getRuntimeContext().getBroadcastVariable("GOLD_EDGES");
            vertexIdPairsAndLabel = goldEdges.stream()
                    .collect(Collectors.toMap(m -> Tuple2.of(m.getSourceId(), m.getTargetId()),
                            m -> m.getPropertyValue(IS_MATCH_PROPERTY_KEY).getBoolean()));
        }

        @Override
        public String map(Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList> input)
                throws Exception {
            StringBuilder simValues = new StringBuilder();

            for (SimilarityField sf : input.f2.getSimilarityFields()) {
                simValues.append(sf.getSimilarityValue()).append(",");
            }

            boolean label = vertexIdPairsAndLabel.getOrDefault(Tuple2.of(input.f0.getId(), input.f1.getId()), false);
            if (label) simValues.append("1.0"); else simValues.append("-1.0");

            return simValues.toString();
        }
    }

    private static Tuple2<LogicalGraph, LogicalGraph> graphSplitter(LogicalGraph goldGraph, double fraction,
            GradoopFlinkConfig config) throws Exception {
        DataSet<EPGMVertex> goldVertices = goldGraph.getVerticesByLabel(GOLD_GRAPH_VERTEX_LABEL);
        DataSet<EPGMEdge> goldEdges = goldGraph.getEdgesByLabel(GOLD_GRAPH_EDGE_LABEL);

        DataSet<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>> goldTuples = EdgeToEdgeSourceVertexTargetVertex
                .execute(goldVertices, goldEdges);

        DataSet<Tuple2<Long, Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>>> inputWithId = DataSetUtils.zipWithUniqueId(goldTuples);

        DataSet<Tuple2<Long, Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>>> leftSplit = DataSetUtils
                .sample(inputWithId,false, fraction);

        DataSet<Tuple2<Long, Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>>> rightSplit = inputWithId.leftOuterJoin(leftSplit)
                .where(0).equalTo(0)
                .with((FlatJoinFunction<Tuple2<Long, Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>>,
                        Tuple2<Long, Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>>,
                        Tuple2<Long, Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>>>) (full, left, collector) -> {
                    if (left == null) collector.collect(full);
                })
                .returns(new TypeHint<Tuple2<Long, Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>>>() {});

        DataSet<EPGMGraphHead> goldHead = goldGraph.getGraphHead();
        DataSet<EPGMVertex> trainVertices = leftSplit.flatMap(
                (FlatMapFunction<Tuple2<Long, Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>>, EPGMVertex>) (
                        input, collector) -> {
                    collector.collect(input.f1.f1);
                    collector.collect(input.f1.f2);
                }).returns(new TypeHint<EPGMVertex>() {});
        DataSet<EPGMVertex> testVertices = rightSplit.flatMap(
                (FlatMapFunction<Tuple2<Long, Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>>, EPGMVertex>) (
                        input, collector) -> {
                    collector.collect(input.f1.f1);
                    collector.collect(input.f1.f2);
                }).returns(new TypeHint<EPGMVertex>() {});

        DataSet<EPGMEdge> trainEdges = leftSplit.map(m -> m.f1.f0);
        DataSet<EPGMEdge> testEdges = rightSplit.map(m -> m.f1.f0);

        LogicalGraph trainGraph = config.getLogicalGraphFactory().fromDataSets(goldHead, trainVertices, trainEdges);
        LogicalGraph testGraph = config.getLogicalGraphFactory().fromDataSets(goldHead, testVertices, testEdges);

        return Tuple2.of(trainGraph, testGraph);
    }

    public static String evaluateHoldOut(ExecutionEnvironment env, ClassifierComponent classifierComponent,
            DataSet<CommonInstance> input, double fraction, URI modelOutputUri) throws Exception {
        ClassBalancer classBalancer = classifierComponent.getClassBalancer();
        DataSet<CommonInstance> balancedInstances = classBalancer.resample(input);
        TrainTestDataSet trainTestDataSet = TrainTestSplitter.trainTestSplit(balancedInstances, fraction, false);

        DataSet<CommonInstance> trainDataSet = trainTestDataSet.getTrainingDataSet();
        DataSet<CommonInstance> testDataSet = trainTestDataSet.getTestingDataSet();

        long numOfTrainData = trainDataSet.count();
        long numOfTestData = testDataSet.count();

        Classifier classifier = classifierComponent instanceof DragonClassifierComponent
                ? new DragonClassifier((DragonClassifierComponent) classifierComponent)
                : new EnsembleDragonClassifier((EnsembleDragonClassifierComponent) classifierComponent);
        classifier.fit(trainDataSet);

        ClassifierEvaluationResult evaluationResult = getClassifierResult(testDataSet, classifier);
        System.out.println(evaluationResult);

        String report = "evaluation.type:\t" + "hold_out" + "\n" +
                "evaluation.numOfTrainData:\t" + numOfTrainData + "\n" +
                "evaluation.numOfTestData:\t" + numOfTestData + "\n" +
                "evaluation.classifierName:\t" + classifier.getClass().getSimpleName() + "\n" +
                "evaluation.classifierComponent:\t" + classifierComponent.getAsJson() + "\n" +
                "result.truePos:\t" + evaluationResult.getTruePos() + "\n" +
                "result.trueNeg:\t" + evaluationResult.getTrueNeg() + "\n" +
                "result.falsePos:\t" + evaluationResult.getFalsePos() + "\n" +
                "result.falseNeg:\t" + evaluationResult.getFalseNeg() + "\n" +
                "result.accuracy:\t" + evaluationResult.getAccuracy() + "\n" +
                "result.precision:\t" + evaluationResult.getPrecision() + "\n" +
                "result.recall:\t" + evaluationResult.getRecall() + "\n" +
                "result.fMeasure:\t" + evaluationResult.getFMeasure() + "\n";
        if (classifier instanceof DragonClassifier) {
            System.out.println(((DragonClassifier) classifier).getDragonTree());
            FileUtils.writeStringToFile(new File(modelOutputUri),
                    ((DragonClassifier)classifier).getAsJson().toString(2), false);
            report += "result.model:\t" + ((DragonClassifier)classifier).getDragonTree().getAsJson();
        } else {
            FileUtils.writeStringToFile(new File(modelOutputUri),
                    ((EnsembleDragonClassifier)classifier).getAsJson().toString(2), false);
            report += "result.model.size:\t" + ((EnsembleDragonClassifier)classifier).getDagonClassifiers().size();
        }
        return report;
    }

    public static String evaluateKFold(ExecutionEnvironment env, ClassifierComponent classifierComponent,
            DataSet<CommonInstance> input, int k) throws Exception {
        ClassBalancer classBalancer = classifierComponent.getClassBalancer();
        DataSet<CommonInstance> balancedInstances = classBalancer.resample(input);
        List<TrainTestDataSet> trainTestDataSets = TrainTestSplitter.kFoldSplit(balancedInstances, k);

        List<ClassifierEvaluationResult> evaluationResults = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append("evaluation.type:\t").append("k_fold").append("\n");
        sb.append("evaluation.k:\t").append(k).append("\n");
        sb.append("evaluation.classifierComponent:\t").append(classifierComponent.getAsJson()).append("\n");
        for (int i=0; i < trainTestDataSets.size(); i++) {
            System.out.println("+++++++++++++++++++++++++++++ " + (i+1) + "/" + k + " +++++++++++++++++++++++++++++");
            DataSet<CommonInstance> trainDataSet = trainTestDataSets.get(i).getTrainingDataSet();
            DataSet<CommonInstance> testDataSet = trainTestDataSets.get(i).getTestingDataSet();
            long numOfTrainData = trainDataSet.count();
            long numOfTestData = testDataSet.count();
            Classifier classifier = classifierComponent instanceof DragonClassifierComponent
                    ? new DragonClassifier((DragonClassifierComponent) classifierComponent)
                    : new EnsembleDragonClassifier((EnsembleDragonClassifierComponent) classifierComponent);
            classifier.fit(trainDataSet);
            ClassifierEvaluationResult evaluationResult = getClassifierResult(testDataSet, classifier);
            evaluationResults.add(evaluationResult);
            if (classifier instanceof DragonClassifier)  System.out.println(((DragonClassifier)classifier).getDragonTree());

            System.out.println(evaluationResult);
            System.out.println("+++++++++++++++++++++++++++++ " + (i+1) + "/" + k + " +++++++++++++++++++++++++++++\n");
            sb.append("+++++++++++++++++++++++++++++ ").append(i + 1).append("/").append(k)
                    .append(" +++++++++++++++++++++++++++++").append("\n");
            sb.append("evaluation.currentRound:\t").append(i+1).append("\n");
            sb.append("evaluation.numOfTrainData:\t").append(numOfTrainData).append("\n");
            sb.append("evaluation.numOfTestData:\t").append(numOfTestData).append("\n");
            sb.append("result.truePos:\t").append(evaluationResult.getTruePos()).append("\n");
            sb.append("result.trueNeg:\t").append(evaluationResult.getTrueNeg()).append("\n");
            sb.append("result.falsePos:\t").append(evaluationResult.getFalsePos()).append("\n");
            sb.append("result.falseNeg:\t").append(evaluationResult.getFalseNeg()).append("\n");
            sb.append("result.accuracy:\t").append(evaluationResult.getAccuracy()).append("\n");
            sb.append("result.precision:\t").append(evaluationResult.getPrecision()).append("\n");
            sb.append("result.recall:\t").append(evaluationResult.getRecall()).append("\n");
            sb.append("result.fMeasure:\t").append(evaluationResult.getFMeasure()).append("\n");
            sb.append("+++++++++++++++++++++++++++++ ").append(i + 1).append("/").append(k)
                    .append(" +++++++++++++++++++++++++++++").append("\n");
        }
        double fMeasureAvg = evaluationResults.stream()
                .mapToDouble(ClassifierEvaluationResult::getFMeasure).average().orElse(0);
        double fMeasureMin = evaluationResults.stream()
                .mapToDouble(ClassifierEvaluationResult::getFMeasure).min().orElse(0);
        double fMeasureMax = evaluationResults.stream()
                .mapToDouble(ClassifierEvaluationResult::getFMeasure).max().orElse(0);
        sb.append("result.avg.fMeasure:\t").append(fMeasureAvg).append("\n");
        sb.append("result.min.fMeasure:\t").append(fMeasureMin).append("\n");
        sb.append("result.max.fMeasure:\t").append(fMeasureMax);
        return sb.toString();
    }

    public static String evaluateTrainingPipeline(ExecutionEnvironment env, URI inputGraphsCsvUri, URI groundTruthUri,
            List<SimilarityComponent> trainingSimilarityComponents, DragonClassifierComponent classifierComponent) throws Exception {

        GradoopFlinkConfig gradoopFlinkConfig = GradoopFlinkConfig.createConfig(env);
        CSVDataSource inputGraphsSource = new CSVDataSource(inputGraphsCsvUri.toString(), gradoopFlinkConfig);
        GraphCollection inputGraphs = inputGraphsSource.getGraphCollection();
        LogicalGraph goldGraph = GoldGraphCreator.getGoldGraph(inputGraphs, groundTruthUri, gradoopFlinkConfig);
        DataSet<BinaryInstance> trainingInstances = TrainDataCreator
                .getTrainingInstances(goldGraph, trainingSimilarityComponents);
        ClassBalancer classBalancer = classifierComponent.getClassBalancer();
        DataSet<CommonInstance> balancedInstances = classBalancer.resample(trainingInstances.map(m -> m));
        DragonClassifier dragonClassifier = new DragonClassifier(classifierComponent);
        dragonClassifier.fit(balancedInstances);
        System.out.println("tree: " + dragonClassifier.getDragonTree());
        return "result.model:\t" + dragonClassifier.getAsJson();
    }

    private static ClassifierEvaluationResult getClassifierResult(DataSet<CommonInstance> testDataSet,
            Classifier classifier) throws Exception {
        long tp = 0;
        long fp = 0;
        long tn = 0;
        long fn = 0;
        List<Tuple2<Boolean, Boolean>> results = testDataSet.map(new ClassifierEvaluator(classifier)).collect();
        for (Tuple2<Boolean, Boolean> result : results) {
            if (result.f0 && result.f1) tp++;
            else if (result.f0) fn++;
            else if (result.f1) fp++;
            else tn++;
        }
        return new ClassifierEvaluationResult(tp, tn, fp, fn);
    }

    private static class ClassifierEvaluator implements MapFunction<CommonInstance, Tuple2<Boolean, Boolean>> {
        private final Classifier classifier;

        public ClassifierEvaluator(Classifier classifier) {
            this.classifier = classifier;
        }

        @Override
        public Tuple2<Boolean, Boolean> map(CommonInstance instance) throws Exception {
            return Tuple2.of(instance.getLabel() == TRUE, classifier.predict(instance).getLabel() == TRUE);
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            System.out.println("Config file missed.");
            System.exit(1);
        }

//      ###########################  LOAD EVAL CONFIG ###########################
        JSONObject jsonConfig = new JSONObject(FileUtils.readFileToString(new File(args[0])));
        EvaluationComponent evaluationComponent = EvaluationComponent.valueOf(jsonConfig.getString("evaluationComponent"));
        URI groundTruthUri = URI.create(jsonConfig.getString("groundTruthUri"));
        URI inputGraphsCsvUri = URI.create(jsonConfig.getString("inputGraphsCsvUri"));
        URI goldGraphCsvUri = URI.create(jsonConfig.getString("goldGraphCsvUri"));
        URI inputDataSetUri = URI.create(jsonConfig.getString("inputDataSetUri"));
        URI modelUri = URI.create(jsonConfig.getString("modelUri"));
        ClassifierEvaluationMethod classifierEvaluationMethod = ClassifierEvaluationMethod.valueOf(jsonConfig.getString("classifierEvaluationMethod"));
        int numOfFolds = jsonConfig.getInt("numOfFolds");
        double trainTestSplitFraction = jsonConfig.getDouble("trainTestSplitFraction");

        boolean classBalanceWithReplacement = jsonConfig.getBoolean("classBalanceWithReplacement");
        ClassBalanceMethod classBalancerMethod = ClassBalanceMethod.valueOf(jsonConfig.getString("classBalancerMethod"));
        ClassBalancer classBalancer = new ClassBalancer(classBalancerMethod, classBalanceWithReplacement);

        double lowerThresholdBound = jsonConfig.getDouble("lowerThresholdBound");
        double lowerThresholdRate = jsonConfig.getDouble("lowerThresholdRate");
        int leafMinSamples = jsonConfig.getInt("leafMinSamples");

        GiniIndexFunction giniIndexFunction = new GiniIndexFunction(lowerThresholdBound, leafMinSamples);
        FMeasureFunction fMeasureFunction = new FMeasureFunction(lowerThresholdBound, lowerThresholdRate, leafMinSamples);
        FitnessFunctionType fitnessFunctionType = FitnessFunctionType.valueOf(jsonConfig.getString("fitnessFunctionType"));
        FitnessFunction fitnessFunction = fitnessFunctionType.equals(FitnessFunctionType.GINI_INDEX)
                ? giniIndexFunction : fMeasureFunction;

        double confidenceLevel = jsonConfig.getDouble("confidenceLevel");
        PruningMethod pruningMethod = PruningMethod.valueOf(jsonConfig.getString("pruningMethod"));
        PruningFunction treePruner = pruningMethod.equals(PruningMethod.ERROR_ESTIMATE)
                ? new ErrorEstimatePruner(confidenceLevel)
                : pruningMethod.equals(PruningMethod.F_MEASURE) ? new FMeasurePruner() : new NonePruner();

        int treeMaxHeight = jsonConfig.getInt("treeMaxHeight");
        JSONArray trainingSimilarityConfigs = jsonConfig.getJSONArray("trainingSimilarityConfigs");
        List<SimilarityComponent> trainingSimilarityComponents = new ArrayList<>();
        List<String> featureNames = new ArrayList<>();
        for (int i = 0; i < trainingSimilarityConfigs.length(); i++) {
            trainingSimilarityComponents.add(getSimilarityComponentFromJson(trainingSimilarityConfigs.getJSONObject(i)));
            featureNames.add(trainingSimilarityConfigs.getJSONObject(i).getString("id"));
        }
        int featureSize = trainingSimilarityComponents.size();

        URI aurocCsvUri = URI.create(jsonConfig.getString("aurocCsvUri"));

        int numBootstraps = jsonConfig.getInt("numBootstraps");
        double sampleFraction = jsonConfig.getDouble("sampleFraction");
        int bootstrapFeatureSize = jsonConfig.getInt("bootstrapFeatureSize");
        int scaleSampleFactor = jsonConfig.getInt("scaleSampleFactor");

        ClassifierComponentBaseConfig baseConfig = new ClassifierComponentBaseConfig(featureNames, CLASS_NAME,
                CLASS_VALUES, classBalancer);
        DragonClassifierComponent dragonComponent = new DragonClassifierComponent(baseConfig, fitnessFunction, treePruner,
                treeMaxHeight, trainingSimilarityConfigs);
        EnsembleDragonClassifierComponent ensembleComponent = new EnsembleDragonClassifierComponent(baseConfig,
                giniIndexFunction, fMeasureFunction,numBootstraps, sampleFraction, bootstrapFeatureSize,
                treeMaxHeight, trainingSimilarityConfigs);
        ClassifierComponent classifierComponent = evaluationComponent.equals(EvaluationComponent.DRAGON_TRAIN_PREDICT)
                ? dragonComponent : ensembleComponent;

        boolean flinkLocalEnvironment = jsonConfig.getBoolean("flinkLocalEnvironment");
        String flinkHome = jsonConfig.getString("flinkHome");
        String restHost;
        int restPort;
        int flinkParallelism;
        JSONArray taskNameHintConfigs = jsonConfig.getJSONArray("taskNameHints");
        List<String> taskNameHints = new ArrayList<>();
        for (int i = 0; i < taskNameHintConfigs.length(); i++) {
            taskNameHints.add(taskNameHintConfigs.getString(i));
        }
        JSONArray metricNameConfigs = jsonConfig.getJSONArray("metricNames");
        List<String> metricNames = new ArrayList<>();
        for (int i = 0; i < metricNameConfigs.length(); i++) {
            metricNames.add(metricNameConfigs.getString(i));
        }
        URI reportFileUri = URI.create(jsonConfig.getString("reportFileUri"));
//      ###########################  LOAD EVAL CONFIG ###########################

//      ###########################  FLINK ENV CONFIG ###########################
        ExecutionEnvironment env;
        if (flinkLocalEnvironment) {
            Configuration conf = new Configuration();
            conf.setBoolean(ConfigConstants.LOCAL_START_WEBSERVER, true);
            conf.setInteger(RestOptions.PORT, 8082);
            restHost = "localhost";
            restPort = 8082;
            flinkParallelism = 16;
//            conf.setString("metrics.reporters", "dragonReport");
//            conf.setString("metrics.reporter.dragonReport.configFileUri", URI.create("file://" + args[0]).toString());
//            conf.setString("metrics.reporter.dragonReport.class", DragonScheduleReporter.class.getName());
//            conf.setString("metrics.reporter.dragonReport.interval", "5 SECONDS");
//            conf.setString("metrics.scope.delimiter", "~");
//            conf.setString("metrics.scope.task", "<job_id>.<task_name>");
//            conf.setString("metrics.scope.operator", "<job_id>.<task_name>");
            env = ExecutionEnvironment.createLocalEnvironment(conf);
        } else {
            String master = Arrays.stream(new String(Files.readAllBytes(Paths.get(flinkHome + "/conf/masters")))
                    .split("\n")).findFirst().orElse("localhost:8082");
            String[] slaves = new String(Files.readAllBytes(Paths.get(flinkHome + "/conf/slaves"))).split("\n");
            restHost = master.split(":")[0] + ".sc.uni-leipzig.de";
            restPort = Integer.parseInt(master.split(":")[1]);
            flinkParallelism = slaves.length*24;
            System.out.println(restHost + " --- " + restPort + " --- " + flinkParallelism);
            env = ExecutionEnvironment.getExecutionEnvironment();
        }
        env.setParallelism(flinkParallelism);
//      ###########################  FLINK ENV CONFIG ###########################


//      ###########################  EVALUATION #################################
        long startTs = System.currentTimeMillis();
        String jobResultReport = "";
        String jobExecutionReport = "";
        switch (evaluationComponent) {
            case GOLD_GRAPH_CREATOR:
                jobResultReport = evaluateGoldGraphCreator(env, inputGraphsCsvUri, groundTruthUri, goldGraphCsvUri);
                break;
            case TRAIN_DATA_CREATOR:
                jobResultReport = evaluateTrainDataCreator(env, goldGraphCsvUri, trainingSimilarityComponents,
                        inputDataSetUri, scaleSampleFactor);
                break;
            case DRAGON_TRAIN_PREDICT:  //DRAGON_TRAIN_PREDICT || ENSEMBLE_TRAIN_PREDICT
            case ENSEMBLE_TRAIN_PREDICT:
                DataSet<CommonInstance> inputDataSet = LibsvmUtils.readFromLibsvm(env, inputDataSetUri, featureSize);
                if (classifierEvaluationMethod.equals(ClassifierEvaluationMethod.HOLD_OUT)) {
                    jobResultReport = evaluateHoldOut(env, classifierComponent, inputDataSet, trainTestSplitFraction, modelUri);
                } else {
                    jobResultReport = evaluateKFold(env, classifierComponent, inputDataSet, numOfFolds);
                }
                break;
            case DRAGON_TRAINING_PIPELINE:
                jobResultReport = evaluateTrainingPipeline(env, inputGraphsCsvUri, groundTruthUri,
                        trainingSimilarityComponents, dragonComponent);
                break;
            case AUROC:
                if (trainingSimilarityComponents.size() == 0) {
                    System.out.println("No Training SimilarityComponent found.");
                    return;
                }
                String sourceGraph = trainingSimilarityComponents.get(0).getSourceGraph();
                String targetGraph = trainingSimilarityComponents.get(0).getTargetGraph();
                EnrichedDragonComponent enrichedDragonComponent = new EnrichedDragonComponent(
                        new SimilarityComponentBaseConfig("Dragon", sourceGraph, "*","*",
                                targetGraph, "*", "*", 1.0),
                        classBalancerMethod, fitnessFunctionType, lowerThresholdBound, lowerThresholdRate,
                        pruningMethod, treeMaxHeight, leafMinSamples,
                        trainingSimilarityComponents
                );
                evaluateAUROC(env, goldGraphCsvUri, enrichedDragonComponent, trainTestSplitFraction, aurocCsvUri);
                break;
            default: break;
        }
        long endTs = System.currentTimeMillis();
        jobExecutionReport = DragonRESTReport.restAPIReport(restHost, restPort, startTs, endTs, taskNameHints, metricNames);
        if (!jobExecutionReport.contains("pipeline.jobDurationSum")) {
            jobExecutionReport = "pipeline.jobDurationSum:\t" + (endTs -startTs);
        }
        jobExecutionReport += "\npipeline.jobRawParallelism:\t" + flinkParallelism;
        jobExecutionReport += "\npipeline.jobRawDurationSum:\t" + (endTs -startTs);
        String report = buildReport(jobResultReport, jobExecutionReport);
        System.out.println(report);
        FileUtils.writeStringToFile(new File(reportFileUri), report, true);
//      ###########################  EVALUATION #################################
    }


    private static String buildReport(String jobResultReport, String jobExecutionReport) {
        StringBuilder sb = new StringBuilder();
        sb.append("\n=========================== Starting metrics report ===========================\n");
        sb.append(jobResultReport).append("\n");
        sb.append(jobExecutionReport).append("\n");
        sb.append(new SimpleDateFormat("'report.datetime:\t'dd.MM.yyyy hh:mm:ss'\n'").format(new Date()));
        sb.append("=========================== Finished metrics report ===========================").append("\n");
        return sb.toString();
    }

    /**
     * Evaluate the results of the classification.
     */
    private static class ClassifierEvaluationResult implements Serializable {

        /**
         * The number of the true positives.
         */
        private final long truePos;

        /**
         * The number of the true negatives.
         */
        private final long trueNeg;

        /**
         * The number of the false positives.
         */
        private final long falsePos;

        /**
         * THe number of the false negatives.
         */
        private final long falseNeg;

        /**
         * Create a new {@link ClassifierEvaluationResult} for the given parameter.
         *
         * @param truePos Number of the true positives.
         * @param trueNeg Number of the true negatives.
         * @param falsePos Number of the false positives.
         * @param falseNeg Number of the false negatives.
         */
        public ClassifierEvaluationResult(long truePos, long trueNeg, long falsePos, long falseNeg) {
            this.truePos = truePos;
            this.trueNeg = trueNeg;
            this.falsePos = falsePos;
            this.falseNeg = falseNeg;
        }

        /**
         * Calculates the accuracy of the classification.
         *
         * @return The accuracy between 0 and 1.
         */
        public double getAccuracy() {
            return (double) (truePos + trueNeg) / (truePos + falsePos + trueNeg + falseNeg);
        }

        /**
         * Calculates the precision of the classification.
         *
         * @return The precision between 0 and 1.
         */
        public double getPrecision() {
            double precision = (double) truePos / (truePos + falsePos);
            return Double.isNaN(precision) ? 0.0 : precision;
        }

        /**
         * Calculates the recall of the classification.
         *
         * @return The recall between 0 and 1.
         */
        public double getRecall() {
            double recall = (double) truePos / (truePos + falseNeg);
            return Double.isNaN(recall) ? 0.0 : recall;
        }

        /**
         * Calculates F-measure
         *
         * @return The F-measure between 0 and 1.
         */
        public double getFMeasure() {
            double fMeasure = 2.0d * getPrecision() * getRecall() / (getPrecision() + getRecall());
            return Double.isNaN(fMeasure) ? 0.0d : fMeasure;
        }

        /**
         * Getter for truePos.
         *
         * @return The number of true positives.
         */
        public long getTruePos() {
            return truePos;
        }

        /**
         * Getter for trueNeg.
         *
         * @return The number of true negatives.
         */
        public long getTrueNeg() {
            return trueNeg;
        }

        /**
         * Getter for falsePos.
         *
         * @return The number of false positives.
         */
        public long getFalsePos() {
            return falsePos;
        }

        /**
         * Getter for falseNeg.
         *
         * @return The number of false negatives.
         */
        public long getFalseNeg() {
            return falseNeg;
        }

        /**
         * toString method for accuracy, precision and recall.
         *
         * @return A string with the values of accuracy, precision and recall.s
         */
        @Override
        public String toString() {
            return "ClassifierEvaluationResult{" +
                    "truePos=" + truePos +
                    ", trueNeg=" + trueNeg +
                    ", falsePos=" + falsePos +
                    ", falseNeg=" + falseNeg +
                    ", accuracy=" + getAccuracy() +
                    ", precision=" + getPrecision() +
                    ", recall=" + getRecall() +
                    ", FMeasure=" + getFMeasure() +
                    '}';
        }
    }
}
