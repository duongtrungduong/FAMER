/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.standardBlocking.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.java.tuple.Tuple6;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Joins a {@code Tuple3<Vertex, BlockingKey, Index>} and a
 * {@code Tuple5<BlockingKey, BlockSize, Index, PrevBlocksPairs, AllPairs>} on the BlockingKey and returns a
 * {@code Tuple6<Vertex, BlockingKey, Index, BlockSize, PrevBlocksPairs, AllPairs>}
 */
@FunctionAnnotation.ForwardedFieldsFirst("f0->f0;f1->f1;f2->f2")
@FunctionAnnotation.ForwardedFieldsSecond("f1->f3;f3->f4;f4->f5")
public class JoinOnBlockingKeyAndAttachBlockInformation implements
  JoinFunction<Tuple3<EPGMVertex, String, Long>, Tuple5<String, Long, Long, Long, Long>,
  Tuple6<EPGMVertex, String, Long, Long, Long, Long>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple6<EPGMVertex, String, Long, Long, Long, Long> reuseTuple = new Tuple6<>();

  @Override
  public Tuple6<EPGMVertex, String, Long, Long, Long, Long> join(
    Tuple3<EPGMVertex, String, Long> vertexBlockingKeyIndex,
    Tuple5<String, Long, Long, Long, Long> blockingKeyBlockSizeIndexPrevPairsAllPairs) throws Exception {
    reuseTuple.f0 = vertexBlockingKeyIndex.f0;
    reuseTuple.f1 = vertexBlockingKeyIndex.f1;
    reuseTuple.f2 = vertexBlockingKeyIndex.f2;
    reuseTuple.f3 = blockingKeyBlockSizeIndexPrevPairsAllPairs.f1;
    reuseTuple.f4 = blockingKeyBlockSizeIndexPrevPairsAllPairs.f3;
    reuseTuple.f5 = blockingKeyBlockSizeIndexPrevPairsAllPairs.f4;
    return reuseTuple;
  }
}
