/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.dataStructures;

import org.gradoop.famer.linking.blocking.keyGeneration.BlockingKeyGenerator;

import java.util.Map;
import java.util.Set;

/**
 * The base configuration class for a blocking component. Wraps commonly used objects for all blocking
 * components.
 */
public class BlockingComponentBaseConfig {

  /**
   * Generates the blocking keys for each entity for the blocking method
   */
  private final BlockingKeyGenerator blockingKeyGenerator;

  /**
   * Mapping of the allowed graph pairs specified by user in configuration file
   */
  private Map<String, Set<String>> graphPairs;

  /**
   * Mapping of the allowed category pairs specified by user in configuration file
   */
  private Map<String, Set<String>> categoryPairs;

  /**
   * Creates an instance of BlockingComponentBaseConfig
   *
   * @param blockingKeyGenerator Generates the blocking keys for each entity for the blocking method
   * @param graphPairs Mapping of the allowed graph pairs specified by user in configuration file
   * @param categoryPairs Mapping of the allowed category pairs specified by user in configuration file
   */
  public BlockingComponentBaseConfig(BlockingKeyGenerator blockingKeyGenerator,
    Map<String, Set<String>> graphPairs, Map<String, Set<String>> categoryPairs) {
    this.blockingKeyGenerator = blockingKeyGenerator;
    this.graphPairs = graphPairs;
    this.categoryPairs = categoryPairs;
  }

  /**
   * Returns the blocking key generator
   *
   * @return The blocking key generator
   */
  public BlockingKeyGenerator getBlockingKeyGenerator() {
    return blockingKeyGenerator;
  }

  /**
   * Returns the mapping of the allowed graph pairs specified by user in configuration file
   *
   * @return The mapping of the allowed graph pairs
   */
  public Map<String, Set<String>> getGraphPairs() {
    return graphPairs;
  }

  /**
   * Sets the mapping of the allowed graph pairs specified by user in configuration file
   *
   * @param graphPairs The mapping of the allowed graph pairs
   */
  public void setGraphPairs(Map<String, Set<String>> graphPairs) {
    this.graphPairs = graphPairs;
  }

  /**
   * Returns the mapping of the allowed category pairs specified by user in configuration file
   *
   * @return The mapping of the allowed category pairs
   */
  public Map<String, Set<String>> getCategoryPairs() {
    return categoryPairs;
  }

  /**
   * Sets the mapping of the allowed category pairs specified by user in configuration file
   *
   * @param categoryPairs The mapping of the allowed category pairs
   */
  public void setCategoryPairs(Map<String, Set<String>> categoryPairs) {
    this.categoryPairs = categoryPairs;
  }
}
