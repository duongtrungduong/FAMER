/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.standardBlocking.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;

/**
 * Reduces a sorted group of {@code Tuple3<BlockingKey, PartitionId, Count>}, grouped on BlockingKey and
 * sorted on PartitionId ascending. Computes the start point for each BlockingKey, where for each key the
 * lowest PartitionId has the start point 0 and for the next ones the start points are incremented by Count.
 * Returns the result as {@code Tuple3<BlockingKey, PartitionId, StartPoint>}
 */
@FunctionAnnotation.ForwardedFields("f0;f1")
public class PartitionEnumerationStartPointComputer implements
  GroupReduceFunction<Tuple3<String, Integer, Long>, Tuple3<String, Integer, Long>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<String, Integer, Long> reuseTuple = new Tuple3<>();

  @Override
  public void reduce(Iterable<Tuple3<String, Integer, Long>> sortedGroup,
    Collector<Tuple3<String, Integer, Long>> out) {
    long totalSum = 0L;
    for (Tuple3<String, Integer, Long> groupItem : sortedGroup) {
      reuseTuple.f0 = groupItem.f0;
      reuseTuple.f1 = groupItem.f1;
      reuseTuple.f2 = totalSum;
      out.collect(reuseTuple);
      totalSum += groupItem.f2;
    }
  }
}
