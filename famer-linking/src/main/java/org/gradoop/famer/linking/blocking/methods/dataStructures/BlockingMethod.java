/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.dataStructures;

/**
 * Available methods to generate the entity blocking
 */
public enum BlockingMethod {
  /**
   * Computes the cartesian product
   */
  CARTESIAN_PRODUCT,
  /**
   * PairRange algorithm for Standard Blocking
   * {@see "https://dbs.uni-leipzig.de/file/ICDE12_conf_full_088.pdf"}
   */
  STANDARD_BLOCKING,
  /**
   * RepSN algorithm for Sorted Neighborhood Blocking
   * {@see "https://dbs.uni-leipzig.de/file/parallel_sn_blocking_mr.pdf"}
   */
  SORTED_NEIGHBORHOOD
}
