package org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.functions.RichMapPartitionFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.util.Collector;
import org.apache.zookeeper.Environment;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.linking.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.famer.linking.linking.functions.Project3To1And2;
import org.gradoop.famer.linking.similarityMeasuring.SimilarityMeasurer;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityField;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityFieldList;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import static org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions.GoldGraphCreator.*;

/**
 * Creates training data set of type {@link BinaryInstance} from a given gold graph and a given setting
 */
public class TrainDataCreator {
    /**
     * Name to access the broadcast set of gold edges
     */
    private static final String GOLD_EDGES_BROADCAST_NAME = "GOLD_EDGES";

    /**
     * Name to access the broadcast set of gold vertex pairs
     */
    private static final String GOLD_VERTICES_BROADCAST_NAME = "GOLD_VERTICES";

    /**
     * @param goldGraph            gold graph
     * @param similarityComponents similarity components for generating training binary instance
     * @return dataset of type {@link BinaryInstance} for training and testing
     */
    public static DataSet<BinaryInstance> getTrainingInstances(LogicalGraph goldGraph,
            List<SimilarityComponent> similarityComponents) throws Exception {
        DataSet<Tuple2<EPGMVertex, EPGMVertex>> goldVertexPairs = getPositiveAndNegativeVertexPairs(goldGraph);
        return goldVertexPairs.flatMap(new SimilarityMeasurer(similarityComponents))
                .map(new TrainingInstanceFactory(similarityComponents))
                .withBroadcastSet(goldGraph.getEdges(), GOLD_EDGES_BROADCAST_NAME);
    }

    public static DataSet<Tuple2<EPGMVertex, EPGMVertex>>  getPositiveAndNegativeVertexPairs(LogicalGraph goldGraph) {
        boolean hasNegativeExamples = checkForNegativeExamples(goldGraph);
        DataSet<EPGMVertex> goldVertices = goldGraph.getVerticesByLabel(GOLD_GRAPH_VERTEX_LABEL);
        DataSet<EPGMEdge> goldEdges = goldGraph.getEdgesByLabel(GOLD_GRAPH_EDGE_LABEL);

        DataSet<Tuple2<EPGMVertex, EPGMVertex>> goldVertexPairs = EdgeToEdgeSourceVertexTargetVertex
                .execute(goldVertices, goldEdges).map(new Project3To1And2<>());

        if (!hasNegativeExamples) {
            DataSet<Tuple2<EPGMVertex, EPGMVertex>> negativeExamples = goldVertexPairs
                    .mapPartition(new NegativeExamplesGenerator())
                    .withBroadcastSet(goldVertexPairs, GOLD_VERTICES_BROADCAST_NAME);
            goldVertexPairs = goldVertexPairs.union(negativeExamples);
        }
        return goldVertexPairs;
    }

    /**
     * If the gold graph does not contain negative examples then generate them.
     * Ratio between number of positive and number of negative examples is 1
     */
    private static class NegativeExamplesGenerator extends RichMapPartitionFunction<Tuple2<EPGMVertex, EPGMVertex>,
            Tuple2<EPGMVertex, EPGMVertex>> {
        /**
         * List of all target vertices in gold graph
         */
        private List<EPGMVertex> targetVertices;

        /**
         * List of all gold vertex paris in gold graph
         */
        private Set<Tuple2<EPGMVertex, EPGMVertex>> goldVertexPairs;

        @Override
        public void open(Configuration parameters) {
            goldVertexPairs = new HashSet<>(getRuntimeContext().getBroadcastVariable(GOLD_VERTICES_BROADCAST_NAME));
            targetVertices = goldVertexPairs.stream().map(m -> m.f1).collect(Collectors.toList());
        }

        @Override
        public void mapPartition(Iterable<Tuple2<EPGMVertex, EPGMVertex>> sourceTargetVertexPairs,
                Collector<Tuple2<EPGMVertex, EPGMVertex>> collector) throws Exception {
            if (targetVertices.size() <= 1) return;
            for (Tuple2<EPGMVertex, EPGMVertex> sourceTargetVertexPair : sourceTargetVertexPairs) {
                EPGMVertex sourceVertex = sourceTargetVertexPair.f0;
                for (; ; ) {
                    int randomIndex = ThreadLocalRandom.current().nextInt(targetVertices.size());
                    EPGMVertex targetVertex = targetVertices.get(randomIndex);
                    Tuple2<EPGMVertex, EPGMVertex> negativeExample = Tuple2.of(sourceVertex, targetVertex);
                    if (!goldVertexPairs.contains(negativeExample)) {
                        collector.collect(negativeExample);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Checks whether the given graph has edges with the property {@code isMatch} set to {@code false}
     *
     * @param goldGraph The graph to check
     * @return {@code true}, if edges with property {@code isMatch} set to {@code false} are present
     */
    private static boolean checkForNegativeExamples(LogicalGraph goldGraph) {
        try {
            return goldGraph.getEdges().filter(f -> !f.getPropertyValue(IS_MATCH_PROPERTY_KEY).getBoolean())
                    .count() > 0L;
        } catch (Exception ex) {
            throw new RuntimeException("Could not check whether gold graph contains negative examples", ex);
        }
    }

    /**
     * Map the calculated similarity values between two vertices to {@link BinaryInstance}
     */
    private static class TrainingInstanceFactory extends RichMapFunction<Tuple3<EPGMVertex, EPGMVertex,
            SimilarityFieldList>, BinaryInstance> {
        /**
         * Map of vertex id pair from gold edges and its label
         */
        private Map<Tuple2<GradoopId, GradoopId>, Boolean> vertexIdPairsAndLabel;

        /**
         * Map of training similarity component and its index
         */
        private final Map<String, Integer> similarityIdAndIndex;

        /**
         * Reused object
         */
        BinaryInstance binaryInstance;

        /**
         * Reused object
         */
        Vector<Double> features;

        private TrainingInstanceFactory(List<SimilarityComponent> trainingSimilarityComponents) {
            this.binaryInstance = new BinaryInstance();
            this.binaryInstance.setProbability(0);
            this.features = new Vector<>();
            this.features.setSize(trainingSimilarityComponents.size());
            this.similarityIdAndIndex = new HashMap<>();
            for (int i = 0; i < trainingSimilarityComponents.size(); i++) {
                SimilarityComponent component = trainingSimilarityComponents.get(i);
                similarityIdAndIndex.put(component.getComponentId(), i);
            }
        }

        @Override
        public void open(Configuration parameters) {
            List<EPGMEdge> goldEdges = getRuntimeContext().getBroadcastVariable(GOLD_EDGES_BROADCAST_NAME);
            vertexIdPairsAndLabel = goldEdges.stream()
                    .collect(Collectors.toMap(m -> Tuple2.of(m.getSourceId(), m.getTargetId()),
                            m -> m.getPropertyValue(IS_MATCH_PROPERTY_KEY).getBoolean()));
        }

        @Override
        public BinaryInstance map(Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList> input) throws Exception {
            boolean label = vertexIdPairsAndLabel.getOrDefault(Tuple2.of(input.f0.getId(), input.f1.getId()), false);
            for (int i = 0; i < features.size(); i++) {
                features.set(i, 0d);
            }
            for (SimilarityField similarityField : input.f2.getSimilarityFields()) {
                int featureIndex = similarityIdAndIndex.get(similarityField.getId());
                double featureValue = similarityField.getSimilarityValue();
                features.set(featureIndex, featureValue);
            }

            binaryInstance.setFeatures(features);
            binaryInstance.setLabel(label ? BinaryInstance.TRUE : BinaryInstance.FALSE);
            return binaryInstance;
        }
    }
}
