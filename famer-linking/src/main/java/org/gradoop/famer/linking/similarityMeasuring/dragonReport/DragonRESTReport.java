package org.gradoop.famer.linking.similarityMeasuring.dragonReport;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.hadoop.shaded.org.apache.http.client.utils.URIBuilder;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class DragonRESTReport {

    public static String restAPIReport(String host, int port, long startTs, long endTs,
            List<String> taskNameHints, List<String> metricNames) throws Exception {
        JSONObject response = restAPIGetAsObject(host, port, "/jobs/");
        if (!response.has("jobs")) return "Could not get job execution values using REST API";
        JSONArray jobs = response.getJSONArray("jobs");
        List<String> pipelineJobIds = new ArrayList<>();
        List<Long> pipelineJobDurations = new ArrayList<>();
        List<Integer> pipelineJobParallelisms = new ArrayList<>();
        Map<Tuple2<String, String>, List<Double>> pipelineMetricValues = new HashMap<>();
        for (int i = 0; i < jobs.length(); i++) {
            String jobId = jobs.getJSONObject(i).getString("id");
            JSONObject jobInfo = restAPIGetAsObject(host, port, "/jobs/" + jobId);
            long jobStartTime = jobInfo.optLong("start-time", Long.MIN_VALUE);
            long jobEndTime = jobInfo.optLong("end-time", Long.MAX_VALUE);
            long jobDuration = jobInfo.optLong("duration", 0);
            JSONArray jobVertices = jobInfo.getJSONArray("vertices");
            if ((startTs <= jobStartTime) && (jobEndTime <= endTs) && isWantedJob(jobId, jobVertices, taskNameHints)) {
                pipelineJobIds.add(jobId);
                pipelineJobDurations.add(jobDuration);
                getVertexParallelisms(jobVertices, pipelineJobParallelisms);
                getVertexMetrics(jobId, jobVertices, pipelineMetricValues, metricNames, host, port);
            }
        }
        return buildReport(pipelineMetricValues, pipelineJobIds, pipelineJobDurations,
                pipelineJobParallelisms);
    }

    private static String buildReport(Map<Tuple2<String, String>, List<Double>> pipelineMetricValues,
            List<String> pipelineJobIds, List<Long> pipelineJobDurations, List<Integer> pipelineJobParallelisms) {
        StringBuilder sb = new StringBuilder();
        pipelineMetricValues.entrySet().stream()
                .sorted(Comparator.comparing(c -> c.getKey().f0))
                .forEach(f -> {
                    double reportValue;
                    switch (f.getKey().f1) {
                        case "min":
                            reportValue = f.getValue().stream().mapToDouble(m -> m).min().orElse(0d);
                            break;
                        case "max":
                            reportValue = f.getValue().stream().mapToDouble(m -> m).max().orElse(0d);
                            break;
                        case "avg":
                            reportValue = f.getValue().stream().mapToDouble(m -> m).average().orElse(0d);
                            break;
                        default:
                            reportValue = 0d;
                    }
                    sb.append("pipeline.").append(f.getKey().f0).append(".").append(f.getKey().f1).append(":\t")
                            .append(reportValue).append("\n");
                });
        long duration = pipelineJobDurations.stream().mapToLong(m -> m).sum();
        int parallelism = pipelineJobParallelisms.stream().mapToInt(m -> m).max().orElse(0);
        sb.append("pipeline.jobIds").append(":\t").append(String.join(",", pipelineJobIds)).append("\n");
        sb.append("pipeline.jobParallelism").append(":\t").append(parallelism).append("\n");
        sb.append("pipeline.jobDurationSum").append(":\t").append(duration);
        return sb.toString();
    }

    private static void getVertexParallelisms(JSONArray jobVertices, List<Integer> pipelineJobParallelisms)
            throws JSONException {
        for (int i = 0; i < jobVertices.length(); i++) {
            int parallelism = jobVertices.getJSONObject(i).optInt("parallelism", 0);
            if (parallelism > 0) {
                pipelineJobParallelisms.add(parallelism);
            }
        }
    }

    private static void getVertexMetrics(String jobId, JSONArray jobVertices,
            Map<Tuple2<String, String>, List<Double>> pipelineMetricValues,
            List<String> metricNames, String host, int port) throws JSONException {
        for (int i = 0; i < jobVertices.length(); i++) {
            for (String metricName : metricNames) {
                String vertexId = jobVertices.getJSONObject(i).getString("id");
                String metricPath = "/jobs/" + jobId + "/vertices/" + vertexId + "/subtasks/metrics";
                String metricQuery = "get=" + metricName;
                JSONArray metricValues = restAPIGetAsArray(host, port, metricPath, metricQuery);
                for (int j = 0; j < metricValues.length(); j++) {
                    double metricMin = metricValues.getJSONObject(j).optDouble("min", 0);
                    double metricMax = metricValues.getJSONObject(j).optDouble("max", 0);
                    double metricAvg = metricValues.getJSONObject(j).optDouble("avg", 0);
                    if (metricMin > 0) {
                        pipelineMetricValues.computeIfAbsent(Tuple2.of(metricName, "min"),
                                v -> new ArrayList<>()).add(metricMin);
                    }
                    if (metricMax > 0) {
                        pipelineMetricValues.computeIfAbsent(Tuple2.of(metricName, "max"),
                                v -> new ArrayList<>()).add(metricMax);
                    }
                    if (metricAvg > 0) {
                        pipelineMetricValues.computeIfAbsent(Tuple2.of(metricName, "avg"),
                                v -> new ArrayList<>()).add(metricAvg);
                    }
                }
            }
        }
    }

    private static boolean isWantedJob(String jobId, JSONArray jobVertices, List<String> taskNameHints)
            throws JSONException {
        for (int i = 0; i < jobVertices.length(); i++) {
            String taskName = jobVertices.getJSONObject(i).getString("name");
            if (taskNameHints.stream().anyMatch(taskName::contains)) return true;
        }
        return false;
    }

    private static String restAPIGet(String host, int port, String path, String query) {
        String response = null;
        try {
            URIBuilder builder = new URIBuilder();
            builder.setScheme("http");
            builder.setHost(host);
            builder.setPort(port);
            builder.setPath(path);
            builder.setCustomQuery(query);
            URL url = builder.build().toURL();
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            int responseCode = conn.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("HttpResponseCode: " + responseCode);
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                response = br.lines().collect(Collectors.joining());
                conn.disconnect();
            }
        } catch (Exception ignored) {
        }
        return response;
    }

    private static JSONObject restAPIGetAsObject(String host, int port, String path) throws JSONException {
        return restAPIGetAsObject(host, port, path, null);
    }

    private static JSONObject restAPIGetAsObject(String host, int port, String path, String query)
            throws JSONException {
        String response = restAPIGet(host, port, path, query);
        return response == null ? new JSONObject() : new JSONObject(response);
    }

    private static JSONArray restAPIGetAsArray(String host, int port, String path) throws JSONException {
        return restAPIGetAsArray(host, port, path, null);
    }

    private static JSONArray restAPIGetAsArray(String host, int port, String path, String query) throws JSONException {
        String response = restAPIGet(host, port, path, query);
        return response == null ? new JSONArray() : new JSONArray(response);
    }
}
