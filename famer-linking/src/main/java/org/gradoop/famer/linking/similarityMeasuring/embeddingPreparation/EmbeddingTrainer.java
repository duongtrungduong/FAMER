/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.embeddingPreparation;

import com.github.jfasttext.JFastText;
import org.apache.flink.util.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.gradoop.famer.linking.similarityMeasuring.embeddingPreparation.dataStructures.EnrichedEmbeddingComponent;
import org.gradoop.famer.linking.similarityMeasuring.embeddingPreparation.functions.CollectPropertiesForCorpus;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Contains the methods related to FastText to either train a new model, or use pre-trained model, or use
 * word vector. Uses the JFastText Maven dependency for https://github.com/vinhkhuc/JFastText
 */
public class EmbeddingTrainer {

  /**
   * File pattern to save a model
   */
  public static final String MODEL_TMP_FILE_PATTERN = "/tmp/%s-model-%s";
  /**
   * File pattern to save a corpus
   */
  public static final String CORPUS_TMP_FILE_PATTERN = "/tmp/%s-corpus-%s.txt";
  /**
   * Word separator for the corpus
   */
  public static final String CORPUS_TMP_FILE_WORD_SEPARATOR = " ";
  /**
   * URI scheme for HDFS
   */
  public static final String HDFS_URI_SCHEME = "hdfs";

  /**
   * The JFastText object
   */
  private static JFastText J_FAST_TEXT;

  /**
   * Runs the fast text algorithm
   *
   * @param embeddingComponent The enriched embedding component
   * @param graph Logical graph
   *
   * @throws Exception if the word-vector file or the temporary corpus can not be written or the word-vector
   *                    file can not be put in HDFS
   */
  public static void runFastText(EnrichedEmbeddingComponent embeddingComponent, LogicalGraph graph)
    throws Exception {
    String wordVectorsFile = "";

    if (embeddingComponent.isUseWordVectorsEnabled()) {
      embeddingComponent.setWordVectorsFileUri(embeddingComponent.getInputFileUri());
      return;
    }

    if (embeddingComponent.isUsePretrainedModelEnabled()) {
      wordVectorsFile = usePreTrainedModel(embeddingComponent, graph);
    } else if (embeddingComponent.isTrainModelEnabled()) {
      String corpusTmpFilePath = writeCorpusToTmpFile(embeddingComponent, graph);
      wordVectorsFile = trainFastTextModel(embeddingComponent, corpusTmpFilePath);
    }
    embeddingComponent.setWordVectorsFileUri(URI.create(wordVectorsFile));

    if (embeddingComponent.getOutputFileUri().getScheme().equals(HDFS_URI_SCHEME)) {
      putToHDFS(wordVectorsFile, embeddingComponent.getOutputFileUri());
    }
  }

  /**
   * Writes the fast text corpus to a temporary file
   *
   * @param embeddingComponent the enriched embedding component
   * @param graph a logical graph
   *
   * @return path to the temporary corpus
   *
   * @throws Exception if the temporary corpus can not be written
   */
  private static String writeCorpusToTmpFile(EnrichedEmbeddingComponent embeddingComponent,
    LogicalGraph graph) throws Exception {
    //words that are written in temporary file, are words from the vertices of the prepared graph
    List<String> wordList = graph.getVertices()
      .flatMap(new CollectPropertiesForCorpus(embeddingComponent)).collect();

    final String tmpCorpusFilePath =
      String.format(CORPUS_TMP_FILE_PATTERN, embeddingComponent.getComponentId(),
        Long.toString(System.currentTimeMillis() / 1000L));

    try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(tmpCorpusFilePath))) {
      for (String s : wordList) {
        writer.write(s + CORPUS_TMP_FILE_WORD_SEPARATOR);
      }
    }

    return tmpCorpusFilePath;
  }

  /**
   * Starts the training and creates the word-vector file
   *
   * @param c the enriched embedding component
   * @param corpusFilePath path to the temporary corpus
   *
   * @return path to the word-vector file after training
   */
  private static String trainFastTextModel(EnrichedEmbeddingComponent c, String corpusFilePath) {
    String outputPath = c.getOutputFileUri().getRawPath();
    String trainingMode = c.isUseCBOWTraining() ? "cbow" : c.isUseSkipgramTraining() ? "skipgram" : "";
    if (c.getOutputFileUri().getScheme().equals(HDFS_URI_SCHEME)) {
      outputPath =
        String.format(MODEL_TMP_FILE_PATTERN, c.getComponentId(),
          Long.toString(System.currentTimeMillis() / 1000L));
    }

    getJftInstance().runCmd(new String[] {
      trainingMode, "-input", corpusFilePath, "-output", outputPath, "-bucket",
      Integer.toString(c.getTrainingModelBucket()), "-dim", Integer.toString(c.getTrainingModelDimension()),
      "-minCount", Integer.toString(c.getTrainingModelMinWordCount())
    });

    return outputPath + ".vec";
  }

  /**
   * Uses a pretrained model to create the word-vector file
   *
   * @param embeddingComponent the enriched embedding component
   * @param graph a logical graph
   *
   * @return path to the word-vector file after parsing vector of each word in temporary corpus from model
   *        file
   *
   * @throws Exception if the word-vector file can not be written
   */
  private static String usePreTrainedModel(EnrichedEmbeddingComponent embeddingComponent,
    LogicalGraph graph) throws Exception {
    String outputPath = embeddingComponent.getOutputFileUri().getRawPath();

    if (embeddingComponent.getOutputFileUri().getScheme().equals(HDFS_URI_SCHEME)) {
      outputPath =
        String.format(MODEL_TMP_FILE_PATTERN, embeddingComponent.getComponentId(),
          Long.toString(System.currentTimeMillis() / 1000L));
    }

    getJftInstance().loadModel(embeddingComponent.getInputFileUri().getRawPath());
    Set<String> wordList = new HashSet<>();

    // prepare vertices for writing
    graph.getVertices().flatMap(new CollectPropertiesForCorpus(embeddingComponent))
      .distinct()
      .collect()
      .forEach(words -> wordList.addAll(Arrays.asList(words.split(CORPUS_TMP_FILE_WORD_SEPARATOR))));

    // get vector of word from FastText model file and write to word-vector file
    try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputPath))) {
      writer.write(wordList.size() + CORPUS_TMP_FILE_WORD_SEPARATOR + getJftInstance().getDim() + "\n");
      for (String s : wordList) {
        List<Float> vector = getJftInstance().getVector(s);
        writer.write(s + " ");
        for (Float f : vector) {
          writer.write(f.toString() + " ");
        }
        writer.write("\n");
      }
    } finally {
      getJftInstance().unloadModel();
    }

    return outputPath;
  }

  /**
   * Writes a file to HDFS
   *
   * @param fileToPut path of the file that is put in HDFS
   * @param destination HDFS URI where the file is saved
   *
   * @throws IOException if the file can not be put in HDFS
   */
  private static void putToHDFS(String fileToPut, URI destination) throws IOException {
    Configuration conf = new Configuration();
    conf.set("fs.defaultFS", destination.getRawPath());

    FileSystem fileSystem = FileSystem.get(destination, conf);
    Path destinationPath = new Path(new File(destination.getRawPath()).getParent());
    if (!fileSystem.exists(destinationPath)) {
      fileSystem.mkdirs(destinationPath);
    }
    FSDataOutputStream fsOutputStream = fileSystem.create(new Path(destination.getRawPath()));
    InputStream inputStream = new BufferedInputStream(new FileInputStream(new File(fileToPut)));
    IOUtils.copyBytes(inputStream, fsOutputStream);
    fsOutputStream.close();
    inputStream.close();
  }

  /**
   * Creates a new or returns the current instance of JFastText
   *
   * @return the instance of JFastText
   */
  private static JFastText getJftInstance() {
    if (null == J_FAST_TEXT) {
      J_FAST_TEXT = new JFastText();
    }
    return J_FAST_TEXT;
  }
}
