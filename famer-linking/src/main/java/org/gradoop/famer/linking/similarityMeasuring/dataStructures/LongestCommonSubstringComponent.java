/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.similarityMeasuring.methods.LongestCommonSubstring;
import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;

/**
 * Similarity component which uses {@link LongestCommonSubstring}.
 */
public class LongestCommonSubstringComponent extends SimilarityComponent {

  /**
   * The second method for computing similarity
   */
  private LongestCommonSubstring.SecondMethod secondMethod;

  /**
   * Creates an instance of LongestCommonSubstringComponent from the given json object.
   *
   * @param config The json object that contains the configuration.
   */
  public LongestCommonSubstringComponent(JSONObject config) {
    super(config);
    try {
      this.secondMethod =
        LongestCommonSubstring.SecondMethod.valueOf(config.getString("secondMethod"));
    } catch (Exception ex) {
      throw new UnsupportedOperationException("LongestCommonSubstringComponent: value for second method " +
        "could not be found or parsed.", ex);
    }
  }

  /**
   * Creates an instance of LongestCommonSubstringComponent
   *
   * @param baseConfig The base configuration for the similarity component
   * @param secondMethod The second method for computing similarity
   */
  public LongestCommonSubstringComponent(SimilarityComponentBaseConfig baseConfig,
    LongestCommonSubstring.SecondMethod secondMethod) {
    super(baseConfig);
    this.secondMethod = secondMethod;
  }

  @Override
  public SimilarityComputation<String> buildSimilarityComputation() {
    return new LongestCommonSubstring(secondMethod);
  }

  public LongestCommonSubstring.SecondMethod getSecondMethod() {
    return secondMethod;
  }
}
