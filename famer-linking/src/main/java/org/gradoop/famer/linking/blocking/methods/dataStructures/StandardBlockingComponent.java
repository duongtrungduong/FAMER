/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.blocking.methods.BlockingExecutor;
import org.gradoop.famer.linking.blocking.methods.standardBlocking.StandardBlocking;

/**
 * Blocking component which uses {@link StandardBlocking}
 */
public class StandardBlockingComponent extends BlockingComponent {

  /**
   * The maximum desired system cores that will be used for load balancing
   */
  private final int parallelismDegree;

  /**
   * The strategy of treating entities with empty keys
   */
  private final StandardBlockingEmptyKeyStrategy emptyKeyStrategy;

  /**
   * Creates an instance of BlockingComponent
   *
   * @param baseConfig The base configuration for the blocking component
   * @param parallelismDegree The maximum desired system cores that will be used for load balancing
   */
  public StandardBlockingComponent(BlockingComponentBaseConfig baseConfig, int parallelismDegree) {
    this(baseConfig, parallelismDegree, StandardBlockingEmptyKeyStrategy.EMPTY_BLOCK);
  }

  /**
   * Creates an instance of BlockingComponent
   *
   * @param baseConfig The base configuration for the blocking component
   * @param parallelismDegree The maximum desired system cores that will be used for load balancing
   * @param emptyKeyStrategy The strategy of treating entities with empty keys
   */
  public StandardBlockingComponent(BlockingComponentBaseConfig baseConfig, int parallelismDegree,
    StandardBlockingEmptyKeyStrategy emptyKeyStrategy) {
    super(baseConfig);
    this.parallelismDegree = parallelismDegree;
    this.emptyKeyStrategy = emptyKeyStrategy;
  }

  /**
   * Constructor used for json parsing
   *
   * @param baseConfig The base configuration for the blocking component
   * @param jsonConfig The blocking component json config part
   */
  public StandardBlockingComponent(BlockingComponentBaseConfig baseConfig, JSONObject jsonConfig) {
    super(baseConfig);
    try {
      this.parallelismDegree = jsonConfig.getInt("parallelismDegree");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("StandardBlockingComponent: value for parallelismDegree " +
        "could not be found or parsed to int", ex);
    }
    String emptyKeyStrategyElem = jsonConfig.optString("emptyKeyStrategy");
    if (emptyKeyStrategyElem.equals("")) {
      this.emptyKeyStrategy = StandardBlockingEmptyKeyStrategy.EMPTY_BLOCK;
    } else {
      try {
        this.emptyKeyStrategy = StandardBlockingEmptyKeyStrategy.valueOf(emptyKeyStrategyElem);
      } catch (Exception ex) {
        throw new UnsupportedOperationException("StandardBlockingComponent: could not parse value for " +
          "emptyKeyStrategy", ex);
      }
    }
  }

  /**
   * Returns the maximum desired system cores that will be used for load balancing
   *
   * @return The maximum desired system cores that will be used for load balancing
   */
  public int getParallelismDegree() {
    return parallelismDegree;
  }

  /**
   * Returns the strategy of treating entities with empty keys
   *
   * @return The empty key strategy
   */
  public StandardBlockingEmptyKeyStrategy getEmptyKeyStrategy() {
    return emptyKeyStrategy;
  }

  @Override
  public BlockingExecutor buildBlockingExecutor() {
    return new StandardBlocking(this);
  }
}
