/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.apache.commons.lang3.math.NumberUtils;
import org.gradoop.common.model.impl.properties.PropertyValue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Computes the similarity of two strings using the Embedding method. Words and their corresponding vectors
 * of a word-vector file are stored in a HashMap
 */
public class Embedding implements SimilarityComputation<String> {

  /**
   * Separator used in the word embedding file
   */
  private static final String EMBEDDING_FILE_SEPARATOR = "\\s+";

  /**
   * Mapping of the embeddings for a word vector file
   */
  private static Map<File, Map<String, Float[]>> EMBEDDINGS;

  /**
   * Mapping of the dimensions of the word vector file
   */
  private static Map<File, Integer> DIMENSIONS;

  /**
   * The word vector file
   */
  private File wordVectorsFile;

  /**
   * Creates an instance of Embedding
   *
   * @param wordVectorsFile The word vector file
   */
  public Embedding(File wordVectorsFile) {
    this.wordVectorsFile = wordVectorsFile;
    fillEmbeddings();
  }

  /**
   * Reads the file given in {@link #wordVectorsFile} and fills the embeddings and dimensions structure
   */
  private void fillEmbeddings() {
    if (null == EMBEDDINGS) {
      EMBEDDINGS = new HashMap<>();
    }
    if (null == DIMENSIONS) {
      DIMENSIONS = new HashMap<>();
    }
    // Map each word-vector file to its dimension and word-vector values
    if (EMBEDDINGS.get(wordVectorsFile) == null) {
      try {
        // Store dimension, expected as second field in first line
        String firstLine = Files.lines(wordVectorsFile.toPath()).findFirst().get();
        int dimension = Integer.parseInt(firstLine.split(EMBEDDING_FILE_SEPARATOR)[1]);
        DIMENSIONS.put(wordVectorsFile, dimension);

        // Store word vectors in HashMap
        HashMap<String, Float[]> wordVectorsOfFile = new HashMap<>();
        Files.lines(wordVectorsFile.toPath()).skip(1).forEach(line -> {
          String[] lineElements = line.split(EMBEDDING_FILE_SEPARATOR);
          String word = lineElements[0].toLowerCase();

          Float[] vector = Arrays.stream(lineElements).skip(1)
            .filter(NumberUtils::isNumber)
            .map(Float::parseFloat)
            .toArray(Float[]::new);

          wordVectorsOfFile.put(word, vector);
        });
        EMBEDDINGS.put(wordVectorsFile, wordVectorsOfFile);
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }
  }

  @Override
  public double computeSimilarity(String sentence1, String sentence2) throws Exception {
    if (sentence1 == null) {
      throw new NullPointerException("sentence1 must not be null");
    }
    if (sentence2 == null) {
      throw new NullPointerException("sentence2 must not be null");
    }

    Map<String, Float[]> embeddingsForFile = EMBEDDINGS.get(wordVectorsFile);
    int dimensionForFile = DIMENSIONS.get(wordVectorsFile);

    Float[] embeddingSentence1 =
      computeUnsupervisedSentenceVector(sentence1, embeddingsForFile, dimensionForFile);
    Float[] embeddingSentence2 =
      computeUnsupervisedSentenceVector(sentence2, embeddingsForFile, dimensionForFile);

    return (embeddingSentence1 == null || embeddingSentence2 == null) ? 0f :
      computeCosineSimilarity(embeddingSentence1, embeddingSentence2);
  }

  @Override
  public String parsePropertyValue(PropertyValue value) {
    return getString(value);
  }

  /**
   * Splits a sentence into words and calculates a vector representing the whole sentence via normed
   * average of single word vectors
   *
   * @param sentence   a string
   * @param embeddings the word vectors
   * @param dimension  dimension of vectors
   *
   * @return the vector representing the whole sentence
   */
  private Float[] computeUnsupervisedSentenceVector(String sentence, Map<String, Float[]> embeddings,
    int dimension) {
    // Get vectors for all words in sentence
    List<Float[]> embeddingWords = new ArrayList<>();
    Pattern.compile(EMBEDDING_FILE_SEPARATOR).splitAsStream(sentence)
      .map(w -> w.toLowerCase().split(EMBEDDING_FILE_SEPARATOR))
      .forEach(e -> {
        if (embeddings.containsKey(e[0].toLowerCase())) {
          embeddingWords.add(embeddings.get(e[0].toLowerCase()));
        }
      });

    // Calculate the vector of sentence using vectors from its words
    Float[] embeddingSentenceVector = new Float[dimension];
    Arrays.fill(embeddingSentenceVector, 0f);
    for (Float[] embeddingWord : embeddingWords) {
      if (embeddingWord == null || embeddingWord.length != dimension) {
        return null;
      }
      float norm = (float) computeVectorNorm(embeddingWord);
      for (int i = 0; i < embeddingWord.length; i++) {
        embeddingSentenceVector[i] += embeddingWord[i] / norm;
      }
    }
    embeddingSentenceVector = Arrays.stream(embeddingSentenceVector)
      .map(v -> v / embeddingWords.size())
      .toArray(Float[]::new);

    return embeddingSentenceVector;
  }

  /**
   * Computes the norm of a vector
   *
   * @param vector a vector
   *
   * @return the norm of this vector
   */
  private double computeVectorNorm(Float[] vector) {
    double sum = 0;
    for (Float aFloat : vector) {
      sum += aFloat * aFloat;
    }
    return Math.sqrt(sum);
  }

  /**
   * Computes the cosine similarity of two vectors
   *
   * @param vector1 the first vector
   * @param vector2 the second vector
   *
   * @return the cosine similarity of the two vectors
   */
  private double computeCosineSimilarity(Float[] vector1, Float[] vector2) {
    double ab = 0;
    for (int i = 0; i < vector1.length; i++) {
      ab += vector1[i] * vector2[i];
    }
    double norm1 = computeVectorNorm(vector1);
    double norm2 = computeVectorNorm(vector2);

    return ab / (norm1 * norm2);
  }
}
