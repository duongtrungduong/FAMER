package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.trainingModule.classifier.methods.DragonClassifier;
import org.gradoop.famer.linking.similarityMeasuring.methods.Dragon;
import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions.JsonComponentHelper.getSimilarityComponentFromJson;

public class DragonComponent extends SimilarityComponent {
    /**
     * The classifier using dragon-decision-tree
     */
    private DragonClassifier dragonClassifier;

    /**
     * Similarity components that are used for predicting. These should be a subset of trainingSimilarityComponents.
     */
    private List<SimilarityComponent> similarityComponents;

    /**
     * Creates an instance of DragonComponent from the given json object.
     *
     * @param config The json object that contains the configuration.
     */
    public DragonComponent(JSONObject config) {
        super(config);
    }

    /**
     * Creates an instance of DragonComponent.
     *
     * @param baseConfig The base configuration for the similarity component
     */
    public DragonComponent(SimilarityComponentBaseConfig baseConfig) {
        super(baseConfig);
    }

    public DragonClassifier getDragonClassifier() {
        return dragonClassifier;
    }

    public void setDragonClassifier(DragonClassifier dragonClassifier) {
        this.dragonClassifier = dragonClassifier;
        JSONArray trainingConfigs = dragonClassifier.getClassifierComponent().getTrainingSimilarityConfigs();
        similarityComponents = new ArrayList<>();
        try {
            Set<Integer> simIdsFromTree = dragonClassifier.getDragonTree().getAsList().stream()
                    .filter(f -> f.isValid() && !f.isLeaf())
                    .map(m -> m.getSplit().getFeatureIndex())
                    .collect(Collectors.toCollection(HashSet::new));
            for (int i = 0; i < trainingConfigs.length(); i++) {
                SimilarityComponent component = getSimilarityComponentFromJson(trainingConfigs.getJSONObject(i));
                component.setSourceGraph(getSourceGraph());
                component.setTargetGraph(getTargetGraph());
                component.setSourceLabel(getSourceLabel());
                component.setTargetLabel(getTargetLabel());
                if (simIdsFromTree.contains(i)) {
                    similarityComponents.add(component);
                }
            }
        } catch (JSONException | ClassNotFoundException ex) {
            throw new UnsupportedOperationException("DragonComponent: similarity components" +
                    " could not be parsed from Json.", ex);
        }
    }

    @Override
    public SimilarityComputation<List<PropertyValue>> buildSimilarityComputation() throws Exception {
        return new Dragon(dragonClassifier);
    }

    public void setSimilarityComponents(List<SimilarityComponent> similarityComponents) {
        this.similarityComponents = similarityComponents;
    }

    public List<SimilarityComponent> getSimilarityComponents() {
        return similarityComponents;
    }
}
