/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.sortedNeighborhood.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Takes the sorted group of {@code Tuple3<Vertex, BlockingKey, Position>}, sorted by BlockingKey, and
 * replaces the Position with the actual Position in the sorted group, starting by {@code 1L}.
 */
@FunctionAnnotation.ForwardedFields("f0;f1")
public class ReplacePositionWithBlockingKeyOrderPosition implements
  GroupReduceFunction<Tuple3<EPGMVertex, String, String>, Tuple3<EPGMVertex, String, Long>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<EPGMVertex, String, Long> reuseTuple = new Tuple3<>();

  @Override
  public void reduce(Iterable<Tuple3<EPGMVertex, String, String>> group,
    Collector<Tuple3<EPGMVertex, String, Long>> out) throws Exception {
    long count = 1L;
    for (Tuple3<EPGMVertex, String, String> groupItem : group) {
      reuseTuple.f0 = groupItem.f0;
      reuseTuple.f1 = groupItem.f1;
      reuseTuple.f2 = count;
      out.collect(reuseTuple);
      count++;
    }
  }
}
