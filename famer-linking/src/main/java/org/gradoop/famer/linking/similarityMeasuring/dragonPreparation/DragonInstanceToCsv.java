package org.gradoop.famer.linking.similarityMeasuring.dragonPreparation;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple5;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonInstance;

public class DragonInstanceToCsv implements MapFunction<DragonInstance, Tuple5<String, String, Integer, Double, Boolean>> {
    private final Tuple5<String, String, Integer, Double, Boolean> reuseTuple = new Tuple5<>();

    @Override
    public Tuple5<String, String, Integer, Double, Boolean> map(DragonInstance dragonInstance) throws Exception {
        reuseTuple.setFields(dragonInstance.getGroupId() + "", '"' + dragonInstance.getFeatureName() + '"',
                dragonInstance.getFeatureIndex(), dragonInstance.getFeatureValue(), dragonInstance.getLabel());
        return reuseTuple;
    }
}
