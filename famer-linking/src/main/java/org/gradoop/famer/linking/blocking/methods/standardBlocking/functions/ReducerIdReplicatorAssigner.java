/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.standardBlocking.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Takes a {@code Tuple6<Vertex, BlockingKey, Index, BlockSize, PrevBlocksPairs, AllPairs>}. Assigns each
 * vertex-BlockingKey to a reducerId based on the PairRange method.
 * {@see "https://dbs.uni-leipzig.de/file/ICDE12_conf_full_088.pdf,  Section V."}
 */
@FunctionAnnotation.ForwardedFields("f0;f1;f2")
public class ReducerIdReplicatorAssigner implements
  FlatMapFunction<Tuple6<EPGMVertex, String, Long, Long, Long, Long>, Tuple5<EPGMVertex, String, Long,
    Boolean, Integer>> {

  /**
   * The number of reducers (usually parallelism degree)
   */
  private final int reducerNo;

  /**
   * Reduce object instantiation
   */
  private final Tuple5<EPGMVertex, String, Long, Boolean, Integer> reuseTuple;

  /**
   * Creates an instance of ReducerIdReplicatorAssigner
   *
   * @param reducerNo The number of reduces
   */
  public ReducerIdReplicatorAssigner(int reducerNo) {
    this.reducerNo = reducerNo;
    this.reuseTuple = new Tuple5<>();
  }

  @Override
  public void flatMap(Tuple6<EPGMVertex, String, Long, Long, Long, Long> in,
    Collector<Tuple5<EPGMVertex, String, Long, Boolean, Integer>> out) {
    // in is of type Tuple6<Vertex, BlockingKey, Index, BlockSize, PrevBlocksPairs, AllPairs>

    // stop when there is no pair in the block
    if (in.f3 <= 1) {
      return;
    }

    /* PairRange: p(x,y) = ((x/2)*(2*blockSize-x-3))+y-1+(overall number of pairs in all preceding blocks)
       - let N be the size of entity e’s block, entity e with index x takes part in the pairs:
         (0, x), ... ,(x−1, x),(x, x+1), ... ,(x, N −1)
    */


    long yMin;
    long xMax;
    long yMax;

    yMin = in.f2; // Index
    xMax = in.f2; // Index
    yMax = in.f3 - 1L; // BlockSize-1

    if (yMin == 0L) {
      yMin = 1L;
    }

    if (xMax == yMax) {
      xMax--;
    }

    // pMin, pMax: entity e’s pairs with the smallest and highest pair index
    long pMin;
    long pMax;

    pMin = yMin - 1 + in.f4;
    pMax = (xMax * (2 * in.f3 - xMax - 3)) / 2 + yMax - 1 + in.f4;


    int minReducerId = Math.toIntExact(reducerNo * pMin / in.f5);
    int maxReducerId = Math.toIntExact(reducerNo * pMax / in.f5);

    // replicated entity has the "false" attached
    if (minReducerId != maxReducerId) {
      for (int i = minReducerId; i < maxReducerId; i++) {
        reuseTuple.f0 = in.f0;
        reuseTuple.f1 = in.f1;
        reuseTuple.f2 = in.f2;
        reuseTuple.f3 = false;
        reuseTuple.f4 = i;
        out.collect(reuseTuple);
      }
    }
    reuseTuple.f0 = in.f0;
    reuseTuple.f1 = in.f1;
    reuseTuple.f2 = in.f2;
    reuseTuple.f3 = true;
    reuseTuple.f4 = maxReducerId;
    out.collect(reuseTuple);
  }
}
