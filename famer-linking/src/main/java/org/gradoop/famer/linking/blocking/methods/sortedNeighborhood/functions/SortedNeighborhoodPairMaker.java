/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.sortedNeighborhood.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.methods.dataStructures.SortedNeighborhoodComponent;

import java.util.ArrayList;
import java.util.List;

/**
 * Creates the tuples of paired vertices
 */
public class SortedNeighborhoodPairMaker implements
  GroupReduceFunction<Tuple4<EPGMVertex, String, Integer, Boolean>, Tuple2<EPGMVertex, EPGMVertex>> {

  /**
   * Structure with required parameters and helper methods
   */
  private final SortedNeighborhoodComponent sortedNeighborhoodComponent;

  /**
   * Reduce object instantiation
   */
  private final Tuple2<EPGMVertex, EPGMVertex> reuseTuple;

  /**
   * Creates an instance of SortedNeighborhoodPairMaker
   *
   * @param sortedNeighborhoodComponent Structure with required parameters and helper methods
   */
  public SortedNeighborhoodPairMaker(SortedNeighborhoodComponent sortedNeighborhoodComponent) {
    this.sortedNeighborhoodComponent = sortedNeighborhoodComponent;
    this.reuseTuple = new Tuple2<>();
  }

  @Override
  public void reduce(Iterable<Tuple4<EPGMVertex, String, Integer, Boolean>> group,
    Collector<Tuple2<EPGMVertex, EPGMVertex>> collector) throws Exception {
    List<Tuple2<EPGMVertex, Boolean>> items = new ArrayList<>();
    for (Tuple4<EPGMVertex, String, Integer, Boolean> item : group) {
      items.add(Tuple2.of(item.f0, item.f3));
    }

    int windowSize = sortedNeighborhoodComponent.getWindowSize();
    int itemsSize = items.size();
    // in order to prevent exception when windowSize > itemSize
    int upperBound = Math.min(windowSize, itemsSize);

    // first window
    for (int i = 0; i < upperBound - 1; i++) {
      EPGMVertex v1 = items.get(i).f0;
      for (int j = i + 1; j < upperBound; j++) {
        if (items.get(j).f1) {
          EPGMVertex v2 = items.get(j).f0;
          if (sortedNeighborhoodComponent.isAllowedPair(v1, v2)) {
            reuseTuple.f0 = v1;
            reuseTuple.f1 = v2;
            collector.collect(reuseTuple);
          }
        }
      }
    }
    // next windows
    for (int j = windowSize; j < itemsSize; j++) {
      if (items.get(j).f1) {
        EPGMVertex v2 = items.get(j).f0;
        for (int i = j - windowSize + 1; i < j; i++) {
          EPGMVertex v1 = items.get(i).f0;
          if (sortedNeighborhoodComponent.isAllowedPair(v1, v2)) {
            reuseTuple.f0 = v1;
            reuseTuple.f1 = v2;
            collector.collect(reuseTuple);
          }
        }
      }
    }
  }
}

