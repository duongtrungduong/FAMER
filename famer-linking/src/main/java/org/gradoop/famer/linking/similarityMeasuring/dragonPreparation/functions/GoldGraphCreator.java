package org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions;

import com.google.common.base.Preconditions;
import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.operators.base.JoinOperatorBase;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.*;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.flink.io.api.DataSource;
import org.gradoop.flink.io.impl.csv.CSVDataSource;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.util.GradoopFlinkConfig;

import java.io.IOException;
import java.net.URI;
import java.util.Collections;

public class GoldGraphCreator {
    /**
     * Graph head label for the gold graph
     */
    public static final String GOLD_GRAPH_HEAD_LABEL = "GoldGraphHeadLabel";

    /**
     * Graph head label for the gold graph
     */
    public static final String GOLD_GRAPH_VERTEX_LABEL = "GoldGraphVertexLabel";

    /**
     * Graph edge label for the gold graph
     */
    public static final String GOLD_GRAPH_EDGE_LABEL = "GoldGraphEdgeLabel";

    /**
     * Default lineage property key of the input graphs
     */
    public static final String LINEAGE_PROPERTY_KEY = "id";

    /**
     * Property determines if a gold edge is a match
     */
    public static final String IS_MATCH_PROPERTY_KEY = "isMatch";

    /**
     * Default splitter of ground truth csv
     */
    public static final String GROUND_TRUTH_SPLITTER = ",";

    /**
     * Default quote symbol of ground truth csv
     */
    public static final Character GROUND_TRUTH_QUOTE = '"';

    /**
     * Creates a gold graph from the given parameters.
     *
     * @param sourceGraph first input graph
     * @param targetGraph second input graph
     * @param groundTruthUri URI to ground truth csv
     */
    public static LogicalGraph getGoldGraph(LogicalGraph sourceGraph, LogicalGraph targetGraph, URI groundTruthUri,
            GradoopFlinkConfig config) {
        GraphCollection inputGraphs = config.getGraphCollectionFactory().fromGraph(sourceGraph);
        inputGraphs = inputGraphs.union(config.getGraphCollectionFactory().fromGraph(targetGraph));
        return getGoldGraph(inputGraphs, groundTruthUri, config);
    }

    /**
     * Creates a gold graph from the given parameters.
     *
     * @param inputGraphs graph collection that contains both input graphs
     * @param groundTruthUri URI to ground truth csv
     * @param config config of gradoop flink to generate gold graph
     */
    public static LogicalGraph getGoldGraph(GraphCollection inputGraphs, URI groundTruthUri, GradoopFlinkConfig config) {
        DataSet<Tuple2<String, String>> perfectMapping = config.getExecutionEnvironment()
                .readCsvFile(groundTruthUri.toString())
                .ignoreFirstLine()
                .parseQuotedStrings(GROUND_TRUTH_QUOTE)
                .fieldDelimiter(GROUND_TRUTH_SPLITTER)
                .types(String.class, String.class);
        return getGoldGraph(inputGraphs, perfectMapping, config);
    }

    /**
     * Creates a gold graph from the given parameters.
     *
     * @param inputGraphs graph collection that contains both input graphs
     * @param perfectMapping ground truth dataset
     * @param config config of gradoop flink to generate gold graph
     */
    public static LogicalGraph getGoldGraph(GraphCollection inputGraphs, DataSet<Tuple2<String, String>> perfectMapping,
            GradoopFlinkConfig config) {
        return getGoldGraph(inputGraphs.getVertices(), perfectMapping, config);
    }

    /**
     * Creates a gold graph from the given parameters.
     *
     * @param sourceGraph first input graph
     * @param targetGraph second input graph
     * @param perfectMapping ground truth dataset
     */
    public static LogicalGraph getGoldGraph(LogicalGraph sourceGraph, LogicalGraph targetGraph,
            DataSet<Tuple2<String, String>> perfectMapping, GradoopFlinkConfig config) {
        DataSet<EPGMVertex> inputVertices = sourceGraph.getVertices().union(targetGraph.getVertices());
        return getGoldGraph(inputVertices, perfectMapping, config);
    }

    /**
     * Creates a gold graph from the given parameters.
     *
     * @param inputVertices input vertices
     * @param perfectMapping ground truth dataset
     */
    public static LogicalGraph getGoldGraph(DataSet<EPGMVertex> inputVertices,
            DataSet<Tuple2<String, String>> perfectMapping, GradoopFlinkConfig gradoopFlinkConfig) {
        checkInputVertices(inputVertices);

        EPGMGraphHead goldHead = new EPGMGraphHeadFactory().createGraphHead(GOLD_GRAPH_HEAD_LABEL);

        DataSet<EPGMVertex> goldVertices = inputVertices
                .map(new AssignLabel<>(GOLD_GRAPH_VERTEX_LABEL))
                .map(new AssignGraphId<>(goldHead.getId()));

        DataSet<EPGMEdge> goldEdges = goldVertices
                .join(perfectMapping, JoinOperatorBase.JoinHint.BROADCAST_HASH_SECOND)
                .where(k -> k.getPropertyValue(LINEAGE_PROPERTY_KEY).toString()).equalTo(0)
                .with(new GetSourceGoldVertexAndTargetId())
                .join(goldVertices, JoinOperatorBase.JoinHint.BROADCAST_HASH_FIRST)
                .where(1).equalTo(k -> k.getPropertyValue(LINEAGE_PROPERTY_KEY).toString())
                .with(new GetSourceGoldVertexAndTargetGoldVertex())
                .map(new CreateGoldEdge(GOLD_GRAPH_EDGE_LABEL, IS_MATCH_PROPERTY_KEY))
                .map(new AssignGraphId<>(goldHead.getId()));

        return gradoopFlinkConfig.getLogicalGraphFactory()
                .fromDataSets(gradoopFlinkConfig.getExecutionEnvironment().fromElements(goldHead),
                        goldVertices, goldEdges);
    }

    /**
     * Check if the input graphs valid. The input graphs should not be null and should be contained property "id"
     *
     * @param vertexCollection input vertices
     */
    private static void checkInputVertices(DataSet<EPGMVertex> vertexCollection) {
        Preconditions.checkNotNull(vertexCollection);
        long invalidCount = 0;
        try {
            invalidCount = vertexCollection.filter(f -> !f.hasProperty(LINEAGE_PROPERTY_KEY)).count();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Preconditions.checkArgument(invalidCount <= 0,
                invalidCount + " vertices of input graph has no " + LINEAGE_PROPERTY_KEY + " property");
    }

    @FunctionAnnotation.ForwardedFieldsFirst("*->f0")
    @FunctionAnnotation.ForwardedFieldsSecond("f1->f1")
    private static class GetSourceGoldVertexAndTargetId implements
            JoinFunction<EPGMVertex, Tuple2<String, String>, Tuple2<EPGMVertex, String>> {
        /**
         * Reused object
         */
        private final Tuple2<EPGMVertex, String> reuseTuple;

        public GetSourceGoldVertexAndTargetId() {
            this.reuseTuple = new Tuple2<>();
        }

        @Override
        public Tuple2<EPGMVertex, String> join(EPGMVertex sourceVertex, Tuple2<String, String> perfectMapping)
                throws Exception {
            reuseTuple.f0 = sourceVertex;
            reuseTuple.f1 = perfectMapping.f1;
            return reuseTuple;
        }
    }

    @FunctionAnnotation.ForwardedFieldsFirst("f0->f0")
    @FunctionAnnotation.ForwardedFieldsSecond("*->f1")
    private static class GetSourceGoldVertexAndTargetGoldVertex implements
            JoinFunction<Tuple2<EPGMVertex, String>, EPGMVertex, Tuple2<EPGMVertex, EPGMVertex>> {
        /**
         * Reused object
         */
        private final Tuple2<EPGMVertex, EPGMVertex> reuseTuple;

        public GetSourceGoldVertexAndTargetGoldVertex() {
            this.reuseTuple = new Tuple2<>();
        }

        @Override
        public Tuple2<EPGMVertex, EPGMVertex> join(Tuple2<EPGMVertex, String> sourceVertexAndTargetId,
                EPGMVertex targetVertex) throws Exception {
            reuseTuple.f0 = sourceVertexAndTargetId.f0;
            reuseTuple.f1 = targetVertex;
            return reuseTuple;
        }
    }

    private static class CreateGoldEdge implements MapFunction<Tuple2<EPGMVertex, EPGMVertex>, EPGMEdge> {
        /**
         * Edge factory
         */
        private final EPGMEdgeFactory edgeFactory;

        /**
         * Label for gold edge
         */
        private final String goldEdgeLabel;

        /**
         * Reused object
         */
        private final Properties matchProperty;

        public CreateGoldEdge(String goldEdgeLabel, String isMatchPropertyKey) {
            this.edgeFactory = new EPGMEdgeFactory();
            this.goldEdgeLabel = goldEdgeLabel;
            this.matchProperty = Properties.createFromMap(Collections.singletonMap(isMatchPropertyKey, Boolean.TRUE));
        }

        @Override
        public EPGMEdge map(Tuple2<EPGMVertex, EPGMVertex> goldVertexPair) throws Exception {
            return edgeFactory.createEdge(goldEdgeLabel, goldVertexPair.f0.getId(), goldVertexPair.f1.getId(),
                    matchProperty);
        }
    }

    /**
     * For a graph element, resets all graph IDs and assigns the given ID as new graph ID.
     *
     * @param <E> GraphElement type
     */
    private static class AssignGraphId<E extends EPGMGraphElement> implements MapFunction<E, E> {
        /**
         * The graph id to assign
         */
        private final GradoopId graphID;

        /**
         * Creates an instance of AssignGraphID
         *
         * @param graphID The graph id to assign
         */
        public AssignGraphId(GradoopId graphID) {
            this.graphID = graphID;
        }

        @Override
        public E map(E graphElement) {
            graphElement.resetGraphIds();
            graphElement.addGraphId(graphID);
            return graphElement;
        }
    }

    /**
     * For a graph element, resets all graph IDs and assigns the given ID as new graph ID.
     *
     * @param <E> GraphElement type
     */
    private static class AssignLabel<E extends EPGMGraphElement> implements MapFunction<E, E> {
        /**
         * The label to assign
         */
        private final String label;

        /**
         * Creates an instance of AssignLabel
         *
         * @param label The label to assign
         */
        public AssignLabel(String label) {
            this.label = label;
        }

        @Override
        public E map(E graphElement) {
            graphElement.setLabel(label);
            return graphElement;
        }
    }
}
