/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring;

import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.DragonComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.EmbeddingComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityFieldList;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.gradoop.famer.linking.linking.Linker.GRAPH_LABEL;

/**
 * Creates all corresponding similarity values of paired vertices, stores them in a
 * {@link SimilarityFieldList} and returns a {@code Tuple3<Vertex1, Vertex2, SimilarityFieldList>}. If the
 * similarity component list in {@link #similarityComponents} contains no appropriate similarity component
 * for the vertex pair, then the flatMap function output returns nothing.
 */
public class SimilarityMeasurer extends
  RichFlatMapFunction<Tuple2<EPGMVertex, EPGMVertex>, Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList>> {

  /**
   * All similarity components of the matching process
   */
  private final List<SimilarityComponent> similarityComponents;

  /**
   * Reduce object instantiation
   */
  private final Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList> reuseTuple;

  /**
   * Creates an instance of SimilarityMeasurer
   *
   * @param similarityComponents All similarity components of the matching process
   */
  public SimilarityMeasurer(List<SimilarityComponent> similarityComponents) {
    this.similarityComponents = similarityComponents;
    this.reuseTuple = new Tuple3<>();
  }

  /**
   * Get word vector files registered in Flinks distributed cache
   *
   * @param parameters The configuration parameters
   */
  @Override
  public void open(Configuration parameters) throws Exception {
    similarityComponents.stream()
      .filter(c -> c instanceof EmbeddingComponent)
      .forEach(c -> {
        EmbeddingComponent component = (EmbeddingComponent) c;
        File vectorFile =
          getRuntimeContext().getDistributedCache().getFile(component.getDistributedCacheKey());
        component.setWordVectorsFile(vectorFile);
      });
  }

  @Override
  public void flatMap(Tuple2<EPGMVertex, EPGMVertex> input,
    Collector<Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList>> out) throws Exception {

    SimilarityFieldList similarityFieldList = new SimilarityFieldList();

    for (SimilarityComponent sc : similarityComponents) {
      Optional<Tuple2<PropertyValue, PropertyValue>> pair;
      if (sc instanceof DragonComponent) {
        pair = getValuePair(input, ((DragonComponent) sc).getSimilarityComponents());
      } else {
        pair = getValuePair(input, sc);
      }
      // compute similarities if the similarity component is related to the pair
      if (pair.isPresent()) {
        Tuple2<PropertyValue, PropertyValue> propValues = pair.get();
        similarityFieldList.add(new SimilarityComputer(sc).computeSimilarity(propValues.f0, propValues.f1));
      }
    }
    if (!similarityFieldList.isEmpty()) {
      reuseTuple.f0 = input.f0;
      reuseTuple.f1 = input.f1;
      reuseTuple.f2 = similarityFieldList;
      out.collect(reuseTuple);
    }
  }

  /**
   * Gets the property values of the input paired vertices, if the similarity component is related to the
   * vertex pair.
   *
   * @param input The vertex pair
   * @param sc The similarity component
   *
   * @return Returns null, if the similarity component is not related to the vertex pair
   */
  private Optional<Tuple2<PropertyValue, PropertyValue>> getValuePair(Tuple2<EPGMVertex, EPGMVertex> input,
    SimilarityComponent sc) {

    Tuple2<PropertyValue, PropertyValue> valuePair = null;
    if (isSourceTargetPair(input.f0, input.f1, sc)) {
      valuePair = new Tuple2<>(input.f0.getPropertyValue(sc.getSourceAttribute()),
        input.f1.getPropertyValue(sc.getTargetAttribute()));
    } else if (isSourceTargetPair(input.f1, input.f0, sc)) {
      valuePair = new Tuple2<>(input.f1.getPropertyValue(sc.getSourceAttribute()),
        input.f0.getPropertyValue(sc.getTargetAttribute()));
    }
    return Optional.ofNullable(valuePair);
  }

  /**
   * Gets the property values of the input paired vertices, if the similarity components is related to the
   * vertex pair.
   *
   * @param input The vertex pair
   * @param scs The similarity components
   *
   * @return Returns null, if the similarity component is not related to the vertex pair
   */
  private Optional<Tuple2<PropertyValue, PropertyValue>> getValuePair(Tuple2<EPGMVertex, EPGMVertex> input,
                                                                      List<SimilarityComponent> scs) {
    List<PropertyValue> sourceValues = new ArrayList<>();
    List<PropertyValue> targetValues = new ArrayList<>();
    for (SimilarityComponent sc : scs) {
      Optional<Tuple2<PropertyValue, PropertyValue>> pair = getValuePair(input, sc);
      if (pair.isPresent()) {
        sourceValues.add(pair.get().f0);
        targetValues.add(pair.get().f1);
      } else {
        sourceValues.add(PropertyValue.NULL_VALUE);
        targetValues.add(PropertyValue.NULL_VALUE);
      }
    }
    Tuple2<PropertyValue, PropertyValue> valuePair =
            new Tuple2<>(PropertyValue.create(sourceValues), PropertyValue.create(targetValues));
    return Optional.of(valuePair);
  }

  /**
   * Checks whether the similarity component is related to the vertex pair
   *
   * @param sourceVertex The first vertex
   * @param targetVertex The second vertex
   * @param sc The similarity component
   *
   * @return Returns true, if the similarity component and the vertex pair are related
   */
  private Boolean isSourceTargetPair(EPGMVertex sourceVertex, EPGMVertex targetVertex,
    SimilarityComponent sc) {
    return
      isGraphLabelMatch(
        getGraphLabel(sourceVertex), getGraphLabel(targetVertex), sc.getSourceGraph(), sc.getTargetGraph()) &&
      isGraphLabelMatch(
        getGraphLabel(targetVertex), getGraphLabel(sourceVertex), sc.getTargetGraph(), sc.getSourceGraph()) &&
      isVertexLabelMatch(sourceVertex.getLabel(), sc.getSourceLabel()) &&
      isVertexLabelMatch(targetVertex.getLabel(), sc.getTargetLabel()) &&
      sourceVertex.hasProperty(sc.getSourceAttribute()) &&
      targetVertex.hasProperty(sc.getTargetAttribute());
  }

  /**
   * Returns the value of the graph label property of the input vertex
   *
   * @param v input vertex
   *
   * @return the value of the graph label property
   */
  private String getGraphLabel(EPGMVertex v) {
    return v.getPropertyValue(GRAPH_LABEL).toString();
  }

  /**
   * Checks whether the the "cur" graph label (entity graph label) is matched with the "ref" (similarity
   * component graph label) graph label - graph label refers to entity provenance (source).
   *
   * @param cur entity graph label
   * @param paired entity graph label (other end)
   * @param ref similarity component graph label
   * @param pairedRef similarity component graph label (other end)
   *
   * @return Returns true, if the "cur" graph label is matched with similarity component graph label
   */
  private boolean isGraphLabelMatch(String cur, String paired, String ref, String pairedRef) {
    if (ref.equals(cur)) {
      return true;
    }
    if (ref.equals("*")) {
      return pairedRef.equals("*") || pairedRef.equals(paired) || pairedRef.equals(cur);
    }
    if (ref.equals("+")) {
      return !cur.equals(paired) && (pairedRef.equals("+") || pairedRef.equals(paired) ||
        pairedRef.equals(cur));
    }
    return false;
  }

  /**
   * Checks whether the the "cur" vertex label is matched with the "ref" (similarity component vertex label) -
   * vertex label refers to entity category/type
   *
   * @param cur vertex label
   * @param ref similarity component vertex label
   *
   * @return Returns true, if the "cur" vertex label equals the "ref" vertex label or if the ref equals "*"
   */
  private boolean isVertexLabelMatch(String cur, String ref) {
    return ref.equals("*") || ref.equals(cur);
  }

}
