package org.gradoop.famer.linking.similarityMeasuring.dragonPreparation;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple5;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonInstance;


public class CsvToDragonInstance implements MapFunction<Tuple5<Long, String, Integer, Double, Boolean>, DragonInstance> {
    @Override
    public DragonInstance map(Tuple5<Long, String, Integer, Double, Boolean> tuple5) throws Exception {
        return new DragonInstance(tuple5.f0, tuple5.f1, tuple5.f2, tuple5.f3, tuple5.f4);
    }
}
