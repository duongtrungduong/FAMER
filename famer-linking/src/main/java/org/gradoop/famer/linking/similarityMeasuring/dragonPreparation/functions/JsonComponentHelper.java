package org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.*;

import java.io.File;
import java.net.URI;

public class JsonComponentHelper {
    public static SimilarityComponent getSimilarityComponentFromJson(JSONObject similarityConfig) throws JSONException,
            ClassNotFoundException {
        String similarityMethodName = similarityConfig.getString("similarityMethod");
        if (similarityMethodName.equals(getSimpleName(EditDistanceComponent.class))) {
            return new EditDistanceComponent(similarityConfig);
        } else if (similarityMethodName.equals(getSimpleName(EmbeddingComponent.class))) {
            EmbeddingComponent embeddingComponent = new EmbeddingComponent(similarityConfig);
            URI wordVectorsFileUri = URI.create(similarityConfig.getString("wordVectorsFileUri"));
            embeddingComponent.setWordVectorsFile(new File(wordVectorsFileUri));
        } else if (similarityMethodName.equals(getSimpleName(ExtendedJaccardComponent.class))) {
            return new ExtendedJaccardComponent(similarityConfig);
        } else if (similarityMethodName.equals(getSimpleName(GeoDistanceComponent.class))) {
            return new GeoDistanceComponent(similarityConfig);
        } else if (similarityMethodName.equals(getSimpleName(JaroWinklerComponent.class))) {
            return new JaroWinklerComponent(similarityConfig);
        } else if (similarityMethodName.equals(getSimpleName(LongestCommonSubstringComponent.class))) {
            return new LongestCommonSubstringComponent(similarityConfig);
        } else if (similarityMethodName.equals(getSimpleName(MongeElkanComponent.class))) {
            return new MongeElkanComponent(similarityConfig);
        } else if (similarityMethodName.equals(getSimpleName(NumericalSimilarityWithMaxDistanceComponent.class))) {
            return new NumericalSimilarityWithMaxDistanceComponent(similarityConfig);
        } else if (similarityMethodName.equals(getSimpleName(NumericalSimilarityWithMaxPercentageComponent.class))) {
            return new NumericalSimilarityWithMaxPercentageComponent(similarityConfig);
        } else if (similarityMethodName.equals(getSimpleName(QGramsComponent.class))) {
            return new QGramsComponent(similarityConfig);
        } else if (similarityMethodName.equals(getSimpleName(TruncateBeginComponent.class))) {
            return new TruncateBeginComponent(similarityConfig);
        } else if (similarityMethodName.equals(getSimpleName(TruncateEndComponent.class))) {
            return new TruncateEndComponent(similarityConfig);
        }
        throw new ClassNotFoundException("Dragon: similarity method with name" + similarityMethodName +
                " not found or not supported.");
    }

    public static JSONObject getSimilarityComponentAsJson(SimilarityComponent similarityComponent) throws JSONException {
        JSONObject similarityConfig = new JSONObject();
        similarityConfig.accumulate("id", similarityComponent.getComponentId());
        similarityConfig.accumulate("sourceGraph", similarityComponent.getSourceGraph());
        similarityConfig.accumulate("targetGraph", similarityComponent.getTargetGraph());
        similarityConfig.accumulate("sourceLabel", similarityComponent.getSourceLabel());
        similarityConfig.accumulate("targetLabel", similarityComponent.getTargetLabel());
        similarityConfig.accumulate("sourceAttribute", similarityComponent.getSourceAttribute());
        similarityConfig.accumulate("targetAttribute", similarityComponent.getTargetAttribute());
        similarityConfig.accumulate("weight", similarityComponent.getWeight());

        if (similarityComponent instanceof EmbeddingComponent) {
            similarityConfig.accumulate("wordVectorsFileUri", ((EmbeddingComponent) similarityComponent).getWordVectorsFileUri());
        } else if (similarityComponent instanceof ExtendedJaccardComponent) {
            similarityConfig.accumulate("tokenizer", ((ExtendedJaccardComponent) similarityComponent).getTokenizer());
            similarityConfig.accumulate("threshold", ((ExtendedJaccardComponent) similarityComponent).getThreshold());
            similarityConfig.accumulate("jaroWinklerThreshold",
                    ((ExtendedJaccardComponent) similarityComponent).getJaroWinklerThreshold());
        } else if (similarityComponent instanceof GeoDistanceComponent) {
            similarityConfig.accumulate("maxToleratedDistance", ((GeoDistanceComponent) similarityComponent).getMaxToleratedDistance());
        } else if (similarityComponent instanceof JaroWinklerComponent) {
            similarityConfig.accumulate("threshold", ((JaroWinklerComponent) similarityComponent).getThreshold());
        } else if (similarityComponent instanceof LongestCommonSubstringComponent) {
            similarityConfig.accumulate("secondMethod",
                    ((LongestCommonSubstringComponent) similarityComponent).getSecondMethod().toString());
        } else if (similarityComponent instanceof MongeElkanComponent) {
            similarityConfig.accumulate("tokenizer", ((MongeElkanComponent) similarityComponent).getTokenizer());
            similarityConfig.accumulate("jaroWinklerThreshold",
                    ((MongeElkanComponent) similarityComponent).getJaroWinklerThreshold());
        } else if (similarityComponent instanceof NumericalSimilarityWithMaxDistanceComponent) {
            similarityConfig.accumulate("maxToleratedDistance",
                    ((NumericalSimilarityWithMaxDistanceComponent) similarityComponent).getMaxToleratedDistance());
        } else if (similarityComponent instanceof NumericalSimilarityWithMaxPercentageComponent) {
            similarityConfig.accumulate("maxToleratedPercentage",
                    ((NumericalSimilarityWithMaxPercentageComponent) similarityComponent).getMaxToleratedPercentage());
        } else if (similarityComponent instanceof QGramsComponent) {
            similarityConfig.accumulate("length", ((QGramsComponent) similarityComponent).getLength());
            similarityConfig.accumulate("padding", ((QGramsComponent) similarityComponent).isPadding());
            similarityConfig.accumulate("secondMethod", ((QGramsComponent) similarityComponent).getMethod().toString());
        } else if (similarityComponent instanceof TruncateBeginComponent) {
            similarityConfig.accumulate("length", ((TruncateBeginComponent) similarityComponent).getLength());
        } else if (similarityComponent instanceof TruncateEndComponent) {
            similarityConfig.accumulate("length", ((TruncateEndComponent) similarityComponent).getLength());
        }
        similarityConfig.accumulate("similarityMethod", similarityComponent.getClass().getSimpleName());
        return similarityConfig;
    }

    private static String getSimpleName(Class<?> clazz) {
        return clazz.getSimpleName();
    }
}
