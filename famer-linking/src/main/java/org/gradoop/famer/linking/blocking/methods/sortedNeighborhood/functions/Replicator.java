/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.sortedNeighborhood.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

import java.util.ArrayList;
import java.util.List;

/**
 * Replicates "windowSize-1" vertices of each reducer for the next reducer
 */
@FunctionAnnotation.ForwardedFields("f0;f1")
public class Replicator implements
  GroupReduceFunction<Tuple3<EPGMVertex, String, Integer>, Tuple4<EPGMVertex, String, Integer, Boolean>> {
  /**
   * The number of reducers (usually parallelism degree)
   */
  private int reducerNo;
  /**
   * The windowSize
   */
  private int windowSize;

  /**
   * Reduce object instantiation
   */
  private final Tuple4<EPGMVertex, String, Integer, Boolean> reuseTuple;

  /**
   * Creates an instance of Replicator
   *
   * @param reducerNo  The number of reduces
   * @param windowSize The size of window
   */
  public Replicator(int reducerNo, int windowSize) {
    this.reducerNo = reducerNo;
    this.windowSize = windowSize;
    this.reuseTuple = new Tuple4<>();
  }

  /**
   * Replicates "windowSize-1" vertices of each reducer for the next reducer. A false is attached
   * to each replicated vertex. For others, true is attached.
   */
  @Override
  public void reduce(Iterable<Tuple3<EPGMVertex, String, Integer>> iterable,
    Collector<Tuple4<EPGMVertex, String, Integer, Boolean>> collector) throws Exception {
    List<Tuple3<EPGMVertex, String, Integer>> items = new ArrayList<>();

    for (Tuple3<EPGMVertex, String, Integer> item : iterable) {
      items.add(item);
    }

    int i = 1;
    int replicationStart = items.size() - (windowSize - 1);
    for (Tuple3<EPGMVertex, String, Integer> item : items) {
      reuseTuple.f0 = item.f0;
      reuseTuple.f1 = item.f1;
      reuseTuple.f2 = item.f2;
      reuseTuple.f3 = true;
      collector.collect(reuseTuple);

      if ((i >= replicationStart) && (item.f2 < reducerNo)) { // no replication for the last reducer
        reuseTuple.f0 = item.f0;
        reuseTuple.f1 = item.f1;
        reuseTuple.f2 = item.f2 + 1;
        reuseTuple.f3 = false;
        collector.collect(reuseTuple);
      }
      i++;
    }
  }
}
