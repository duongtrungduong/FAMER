/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.dataStructures;

import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.keyGeneration.BlockingKeyGenerator;
import org.gradoop.famer.linking.blocking.methods.BlockingExecutor;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import static org.gradoop.famer.linking.linking.Linker.GRAPH_LABEL;

/**
 * The abstract base class for all blocking component methods
 */
public abstract class BlockingComponent implements Serializable {

  /**
   * Generates the blocking keys for each entity for the blocking method
   */
  private final BlockingKeyGenerator blockingKeyGenerator;

  /**
   * Mapping of the allowed graph pairs specified by user in configuration file
   */
  private final Map<String, Set<String>> graphPairs;

  /**
   * Mapping of the allowed category pairs specified by user in configuration file
   */
  private final Map<String, Set<String>> categoryPairs;

  /**
   * Creates an instance of BlockingComponent
   *
   * @param baseConfig The base configuration for the blocking component
   */
  public BlockingComponent(BlockingComponentBaseConfig baseConfig) {
    this.blockingKeyGenerator = baseConfig.getBlockingKeyGenerator();
    this.graphPairs = baseConfig.getGraphPairs();
    this.categoryPairs = baseConfig.getCategoryPairs();
  }

  /**
   * Returns the blocking key generator
   *
   * @return The blocking key generator
   */
  public BlockingKeyGenerator getBlockingKeyGenerator() {
    return blockingKeyGenerator;
  }

  /**
   * Returns the mapping of the allowed graph pairs specified by user in configuration file
   *
   * @return The mapping of the allowed graph pairs
   */
  public Map<String, Set<String>> getGraphPairs() {
    return graphPairs;
  }

  /**
   * Returns the mapping of the allowed category pairs specified by user in configuration file
   *
   * @return The mapping of the allowed category pairs
   */
  public Map<String, Set<String>> getCategoryPairs() {
    return categoryPairs;
  }

  /**
   * Checks whether the given vertex pair is allowed to be paired
   *
   * @param sourceVertex The first vertex
   * @param targetVertex The second vertex
   * @return false when the vertex pair shall not be paired
   */
  public boolean isAllowedPair(EPGMVertex sourceVertex, EPGMVertex targetVertex) {
    return isAllowedGraphPair(sourceVertex, targetVertex) &&
      isAllowedCategoryPair(sourceVertex, targetVertex);
  }

  /**
   * Checks based on {@link #graphPairs} whether the given vertex pair is allowed to be paired
   *
   * @param sourceVertex The first vertex
   * @param targetVertex The second vertex
   * @return false when the vertex pair is not in the allowed list of paired sources
   */
  private boolean isAllowedGraphPair(EPGMVertex sourceVertex, EPGMVertex targetVertex) {
    if (!sourceVertex.hasProperty(GRAPH_LABEL) || !targetVertex.hasProperty(GRAPH_LABEL)) {
      throw new NullPointerException("No graphLabel property exists in source or/and target graph(s)");
    }
    String sourceGraph = sourceVertex.getPropertyValue(GRAPH_LABEL).getString();
    String targetGraph = targetVertex.getPropertyValue(GRAPH_LABEL).getString();

    if (graphPairs.containsKey("*")) {
      Set<String> pairs = graphPairs.get("*");
      if (pairs.contains("*") || pairs.contains(sourceGraph) || pairs.contains(targetGraph)) {
        return true;
      }
    }

    if (graphPairs.containsKey("+")) {
      Set<String> pairs = graphPairs.get("+");
      if (!sourceGraph.equals(targetGraph) &&
        (pairs.contains("+") || pairs.contains(sourceGraph) || pairs.contains(targetGraph))) {
        return true;
      }
    }

    Set<String> allowedGraphs = graphPairs.get(sourceGraph);
    // allowedGraphs can be null if graph is not used for similarity measuring
    return (allowedGraphs != null) && allowedGraphs.contains(targetGraph);
  }

  /**
   * Checks based on {@link #categoryPairs} whether the given vertex pair is allowed to be paired
   *
   * @param sourceVertex The first vertex
   * @param targetVertex The second vertex
   * @return false when the vertex pair is not in the allowed list of paired categories
   */
  private boolean isAllowedCategoryPair(EPGMVertex sourceVertex, EPGMVertex targetVertex) {
    String sourceCategory = sourceVertex.getLabel();
    String targetCategory = targetVertex.getLabel();
    if (categoryPairs.containsKey("*")) {
      return true;
    }
    Set<String> allowedCategories = categoryPairs.get(sourceCategory);
    // allowedCategories can be null if graph is not used for similarity measuring
    return (allowedCategories != null) && allowedCategories.contains(targetCategory);
  }

  /**
   * Checks whether the key generation method could generate more than one key for each vertex
   *
   * @return false when the key list contains only one key per entity (vertex)
   */
  public boolean hasRedundantPairs() {
    return blockingKeyGenerator.returnsMultipleKeys();
  }

  /**
   * Abstract method for building the blocking execution that must be implemented by all blocking methods
   * components
   *
   * @return The blocking method object
   */
  public abstract BlockingExecutor buildBlockingExecutor();
}
