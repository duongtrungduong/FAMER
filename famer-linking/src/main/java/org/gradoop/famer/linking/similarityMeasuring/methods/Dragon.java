package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.apache.flink.api.java.tuple.Tuple2;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;
import org.gradoop.famer.trainingModule.classifier.methods.DragonClassifier;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;

import java.util.*;
import java.util.stream.Collectors;

import static org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions.JsonComponentHelper.getSimilarityComponentFromJson;


/**
 * Computes the similarity degree of two strings using the dragon-decision-tree method.
 */
public class Dragon implements SimilarityComputation<List<PropertyValue>> {
    /**
     * The binary classifier using dragon-decision-tree
     */
    private final DragonClassifier dragonClassifier;

    /**
     * Size of the feature vector, which were used to train the dragon-decision-tree
     */
    private static Integer featureSize;

    /**
     * Similarity components and their indices in the feature vector
     */
    private static Map<Integer, Tuple2<Integer, SimilarityComponent>> simComponentsAndIndex;

    /**
     * Creates an instance of Dragon
     *
     * @param dragonClassifier The binary classifier using dragon-decision-tree
     */
    public Dragon(DragonClassifier dragonClassifier) {
        this.dragonClassifier = dragonClassifier;
        buildComponentsForPredicting(dragonClassifier);
    }

    @Override
    public double computeSimilarity(List<PropertyValue> list1, List<PropertyValue> list2) throws Exception {
        if (list1 == null) {
            throw new NullPointerException("list1 must not be null");
        }
        if (list2 == null) {
            throw new NullPointerException("list2 must not be null");
        }
        BinaryInstance unlabeledInstance = createUnlabeledInstance(list1, list2);
        if (unlabeledInstance == null) return 0.0d;
        BinaryInstance labeledInstance = (BinaryInstance) dragonClassifier.predict(unlabeledInstance);
        return labeledInstance.isTrue() ? labeledInstance.getProbability() : 1d - labeledInstance.getProbability();
    }

    @Override
    public List<PropertyValue> parsePropertyValue(PropertyValue value) {
        return value.getList();
    }

    /**
     * The decision-tree after training may not use all features (a.k.a all similarity methods) of the training dataset,
     * therefore only necessary similarity methods are collected here. The indices of similarity methods for
     * predicting are also noticed.
     *
     * @param dragonClassifier the trained model
     */
    private void buildComponentsForPredicting(DragonClassifier dragonClassifier) {
        JSONArray trainingConfigs = dragonClassifier.getClassifierComponent().getTrainingSimilarityConfigs();
        if (null == featureSize) {
            featureSize = trainingConfigs.length();
        }
        if (null == simComponentsAndIndex) {
            simComponentsAndIndex = new HashMap<>();
        }
        if (simComponentsAndIndex.size() == 0) {
            try {
                Set<Integer> simIndicesFromTree = dragonClassifier.getDragonTree().getAsList().stream()
                        .filter(f -> f.isValid() && !f.isLeaf())
                        .map(m -> m.getSplit().getFeatureIndex())
                        .collect(Collectors.toCollection(HashSet::new));
                int c = 0;
                for (int i = 0; i < trainingConfigs.length(); i++) {
                    if (simIndicesFromTree.contains(i)) {
                        SimilarityComponent component = getSimilarityComponentFromJson(
                                trainingConfigs.getJSONObject(i));
                        simComponentsAndIndex.put(c++, Tuple2.of(i, component));
                    }
                }
            } catch (JSONException | ClassNotFoundException ex) {
                throw new UnsupportedOperationException("DragonComponent: similarity components" +
                        " could not be parsed from Json.", ex);
            }
        }
    }

    /**
     * Create a {@link BinaryInstance} from two given property value lists using similarity components for predicting
     * @param list1 first property value list
     * @param list2 second property value list
     * @return corresponding binary instance
     * @throws Exception thrown on errors while computing the similarity
     */
    private BinaryInstance createUnlabeledInstance(List<PropertyValue> list1, List<PropertyValue> list2)
            throws Exception {
        if (list1.size() != simComponentsAndIndex.size() || list2.size() != simComponentsAndIndex.size()) {
            return null;
        }
        Vector<Double> features = new Vector<>();
        features.setSize(featureSize);
        for (int i = 0; i < simComponentsAndIndex.size(); i++) {
            int featureIndex = simComponentsAndIndex.get(i).f0;
            if (list1.get(i).equals(PropertyValue.NULL_VALUE) || list2.get(i).equals(PropertyValue.NULL_VALUE)) {
                features.set(featureIndex, 0d);
            } else {
                SimilarityComputation<?> computation = simComponentsAndIndex.get(i).f1.buildSimilarityComputation();
                double featureValue = computation.computeSimilarity(list1.get(i), list2.get(i));
                features.set(featureIndex, featureValue);
            }
        }
        return new BinaryInstance(features);
    }
}
