/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.standardBlocking.functions;

import org.apache.flink.api.common.functions.CrossFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

import static org.gradoop.famer.linking.blocking.methods.standardBlocking.StandardBlocking.KEY_ADDED_FLAG;

/**
 * Adds each generated key to each vertex. Additionally adds the property value "ka (key added)" to this
 * vertex. It indicates that the key is not originally generated from the vertex property values but it is an
 * existing key attached to a vertex with empty key.
 */
@FunctionAnnotation.ForwardedFieldsSecond("*->f1")
public class KeyAdder implements CrossFunction<EPGMVertex, String, Tuple2<EPGMVertex, String>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple2<EPGMVertex, String> reuseTuple = new Tuple2<>();

  @Override
  public Tuple2<EPGMVertex, String> cross(EPGMVertex vertex, String key) throws Exception {
    vertex.setProperty(KEY_ADDED_FLAG, "ka");
    reuseTuple.f0 = vertex;
    reuseTuple.f1 = key;
    return reuseTuple;
  }
}
