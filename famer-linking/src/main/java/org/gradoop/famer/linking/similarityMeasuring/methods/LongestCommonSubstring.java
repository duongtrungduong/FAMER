/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;

/**
 * Computes the similarity degree of two strings using the LongestCommonSubstring method
 */
public class LongestCommonSubstring implements SimilarityComputation<String> {

  /**
   * Second methods used for the similarity computation
   */
  public enum SecondMethod {
    /**
     * Use overlap
     */
    OVERLAP,
    /**
     * USe jaccard
     */
    JACCARD,
    /**
     * Use dice
     */
    DICE
  }

  /**
   * The second similarity method
   */
  private final SecondMethod secondMethod;

  /**
   * Creates an instance of LongestCommonSubstring
   *
   * @param secondMethod The second similarity method
   */
  public LongestCommonSubstring(SecondMethod secondMethod) {
    this.secondMethod = secondMethod;
  }

  @Override
  public double computeSimilarity(String str1, String str2) throws Exception {
    if (str1 == null) {
      throw new NullPointerException("str1 must not be null");
    }
    if (str2 == null) {
      throw new NullPointerException("str2 must not be null");
    }
    if (str1.length() == 0 || str2.length() == 0) {
      return 0;
    }
    int str1Length = str1.length();
    int str2Length = str2.length();

    double simDegree;
    int curLongest = 0;
    int maxLength = Math.max(str1.length(), str2.length());
    int[] v0 = new int[maxLength];
    int[] v1 = new int[maxLength];
    for (int i = 0; i < str1.length(); i++) {
      for (int j = 0; j < str2.length(); j++) {
        if (str1.charAt(i) == str2.charAt(j)) {
          if (i == 0 || j == 0) {
            v1[j] = 1;
          } else {
            v1[j] = v0[j - 1] + 1;
          }
          if (v1[j] > curLongest) {
            curLongest = v1[j];
          }
        } else {
          v1[j] = 0;
        }
      }
      final int[] swap = v0;
      v0 = v1;
      v1 = swap;
    }
    switch (secondMethod) {
    case OVERLAP:
      simDegree = curLongest / (double) Math.min(str1Length, str2Length);
      break;
    case JACCARD:
      simDegree = curLongest / (double) (str1Length + str2Length - curLongest);
      break;
    case DICE:
      simDegree = 2 * curLongest / (double) (str1Length + str2Length);
      break;
    default:
      // JACCARD
      simDegree = curLongest / (double) (str1Length + str2Length - curLongest);
    }
    return simDegree;
  }

  @Override
  public String parsePropertyValue(PropertyValue value) {
    return getString(value);
  }
}
