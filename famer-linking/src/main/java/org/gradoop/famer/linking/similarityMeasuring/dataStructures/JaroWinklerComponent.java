/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.similarityMeasuring.methods.JaroWinkler;
import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;

/**
 * Similarity component which uses {@link JaroWinkler}.
 */
public class JaroWinklerComponent extends SimilarityComponent {

  /**
   * The threshold
   */
  private final double threshold;

  /**
   * Creates an instance of JaroWinklerComponent from the given json object.
   *
   * @param config The json object that contains the configuration.
   */
  public JaroWinklerComponent(JSONObject config) {
    super(config);
    try {
      this.threshold = config.getDouble("threshold");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("JaroWinklerComponent: value for threshold could " +
        "not be found or parsed to double.", ex);
    }
  }

  /**
   * Creates an instance of JaroWinklerComponent
   *
   * @param baseConfig The base configuration for the similarity component
   * @param threshold The threshold
   */
  public JaroWinklerComponent(SimilarityComponentBaseConfig baseConfig, double threshold) {
    super(baseConfig);
    this.threshold = threshold;
  }

  @Override
  public SimilarityComputation<String> buildSimilarityComputation() {
    return new JaroWinkler(threshold);
  }

  public double getThreshold() {
    return threshold;
  }
}
