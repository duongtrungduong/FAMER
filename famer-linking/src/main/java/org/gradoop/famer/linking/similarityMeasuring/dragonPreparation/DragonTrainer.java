package org.gradoop.famer.linking.similarityMeasuring.dragonPreparation;

import org.apache.commons.io.FileUtils;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.DragonComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;
import org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.dataStructures.EnrichedDragonComponent;
import org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions.GoldGraphCreator;
import org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions.TrainDataCreator;
import org.gradoop.famer.trainingModule.classifier.dataStructures.DragonClassifierComponent;
import org.gradoop.famer.trainingModule.classifier.methods.DragonClassifier;
import org.gradoop.famer.trainingModule.common.dataStructures.CommonInstance;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.util.GradoopFlinkConfig;

import java.io.File;
import java.net.URI;
import java.util.List;

public class DragonTrainer {

    public static void trainDragon(EnrichedDragonComponent dragonComponent, GraphCollection inputGraphs,
            URI groundTruthUri, GradoopFlinkConfig gradoopConfig) throws Exception {
        LogicalGraph goldGraph = GoldGraphCreator.getGoldGraph(inputGraphs, groundTruthUri, gradoopConfig);
        trainDragon(dragonComponent, goldGraph);
    }

    public static void trainDragon(EnrichedDragonComponent dragonComponent, LogicalGraph sourceGraph,
            LogicalGraph targetGraph, URI groundTruthUri, GradoopFlinkConfig gradoopConfig) throws Exception {
        LogicalGraph goldGraph = GoldGraphCreator.getGoldGraph(sourceGraph, targetGraph, groundTruthUri, gradoopConfig);
        trainDragon(dragonComponent, goldGraph);
    }

    public static void trainDragon(EnrichedDragonComponent dragonComponent, LogicalGraph sourceGraph,
            LogicalGraph targetGraph, DataSet<Tuple2<String, String>> perfectMapping, GradoopFlinkConfig gradoopConfig)
            throws Exception {
        LogicalGraph goldGraph = GoldGraphCreator.getGoldGraph(sourceGraph, targetGraph, perfectMapping, gradoopConfig);
        trainDragon(dragonComponent, goldGraph);
    }

    public static void trainDragon(EnrichedDragonComponent dragonComponent, LogicalGraph goldGraph) throws Exception {
        List<SimilarityComponent> similarityComponents = dragonComponent.getTrainingSimilarityComponents();
        DataSet<CommonInstance> trainingInstances =
                TrainDataCreator.getTrainingInstances(goldGraph, similarityComponents).map(m -> m);
        trainDragon(dragonComponent, trainingInstances);
    }

    public static void trainDragon(EnrichedDragonComponent dragonComponent, DataSet<CommonInstance> trainDataSet)
            throws Exception {
        DragonClassifierComponent classifierComponent = dragonComponent.buildClassifierComponent();
        DragonClassifier dragonClassifier = new DragonClassifier(classifierComponent);
        DataSet<CommonInstance> balancedDataSet = classifierComponent.getClassBalancer().resample(trainDataSet);
        dragonClassifier.fit(balancedDataSet);
        dragonComponent.setDragonClassifier(dragonClassifier);
    }

    public static void loadDragon(DragonComponent dragonComponent, URI modelUri) throws Exception {
        JSONObject jsonModel = new JSONObject(FileUtils.readFileToString(new File(modelUri)));
        DragonClassifier dragonClassifier = new DragonClassifier(jsonModel);
        dragonComponent.setDragonClassifier(dragonClassifier);
    }
}
