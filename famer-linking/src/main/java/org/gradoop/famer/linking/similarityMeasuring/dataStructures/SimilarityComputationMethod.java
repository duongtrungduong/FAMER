/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

/**
 * Available similarity computation methods
 */
public enum SimilarityComputationMethod {
  /**
   * Computes the similarity degree of two strings using the EditDistanceLevenshtein method
   */
  EDIT_DISTANCE_LEVENSHTEIN,
  /**
   * Computes the similarity of two strings using the Embedding method with word vectors
   */
  EMBEDDING,
  /**
   * Computes the similarity degree of two strings using the extended Jaccard method
   */
  EXTENDED_JACCARD,
  /**
   * Computes the similarity degree of two geographical points from their geographical coordinates
   */
  GEO_DISTANCE,
  /**
   * Computes the similarity degree of two strings using the JaroWinkler method from
   * {@link info.debatty.java.stringsimilarity}
   */
  JARO_WINKLER,
  /**
   * Computes the similarity degree of two list of values using a user specified similarity computation
   * method
   */
  LIST_SIMILARITY,
  /**
   * Computes the similarity degree of two strings using the LongestCommonSubstring method
   */
  LONGEST_COMMON_SUBSTRING,
  /**
   * Computes the similarity degree of two strings using the MongeElkan-JaroWinkler method
   */
  MONGE_ELKAN_JARO_WINKLER,
  /**
   * Computes the similarity degree of two numbers using the NumericalSimilarity method with a defined
   * maximum distance
   */
  NUMERICAL_SIMILARITY_MAX_DISTANCE,
  /**
   * Computes the similarity degree of two numbers using the NumericalSimilarity method with a defined
   * maximum percentage of distance
   */
  NUMERICAL_SIMILARITY_MAX_PERCENTAGE,
  /**
   * Computes the similarity degree of two strings using the QGram method
   */
  Q_GRAMS,
  /**
   * Computes the similarity degree of two strings using the Truncate-Begin method
   */
  TRUNCATE_BEGIN,
  /**
   * Computes the similarity degree of two strings using the Truncate-End method
   */
  TRUNCATE_END
}
