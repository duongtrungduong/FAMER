/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.similarityMeasuring.methods.ExtendedJaccard;
import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;

/**
 * Similarity component which uses {@link ExtendedJaccardComponent}.
 */
public class ExtendedJaccardComponent extends SimilarityComponent {

  /**
   * The tokenizer string parameter
   */
  private final String tokenizer;

  /**
   * The threshold a similarity value must reach or exceed
   */
  private final double threshold;

  /**
   * The threshold used for the nested jaro winkler algorithm
   */
  private final double jaroWinklerThreshold;

  /**
   * Creates an instance of ExtendedJaccardComponent from the given json object
   *
   * @param config The json object that contains the configuration.
   */
  public ExtendedJaccardComponent(JSONObject config) {
    super(config);
    try {
      this.tokenizer = config.getString("tokenizer");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("ExtendedJaccardComponent: value for tokenizer could not be " +
        "found or parsed to string.", ex);
    }
    try {
      this.threshold = config.getDouble("threshold");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("ExtendedJaccardComponent: value for threshold could not be " +
        "found or parsed to double.", ex);
    }
    try {
      this.jaroWinklerThreshold = config.getDouble("jaroWinklerThreshold");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("ExtendedJaccardComponent: value for jaroWinklerThreshold " +
        "could not be found or parsed to double.", ex);
    }
  }

  /**
   * Creates an instance of ExtendedJaccardComponent
   *
   * @param baseConfig The base configuration for the similarity component
   * @param tokenizer The tokenizer string parameter
   * @param threshold The threshold a similarity value must reach or exceed
   * @param jaroWinklerThreshold The threshold used for the nested jaro winkler algorithm
   */
  public ExtendedJaccardComponent(SimilarityComponentBaseConfig baseConfig, String tokenizer,
    double threshold, double jaroWinklerThreshold) {
    super(baseConfig);
    this.tokenizer = tokenizer;
    this.threshold = threshold;
    this.jaroWinklerThreshold = jaroWinklerThreshold;
  }

  @Override
  public SimilarityComputation<String> buildSimilarityComputation() {
    return new ExtendedJaccard(tokenizer, threshold, jaroWinklerThreshold);
  }

  public String getTokenizer() {
    return tokenizer;
  }

  public double getThreshold() {
    return threshold;
  }

  public double getJaroWinklerThreshold() {
    return jaroWinklerThreshold;
  }
}
