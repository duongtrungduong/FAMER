/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.selection.dataStructures.aggregatorRule;

import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityFieldList;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * The structure that contains the similarity aggregator rule as well as the method for computing
 * aggregated similarity value by the similarity aggregator rule
 */
public class AggregatorRule implements Serializable {

  /**
   * List for the aggregator rule components
   */
  private List<AggregatorRuleComponent> aggregatorRuleComponents;

  /**
   * The minimum desired similarity value
   */
  private double aggregationThreshold;

  /**
   * Creates an instance of AggregatorRule
   *
   * @param aggregationThreshold The minimum desired similarity value
   */
  public AggregatorRule(double aggregationThreshold) {
    this(null, aggregationThreshold);
  }

  /**
   * Creates an instance of AggregatorRule
   *
   * @param aggregatorRuleComponents Contains the aggregator rule
   * @param aggregationThreshold     The minimum desired similarity value
   */
  public AggregatorRule(List<AggregatorRuleComponent> aggregatorRuleComponents, double aggregationThreshold) {
    this.aggregatorRuleComponents = aggregatorRuleComponents;
    this.aggregationThreshold = aggregationThreshold;
  }

  public List<AggregatorRuleComponent> getAggregatorRuleComponents() {
    return aggregatorRuleComponents;
  }

  public void setAggregatorRuleComponents(List<AggregatorRuleComponent> aggregatorRuleComponents) {
    this.aggregatorRuleComponents = aggregatorRuleComponents;
  }

  public double getAggregationThreshold() {
    return aggregationThreshold;
  }

  public void setAggregationThreshold(double aggregationThreshold) {
    this.aggregationThreshold = aggregationThreshold;
  }

  /**
   * Checks whether the aggregator rule contains a user-defined rule
   *
   * @return false if there is no user-defined aggregator rule
   */
  public boolean hasAggregatorRuleComponents() {
    return aggregatorRuleComponents != null;
  }

  /**
   * Computes the aggregated similarity value by the similarity aggregator rule
   *
   * @param similarityValues The similarity values
   *
   * @return aggregated similarity value
   */
  public double computeAggregatedSimilarity(SimilarityFieldList similarityValues) {
    // use of last-in-first-out (LIFO) Stack
    Stack<AggregatorRuleComponent> operatorStack = new Stack<>();
    List<AggregatorRuleComponent> parsingList = new ArrayList<>();
    AggregatorRuleComponent operator = null;
    for (AggregatorRuleComponent component : aggregatorRuleComponents) {
      AggregatorRuleComponentType componentType = component.getAggregatorRuleComponentType();
      switch (componentType) {
      case SIMILARITY_FIELD_ID:
        double similarityValue = 0d;
        if (similarityValues.getSimilarityField(component.getValue()) != null) {
          similarityValue = similarityValues.getSimilarityField(component.getValue()).getSimilarityValue();
        }
        AggregatorRuleComponent constantValue =
          new AggregatorRuleComponent(AggregatorRuleComponentType.CONSTANT, similarityValue + "");
        parsingList.add(constantValue);
        if (operator != null) {
          parsingList.add(operator);
          operator = null;
        }
        break;
      case CONSTANT:
        parsingList.add(component);
        if (operator != null) {
          parsingList.add(operator);
          operator = null;
        }
        break;
      case OPEN_PARENTHESIS:
        operatorStack.push(component);
        break;
      case ARITHMETIC_OPERATOR:
        operator = pushPopOperator(parsingList, operatorStack, component);
        break;
      case CLOSE_PARENTHESIS:
        popOperators(parsingList, operatorStack);
        break;
      default:
        throw new IllegalArgumentException("Illegal rule component!");
      }
    }
    while (!operatorStack.empty()) {
      parsingList.add(operatorStack.pop());
    }
    Double result = evaluateExpression(parsingList);
    assert result != null;
    return result;
  }

  /**
   * Based on operator priorities decides about pushing to or popping operators from the operatorStack. It
   * may also keep the operator and the stack as it is and return the input operator
   *
   * @param parsingList   The list that contains the aggregation rule components with the correct of
   *                      arithmetic operations (postfix expression)
   * @param operatorStack The stack for keeping arithmetic operators
   * @param component     The component contains the operator we are deciding about
   * @return AggregatorRuleComponent.ArithmeticOperator
   */
  private AggregatorRuleComponent pushPopOperator(List<AggregatorRuleComponent> parsingList,
    Stack<AggregatorRuleComponent> operatorStack, AggregatorRuleComponent component) {
    AggregatorRuleComponent output = null;
    if (operatorStack.empty()) {
      operatorStack.push(component);
    } else {
      AggregatorRuleComponent poppedComp = operatorStack.pop();
      String poppedValue = poppedComp.getValue();
      if (poppedValue.equals("(")) {
        operatorStack.push(poppedComp);
        operatorStack.push(component);
      } else {
        poppedValue = poppedValue.equals("MINUS") || poppedValue.equals("PLUS") ? "0" : "1";
        String componentValue = component.getValue();
        componentValue = componentValue.equals("MINUS") || componentValue.equals("PLUS") ? "0" : "1";
        if (componentValue.compareTo(poppedValue) > 0) {
          operatorStack.push(poppedComp);
          output = component;
        } else {
          parsingList.add(parsingList.size(), poppedComp);
          operatorStack.push(component);
        }
      }
    }
    return output;
  }

  /**
   * Pops all operators from operatorStack and adds them to parsingList till "(" occurs.
   *
   * @param parsingList   The list that contains the aggregation rule components with the correct of
   *                      arithmetic operations (postfix expression)
   * @param operatorStack The stack for keeping arithmetic operators
   */
  private void popOperators(List<AggregatorRuleComponent> parsingList,
    Stack<AggregatorRuleComponent> operatorStack) {

    AggregatorRuleComponent poppedComp = operatorStack.pop();
    String poppedValue = poppedComp.getValue();

    while (!poppedValue.equals("(")) {
      parsingList.add(parsingList.size(), poppedComp);
      poppedComp = operatorStack.pop();
      poppedValue = poppedComp.getValue();
    }
  }

  /**
   * Evaluates the expression in parsingList and computes the similarity value
   *
   * @param parsingList The list that contains the aggregation rule components with the correct of
   *                    arithmetic operations (postfix expression)
   * @return similarity value
   */
  private Double evaluateExpression(List<AggregatorRuleComponent> parsingList) {
    do {
      int operatorIndex = findFirstOperator(parsingList);
      if (operatorIndex < 2) {
        return null;
      }
      ArithmeticOperator operator = ArithmeticOperator.valueOf(parsingList.get(operatorIndex).getValue());
      BigDecimal operand1 = new BigDecimal(parsingList.get(operatorIndex - 2).getValue());
      BigDecimal operand2 = new BigDecimal(parsingList.get(operatorIndex - 1).getValue());
      BigDecimal result = compute(operator, operand1, operand2);
      parsingList.remove(operatorIndex);
      parsingList.remove(operatorIndex - 1);
      parsingList.remove(operatorIndex - 2);

      AggregatorRuleComponent constantValue =
        new AggregatorRuleComponent(AggregatorRuleComponentType.CONSTANT, result + "");
      parsingList.add(operatorIndex - 2, constantValue);
    } while (parsingList.size() > 1);

    return Double.parseDouble(parsingList.get(0).getValue());
  }

  /**
   * Finds the first arithmetic operator in parsingList and returns the index
   *
   * @param parsingList The list that contains the aggregation rule components with the correct of
   *                    *    *                    arithmetic operations (postfix expression)
   * @return -1 when the expression elements are not in a correct order
   */
  private int findFirstOperator(List<AggregatorRuleComponent> parsingList) {
    int pos = 0;
    for (AggregatorRuleComponent component : parsingList) {
      if (component.getAggregatorRuleComponentType()
        .equals(AggregatorRuleComponentType.ARITHMETIC_OPERATOR)) {
        return pos;
      }
      pos++;
    }
    return -1;
  }

  /**
   * Computes the result of applying one arithmetic operator (+, -, *, /) on two operands
   *
   * @param operator The operator
   * @param operand1 The first operand
   * @param operand2 The second operand
   * @return The computed result
   */
  private BigDecimal compute(ArithmeticOperator operator, BigDecimal operand1, BigDecimal operand2) {
    BigDecimal result = new BigDecimal(-1);
    if (operator.equals(ArithmeticOperator.DIVISION)) {
      if (!operand1.equals(new BigDecimal(0))) {
        result = operand1.divide(operand2, MathContext.DECIMAL128);
      }
    } else if (operator.equals(ArithmeticOperator.MINUS)) {
      result = operand1.subtract(operand2);
    } else if (operator.equals(ArithmeticOperator.PLUS)) {
      result = operand1.add(operand2);
    } else if (operator.equals(ArithmeticOperator.MULTIPLY)) {
      result = operand1.multiply(operand2);
    }
    return result;
  }
}
