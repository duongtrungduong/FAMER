/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.apache.flink.api.java.tuple.Tuple2;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.linking.similarityMeasuring.methods.GeoDistance;
import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;

/**
 * Similarity component which uses {@link GeoDistance}.
 */
public class GeoDistanceComponent extends SimilarityComponent {

  /**
   * The maximum tolerated distance
   */
  private final double maxToleratedDistance;

  /**
   * Creates an instance of GeoDistanceComponent from the given json object.
   *
   * @param config The json object that contains the configuration.
   */
  public GeoDistanceComponent(JSONObject config) {
    super(config);
    try {
      this.maxToleratedDistance = config.getDouble("maxToleratedDistance");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("GeoDistanceComponent: value for maxToleratedDistance could " +
        "not be found or parsed to double.", ex);
    }
  }

  /**
   * Creates an instance of GeoDistanceComponent
   *
   * @param baseConfig The base configuration for the similarity component
   * @param maxToleratedDistance The maximum tolerated distance
   */
  public GeoDistanceComponent(SimilarityComponentBaseConfig baseConfig, double maxToleratedDistance) {
    super(baseConfig);
    this.maxToleratedDistance = maxToleratedDistance;
  }

  @Override
  public SimilarityComputation<Tuple2<PropertyValue, PropertyValue>> buildSimilarityComputation() {
    return new GeoDistance(maxToleratedDistance);
  }

  public double getMaxToleratedDistance() {
    return maxToleratedDistance;
  }
}
