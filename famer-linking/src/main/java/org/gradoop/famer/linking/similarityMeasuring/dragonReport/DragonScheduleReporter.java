package org.gradoop.famer.linking.similarityMeasuring.dragonReport;

import org.apache.commons.io.FileUtils;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.hadoop.shaded.org.apache.http.client.utils.URIBuilder;
import org.apache.flink.metrics.MetricConfig;
import org.apache.flink.metrics.reporter.AbstractReporter;
import org.apache.flink.metrics.reporter.Scheduled;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class DragonScheduleReporter extends AbstractReporter implements Scheduled {
    private static final String METRICS_DELIMITER = "~";
    private static ConcurrentHashMap<Tuple3<String, String, String>, List<Double>> meterReport;
    private String restHost;
    private int restPort;
    private URI reportFileUri;
    private List<String> taskNameHints;
    private List<String> metricNames;
    private int reportTrigger;

    @Override
    public void report() {
        meters.forEach((metricKey, metricValue) -> {
            String[] metricInfo = metricValue.split(METRICS_DELIMITER);
            if (metricInfo.length == 3 && metricKey.getRate() > 0) {
                Tuple3<String, String, String> reportKey = Tuple3.of(metricInfo[0], metricInfo[1], metricInfo[2]);
                Double reportValue = metricKey.getRate();
                meterReport.computeIfAbsent(reportKey, v -> new ArrayList<>()).add(reportValue);
            }
        });

        if (!isJobRunning(restHost, restPort)) {
            reportTrigger++;
            if (reportTrigger > 10) {
                exportReport(reportFileUri, taskNameHints, metricNames);
                reportTrigger = 0;
            }
        } else {
            reportTrigger = 0;
        }

        System.out.println("@@@@@@ " + meterReport.size() + " @@@@ " + reportTrigger);
        System.out.println(java.time.LocalDateTime.now());
        System.out.println("\n\n");
    }

    private void exportReport(URI reportFileUri, List<String> taskNameHints, List<String> metricNames) {
        if (meterReport == null) {
            System.out.println("Could not get the report, meterReport is null.");
            return;
        }
        String report = buildReport(taskNameHints, metricNames);
        System.out.println(report);
        try {
            FileUtils.writeStringToFile(new File(reportFileUri), report, true);
        } catch (IOException e) {
            System.out.println("Could not save report to file. " + e);
        }
        meterReport.clear();
    }

    private String buildReport(List<String> taskNameHints, List<String> metricNames) {
        List<String> jobIds = getJobIdsFromTaskNameHints(taskNameHints);
        long pipelineDuration = jobIds.stream().mapToLong(f -> getJobDuration(restHost, restPort, f)).sum();
        int pipelineParallelism = jobIds.stream().mapToInt(f -> getJobParallelism(restHost, restPort, f)).max().orElse(0);
        Map<String, List<Double>> pipelineMeterReport = new HashMap<>();
        meterReport.forEach((metricKey, metricValues) -> {
            String jobId = metricKey.f0;
            String metricName = metricKey.f2;
            if (jobIds.contains(jobId) && metricNames.contains(metricName)) {
                pipelineMeterReport.computeIfAbsent(metricName, v -> new ArrayList<>()).addAll(metricValues);
            }
        });
        StringBuilder sb = new StringBuilder();
        pipelineMeterReport.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .forEach(f -> {
                    double min = f.getValue().stream().mapToDouble(m -> m).min().orElse(0d);
                    double max = f.getValue().stream().mapToDouble(m -> m).max().orElse(0d);
                    double avg = f.getValue().stream().mapToDouble(m -> m).average().orElse(0d);
                    sb.append("pipeline.").append(f.getKey()).append(".min").append(":\t").append(min).append("\n");
                    sb.append("pipeline.").append(f.getKey()).append(".max").append(":\t").append(max).append("\n");
                    sb.append("pipeline.").append(f.getKey()).append(".avg").append(":\t").append(avg).append("\n");
                });
        if (!sb.toString().isEmpty()) {
            sb.insert(0, "\n=========================== Starting metrics report ===========================\n");
            sb.append("pipeline.jobIds").append(":\t").append(String.join(",", jobIds)).append("\n");
            sb.append("pipeline.jobParallelism").append(":\t").append(pipelineParallelism).append("\n");
            sb.append("pipeline.jobDurationSum").append(":\t").append(pipelineDuration).append("\n");
            sb.append(new SimpleDateFormat("'report.datetime:\t'dd.MM.yyyy hh:mm:ss'\n'").format(new Date()));
            sb.append("=========================== Finished metrics report ===========================").append("\n");
        }
        return sb.toString();
    }

    private List<String> getJobIdsFromTaskNameHints(List<String> taskNameHints) {
        List<String> jobIds = new ArrayList<>();
        meterReport.forEach((k, v) -> {
            String jobId = k.f0;
            String taskName = k.f1;
            if (!jobIds.contains(jobId) && taskNameHints.stream().allMatch(taskName::contains)) {
                jobIds.add(jobId);
            }
        });
        return jobIds;
    }

    private long getJobDuration(String host, int port, String jobId) {
        try {
            JSONObject response = restAPIGetAsObject(host, port, "/jobs/" + jobId);
            return response.getLong("duration");
        } catch (Exception ignored) {}
        return 0L;
    }

    private int getJobParallelism(String host, int port, String jobId) {
        try {
            JSONObject response = restAPIGetAsObject(host, port, "/jobs/" + jobId + "/config");
            return response.getJSONObject("execution-config").getInt("job-parallelism");
        } catch (Exception ignored) {}
        return 0;
    }

    private boolean isJobRunning(String host, int port) {
        try {
            JSONObject response = restAPIGetAsObject(host, port, "/jobs");
            JSONArray jobs = response.getJSONArray("jobs");
            for (int i = 0; i < jobs.length(); i++) {
                if (jobs.getJSONObject(i).getString("status").equals("RUNNING")) return true;
            }
        } catch (Exception ignored) {}
        return false;
    }

    private static String restAPIGet(String host, int port, String path, String query) {
        String response = null;
        try {
            URIBuilder builder = new URIBuilder();
            builder.setScheme("http");
            builder.setHost(host);
            builder.setPort(port);
            builder.setPath(path);
            builder.setCustomQuery(query);
            URL url = builder.build().toURL();
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            int responseCode = conn.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("HttpResponseCode: " + responseCode);
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                response = br.lines().collect(Collectors.joining());
                conn.disconnect();
            }
        } catch (Exception ignored) {}
        return response;
    }

    private static JSONObject restAPIGetAsObject(String host, int port, String path) throws JSONException {
        return restAPIGetAsObject(host, port, path, null);
    }

    private static JSONObject restAPIGetAsObject(String host, int port, String path, String query)
            throws JSONException {
        String response = restAPIGet(host, port, path, query);
        return response == null ? new JSONObject() : new JSONObject(response);
    }

    private static JSONArray restAPIGetAsArray(String host, int port, String path) throws JSONException {
        return restAPIGetAsArray(host, port, path, null);
    }

    private static JSONArray restAPIGetAsArray(String host, int port, String path, String query) throws JSONException {
        String response = restAPIGet(host, port, path, query);
        return response == null ? new JSONArray() : new JSONArray(response);
    }

    @Override
    public String filterCharacters(String input) {
        return input;
    }

    @Override
    public void open(MetricConfig metricConfig) {
        if (null == meterReport) meterReport = new ConcurrentHashMap<>();
        try {
            URI configFileUri = URI.create(metricConfig.getString("configFileUri", null));
            JSONObject config = new JSONObject(FileUtils.readFileToString(new File(configFileUri)));
            restHost = config.getString("restHost");
            restPort = config.getInt("restPort");
            reportFileUri = URI.create(config.getString("reportFileUri") + "_schedule_report");
            JSONArray taskNameHintConfigs = config.getJSONArray("taskNameHints");
            taskNameHints = new ArrayList<>();
            for (int i = 0; i < taskNameHintConfigs.length(); i++) {
                taskNameHints.add(taskNameHintConfigs.getString(i));
            }
            JSONArray metricNameConfigs = config.getJSONArray("metricNames");
            metricNames = new ArrayList<>();
            for (int i = 0; i < metricNameConfigs.length(); i++) {
                metricNames.add(metricNameConfigs.getString(i));
            }
            taskNameHints = Arrays.asList("TrainDataCreator", "getTrainingInstances");
        } catch (Exception e) {e.printStackTrace();}
    }

    @Override
    public void close() {
        exportReport(reportFileUri, taskNameHints, metricNames);
        meterReport.clear();
        reportTrigger = 0;
    }

    private static class Singleton {
        private static final Singleton inst = new Singleton();

        private Singleton() {
            super();
        }

        public synchronized void writeToFile(URI fileUri, String value) {
            try {
                FileUtils.writeStringToFile(new File(fileUri), value, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public static Singleton getInstance() {
            return inst;
        }

    }
}
