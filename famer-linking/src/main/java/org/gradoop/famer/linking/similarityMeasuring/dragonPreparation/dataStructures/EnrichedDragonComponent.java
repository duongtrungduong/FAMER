package org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.dataStructures;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.DragonComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponentBaseConfig;
import org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions.JsonComponentHelper;
import org.gradoop.famer.trainingModule.classifier.dataStructures.ClassifierComponentBaseConfig;
import org.gradoop.famer.trainingModule.classifier.dataStructures.DragonClassifierComponent;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FMeasureFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FitnessFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FitnessFunction.FitnessFunctionType;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.GiniIndexFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.ErrorEstimatePruner;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.FMeasurePruner;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.PruningFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.PruningFunction.PruningMethod;
import org.gradoop.famer.trainingModule.common.functions.ClassBalancer;
import org.gradoop.famer.trainingModule.common.functions.ClassBalancer.ClassBalanceMethod;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class EnrichedDragonComponent extends DragonComponent {
    /**
     * Name of the class using dragon classifier
     */
    public static final String CLASS_NAME = "isMatch";

    /**
     * Values of the class using dragon binary classifier
     */
//    public static final List<String> CLASS_VALUES = Collections.unmodifiableList(Arrays.asList("true", "false"));
    public static final List<String> CLASS_VALUES = Arrays.asList("true", "false");

    /**
     * How the dragon tree is trained
     */
    private FitnessFunctionType fitnessFunctionType;

    /**
     * How the dragon tree is pruned
     */
    private PruningMethod pruningMethod;

    /**
     * How the training data set is resampled
     */
    private ClassBalanceMethod classBalanceMethod;

    /**
     * Should replacement used for class balancing
     */
    private boolean withReplacement;

    /**
     * Lower bound threshold
     */
    private double lowerThresholdBound;

    /**
     * Rate for lowering the threshold when F-Measure fitness function is used
     */
    private double lowerThresholdRate;

    /**
     * Confidence Level used for error estimating pruning
     */
    private double confidenceLevel;

    /**
     * Max height of the dragon tree
     */
    private int treeMaxHeight;

    /**
     * Min samples at leaf
     */
    private int leafMinSamples;

    /**
     * Similarity components that are or were used to train the tree.
     */
    private List<SimilarityComponent> trainingSimilarityComponents;

    public EnrichedDragonComponent(SimilarityComponentBaseConfig baseConfig,
            ClassBalanceMethod classBalanceMethod,
            FitnessFunctionType fitnessFunctionType, double lowerThresholdBound,
            PruningMethod pruningMethod,
            int treeMaxHeight, int leafMinSamples, List<SimilarityComponent> trainingSimilarityComponents) {
        this(baseConfig, classBalanceMethod, false,
                fitnessFunctionType, lowerThresholdBound, 0d,
                pruningMethod, 0d,
                treeMaxHeight, leafMinSamples, trainingSimilarityComponents);
    }

    public EnrichedDragonComponent(SimilarityComponentBaseConfig baseConfig,
            ClassBalanceMethod classBalanceMethod, boolean withReplacement,
            FitnessFunctionType fitnessFunctionType, double lowerThresholdBound,
            PruningMethod pruningMethod,
            int treeMaxHeight, int leafMinSamples, List<SimilarityComponent> trainingSimilarityComponents) {
        this(baseConfig, classBalanceMethod, withReplacement,
                fitnessFunctionType, lowerThresholdBound, 0d,
                pruningMethod, 0d,
                treeMaxHeight, leafMinSamples, trainingSimilarityComponents);
    }

    public EnrichedDragonComponent(SimilarityComponentBaseConfig baseConfig,
            ClassBalanceMethod classBalanceMethod,
            FitnessFunctionType fitnessFunctionType, double lowerThresholdBound, double lowerThresholdRate,
            PruningMethod pruningMethod,
            int treeMaxHeight, int leafMinSamples, List<SimilarityComponent> trainingSimilarityComponents) {
        this(baseConfig, classBalanceMethod, false,
                fitnessFunctionType, lowerThresholdBound, lowerThresholdRate,
                pruningMethod, 0d,
                treeMaxHeight, leafMinSamples, trainingSimilarityComponents);
    }

    public EnrichedDragonComponent(SimilarityComponentBaseConfig baseConfig,
            ClassBalanceMethod classBalanceMethod, boolean withReplacement,
            FitnessFunctionType fitnessFunctionType, double lowerThresholdBound, double lowerThresholdRate,
            PruningMethod pruningMethod,
            int treeMaxHeight, int leafMinSamples, List<SimilarityComponent> trainingSimilarityComponents) {
        this(baseConfig, classBalanceMethod, withReplacement,
                fitnessFunctionType, lowerThresholdBound, lowerThresholdRate,
                pruningMethod, 0d,
                treeMaxHeight, leafMinSamples, trainingSimilarityComponents);
    }

    public EnrichedDragonComponent(SimilarityComponentBaseConfig baseConfig,
            ClassBalanceMethod classBalanceMethod,
            FitnessFunctionType fitnessFunctionType, double lowerThresholdBound,
            PruningMethod pruningMethod, double confidenceLevel,
            int treeMaxHeight, int leafMinSamples, List<SimilarityComponent> trainingSimilarityComponents) {
        this(baseConfig, classBalanceMethod, false,
                fitnessFunctionType, lowerThresholdBound, 0d,
                pruningMethod, confidenceLevel,
                treeMaxHeight, leafMinSamples, trainingSimilarityComponents);
    }

    public EnrichedDragonComponent(SimilarityComponentBaseConfig baseConfig,
            ClassBalanceMethod classBalanceMethod, boolean withReplacement,
            FitnessFunctionType fitnessFunctionType, double lowerThresholdBound,
            PruningMethod pruningMethod, double confidenceLevel,
            int treeMaxHeight, int leafMinSamples, List<SimilarityComponent> trainingSimilarityComponents) {
        this(baseConfig, classBalanceMethod, withReplacement,
                fitnessFunctionType, lowerThresholdBound, 0d,
                pruningMethod, confidenceLevel,
                treeMaxHeight, leafMinSamples, trainingSimilarityComponents);
    }

    public EnrichedDragonComponent(SimilarityComponentBaseConfig baseConfig,
            ClassBalanceMethod classBalanceMethod,
            FitnessFunctionType fitnessFunctionType, double lowerThresholdBound, double lowerThresholdRate,
            PruningMethod pruningMethod, double confidenceLevel,
            int treeMaxHeight, int leafMinSamples, List<SimilarityComponent> trainingSimilarityComponents) {
        this(baseConfig, classBalanceMethod, false,
                fitnessFunctionType, lowerThresholdBound, lowerThresholdRate,
                pruningMethod, confidenceLevel,
                treeMaxHeight, leafMinSamples, trainingSimilarityComponents);
    }

    public EnrichedDragonComponent(SimilarityComponentBaseConfig baseConfig,
            ClassBalanceMethod classBalanceMethod, boolean withReplacement,
            FitnessFunctionType fitnessFunctionType, double lowerThresholdBound, double lowerThresholdRate,
            PruningMethod pruningMethod, double confidenceLevel,
            int treeMaxHeight, int leafMinSamples, List<SimilarityComponent> trainingSimilarityComponents) {
        super(baseConfig);
        this.fitnessFunctionType = fitnessFunctionType;
        this.pruningMethod = pruningMethod;
        this.classBalanceMethod = classBalanceMethod;
        this.withReplacement = withReplacement;
        this.lowerThresholdBound = lowerThresholdBound;
        this.lowerThresholdRate = lowerThresholdRate;
        this.confidenceLevel = confidenceLevel;
        this.treeMaxHeight = treeMaxHeight;
        this.trainingSimilarityComponents = trainingSimilarityComponents;
    }

    public DragonClassifierComponent buildClassifierComponent() {
        ClassBalancer classBalancer = new ClassBalancer(classBalanceMethod, withReplacement);
        List<String> featureNames = trainingSimilarityComponents.stream()
                .map(SimilarityComponent::getComponentId)
                .collect(Collectors.toList());
        FitnessFunction fitnessFunction = fitnessFunctionType == FitnessFunctionType.GINI_INDEX
                ? new GiniIndexFunction(lowerThresholdBound, leafMinSamples)
                : new FMeasureFunction(lowerThresholdBound, lowerThresholdRate, leafMinSamples);
        PruningFunction treePruner = pruningMethod.equals(PruningMethod.ERROR_ESTIMATE)
                ? new ErrorEstimatePruner(confidenceLevel)
                : new FMeasurePruner();
        JSONArray trainingSimilarityConfigs = new JSONArray();
        try {
            for (SimilarityComponent similarityComponent : trainingSimilarityComponents) {
                JSONObject similarityConfig = JsonComponentHelper.getSimilarityComponentAsJson(similarityComponent);
                trainingSimilarityConfigs.put(similarityConfig);
            }
        } catch (JSONException ex) {
            throw new UnsupportedOperationException("EnrichedDragonComponent: similarity components for training" +
                    " could not be parsed to Json.", ex);
        }
        return new DragonClassifierComponent(new ClassifierComponentBaseConfig(featureNames, CLASS_NAME, CLASS_VALUES,
                classBalancer), fitnessFunction, treePruner, treeMaxHeight, trainingSimilarityConfigs);
    }

    public FitnessFunctionType getFitnessFunctionType() {
        return fitnessFunctionType;
    }

    public void setFitnessFunctionType(FitnessFunctionType fitnessFunctionType) {
        this.fitnessFunctionType = fitnessFunctionType;
    }

    public PruningMethod getPruningMethod() {
        return pruningMethod;
    }

    public void setPruningMethod(PruningMethod pruningMethod) {
        this.pruningMethod = pruningMethod;
    }

    public ClassBalanceMethod getClassBalanceMethod() {
        return classBalanceMethod;
    }

    public void setClassBalanceMethod(ClassBalanceMethod classBalanceMethod) {
        this.classBalanceMethod = classBalanceMethod;
    }

    public boolean isWithReplacement() {
        return withReplacement;
    }

    public void setWithReplacement(boolean withReplacement) {
        this.withReplacement = withReplacement;
    }

    public double getLowerThresholdBound() {
        return lowerThresholdBound;
    }

    public void setLowerThresholdBound(double lowerThresholdBound) {
        this.lowerThresholdBound = lowerThresholdBound;
    }

    public double getLowerThresholdRate() {
        return lowerThresholdRate;
    }

    public void setLowerThresholdRate(double lowerThresholdRate) {
        this.lowerThresholdRate = lowerThresholdRate;
    }

    public double getConfidenceLevel() {
        return confidenceLevel;
    }

    public void setConfidenceLevel(double confidenceLevel) {
        this.confidenceLevel = confidenceLevel;
    }

    public int getTreeMaxHeight() {
        return treeMaxHeight;
    }

    public void setTreeMaxHeight(int treeMaxHeight) {
        this.treeMaxHeight = treeMaxHeight;
    }

    public List<SimilarityComponent> getTrainingSimilarityComponents() {
        return trainingSimilarityComponents;
    }

    public void setTrainingSimilarityComponents(List<SimilarityComponent> trainingSimilarityComponents) {
        this.trainingSimilarityComponents = trainingSimilarityComponents;
    }

    public int getLeafMinSamples() {
        return leafMinSamples;
    }

    public void setLeafMinSamples(int leafMinSamples) {
        this.leafMinSamples = leafMinSamples;
    }
}


