/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.standardBlocking;

import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.operators.UnsortedGrouping;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.java.utils.DataSetUtils;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.methods.BlockingExecutor;
import org.gradoop.famer.linking.blocking.methods.dataStructures.StandardBlockingComponent;
import org.gradoop.famer.linking.blocking.methods.standardBlocking.functions.AddUniqueLongIdToBlock;
import org.gradoop.famer.linking.blocking.methods.standardBlocking.functions.BlockSizeComputer;
import org.gradoop.famer.linking.blocking.methods.standardBlocking.functions.JoinOnBlockingKeyAndAttachBlockInformation;
import org.gradoop.famer.linking.blocking.methods.standardBlocking.functions.JoinOnBlockingKeyPartitionIdAndAttachStartPoint;
import org.gradoop.famer.linking.blocking.methods.standardBlocking.functions.KeyAdder;
import org.gradoop.famer.linking.blocking.methods.standardBlocking.functions.PartitionEntitiesCounter;
import org.gradoop.famer.linking.blocking.methods.standardBlocking.functions.PartitionEnumerationStartPointComputer;
import org.gradoop.famer.linking.blocking.methods.standardBlocking.functions.PartitionIdDiscoverer;
import org.gradoop.famer.linking.blocking.methods.standardBlocking.functions.PrevBlocksPairsAllPairsComputer;
import org.gradoop.famer.linking.blocking.methods.standardBlocking.functions.ReducerIdReplicatorAssigner;
import org.gradoop.famer.linking.blocking.methods.standardBlocking.functions.StandardBlockingPairMaker;
import org.gradoop.famer.linking.blocking.methods.standardBlocking.functions.VertexIndexAssigner;
import org.gradoop.famer.linking.blocking.methods.standardBlocking.functions.VerticesPartitioner;
import org.gradoop.flink.model.impl.functions.tuple.Project2To1;
import org.gradoop.flink.model.impl.functions.tuple.Value0Of2;
import org.gradoop.flink.model.impl.functions.tuple.ValueOf1;

/**
 * The Flink implementation of the PairRange algorithm for Standard Blocking.
 * {@see "https://dbs.uni-leipzig.de/file/ICDE12_conf_full_088.pdf"}
 */
public class StandardBlocking implements BlockingExecutor {

  /**
   * Flag to keep track if a blocking key is already set
   */
  public static final String KEY_ADDED_FLAG = "ka";

  /**
   * Blocking component structure with required parameters and helper methods
   */
  private final StandardBlockingComponent standardBlockingComponent;

  /**
   * Creates an instance of StandardBlocking
   *
   * @param standardBlockingComponent Blocking component structure with required parameters and helper methods
   */
  public StandardBlocking(StandardBlockingComponent standardBlockingComponent) {
    this.standardBlockingComponent = standardBlockingComponent;
  }

  @Override
  public DataSet<Tuple2<EPGMVertex, EPGMVertex>> generatePairedVertices(DataSet<EPGMVertex> vertices) {

    /* Generate keys */
    DataSet<Tuple2<EPGMVertex, String>> vertexBlockingKey =
      vertices.flatMap(standardBlockingComponent.getBlockingKeyGenerator());

    /* Manage empty keys */
    switch (standardBlockingComponent.getEmptyKeyStrategy()) {
    case REMOVE:
      vertexBlockingKey = vertexBlockingKey.filter(tuple -> !tuple.f1.equals(""));
      break;
    case ADD_TO_ALL:
      DataSet<String> allKeys = vertexBlockingKey.map(new Project2To1<>())
        .filter(tuple -> !tuple.f0.equals(""))
        .distinct(0)
        .map(new ValueOf1<>());

      DataSet<EPGMVertex> verticesWithNoKey = vertexBlockingKey.filter(tuple -> tuple.f1.equals(""))
        .map(new Value0Of2<>());
      DataSet<Tuple2<EPGMVertex, String>> verticesWithAddedKeys = verticesWithNoKey.cross(allKeys)
        .with(new KeyAdder());

      vertexBlockingKey = verticesWithAddedKeys.union(vertexBlockingKey);
      break;
    case EMPTY_BLOCK:
    default:
      break;
    }

    /* Prepare block information (size, index, No. of pairs in prev blocks, No. of all pairs) */
    DataSet<Tuple3<EPGMVertex, String, Integer>> vertexBlockingKeyPartitionId = vertexBlockingKey
      .map(new PartitionIdDiscoverer());

    /* Group by PartitionId and BlockingKey, count group entities, group by BlockingKey again */
    UnsortedGrouping<Tuple3<String, Integer, Long>> groupedBlockingKeyPartitionIdsWithCount =
      vertexBlockingKeyPartitionId
        .groupBy(2, 1)
        .combineGroup(new PartitionEntitiesCounter())
        .groupBy(0);

    DataSet<Tuple3<String, Integer, Long>> blockingKeyPartitionIdStartPoint =
      groupedBlockingKeyPartitionIdsWithCount
        .sortGroup(1, Order.ASCENDING)
        .reduceGroup(new PartitionEnumerationStartPointComputer());

    DataSet<Tuple3<EPGMVertex, String, Long>> vertexBlockingKeyIndex =
      blockingKeyPartitionIdStartPoint.join(vertexBlockingKeyPartitionId)
        .where(0, 1).equalTo(1, 2)
        .with(new JoinOnBlockingKeyPartitionIdAndAttachStartPoint())
        .groupBy(1, 2)
        .reduceGroup(new VertexIndexAssigner());

    /* Compute block sizes for each BlockingKey */
    DataSet<Tuple2<String, Long>> blockingKeyBlockSize =
      groupedBlockingKeyPartitionIdsWithCount.reduceGroup(new BlockSizeComputer());

    /* Generate a unique long id for each block and add it to the tuples */
    DataSet<Tuple3<String, Long, Long>> blockingKeyBlockSizeIndex =
      DataSetUtils.zipWithUniqueId(blockingKeyBlockSize).map(new AddUniqueLongIdToBlock());

    /* Use reduceGroup here to have access to all blockingKeyBlockSizeIndex tuples at once */
    DataSet<Tuple5<String, Long, Long, Long, Long>> blockingKeyBlockSizeIndexPrevBlocksPairsAllPairs =
      blockingKeyBlockSizeIndex.reduceGroup(new PrevBlocksPairsAllPairsComputer());

    /* Attach provided information (BlockIndex, BlockSize, PrevBlockPairs, allPairs) to each vertex */
    /* Load Balancing: replicates vertexBlockingKeys and assigns them to a partition in a balanced way
       - i.e. roughly the same number of pairs on each partition/reducer */
    int reducerNo = standardBlockingComponent.getParallelismDegree();

    DataSet<Tuple5<EPGMVertex, String, Long, Boolean, Integer>> vertexKeyIndexIsLastReducerId =
      vertexBlockingKeyIndex.join(blockingKeyBlockSizeIndexPrevBlocksPairsAllPairs)
        .where(1).equalTo(0)
        .with(new JoinOnBlockingKeyAndAttachBlockInformation())
        .flatMap(new ReducerIdReplicatorAssigner(reducerNo));

    vertexKeyIndexIsLastReducerId = vertexKeyIndexIsLastReducerId
      .partitionCustom(new VerticesPartitioner(), 4);

    /* Make and return pairs */
    return vertexKeyIndexIsLastReducerId
      .groupBy(1)
      .sortGroup(2, Order.ASCENDING)
      .combineGroup(new StandardBlockingPairMaker(standardBlockingComponent));
  }
}
