/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.methods.BlockingExecutor;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponent;

import java.io.Serializable;

/**
 * Generate Vertex pairs based on the specified blocking method
 */
public class BlockMaker implements Serializable {

  /**
   * Blocking component structure with required parameters and helper methods
   */
  private final BlockingComponent blockingComponent;

  /**
   * Creates an instance of BlockMaker
   *
   * @param blockingComponent Blocking component structure with required parameters and helper methods
   */
  public BlockMaker(BlockingComponent blockingComponent) {
    this.blockingComponent = blockingComponent;
  }

  /**
   * Executes the block making on the given vertices with the given blocking component.
   *
   * @param vertices The input vertices
   *
   * @return DataSet with tuples of paired vertices
   */
  public DataSet<Tuple2<EPGMVertex, EPGMVertex>> execute(DataSet<EPGMVertex> vertices) {
    BlockingExecutor blockingExecutor = blockingComponent.buildBlockingExecutor();
    return blockingExecutor.generatePairedVertices(vertices);
  }
}
