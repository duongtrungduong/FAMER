/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.sortedNeighborhood;

import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.aggregation.Aggregations;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.methods.BlockingExecutor;
import org.gradoop.famer.linking.blocking.methods.dataStructures.SortedNeighborhoodComponent;
import org.gradoop.famer.linking.blocking.methods.sortedNeighborhood.functions.AttachMaxPositionToVertexBlockingKeyPosition;
import org.gradoop.famer.linking.blocking.methods.sortedNeighborhood.functions.ExtendVertexBlockingKeyWithPosition;
import org.gradoop.famer.linking.blocking.methods.sortedNeighborhood.functions.ReducerIdAssigner;
import org.gradoop.famer.linking.blocking.methods.sortedNeighborhood.functions.ReplacePositionWithBlockingKeyOrderPosition;
import org.gradoop.famer.linking.blocking.methods.sortedNeighborhood.functions.Replicator;
import org.gradoop.famer.linking.blocking.methods.sortedNeighborhood.functions.SortedNeighborhoodPairMaker;
import org.gradoop.flink.model.impl.functions.tuple.Value2Of3;

/**
 * The Flink implementation of the RepSN algorithm for Sorted Neighborhood Blocking.
 * {@see "https://dbs.uni-leipzig.de/file/parallel_sn_blocking_mr.pdf"}
 */
public class SortedNeighborhood implements BlockingExecutor {

  /**
   * Blocking component structure with required parameters and helper methods
   */
  private final SortedNeighborhoodComponent sortedNeighborhoodComponent;

  /**
   * Creates an instance of SortedNeighborhood
   *
   * @param sortedNeighborhoodComponent Blocking component structure with required parameters and helper
   *                                    methods
   */
  public SortedNeighborhood(SortedNeighborhoodComponent sortedNeighborhoodComponent) {
    this.sortedNeighborhoodComponent = sortedNeighborhoodComponent;
  }

  @Override
  public DataSet<Tuple2<EPGMVertex, EPGMVertex>> generatePairedVertices(DataSet<EPGMVertex> vertices) {

    int windowSize = sortedNeighborhoodComponent.getWindowSize();
    int reducerNo = sortedNeighborhoodComponent.getParallelismDegree();

    // 1. Generate key for each vertex
    // In order to have a unique order of entities for all runs, vertex id is added to each key
    DataSet<Tuple2<EPGMVertex, String>> vertexBlockingKey = vertices
      .flatMap(sortedNeighborhoodComponent.getBlockingKeyGenerator())
      .map(tuple -> {
        String blockingKey = tuple.f1;
        tuple.f1 = blockingKey + tuple.f0.getId().toString();
        return tuple;
      }).returns(new TypeHint<Tuple2<EPGMVertex, String>>() { });

    // 2. Split vertices on reducers evenly
    // Assign vertices a number from [0, n-1] in the order of keys
    DataSet<Tuple3<EPGMVertex, String, Long>> vertexKeyPosition = vertexBlockingKey
      .map(new ExtendVertexBlockingKeyWithPosition())
      .groupBy(2)
      .sortGroup(1, Order.ASCENDING)
      .reduceGroup(new ReplacePositionWithBlockingKeyOrderPosition());

    // Attach global position of each (the assigned number) and the number of all vertices to each vertex key
    DataSet<Long> maxPosition = vertexKeyPosition.aggregate(Aggregations.MAX, 2).map(new Value2Of3<>());

    DataSet<Tuple4<EPGMVertex, String, Long, Long>> vertexKeyPositionMaxPos =
      vertexKeyPosition.cross(maxPosition).with(new AttachMaxPositionToVertexBlockingKeyPosition());

    /* Each vertex key is assigned to a reducer based on its global position, the number of all vertices
    and the number of reducers */
    DataSet<Tuple3<EPGMVertex, String, Integer>> vertexKeyReducerId =
      vertexKeyPositionMaxPos.map(new ReducerIdAssigner(reducerNo, windowSize));

    // 3. Replicate "windowSize-1" vertices of each reducer for the next reducer
    DataSet<Tuple4<EPGMVertex, String, Integer, Boolean>> vertexKeyReducerIdIsReplicated = vertexKeyReducerId
      .groupBy(2)
      .sortGroup(1, Order.ASCENDING)
      .reduceGroup(new Replicator(reducerNo, windowSize));

    // 4. Create pairs of vertices inside each reducer by sliding down a window
    return vertexKeyReducerIdIsReplicated
      .groupBy(2)
      .sortGroup(1, Order.ASCENDING)
      .reduceGroup(new SortedNeighborhoodPairMaker(sortedNeighborhoodComponent));
  }
}
