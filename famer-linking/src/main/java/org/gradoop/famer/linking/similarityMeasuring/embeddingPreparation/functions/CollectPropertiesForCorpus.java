/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.embeddingPreparation.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.EmbeddingComponent;

import static org.gradoop.famer.linking.linking.Linker.GRAPH_LABEL;

/**
 * Collect and process words that are written to temporary corpus
 */
public class CollectPropertiesForCorpus implements FlatMapFunction<EPGMVertex, String> {

  /**
   * The embedding component structure
   */
  private final EmbeddingComponent embeddingComponent;

  /**
   * Creates an instance of CollectPropertiesForCorpus
   *
   * @param embeddingComponent The embedding component structure
   */
  public CollectPropertiesForCorpus(EmbeddingComponent embeddingComponent) {
    this.embeddingComponent = embeddingComponent;
  }

  @Override
  public void flatMap(EPGMVertex vertex, Collector<String> out) {
    String graphLabel = vertex.getPropertyValue(GRAPH_LABEL).getString();

    if (embeddingComponent.getSourceGraph().equals(graphLabel) ||
      embeddingComponent.getSourceLabel().equals("*")) {
      if (embeddingComponent.getSourceLabel().equals(vertex.getLabel())) {
        PropertyValue propVal = vertex.getPropertyValue(embeddingComponent.getSourceAttribute());
        if (propVal != null) {
          out.collect(propVal.getString().toLowerCase());
        }
      }
    } else if (embeddingComponent.getTargetGraph().equals(graphLabel) ||
      embeddingComponent.getTargetGraph().equals("*")) {
      if (embeddingComponent.getTargetLabel().equals(vertex.getLabel())) {
        PropertyValue propVal = vertex.getPropertyValue(embeddingComponent.getTargetAttribute());
        if (propVal != null) {
          out.collect(propVal.getString().toLowerCase());
        }
      }
    }
  }
}
