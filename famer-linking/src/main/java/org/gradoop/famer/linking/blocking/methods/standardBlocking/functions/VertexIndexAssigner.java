/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.standardBlocking.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Reduces a group of {@code Tuple4<Vertex, BlockingKey, PartitionId, StartPoint>}, grouped by BlockingKey
 * and PartitionId, and returns a {@code  Tuple3<Vertex, BlockingKey, Index>}. The index base starts at 0,
 * the StartPoint is added and this sum is assigned to the result tuple as Index. For each group item,
 * the base of the index is incremented by 1.
 */
@FunctionAnnotation.ForwardedFields("f0;f1")
public class VertexIndexAssigner implements
  GroupReduceFunction<Tuple4<EPGMVertex, String, Integer, Long>, Tuple3<EPGMVertex, String, Long>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<EPGMVertex, String, Long> reuseTuple = new Tuple3<>();

  @Override
  public void reduce(Iterable<Tuple4<EPGMVertex, String, Integer, Long>> group,
    Collector<Tuple3<EPGMVertex, String, Long>> out) {
    long count = 0L;
    for (Tuple4<EPGMVertex, String, Integer, Long> groupItem : group) {
      reuseTuple.f0 = groupItem.f0;
      reuseTuple.f1 = groupItem.f1;
      reuseTuple.f2 = count + groupItem.f3;
      out.collect(reuseTuple);
      count++;
    }
  }
}
