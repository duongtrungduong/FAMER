/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.linking.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMEdgeFactory;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.incremental.parallelIncremental.common.utils.IncrementalUtils;

import static org.gradoop.famer.incremental.common.PropertyNames.NEIGHBOR;
import static org.gradoop.famer.incremental.common.PropertyNames.NEW_LINK;

/**
 * Creates an edge for an input Tuple3<Vertex, Vertex, SimilarityDegree> and assigns a property with the given
 * {@link #propertyKey} to the created edge with the SimilarityDegree as the property value.
 */
@FunctionAnnotation.ReadFields("f0;f1;f2")
public class LinkMaker implements MapFunction<Tuple3<EPGMVertex, EPGMVertex, Double>, EPGMEdge> {

  /**
   * Factory for creating Gradoop edges
   */
  private final EPGMEdgeFactory edgeFactory;

  /**
   * The property that keeps the similarity value of an edge
   */
  private final String propertyKey;

  /**
   * The parameter that specifies whether the neighbor cluster of the new entity (vertex) should be stored
   * as a propertyValue on the link. Needed for incremental module.
   */
  private final boolean tagNeighborCluster;

  /**
   * Whether the link between newly added entities (vertices) should be specified. Needed for incremental
   * module.
   */
  private final boolean tagNewLink;

  /**
   * Creates an instance of LinkMaker
   *
   * @param edgeFactory The Gradoop edge factory
   * @param propertyKey The property that keeps the similarity value of an edge
   * @param tagNeighborCluster The parameter that specifies whether the neighbor cluster of the new entity
   *                           (vertex) should be stored as a propertyValue on the link
   * @param tagNewLink The parameter that specifies whether the link between newly added entities
   *                      (vertices) should be specified
   */
  public LinkMaker(EPGMEdgeFactory edgeFactory, String propertyKey, boolean tagNeighborCluster,
    boolean tagNewLink) {
    this.edgeFactory = edgeFactory;
    this.propertyKey = propertyKey;
    this.tagNeighborCluster = tagNeighborCluster;
    this.tagNewLink = tagNewLink;
  }

  @Override
  public EPGMEdge map(Tuple3<EPGMVertex, EPGMVertex, Double> vertexPairWithSimDeg) {
    EPGMEdge edge = edgeFactory.createEdge(vertexPairWithSimDeg.f0.getId(), vertexPairWithSimDeg.f1.getId());
    edge.setProperty(propertyKey, vertexPairWithSimDeg.f2);

    // prepare properties for incremental module
    int linkStatus = IncrementalUtils.getLinkStatus(vertexPairWithSimDeg.f0, vertexPairWithSimDeg.f1);
    // write already existing cluster id of either source or target to edge
    if (tagNeighborCluster && (linkStatus == 1)) {
      String clusterId =
        IncrementalUtils.getNeighborClusterId(vertexPairWithSimDeg.f0, vertexPairWithSimDeg.f1);
      edge.setProperty(NEIGHBOR, clusterId);
    }
    // tag edge as new link, if source and target are new and unclustered entities
    if (tagNewLink && (linkStatus == 0)) {
      edge.setProperty(NEW_LINK, true);
    }

    return edge;
  }
}
