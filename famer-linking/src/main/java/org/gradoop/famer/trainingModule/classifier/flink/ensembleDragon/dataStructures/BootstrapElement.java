package org.gradoop.famer.trainingModule.classifier.flink.ensembleDragon.dataStructures;

import org.apache.flink.api.java.tuple.Tuple4;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;

import java.util.Arrays;
import java.util.stream.Collectors;

public class BootstrapElement extends Tuple4<String, Integer, int[], BinaryInstance> {
    private static final String ID_CONCAT_CHAR = ":";

    public BootstrapElement() {
    }

    public BootstrapElement(int bootstrapIndex, int[] ensembleIndices, BinaryInstance binaryInstance) {
        super(generateId(bootstrapIndex, ensembleIndices), bootstrapIndex, ensembleIndices, binaryInstance);
    }

    public void setId(String id) {
        f0 = id;
    }

    public String getId() {
        return f0;
    }

    public void setBootstrapIndex(int bootstrapIndex) {
        f1 = bootstrapIndex;
    }

    public int getBootstrapIndex() {
        return f1;
    }

    public void setEnsembleIndices(int[] ensembleIndices) {
        f2 = ensembleIndices;
    }

    public int[] getEnsembleIndices() {
        return f2;
    }

    public void setBinaryInstance(BinaryInstance binaryInstance) {
        f3 = binaryInstance;
    }

    public BinaryInstance getBinaryInstance() {
        return f3;
    }

    public static String generateId(int bootstrapIndex, int[] ensembleIndices) {
        return bootstrapIndex + ID_CONCAT_CHAR +
                Arrays.stream(ensembleIndices).boxed().map(String::valueOf).collect(Collectors.joining(ID_CONCAT_CHAR));
    }
}
