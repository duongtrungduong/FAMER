package org.gradoop.famer.trainingModule.common.functions;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;
import org.apache.flink.api.common.functions.FlatJoinFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.Utils;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.DataSetUtils;
import org.gradoop.famer.trainingModule.common.dataStructures.CommonInstance;
import org.gradoop.famer.trainingModule.common.dataStructures.TrainTestDataSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TrainTestSplitter {

    public static Tuple2<DataSet<CommonInstance>, DataSet<CommonInstance>> randomSplit(DataSet<CommonInstance> input,
            double fraction, boolean precise) throws Exception {
        DataSet<Tuple2<Long, CommonInstance>> inputWithId = DataSetUtils.zipWithUniqueId(input);

        if ((fraction >= 1) || (fraction <= 0)) {
            throw new IllegalArgumentException("sampling fraction outside of (0,1) bounds is nonsensical");
        }

        long seed = Utils.RNG.nextLong();
        DataSet<Tuple2<Long, CommonInstance>> leftSplit;
        if (precise) {
            int numSamples = Math.round((float) fraction * inputWithId.count());
            leftSplit = DataSetUtils.sampleWithSize(inputWithId, false, numSamples, seed);
        } else {
            leftSplit = DataSetUtils.sample(inputWithId, false, fraction, seed);
        }

        DataSet<Tuple2<Long, CommonInstance>> rightSplit = inputWithId.leftOuterJoin(leftSplit)
                .where(0).equalTo(0)
                .with((FlatJoinFunction<Tuple2<Long, CommonInstance>, Tuple2<Long, CommonInstance>,
                        Tuple2<Long, CommonInstance>>) (full, left, collector) -> {
                    if (left == null) collector.collect(full);
                })
                .returns(new TypeHint<Tuple2<Long, CommonInstance>>() {});

        return Tuple2.of(leftSplit.map(m -> m.f1), rightSplit.map(m -> m.f1));
    }

    public static List<DataSet<CommonInstance>> multiRandomSplit(DataSet<CommonInstance> input, List<Double> fractions) {
        List<DataSet<CommonInstance>> randomSplits = new ArrayList<>();

        EnumeratedIntegerDistribution eid =
                new EnumeratedIntegerDistribution(IntStream.range(0, fractions.size()).toArray(),
                        fractions.stream().mapToDouble(m -> m).toArray());
        eid.reseedRandomGenerator(Utils.RNG.nextLong());
        DataSet<Tuple2<Integer, CommonInstance>> tmpDataSet = input
                .map(m -> Tuple2.of(eid.sample(), m))
                .returns(new TypeHint<Tuple2<Integer, CommonInstance>>() {});

        IntStream.range(0, fractions.size()).forEach(i ->
                randomSplits.add(tmpDataSet.filter(f -> f.f0 == i).map(m -> m.f1)));

        return randomSplits;
    }

    public static List<TrainTestDataSet> kFoldSplit(DataSet<CommonInstance> input, int k) {
        List<Double> fractions = new ArrayList<>(Collections.nCopies(k, 1.0d));
        List<DataSet<CommonInstance>> chunkDataSet = multiRandomSplit(input, fractions);

        return chunkDataSet.stream().map(m ->
                new TrainTestDataSet(chunkDataSet.stream()
                        .filter(f -> f != m)
                        .reduce(DataSet::union)
                        .orElseThrow(() -> new IllegalStateException("Could not create k-fold dataset")), m))
                .collect(Collectors.toList());
    }

    public static TrainTestDataSet trainTestSplit(DataSet<CommonInstance> input, double fraction, boolean precise)
            throws Exception {
        Tuple2<DataSet<CommonInstance>, DataSet<CommonInstance>> trainTestPair = randomSplit(input, fraction, precise);
        return new TrainTestDataSet(trainTestPair.f0, trainTestPair.f1);
    }
}