package org.gradoop.famer.trainingModule.classifier.flink.ensembleDragon.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.famer.trainingModule.classifier.methods.DragonClassifier;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;
import org.gradoop.famer.trainingModule.common.dataStructures.CommonInstance;

import java.util.List;

public class EnsembleDragonPredictor implements MapFunction<CommonInstance, CommonInstance> {
    private final List<DragonClassifier> dragonClassifiers;

    public EnsembleDragonPredictor(List<DragonClassifier> dragonClassifiers) {
        this.dragonClassifiers = dragonClassifiers;
    }

    @Override
    public CommonInstance map(CommonInstance unlabeledInstance) throws Exception {
        return predict(unlabeledInstance, dragonClassifiers);
    }

    public static BinaryInstance predict(CommonInstance unlabeledInstance, List<DragonClassifier> dragonClassifiers)
            throws Exception {
        double probabilitiesSum = 0;
        for (DragonClassifier dragonClassifier : dragonClassifiers) {
            BinaryInstance labeledInstance = (BinaryInstance) dragonClassifier.predict(unlabeledInstance);
            if (labeledInstance.isTrue()) {
                probabilitiesSum += labeledInstance.getProbability();
            } else {
                probabilitiesSum += (1 - labeledInstance.getProbability());
            }
        }
        double probability = probabilitiesSum/dragonClassifiers.size();
        return new BinaryInstance(unlabeledInstance.getFeatures(), probability >= 0.5, probability);
    }

//    public static BinaryInstance predict(CommonInstance unlabeledInstance, List<DragonClassifier> dragonClassifiers)
//            throws Exception {
//        int numTrueVotes = 0;
//        int totalVotes = dragonClassifiers.size();
//        for (DragonClassifier dragonClassifier : dragonClassifiers) {
//            if (dragonClassifier.predict(unlabeledInstance).getLabel() == BinaryInstance.TRUE) numTrueVotes++;
//        }
////        boolean label = numTrueVotes >= (totalVotes - numTrueVotes);
//        boolean label = ((double)numTrueVotes/totalVotes) >= 0.1d;
//        double probability = label ? (double) numTrueVotes/totalVotes: 1d - (double) numTrueVotes/totalVotes;
//        return new BinaryInstance(unlabeledInstance.getFeatures(), label, probability);
//    }

    @Deprecated
    public static BinaryInstance predict(CommonInstance unlabeledInstance, List<DragonClassifier> dragonClassifiers,
            int minTrueVotes) throws Exception {
        int numTrueVotes = 0;
        int totalVotes = dragonClassifiers.size();
        for (DragonClassifier dragonClassifier : dragonClassifiers) {
            if (dragonClassifier.predict(unlabeledInstance).getLabel() == BinaryInstance.TRUE) numTrueVotes++;
        }
        boolean label = numTrueVotes >= minTrueVotes;
        double probability = label ? (double) numTrueVotes/totalVotes: 1d - (double) numTrueVotes/totalVotes;
        return new BinaryInstance(unlabeledInstance.getFeatures(), label, probability);
    }
}
