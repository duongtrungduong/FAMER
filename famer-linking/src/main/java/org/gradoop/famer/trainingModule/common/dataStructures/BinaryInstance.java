package org.gradoop.famer.trainingModule.common.dataStructures;

import java.util.Vector;

/***
 * A machine learning instance. This instance stores the features and their probability. The associated
 * labels to the features are stored as binary. If a label exists, the default value is 1.0, else the default
 * value is -1.0.
 */
public class BinaryInstance extends CommonInstance {

  /**
   * The default label value if a label exist.
   */
  public static final double TRUE = 1.0d;

  /**
   * The default label value if a label does not exist.
   */
  public static final double FALSE = -1.0d;

  /**
   * Default constructor for Flink POJO
   */
  public BinaryInstance() {
  }

  /**
   * Construct a {@link BinaryInstance} for the given features.
   *
   * @param features The features of the instance.
   */
  public BinaryInstance(Vector<Double> features) {
    super(features);
  }

  /**
   * Construct a {@link BinaryInstance} for the given parameter.
   *
   * @param features The features of the instance.
   * @param label If a label exist, the value is 1.0, else the value is -1.0.
   * @param probability The probability of the instance.
   */
  public BinaryInstance(Vector<Double> features, boolean label, double probability) {
    super(features, label ? TRUE : FALSE, probability);
  }

  /**
   * Get the label of the instance if it exist.
   *
   * @return The value of the label.
   */
  public boolean isTrue() {
    assert isLabeled();
    return TRUE == getLabel();
  }

  /**
   * Creating an identical copy of the instance.
   *
   * @return The copy of the instance.
   */
  @Override
  public BinaryInstance duplicate() {
    return new BinaryInstance(new Vector<>(getFeatures()), isTrue(), getProbability());
  }
}
