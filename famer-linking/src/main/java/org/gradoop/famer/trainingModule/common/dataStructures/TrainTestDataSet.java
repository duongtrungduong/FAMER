package org.gradoop.famer.trainingModule.common.dataStructures;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.LibsvmUtils;

import java.io.Serializable;
import java.net.URI;
import java.util.List;

public class TrainTestDataSet implements Serializable {
    public final String TRAIN_DATASET_DIR = "trainDs";
    public final String TEST_DATASET_DIR = "testDs";

    private final DataSet<CommonInstance> trainingDataSet;
    private final DataSet<CommonInstance> testingDataSet;

    public TrainTestDataSet(ExecutionEnvironment env, URI fileUri) {
        try {
            this.trainingDataSet = LibsvmUtils.readFromLibsvm(env, URI.create(fileUri + "/" + TRAIN_DATASET_DIR));
            this.testingDataSet = LibsvmUtils.readFromLibsvm(env, URI.create(fileUri + "/" + TEST_DATASET_DIR));
        } catch (Exception e) {
            throw new RuntimeException("Could not read train and test dataset from file.", e);
        }
    }

    public TrainTestDataSet(DataSet<CommonInstance> trainingDataSet, DataSet<CommonInstance> testingDataSet) {
        this.trainingDataSet = trainingDataSet;
        this.testingDataSet = testingDataSet;
    }

    public Tuple2<List<CommonInstance>, List<CommonInstance>> getAsList() throws Exception {
        return Tuple2.of(trainingDataSet.collect(), testingDataSet.collect());
    }

    public void writeAsLibsvm(ExecutionEnvironment env, URI fileUri) throws Exception {
        LibsvmUtils.writeAsLibsvm(trainingDataSet, URI.create(fileUri + "/" + TRAIN_DATASET_DIR), true);
        LibsvmUtils.writeAsLibsvm(testingDataSet, URI.create(fileUri + "/" + TEST_DATASET_DIR), true);
        env.execute();
    }

    public DataSet<CommonInstance> getTrainingDataSet() {
        return trainingDataSet;
    }

    public DataSet<CommonInstance> getTestingDataSet() {
        return testingDataSet;
    }
}
