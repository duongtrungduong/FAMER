package org.gradoop.famer.trainingModule.classifier.dataStructures;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.common.functions.ClassBalancer;

import java.util.ArrayList;
import java.util.List;

public class ClassifierComponentBaseConfig {
    /**
     * The names of the (numeric) features.
     */
    private final List<String> featureNames;

    /**
     * The name of the class (e.g. isMatch).
     */
    private final String className;

    /**
     * The values of the class (e.g. true/false).
     */
    private final List<String> classValues;

    /**
     * Define the balancing techniques to handle imbalanced data.
     */
    private final ClassBalancer classBalancer;

    public ClassifierComponentBaseConfig(JSONObject jsonConfig) {
        try {
            this.featureNames = new ArrayList<>();
            JSONArray jsonFeatureNames = jsonConfig.getJSONArray("featureNames");
            for (int i = 0; i < jsonFeatureNames.length(); i++) {
                this.featureNames.add(jsonFeatureNames.getString(i));
            }
        } catch (Exception ex) {
            throw new UnsupportedOperationException("Value for featureNames in classifier component could not be " +
                    "found or parsed to list of strings.", ex);
        }
        try {
            this.className = jsonConfig.getString("className");
        } catch (Exception ex) {
            throw new UnsupportedOperationException("Value for className in classifier component could not be " +
                    "found or parsed to string.", ex);
        }
        try {
            this.classValues = new ArrayList<>();
            JSONArray jsonClassValues = jsonConfig.getJSONArray("classValues");
            for (int i = 0; i < jsonClassValues.length(); i++) {
                this.classValues.add(jsonClassValues.getString(i));
            }
        } catch (Exception ex) {
            throw new UnsupportedOperationException("Value for classValues in classifier component could not be " +
                    "found or parsed to list of strings.", ex);
        }
        try {
            this.classBalancer = new ClassBalancer(jsonConfig.getJSONObject("classBalancer"));
        } catch (Exception ex) {
            throw new UnsupportedOperationException("Value for balanceMethod in classifier component could not be " +
                    "found or parsed to string.", ex);
        }
    }

    /**
     * Creates a {@link ClassifierComponentBaseConfig} based on the given parameter.
     *
     * @param featureNames The names of the features.
     * @param className The name of the class attribute.
     * @param classValues classValues THe values of the class attribute.
     * @param classBalancer The class balancer.
     */
    public ClassifierComponentBaseConfig(List<String> featureNames, String className, List<String> classValues,
            ClassBalancer classBalancer) {
        this.featureNames = featureNames;
        this.className = className;
        this.classValues = classValues;
        this.classBalancer = classBalancer;
    }

    public List<String> getFeatureNames() {
        return featureNames;
    }

    public String getClassName() {
        return className;
    }

    public List<String> getClassValues() {
        return classValues;
    }

    public ClassBalancer getClassBalancer() {
        return classBalancer;
    }
}
