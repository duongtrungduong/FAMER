package org.gradoop.famer.trainingModule.common.functions;

import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.DataSetUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.common.dataStructures.CommonInstance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ClassBalancer implements Serializable {
    /**
     * Class balancer methods that can be used.
     */
    public enum ClassBalanceMethod {
        /**
         * No method
         */
        NONE,

        /**
         * Oversampling method for oversampling instances of the minority class
         */
        OVER_SAMPLING,

        /**
         * Undersampling method for undersampling instances of the majority class
         */
        UNDER_SAMPLING,

        /**
         * Combined sampling method
         */
        COMBINED_SAMPLING
    }

    private final ClassBalanceMethod classBalanceMethod;
    private boolean withReplacement;
    private double fraction;

    public ClassBalancer(JSONObject jsonConfig) {
        try {
            this.classBalanceMethod = ClassBalanceMethod.valueOf(jsonConfig.getString("classBalanceMethod"));
            if (jsonConfig.has("withReplacement")) {
                this.withReplacement = jsonConfig.getBoolean("withReplacement");
            }
            if (jsonConfig.has("fraction")) {
                this.fraction = jsonConfig.getDouble("fraction");
            }
        } catch (JSONException ex) {
            throw new UnsupportedOperationException("ClassBalancer: value for classBalancer could " +
                    "not be found or parsed.", ex);
        }
    }

    public ClassBalancer(ClassBalanceMethod classBalanceMethod) {
        this(classBalanceMethod, false, 0d);
    }

    public ClassBalancer(ClassBalanceMethod classBalanceMethod, boolean withReplacement) {
        this(classBalanceMethod, withReplacement, 0d);
    }

    public ClassBalancer(ClassBalanceMethod classBalanceMethod, boolean withReplacement, double fraction) {
        this.classBalanceMethod = classBalanceMethod;
        this.withReplacement = withReplacement;
        this.fraction = fraction;
    }

    public DataSet<CommonInstance> resample(DataSet<CommonInstance> input) throws Exception {
        if (fraction == 0) {
            return resample(input, classBalanceMethod, withReplacement);
        } else {
            return resample(input, classBalanceMethod, withReplacement, fraction);
        }
    }

    private DataSet<CommonInstance> resample(DataSet<CommonInstance> input, ClassBalanceMethod method,
            boolean withReplacement) throws Exception {

        if (method == ClassBalanceMethod.NONE) return input;

        List<Tuple2<Double, Long>> labelWithCountList = getLabelWithCount(input);
        Tuple2<Double, Long> labelWithMinCount = getLabelWithMinCount(labelWithCountList);
        Tuple2<Double, Long> labelWithMaxCount = getLabelWithMaxCount(labelWithCountList);
        int sampleSize;
        switch (method) {
            case UNDER_SAMPLING: sampleSize = labelWithMinCount.f1.intValue(); break;
            case OVER_SAMPLING: sampleSize = labelWithMaxCount.f1.intValue(); break;
            case COMBINED_SAMPLING: sampleSize = Math.round((labelWithMinCount.f1 + labelWithMaxCount.f1) / 2f); break;
            default: return input;
        }
        return buildResampledDataSet(input, labelWithCountList, sampleSize, withReplacement);
    }

    private DataSet<CommonInstance> resample(DataSet<CommonInstance> input, ClassBalanceMethod method,
            boolean withReplacement, double fraction) throws Exception {

        if (method == ClassBalanceMethod.NONE) return input;

        if (fraction <= 0 || fraction > 1) throw new IllegalArgumentException("Fraction must be in (0,1].");

        List<Tuple2<Double, Long>> labelWithCountList = getLabelWithCount(input);
        Tuple2<Double, Long> labelWithMinCount = getLabelWithMinCount(labelWithCountList);
        Tuple2<Double, Long> labelWithMaxCount = getLabelWithMaxCount(labelWithCountList);
        List<Tuple2<Double, Double>> labelWithFractionList = new ArrayList<>();
        switch (method) {
            case UNDER_SAMPLING:
                long minCount = labelWithMinCount.f1;
                for (Tuple2<Double, Long> labelWithCount : labelWithCountList) {
                    double fractionForLabel = minCount / (fraction * labelWithCount.f1);
                    if (fractionForLabel > 1.0) fractionForLabel = (double) minCount / labelWithCount.f1;
                    if (labelWithCount.f1 == minCount) fractionForLabel = 1.0;
                    labelWithFractionList.add(Tuple2.of(labelWithCount.f0, fractionForLabel));
                }
                break;
            case OVER_SAMPLING:
                long maxCount = labelWithMaxCount.f1;
                for (Tuple2<Double, Long> labelWithCount : labelWithCountList) {
                    double fractionForLabel = maxCount * fraction / labelWithCount.f1;
                    if (fractionForLabel < 1.0 || labelWithCount.f1 == maxCount) fractionForLabel = 1.0;
                    labelWithFractionList.add(Tuple2.of(labelWithCount.f0, fractionForLabel));
                }
                break;
            case COMBINED_SAMPLING:
                throw new UnsupportedOperationException("Combined sampling is not supported with faction mode.");
            default: return input;
        }
        return buildResampledDataSet(input, labelWithFractionList, withReplacement);
    }

    private DataSet<CommonInstance> buildResampledDataSet(DataSet<CommonInstance> input,
            List<Tuple2<Double, Long>> labelWithCountList, int sampleSize, boolean withReplacement) {
        DataSet<CommonInstance> resampledDataSet = null;
        for (Tuple2<Double, Long> labelWithCount : labelWithCountList) {
            double label = labelWithCount.f0;
            long count = labelWithCount.f1;
            DataSet<CommonInstance> tmpDs;
            if (count == sampleSize) {
                tmpDs = input.filter(f -> f.getLabel() == label);
            } else {
                boolean shouldReplace = count < sampleSize || withReplacement;
                tmpDs = DataSetUtils.sampleWithSize(input.filter(f -> f.getLabel() == label), shouldReplace, sampleSize);
            }
            resampledDataSet = resampledDataSet == null ? tmpDs : resampledDataSet.union(tmpDs);
        }
        return resampledDataSet;
    }

    private DataSet<CommonInstance> buildResampledDataSet(DataSet<CommonInstance> input,
            List<Tuple2<Double, Double>> labelWithFractionList, boolean withReplacement) {

        DataSet<CommonInstance> resampledDataSet = null;
        for (Tuple2<Double, Double> labelWithFraction : labelWithFractionList) {
            double label = labelWithFraction.f0;
            double fraction = labelWithFraction.f1;
            DataSet<CommonInstance> tmpDs;
            if (fraction == 1.0) {
                tmpDs = input.filter(f -> f.getLabel() == label);
            } else {
                boolean shouldReplace = fraction > 1.0 || withReplacement;
                tmpDs = DataSetUtils.sample(input.filter(f -> f.getLabel() == label), shouldReplace, fraction);
            }
            resampledDataSet = resampledDataSet == null ? tmpDs : resampledDataSet.union(tmpDs);
        }
        return resampledDataSet;

    }

    private List<Tuple2<Double, Long>> getLabelWithCount(DataSet<CommonInstance> input) throws Exception {
        return input.map(m -> Tuple2.of(m.getLabel(), 1L))
                .returns(new TypeHint<Tuple2<Double, Long>>() {})
                .groupBy(0)
                .sum(1)
                .collect();
    }

    private Tuple2<Double, Long> getLabelWithMinCount(List<Tuple2<Double, Long>> labelWithCount) {
        return labelWithCount.stream()
                .min(Comparator.comparingLong(c -> c.f1))
                .orElseThrow(() -> new IllegalStateException("Could not resample the dataset"));
    }

    private Tuple2<Double, Long> getLabelWithMaxCount(List<Tuple2<Double, Long>> labelWithCount) {
        return labelWithCount.stream()
                .max(Comparator.comparingLong(c -> c.f1))
                .orElseThrow(() -> new IllegalStateException("Could not resample the dataset"));
    }

    public JSONObject getAsJson() throws JSONException {
        JSONObject jsonObject = new JSONObject().accumulate("classBalanceMethod", classBalanceMethod);
        if (classBalanceMethod.equals(ClassBalanceMethod.UNDER_SAMPLING)) {
            jsonObject.accumulate("withReplacement", withReplacement);
        }
        if (fraction > 0 && fraction <= 1) {
            jsonObject.accumulate("fraction", fraction);
        }
        return jsonObject;
    }
}