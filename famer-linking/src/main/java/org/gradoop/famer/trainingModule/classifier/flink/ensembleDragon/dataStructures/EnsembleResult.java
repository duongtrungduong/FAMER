package org.gradoop.famer.trainingModule.classifier.flink.ensembleDragon.dataStructures;

import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.api.java.tuple.Tuple8;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;


/**
 * f0: familyId
 * f1: minTrueVotes
 * f2: tp
 * f3: fp
 * f4: tn
 * f5: fn
 */
public class EnsembleResult extends Tuple8<String, Integer, Long, Long, Long, Long, Double, Double> {

    public EnsembleResult() {
    }

    public EnsembleResult(String familyId, double precision, double recall) {
        super(familyId, -1, -1L, -1L, -1L, -1L, precision, recall);
    }

    public EnsembleResult(String familyId, int minTrueVotes, long tp, long fp, long tn, long fn) {
        super(familyId, minTrueVotes, tp, fp, tn, fn, calculatePrecision(tp, fp), calculateRecall(tp, fn));
    }

    public void setFamilyId(String id) {
        f0 = id;
    }

    public String getFamilyId() {
        return f0;
    }

    public void setMinTrueVotes(int minTrueVotes) {
        f1 = minTrueVotes;
    }

    public long getMinTrueVotes() {
        return f1;
    }

    public void setTruePositive(long tp) {
        f2 = tp;
    }

    public long getTruePositive() {
        return f2;
    }

    public void setFalsePositive(long fp) {
        f3 = fp;
    }

    public long getFalsePositive() {
        return f3;
    }

    public void setTrueNegative(long tn) {
        f4 = tn;
    }

    public long getTrueNegative() {
        return f4;
    }

    public void setFalseNegative(long fn) {
        f5 = fn;
    }

    public long getFalseNegative() {
        return f5;
    }

    public void setPrecision(double precision) {
        this.f6 = precision;
    }

    public double getPrecision() {
        return f6;
    }


    public void setRecall(double recall) {
        this.f7 = recall;
    }

    public double getRecall() {
        return f7;
    }

    private static double calculatePrecision(long tp, long fp) {
        double precision = (double) tp / (tp + fp);
        return Double.isNaN(precision) || Double.isInfinite(precision) ? 0d : precision;
    }

    private static double calculateRecall(long tp, long fn) {
        double recall = (double) tp / (tp + fn);
        return Double.isNaN(recall) || Double.isInfinite(recall) ? 0d : recall;
    }

    public JSONObject getAsJson() throws JSONException {
        return new JSONObject().accumulate("familyId", f0)
                .accumulate("minTrueVotes", f1)
                .accumulate("tp", f2)
                .accumulate("fp", f3)
                .accumulate("tn", f4)
                .accumulate("fn", f5)
                .accumulate("pre", f6)
                .accumulate("rec", f7);
    }

    @Override
    public String toString() {
        return "EnsembleResult{" +
                "familyId='" + getFamilyId() + '\'' +
                ", minTrueVotes=" + getMinTrueVotes() +
                ", truePositive=" + getTruePositive() +
                ", falsePositive=" + getFalsePositive() +
                ", trueNegative=" + getTrueNegative() +
                ", falseNegative=" + getFalseNegative() +
                ", precision=" + f6 +
                ", recall=" + f7 +
                '}';
    }
}
