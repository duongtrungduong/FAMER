package org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.*;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.DragonCounter;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.DragonInstanceFactory;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.DragonPredictor;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;

import java.util.*;
import java.util.stream.Collectors;

public class FMeasureFunction extends FitnessFunction {
    /**
     * Rate for lowering the threshold when F-Measure fitness function is used
     */
    private final double lowerThresholdRate;

    /**
     * Initialized values when FMeasure is used, this contains a list of splits with highest F1 value
     * (for each similarity method)
     */
    private List<DragonSplit> initSplits;

    /**
     * Global base tree, uses to store the tree while training and to generate new probe trees
     */
    private DragonTree baseTree;

    /**
     * Store the last f-measure value
     */
    private double lastFMeasure;

    /**
     * Constructor with given parameters
     *
     * @param lowerThreshold     lower threshold of the f-measure function
     * @param lowerThresholdRate lower threshold rate of the f-measure function
     */
    public FMeasureFunction(double lowerThreshold, double lowerThresholdRate, int leafMinSamples) {
        super(lowerThreshold, leafMinSamples);
        this.lowerThresholdRate = lowerThresholdRate;
    }

    @Override
    public DataSet<BinaryInstance> initialize(DataSet<BinaryInstance> binaryInstances, List<String> featureNames) throws Exception {
        DataSet<DragonInstance> dragonInstances = DragonInstanceFactory.execute(binaryInstances, featureNames);
        DataSet<DragonCount> dragonCounts = DragonCounter.count(dragonInstances);
        this.initSplits = dragonCounts
                .filter(f -> f.getFeatureValue() >= getLowerThreshold() && f.hasMinSamples(getLeafMinSamples()))
                .map(new FMeasureInitializer(this))
                .groupBy(1)
                .reduce(new FMeasureInitializer(this))
                .map(m -> m.f0)
                .collect();
        this.lastFMeasure = 0d;
        this.baseTree = null;
        return binaryInstances;
    }

    @Override
    public DragonSplit getBestSplitFeature(DataSet<BinaryInstance> trainingInstances) throws Exception {
        if (initSplits.isEmpty()) return null;
        Optional<DragonSplit> bestSplit = Optional.empty();
        double bestFScore = 0d;
        if (baseTree == null) {
            bestSplit = initSplits.stream().max(Comparator.comparing(DragonSplit::getSplitValue)
                    .thenComparing(DragonSplit::getFeatureIndex, Comparator.reverseOrder()));
            if (!bestSplit.isPresent()) return null;
            bestFScore = bestSplit.get().getSplitValue();
            baseTree = new DragonTree(new DragonNode(bestSplit.get()));
        } else {
            List<DragonTree> probeTrees = generateProbeTrees(baseTree);
            Optional<Tuple2<DragonTree, Double>> bestTreeAndScore = trainingInstances
                    .flatMap(new FMeasureProbe(probeTrees))
                    .groupBy(0)
                    .reduceGroup(new FMeasureProbe(probeTrees))
                    .maxBy(1)
                    .collect().stream().findAny();
            if (bestTreeAndScore.isPresent()) {
                baseTree = bestTreeAndScore.get().f0.copy();
                bestFScore = bestTreeAndScore.get().f1;
                bestSplit = baseTree.getAsList().stream()
                        .filter(f -> !f.isLeaf() && initSplits.contains(f.getSplit()))
                        .map(DragonNode::getSplit).findAny();
            }
        }
        if (bestSplit.isPresent()) {
            initSplits.remove(bestSplit.get());
            bestSplit.get().setSplitValue(bestFScore);
        }
        return bestSplit.orElse(null);
    }

    @Override
    public boolean checkStopCondition(DragonSplit split) {
        if (Double.compare(split.getSplitValue(), lastFMeasure) <= 0) return true;
        this.lastFMeasure = split.getSplitValue();
        return false;
    }

    /**
     *
     */
    @FunctionAnnotation.ForwardedFields("f1")
    private static class FMeasureInitializer implements MapFunction<DragonCount, Tuple2<DragonSplit, Integer>>,
            ReduceFunction<Tuple2<DragonSplit, Integer>> {
        private final FMeasureFunction fFunction;
        private final double lt;
        private final double ltr;

        public FMeasureInitializer(FMeasureFunction fMeasureFunction) {
            this.fFunction = fMeasureFunction;
            this.lt = fFunction.getLowerThreshold();
            this.ltr = fFunction.getLowerThresholdRate();
        }

        @Override
        public Tuple2<DragonSplit, Integer> map(DragonCount dragonCount) throws Exception {
            double closestThreshold = 0d;
            for (double threshold = 1.0d; threshold >= lt; threshold -= ltr) {
                if (dragonCount.getFeatureValue() >= threshold) {
                    closestThreshold = threshold;
                    break;
                }
            }
            DragonSplit dragonSplit = fFunction.parseCountToSplit(dragonCount);
            dragonSplit.setFeatureValue(closestThreshold);
            if (closestThreshold == 0) dragonSplit.setSplitValue(0d);
            return Tuple2.of(dragonSplit, dragonCount.getFeatureIndex());
        }

        @Override
        public Tuple2<DragonSplit, Integer> reduce(Tuple2<DragonSplit, Integer> a, Tuple2<DragonSplit, Integer> b)
                throws Exception {
            if (a.f0.getSplitValue() > b.f0.getSplitValue()) {
                return a;
            } else if (a.f0.getSplitValue() == b.f0.getSplitValue()) {
                return a.f0.getFeatureIndex() < b.f0.getFeatureIndex() ? a : b;
            } else {
                return b;
            }
        }
    }

    /**
     * Tuple3: probeTree, groundTruth, prediction
     */
    private static class FMeasureProbe implements FlatMapFunction<BinaryInstance, Tuple3<Integer, Boolean,
            Boolean>>, GroupReduceFunction<Tuple3<Integer, Boolean, Boolean>, Tuple2<DragonTree, Double>> {

        private final List<DragonTree> probeTrees;

        private final Tuple3<Integer, Boolean, Boolean> reusedTuple3;

        public FMeasureProbe(List<DragonTree> probeTrees) {
            this.probeTrees = probeTrees;
            this.reusedTuple3 = new Tuple3<>();
        }

        @Override
        public void flatMap(BinaryInstance instance, Collector<Tuple3<Integer, Boolean, Boolean>> collector)
                throws Exception {
            for (int i = 0; i < probeTrees.size(); i++) {
                reusedTuple3.f0 = i;
                reusedTuple3.f1 = instance.isTrue();
                reusedTuple3.f2 = DragonPredictor.predict(instance, probeTrees.get(i)).isTrue();
                collector.collect(reusedTuple3);
            }
        }

        @Override
        public void reduce(Iterable<Tuple3<Integer, Boolean, Boolean>> iterable,
                Collector<Tuple2<DragonTree, Double>> collector) throws Exception {
            int tp = 0;
            int fp = 0;
            int fn = 0;
            DragonTree probeTree = null;
            for (Tuple3<Integer, Boolean, Boolean> values : iterable) {
                if (probeTree == null) {
                    probeTree = probeTrees.get(values.f0);
                }
                if (values.f1) {
                    if (values.f2) {
                        tp++;
                    } else {
                        fn++;
                    }
                } else {
                    if (values.f2) fp++;
                }
            }
            double fMeasure = tp / (tp + 0.5d * (fp + fn));
            if (probeTree != null && !Double.isNaN(fMeasure)) {
                collector.collect(Tuple2.of(probeTree, fMeasure));
            }
        }
    }

    private List<DragonTree> generateProbeTrees(DragonTree baseTree) {
        List<DragonTree> probeTrees = new ArrayList<>();
        List<DragonNode> leavesFromBaseTree = baseTree.getAsList().stream()
                .filter(DragonNode::isLeaf).collect(Collectors.toList());
        for (DragonSplit split : initSplits) {
            for (DragonNode leaf : leavesFromBaseTree) {
                boolean leafLabel = leaf.getLabel().isTrue();
                leaf.setSplit(split);
                probeTrees.add(baseTree.copy());
                leaf.setLabel(new DragonLabel(leafLabel));
            }
        }
        return probeTrees;
    }

    @Override
    public JSONObject getAsJson() throws JSONException {
        return new JSONObject().accumulate("fitnessFunctionType", FitnessFunctionType.F_MEASURE)
                .accumulate("lowerThreshold", getLowerThreshold())
                .accumulate("lowerThresholdRate", lowerThresholdRate)
                .accumulate("leafMinSamples", getLeafMinSamples());
    }

    @Override
    public FitnessFunction copy() {
        return new FMeasureFunction(getLowerThreshold(), lowerThresholdRate, getLeafMinSamples());
    }

    public double getLowerThresholdRate() {
        return lowerThresholdRate;
    }

    public DragonTree getBaseTree() {
        return baseTree;
    }

    public List<DragonSplit> getInitSplits() {
        return initSplits;
    }

    public void setInitSplits(List<DragonSplit> initSplits) {
        this.initSplits = initSplits;
    }
}
