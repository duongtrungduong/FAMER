package org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple3;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonNode;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonTree;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.DragonPredictor;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FMeasurePruner extends PruningFunction {

    @Override
    public DragonTree prune(DataSet<BinaryInstance> trainingInstances, DragonTree tree) throws Exception {
        if (tree.isEmpty()) return tree;
        List<DragonTree> probeTrees = new ArrayList<>(Collections.singletonList(tree));
        for (int height=2; height<=tree.getHeight();height++) {
            probeTrees.addAll(generateProbeTrees(tree.copy(), height));
        }
        double fMeasureMax = 0d;
        for (DragonTree probeTree : probeTrees) {
            double probeFMeasure = calculateFMeasure(trainingInstances, probeTree);
            if (Double.compare(fMeasureMax, probeFMeasure) <= 0) {
                fMeasureMax = probeFMeasure;
                tree = probeTree.copy();
            }
        }
        return tree;
    }

    public List<DragonTree> generateProbeTrees(DragonTree tree, int height) {
        List<DragonNode> nodesAtHeight = tree.getAsList().stream()
                .filter(f -> f.getHeight() == height && !f.isLeaf())
                .collect(Collectors.toList());
        List<DragonTree> probeTrees = new ArrayList<>();
        DragonNode tmpLeftNode;
        DragonNode tmpRightNode;
        for (DragonNode nodeAtHeight : nodesAtHeight) {
            if (!nodeAtHeight.getLeftNode().isLeaf()) {
                tmpLeftNode = nodeAtHeight.getLeftNode().copy();
                nodeAtHeight.removeLeftNode();
                probeTrees.add(tree.copy());
                nodeAtHeight.setLeftNode(tmpLeftNode);
            }
            if (!nodeAtHeight.getRightNode().isLeaf()) {
                tmpRightNode = nodeAtHeight.getRightNode().copy();
                nodeAtHeight.removeRightNode();
                probeTrees.add(tree.copy());
                nodeAtHeight.setRightNode(tmpRightNode);
            }
            if (!nodeAtHeight.getLeftNode().isLeaf() && !nodeAtHeight.getRightNode().isLeaf()) {
                tmpLeftNode = nodeAtHeight.getLeftNode().copy();
                tmpRightNode = nodeAtHeight.getRightNode().copy();
                nodeAtHeight.removeLeftNode();
                nodeAtHeight.removeRightNode();
                probeTrees.add(tree.copy());
                nodeAtHeight.setLeftNode(tmpLeftNode);
                nodeAtHeight.setRightNode(tmpRightNode);
            }
        }
        return probeTrees;
    }

    private double calculateFMeasure(DataSet<BinaryInstance> trainingInstances, DragonTree tree) throws Exception {
        long tp = 0;
        long fp = 0;
        long fn = 0;
        List<Tuple3<Boolean, Boolean, Long>> evaluationResults = trainingInstances
                .map(new FMeasureEvaluator(tree))
                .groupBy(0, 1)
                .sum(2)
                .collect();
        for (Tuple3<Boolean, Boolean, Long> result : evaluationResults) {
            if (result.f0 && result.f1) tp = result.f2;
            if (result.f0 && !result.f1) fn = result.f2;
            if (!result.f0 && result.f1) fp = result.f2;
        }
        double f1 = tp / (tp + 0.5d * (fp + fn));
        if (Double.isNaN(f1) || Double.isInfinite(f1)) return 0d;
        return f1;
    }

    private static class FMeasureEvaluator implements MapFunction<BinaryInstance, Tuple3<Boolean, Boolean, Long>> {
        private final DragonTree tree;

        public FMeasureEvaluator(DragonTree dragonTree) {
            this.tree = dragonTree;
        }

        @Override
        public Tuple3<Boolean, Boolean, Long> map(BinaryInstance instance) throws Exception {
            return Tuple3.of(instance.isTrue(), DragonPredictor.predict(instance, tree).isTrue(), 1L);
        }
    }

    @Override
    public JSONObject getAsJson() throws JSONException {
        return new JSONObject().accumulate("pruningMethod", PruningMethod.F_MEASURE);
    }
}
