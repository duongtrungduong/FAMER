package org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures;

import org.apache.flink.api.java.tuple.Tuple7;

/**
 * Count information that is used for training dragon-decision-tree.
 * Each count instance has a feature index and a feature value which come from feature vector
 * From each valid count instance we know how many positive and negative examples on the left and the right side
 * of a split with corresponding feature value and feature value.
 *
 * <pre>
 * f0: feature name
 * f1: feature index
 * f2: feature value
 * f3: number of negative examples whose feature value less than f1
 * f4: number of positive examples whose feature value less than f1
 * f5: number of negative examples whose feature value greater than f1
 * f6: number of positive examples whose feature value greater than f1
 * </pre>
 */
public class DragonCount extends Tuple7<String, Integer, Double, Integer, Integer, Integer, Integer> {

    public DragonCount() {
    }

    public DragonCount(String featureName, Integer featureIndex, Double featureValue, Integer leftNegative,
            Integer leftPositive, Integer rightNegative, Integer rightPositive) {
        super(featureName, featureIndex, featureValue, leftNegative, leftPositive, rightNegative, rightPositive);
    }

    /**
     * Calculates accuracy
     *
     * @return The accuracy between 0 and 1.
     */
    public double getAccuracy() {
        return (double) (getRightPositive() + getLeftNegative())
                / (getRightPositive() + getRightNegative() + getLeftNegative() + getRightNegative());
    }

    /**
     * Calculates precision
     *
     * @return The precision between 0 and 1.
     */
    public double getPrecision() {
        double precision = (double) getRightPositive() / (getRightPositive() + getLeftPositive());
        return Double.isNaN(precision) ? 0.0d : precision;
    }

    /**
     * Calculates recall
     *
     * @return The recall between 0 and 1.
     */
    public double getRecall() {
        double recall = (double) getRightPositive() / (getRightPositive() + getRightNegative());
        return Double.isNaN(recall) ? 0.0d : recall;
    }

    /**
     * Calculates F-measure
     *
     * @return The F-measure between 0 and 1.
     */
    public double getFMeasure() {
        double fMeasure = 2.0d * getPrecision() * getRecall() / (getPrecision() + getRecall());
        return Double.isNaN(fMeasure) ? 0.0d : fMeasure;
    }

    public double getAverageGiniIndex() {
        int ln = getLeftNegative();
        int lp = getLeftPositive();
        int rn = getRightNegative();
        int rp = getRightPositive();
        int tl = getTotalLeft();
        int tr = getTotalRight();
        double lg = getGiniIndex(ln, lp);
        double rg = getGiniIndex(rn, rp);
        double averageGiniIndex = (lg * tl + rg * tr) / (tl + tr);
        return Double.isNaN(averageGiniIndex) ? Double.MAX_VALUE : averageGiniIndex;
    }

    private double getGiniIndex(int negativeCount, int positiveCount) {
        if (negativeCount == 0 || positiveCount == 0) return 0d;
        return 1.0d - (Math.pow((double) negativeCount / (negativeCount + positiveCount), 2)
                + Math.pow((double) positiveCount / (negativeCount + positiveCount), 2));
    }

    public boolean hasMinSamples(int minSamples) {
        return getTotalLeft() >= minSamples && getTotalRight() >= minSamples;
    }

    public int getTotalLeft() {
        return getLeftNegative() + getLeftPositive();
    }

    public int getTotalRight() {
        return getRightNegative() + getRightPositive();
    }

    public String getFeatureName() {
        return f0;
    }

    public void setFeatureName(String featureName) {
        f0 = featureName;
    }

    public int getFeatureIndex() {
        return f1;
    }

    public void setFeatureIndex(int featureIndex) {
        f1 = featureIndex;
    }

    public double getFeatureValue() {
        return f2;
    }

    public void setFeatureValue(double featureValue) {
        f2 = featureValue;
    }

    public int getLeftNegative() {
        return f3;
    }

    public void setLeftNegative(int leftNegative) {
        f3 = leftNegative;
    }

    public int getLeftPositive() {
        return f4;
    }

    public void setLeftPositive(int leftPositive) {
        f4 = leftPositive;
    }

    public int getRightNegative() {
        return f5;
    }

    public void setRightNegative(int rightNegative) {
        f5 = rightNegative;
    }

    public int getRightPositive() {
        return f6;
    }

    public void setRightPositive(int rightPositive) {
        f6 = rightPositive;
    }

    public DragonCount copy() {
        return new DragonCount(getFeatureName(), getFeatureIndex(), getFeatureValue(),
                getLeftNegative(), getLeftPositive(), getRightNegative(), getRightPositive());
    }
}
