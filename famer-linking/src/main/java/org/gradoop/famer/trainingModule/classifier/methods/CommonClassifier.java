package org.gradoop.famer.trainingModule.classifier.methods;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.classifier.dataStructures.ClassifierComponent;
import org.gradoop.famer.trainingModule.classifier.dataStructures.DragonClassifierComponent;
import org.gradoop.famer.trainingModule.classifier.dataStructures.EnsembleDragonClassifierComponent;

public abstract class CommonClassifier implements Classifier {
    private final ClassifierComponent classifierComponent;

    public CommonClassifier(JSONObject jsonClassifier) {
        if (jsonClassifier.opt("classifierComponent") == null) {
            throw new UnsupportedOperationException("Value for classifierComponent could not be found or parsed.");
        }
        JSONObject jsonComponent = jsonClassifier.optJSONObject("classifierComponent");

        if (jsonComponent.opt("classifierMethod") == null) {
            throw new UnsupportedOperationException("Value for classifierMethod could not be found or parsed.");
        }
        String classifierMethod = jsonComponent.optString("classifierMethod");
        switch (classifierMethod) {
            case "DragonClassifierComponent":
                this.classifierComponent = new DragonClassifierComponent(jsonComponent);
                break;
            case "EnsembleDragonClassifierComponent":
                this.classifierComponent = new EnsembleDragonClassifierComponent(jsonComponent);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + classifierMethod);
        }
    }

    public CommonClassifier(ClassifierComponent classifierComponent) {
        this.classifierComponent = classifierComponent;
    }

    public ClassifierComponent getClassifierComponent() {
        return classifierComponent;
    }

    public String getName() {
        return getClass().getSimpleName();
    }
}
