package org.gradoop.famer.trainingModule.classifier.flink.ensembleDragon.functions;

import org.apache.commons.math3.util.CombinatoricsUtils;
import org.apache.flink.api.common.functions.*;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.utils.DataSetUtils;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.util.Collector;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.gradoop.famer.trainingModule.classifier.dataStructures.ClassifierComponentBaseConfig;
import org.gradoop.famer.trainingModule.classifier.dataStructures.DragonClassifierComponent;
import org.gradoop.famer.trainingModule.classifier.dataStructures.EnsembleDragonClassifierComponent;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.*;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.DragonPredictor;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FMeasureFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FitnessFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.GiniIndexFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.ErrorEstimatePruner;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.NonePruner;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.PruningFunction;
import org.gradoop.famer.trainingModule.classifier.flink.ensembleDragon.dataStructures.BootstrapElement;
import org.gradoop.famer.trainingModule.classifier.flink.ensembleDragon.dataStructures.EnsembleElement;
import org.gradoop.famer.trainingModule.classifier.flink.ensembleDragon.dataStructures.EnsembleResult;
import org.gradoop.famer.trainingModule.classifier.methods.DragonClassifier;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;
import org.gradoop.famer.trainingModule.common.dataStructures.TrainTestDataSet;
import org.gradoop.famer.trainingModule.common.functions.TrainTestSplitter;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class EnsembleDragonTrainer {
    public static final String ELEMENTS_BROADCAST = "ELEMENTS_BROADCAST_NAME";

    EnsembleDragonClassifierComponent component;

    public EnsembleDragonTrainer(EnsembleDragonClassifierComponent component) {
        this.component = component;
    }

    public List<DragonClassifier> trainEnsembleDragon(DataSet<BinaryInstance> input) throws Exception {
        TrainTestDataSet trainValidation = TrainTestSplitter.trainTestSplit(input.map(m -> m), 0.7, false);
        DataSet<BinaryInstance> trainDataSet = trainValidation.getTrainingDataSet().map(m ->
                new BinaryInstance(m.getFeatures(), m.getLabel() == BinaryInstance.TRUE, m.getProbability()));
        DataSet<BinaryInstance> validationDataSet = trainValidation.getTestingDataSet().map(m ->
                new BinaryInstance(m.getFeatures(), m.getLabel() == BinaryInstance.TRUE, m.getProbability()));

        List<EnsembleElement> initialElements = generateEnsembleElements(component);
        DataSet<BootstrapElement> bootstraps = generateBootstraps(input, component.getSampleFraction(),
                component.getNumBootstraps(), initialElements);

        DataSet<EnsembleElement> trainedElements = bootstraps.groupBy(0)
                .reduceGroup(new EnsembleTrainer(initialElements));

        DataSet<EnsembleResult> cResults = input
                .flatMap(new EnsembleResultCalculator())
                .withBroadcastSet(trainedElements, ELEMENTS_BROADCAST)
                .groupBy(0, 1)
                .reduceGroup(new EnsembleResultCalculator());
        DataSet<EnsembleResult> iResults = cResults.groupBy(0)
                .reduceGroup(new EnsembleResultInterpolator());
        DataSet<EnsembleResult> eResults = cResults.union(iResults);

        DataSet<DragonClassifier> dragonClassifiers = eResults
                .groupBy(0)
                .reduceGroup(new AUCPRCalculator())
                .maxBy(1)
                .join(trainedElements)
                .where(0).equalTo(0)
                .with((FlatJoinFunction<Tuple2<String, Double>, EnsembleElement, DragonClassifier>)
                        (idAndArea, ensembleElement, collector) -> {
                            collector.collect(ensembleElement.getGiniIndexClassifier());
                            collector.collect(ensembleElement.getFMeasureClassifier());
                        })
                .returns(new TypeHint<DragonClassifier>() {
                });

        List<DragonClassifier> dragonClassifierList = dragonClassifiers.collect();

        dragonClassifierList.forEach(f -> {
            System.out.println(f.getDragonTree());
        });

        return dragonClassifierList;
//        return dragonClassifiers.collect();

////        List<EnsembleElement> initialElements = generateEnsembleElements(component);
////        DataSet<BootstrapElement> bootstraps = generateBootstraps(trainingInstances, component.getSampleFraction(),
////                component.getNumBootstraps(), initialElements);
////        DataSet<EnsembleElement> trainedElements = bootstraps.groupBy(0)
////                .reduceGroup(new EnsembleTrainer(initialElements));
////
////        List<EnsembleElement> trainedElementList = trainedElements.collect();
////        URI trainedElementsUri = URI.create("file:///home/duongtrungduong/Desktop/MA/test/model/dblp-acm" +
////                "-ensemble-family-25b.json");
////        JSONArray jsonEnsembleElements = new JSONArray();
////        for (EnsembleElement trainedElement : trainedElementList) {
////            jsonEnsembleElements.put(trainedElement.getAsJson());
////        }
////        FileUtils.writeStringToFile(new File(trainedElementsUri), jsonEnsembleElements.toString(2), false);
//
//        URI trainedElementsUri = URI.create("file:///home/duongtrungduong/Desktop/MA/test/model/dblp-acm" +
//                "-ensemble-family-25b.json");
//        List<EnsembleElement> trainedElementList = new ArrayList<>();
//        JSONArray jsonElements = new JSONArray(FileUtils.readFileToString(new File(trainedElementsUri)));
//        for (int i = 0; i < jsonElements.length(); i++) {
//            JSONObject jElement = jsonElements.getJSONObject(i);
//            int[] indices = Arrays.stream(jElement.getString("familyId").split(":")).mapToInt(Integer::parseInt)
//                    .toArray();
//            DragonClassifier giniClassifier = new DragonClassifier(jElement.getJSONObject("giniIndexClassifier"));
//            DragonClassifier fMeasureClassifier = new DragonClassifier(jElement.getJSONObject("fMeasureClassifier"));
//            trainedElementList.add(new EnsembleElement(indices, giniClassifier, fMeasureClassifier));
//        }
////        trainedElementList.forEach(f -> {
////            System.out.println("\n######################################################");
////            System.out.println(f.f0);
////            System.out.println(Arrays.toString(f.f1));
////            System.out.println(f.f2.getDragonTree() + "\n" + f.f3.getDragonTree());
////            System.out.println("######################################################\n");
////        });
//        System.out.println("COUNT: " + trainedElementList.stream().filter(f -> f.f2.getDragonTree() != null
//                && f.f3.getDragonTree() != null).count());
//        DataSet<EnsembleElement> trainedElements = trainingInstances
//                .getExecutionEnvironment().fromCollection(trainedElementList);
//
////        DataSet<EnsembleResult> ensembleResults = trainingInstances
////                .flatMap(new EnsembleResultCalculator())
////                .withBroadcastSet(trainedElements, ELEMENTS_BROADCAST)
////                .groupBy(0, 1)
////                .reduceGroup(new EnsembleResultCalculator());
//
//
////        List<EnsembleResult> resultList = ensembleResults.collect();
////        URI resultsUri = URI.create("file:///home/duongtrungduong/Desktop/MA/test/model/dblp-acm" +
////                "-ensemble-result-25b.json");
////        JSONArray jResults = new JSONArray();
////        for (EnsembleResult result : resultList) {
////            jResults.put(result.getAsJson());
////        }
////        FileUtils.writeStringToFile(new File(resultsUri), jResults.toString(2), false);
//
//        URI resultsUri = URI.create("file:///home/duongtrungduong/Desktop/MA/test/model/dblp-acm" +
//                "-ensemble-result-25b.json");
//        List<EnsembleResult> resultList = new ArrayList<>();
//        JSONArray jResults = new JSONArray(FileUtils.readFileToString(new File(resultsUri)));
//        for (int i = 0; i < jResults.length(); i++) {
//            JSONObject jResult = jResults.getJSONObject(i);
//            String id = jResult.getString("familyId");
//            int minTrueVotes = jResult.getInt("minTrueVotes");
//            long tp = jResult.getLong("tp");
//            long fp = jResult.getLong("fp");
//            long tn = jResult.getLong("tn");
//            long fn = jResult.getLong("fn");
//            resultList.add(new EnsembleResult(id, minTrueVotes, tp, fp, tn, fn));
//        }
//
//        DataSet<EnsembleResult> cResults = trainingInstances.getExecutionEnvironment()
//                .fromCollection(resultList);
//        DataSet<EnsembleResult> iResults = cResults.groupBy(0)
//                .reduceGroup(new EnsembleResultInterpolator());
//        DataSet<EnsembleResult> eResults = cResults.union(iResults).distinct(0, 6, 7);
//
//        DataSet<DragonClassifier> dragonClassifiers = eResults.groupBy(0)
//                .reduceGroup(new AUCPRCalculator())
//                .maxBy(1, 0)
//                .rightOuterJoin(trainedElements)
//                .where(0).equalTo(0)
//                .with((FlatJoinFunction<Tuple2<String, Double>, EnsembleElement, DragonClassifier>)
//                        (idAreaPair, ensembleElement, collector) -> {
//                            collector.collect(ensembleElement.getGiniIndexClassifier());
//                            collector.collect(ensembleElement.getFMeasureClassifier());
//                        })
//                .returns(new TypeHint<DragonClassifier>() {
//                });
//
//        List<DragonClassifier> a = dragonClassifiers.collect();
////        a.stream().forEach(f -> {
////            try {
////                System.out.println(f.getAsJson().toString(2));
////            } catch (JSONException e) {
////                e.printStackTrace();
////            }
////        });
//        System.out.println(a.size());
//
//
////                .collect()
////                .stream().sorted(Comparator.comparingDouble(c -> c.f1))
////                .forEach(System.out::println);
//
//
//        return a;
    }

    private static class AUCPRCalculator implements GroupReduceFunction<EnsembleResult, Tuple2<String, Double>> {
        @Override
        public void reduce(Iterable<EnsembleResult> iterable, Collector<Tuple2<String, Double>> collector)
                throws Exception {
            List<EnsembleResult> eResults = new ArrayList<>();
            for (EnsembleResult result : iterable) {
                eResults.add(result);
            }
            if (eResults.isEmpty()) return;
            String id = eResults.get(0).getFamilyId();
            eResults.add(new EnsembleResult(id, eResults.get(0).getPrecision(), 0d));
            eResults.sort(Comparator.comparingDouble(EnsembleResult::getRecall));
            double area = 0d;
            for (int i = 1; i < eResults.size(); i++) {
                double p1 = eResults.get(i - 1).getPrecision();
                double r1 = eResults.get(i - 1).getRecall();
                double p2 = eResults.get(i).getPrecision();
                double r2 = eResults.get(i).getRecall();
                area += trapezoid(p1, r1, p2, r2);
            }
            collector.collect(Tuple2.of(id, area));
        }

        private double trapezoid(double p1, double r1, double p2, double r2) {
            double area = (r2 - r1) * (p2 + p1) / 2;
            return Double.isNaN(area) || Double.isInfinite(area) ? 0d : area;
        }
    }

    private static class EnsembleResultInterpolator implements GroupReduceFunction<EnsembleResult, EnsembleResult> {
        @Override
        public void reduce(Iterable<EnsembleResult> iterable, Collector<EnsembleResult> collector) throws Exception {
            List<EnsembleResult> cResults = new ArrayList<>();
            for (EnsembleResult result : iterable) {
                cResults.add(result);
            }
            if (cResults.isEmpty()) return;
            String id = cResults.get(0).getFamilyId();
            long p = cResults.get(0).getTruePositive() + cResults.get(0).getFalseNegative();
            cResults.sort(Comparator.comparingLong(EnsembleResult::getTruePositive));
            for (int i = 1; i < cResults.size(); i++) {
                EnsembleResult cResultA = cResults.get(i - 1);
                EnsembleResult cResultB = cResults.get(i);
                long tpa = cResultA.getTruePositive();
                long fpa = cResultA.getFalsePositive();
                long tpb = cResultB.getTruePositive();
                long fpb = cResultB.getFalsePositive();
                double skew = (double) (fpb - fpa) / (tpb - tpa);
                for (long x = 1; x <= tpb - tpa; x++) {
                    double pre = (double) (tpa + x) / (tpa + x + fpa + skew * x);
                    double rec = (double) (tpa + x) / p;
                    EnsembleResult iResult = new EnsembleResult(id, pre, rec);
                    iResult.setPrecision(pre);
                    iResult.setRecall(rec);
                    collector.collect(iResult);
                }
            }
        }
    }

    private static class EnsembleResultCalculator extends RichFlatMapFunction<BinaryInstance, Tuple4<String,
            Integer, Boolean, Boolean>> implements GroupReduceFunction<Tuple4<String, Integer, Boolean,
            Boolean>, EnsembleResult> {
        private final Map<String, List<DragonClassifier>> familyClassifiers;

        public EnsembleResultCalculator() {
            this.familyClassifiers = new HashMap<>();
        }

        @Override
        public void open(Configuration parameters) {
            if (!getRuntimeContext().hasBroadcastVariable(ELEMENTS_BROADCAST)) return;
            List<EnsembleElement> ensembleElements = getRuntimeContext().getBroadcastVariable(ELEMENTS_BROADCAST);
            for (EnsembleElement e : ensembleElements) {
                String id = e.getFamilyId();
                if (!familyClassifiers.containsKey(e.getFamilyId())) familyClassifiers.put(id, new ArrayList<>());
                DragonTree gTree = e.getGiniIndexClassifier().getDragonTree();
                if (gTree != null && !gTree.isEmpty()) familyClassifiers.get(id).add(e.getGiniIndexClassifier());
                DragonTree fTree = e.getFMeasureClassifier().getDragonTree();
                if (fTree != null && !fTree.isEmpty()) familyClassifiers.get(id).add(e.getFMeasureClassifier());
//                familyClassifiers.computeIfAbsent(e.getFamilyId(), v -> new ArrayList<>())
//                        .addAll(Arrays.asList(e.getGiniIndexClassifier(), e.getFMeasureClassifier()));
            }
        }

        @Override
        public void flatMap(BinaryInstance instance, Collector<Tuple4<String, Integer, Boolean, Boolean>> collector)
                throws Exception {
            familyClassifiers.forEach((eFamilyId, dragonClassifiers) -> {
                int numTrueVotes = 0;
//                dragonClassifiers = dragonClassifiers.stream()
//                        .filter(f -> f.getDragonTree() !=null && !f.getDragonTree().isEmpty()).collect(Collectors.toList());
                for (DragonClassifier dragonClassifier : dragonClassifiers) {
                    if (DragonPredictor.predict(instance, dragonClassifier.getDragonTree()).isTrue()) numTrueVotes++;
                }
                for (int k = 0; k <= dragonClassifiers.size(); k++) {
                    collector.collect(Tuple4.of(eFamilyId, k, instance.isTrue(), numTrueVotes >= k));
                }
            });
        }

        @Override
        public void reduce(Iterable<Tuple4<String, Integer, Boolean, Boolean>> iterable,
                Collector<EnsembleResult> collector) throws Exception {
            long tp = 0;
            long fp = 0;
            long tn = 0;
            long fn = 0;
            int k = -1;
            String eFamilyId = null;
            for (Tuple4<String, Integer, Boolean, Boolean> predict : iterable) {
                if (predict.f2 && predict.f3) tp++;
                else if (predict.f2) fn++;
                else if (predict.f3) fp++;
                else tn++;
                if (k == -1) k = predict.f1;
                if (eFamilyId == null) eFamilyId = predict.f0;
            }
            collector.collect(new EnsembleResult(eFamilyId, k, tp, fp, tn, fn));
        }
    }

    private static class EnsembleTrainer implements GroupReduceFunction<BootstrapElement, EnsembleElement> {
        private final List<EnsembleElement> elements;
        private final List<BinaryInstance> binaryInstances;

        public EnsembleTrainer(List<EnsembleElement> ensembleElements) {
            this.elements = ensembleElements;
            this.binaryInstances = new ArrayList<>();
        }

        @Override
        public void reduce(Iterable<BootstrapElement> iterable, Collector<EnsembleElement> collector) throws Exception {
            int[] eIndices = null;
            binaryInstances.clear();
            for (BootstrapElement bootstrapElement : iterable) {
                if (eIndices == null) eIndices = bootstrapElement.getEnsembleIndices();
                binaryInstances.add(bootstrapElement.getBinaryInstance());
            }
            for (EnsembleElement element : elements) {
                if (Arrays.equals(element.getIndices(), eIndices)) {
                    DragonClassifier gClassifier = element.getGiniIndexClassifier();
                    DragonClassifier fClassifier = element.getFMeasureClassifier();
                    gClassifier.setDragonTree(updateFeatureIndex(element, getGTree(binaryInstances, gClassifier)));
                    fClassifier.setDragonTree(updateFeatureIndex(element, getFTree(binaryInstances, fClassifier)));
                    collector.collect(element);
                }
            }
        }

        private DragonTree getGTree(List<BinaryInstance> binaryInstances, DragonClassifier gClassifier) {
            DragonClassifierComponent gComponent = gClassifier.getClassifierComponent();
            GiniIndexFunction gFunction = (GiniIndexFunction) gComponent.getFitnessFunction();
            int treeMaxHeight = gComponent.getTreeMaxHeight();
            List<String> featureNames = gComponent.getFeatureNames();
            LocalGiniTrainer gTrainer = new LocalGiniTrainer(gFunction, treeMaxHeight);
            return gTrainer.execute(binaryInstances, featureNames);
        }

        private DragonTree getFTree(List<BinaryInstance> binaryInstances, DragonClassifier fClassifier) {
            DragonClassifierComponent fComponent = fClassifier.getClassifierComponent();
            FMeasureFunction fFunction = (FMeasureFunction) fComponent.getFitnessFunction();
            int treeMaxHeight = fComponent.getTreeMaxHeight();
            List<String> featureNames = fComponent.getFeatureNames();
            LocalFMeasureTrainer fTrainer = new LocalFMeasureTrainer(fFunction, treeMaxHeight);
            return fTrainer.execute(binaryInstances, featureNames);
        }

        private DragonTree updateFeatureIndex(EnsembleElement element, DragonTree tree) {
            if (tree == null || tree.isEmpty()) return tree;
            int[] indexCombination = element.getIndices();
            tree.getAsList().stream().filter(f -> !f.isLeaf()).forEach(f -> {
                int currentIndex = f.getSplit().getFeatureIndex();
                f.getSplit().setFeatureIndex(indexCombination[currentIndex]);
            });
            return tree;
        }
    }

    private static class BootstrapGenerator implements FlatMapFunction<BinaryInstance, BootstrapElement> {
        private final int numBootstraps;
        private final List<int[]> indexCombinations;
        private final BinaryInstance binaryInstance;

        public BootstrapGenerator(int numBootstraps, List<EnsembleElement> elements) {
            this.numBootstraps = numBootstraps;
            this.indexCombinations = elements.stream().map(EnsembleElement::getIndices).collect(Collectors.toList());
            this.binaryInstance = new BinaryInstance();
        }

        @Override
        public void flatMap(BinaryInstance instance, Collector<BootstrapElement> collector) throws Exception {
            for (int[] indexCombination : indexCombinations) {
                Vector<Double> featureValues = new Vector<>();
                for (int featureIndex : indexCombination) {
                    featureValues.add(instance.getFeatures().get(featureIndex));
                }
                binaryInstance.setFeatures(featureValues);
                binaryInstance.setLabel(instance.getLabel());
                binaryInstance.setProbability(instance.getProbability());
                for (int i = 0; i < numBootstraps; i++) {
                    collector.collect(new BootstrapElement(i, indexCombination, binaryInstance));
                }
            }
        }
    }

    private DataSet<BootstrapElement> generateBootstraps(DataSet<BinaryInstance> binaryInstances,
            double sampleFraction, int numBootstraps, List<EnsembleElement> ensembleElements) throws Exception {
        return DataSetUtils.sample(binaryInstances.flatMap(new BootstrapGenerator(numBootstraps, ensembleElements)),
                true, sampleFraction);
    }

    private List<EnsembleElement> generateEnsembleElements(EnsembleDragonClassifierComponent component)
            throws JSONException {
        int featureSize = component.getTrainingSimilarityConfigs().length();
        int bFeatureSize = component.getBootstrapFeatureSize();

        List<int[]> indexCombinations = new ArrayList<>();
        if (featureSize != bFeatureSize) indexCombinations.add(IntStream.range(0, featureSize).toArray());
        CombinatoricsUtils.combinationsIterator(featureSize, bFeatureSize).forEachRemaining(indexCombinations::add);

        List<EnsembleElement> ensembleElements = new ArrayList<>();
        for (int[] indexCombination : indexCombinations) {
            DragonClassifierComponent gComponent = generateGiniComponent(indexCombination, component);
            DragonClassifierComponent fComponent = generateFMeasureComponent(indexCombination, component);

            DragonClassifier gClassifier = new DragonClassifier(gComponent);
            DragonClassifier fClassifier = new DragonClassifier(fComponent);

            EnsembleElement ensembleElement = new EnsembleElement(indexCombination, gClassifier, fClassifier);
            ensembleElements.add(ensembleElement);
        }
        return ensembleElements;
    }

    private DragonClassifierComponent generateGiniComponent(int[] featureIndexCombination,
            EnsembleDragonClassifierComponent component) throws JSONException {
        return generateDragonComponent(featureIndexCombination, component, component.getGiniIndexFunction());
    }

    private DragonClassifierComponent generateFMeasureComponent(int[] featureIndexCombination,
            EnsembleDragonClassifierComponent component) throws JSONException {
        return generateDragonComponent(featureIndexCombination, component, component.getFMeasureFunction());
    }

    private DragonClassifierComponent generateDragonComponent(int[] featureIndexCombination,
            EnsembleDragonClassifierComponent component, FitnessFunction fitnessFunction) throws JSONException {
        List<String> featureNames = component.getFeatureNames();
        PruningFunction pruningFunction = new NonePruner();
        int treeMaxHeight = component.getTreeMaxHeight();
        JSONArray trainingSimilarityConfigs = component.getTrainingSimilarityConfigs();

        List<String> bootstrapFeatureNames = new ArrayList<>();
        JSONArray bootstrapSimilarityConfigs = new JSONArray();
        for (int i : featureIndexCombination) {
            bootstrapFeatureNames.add(featureNames.get(i));
            bootstrapSimilarityConfigs.put(trainingSimilarityConfigs.get(i));
        }

        ClassifierComponentBaseConfig baseConfig = new ClassifierComponentBaseConfig(bootstrapFeatureNames,
                component.getClassName(), component.getClassValues(), component.getClassBalancer());

        return new DragonClassifierComponent(baseConfig, fitnessFunction, pruningFunction, treeMaxHeight,
                bootstrapSimilarityConfigs);
    }

    private static abstract class LocalDragonTrainer {
        private final FitnessFunction fitnessFunction;
        private final int treeMaxHeight;

        public LocalDragonTrainer(FitnessFunction fitnessFunction, int treeMaxHeight) {
            this.fitnessFunction = fitnessFunction;
            this.treeMaxHeight = treeMaxHeight;
        }

        public abstract DragonTree execute(List<BinaryInstance> binaryInstances, List<String> featureNames);

        public List<DragonInstance> generateDragonInstances(List<BinaryInstance> binaryInstances,
                List<String> featureNames) {
            List<DragonInstance> dragonInstances = new ArrayList<>();
            for (int i = 0; i < binaryInstances.size(); i++) {
                boolean label = binaryInstances.get(i).isTrue();
                Vector<Double> featureValues = binaryInstances.get(i).getFeatures();
                for (int j = 0; j < featureValues.size(); j++) {
                    double featureValue = featureValues.get(j);
                    String featureName = featureNames.get(j);
                    dragonInstances.add(new DragonInstance((long) i, featureName, j, featureValue, label));
                }
            }
            return dragonInstances;
        }

        public List<DragonCount> count(List<DragonInstance> dragonInstances) {
            List<DragonCount> countData = new ArrayList<>();
            dragonInstances.stream()
                    .collect(Collectors.groupingBy(DragonInstance::getFeatureIndex)).
                    forEach((k, v) -> {
                        int lp = 0;
                        int ln = 0;
                        int rp = (int) v.stream().filter(DragonInstance::isTrue).count();
                        int rn = v.size() - rp;
                        v.sort(Comparator.comparingDouble(DragonInstance::getFeatureValue));
                        for (int i = 0; i < v.size(); i++) {
                            double featureValue = v.get(i).getFeatureValue();
                            String featureName = v.get(i).getFeatureName();
                            int featureIndex = v.get(i).getFeatureIndex();
                            if (i > 0) {
                                if (v.get(i - 1).isTrue()) {
                                    ++lp;
                                    --rp;
                                } else {
                                    ++ln;
                                    --rn;
                                }
                                countData.add(new DragonCount(featureName, featureIndex, featureValue, ln, lp, rn, rp));
                            }
                        }
                    });
            return countData;
        }

        public void updateLabelProbability(List<BinaryInstance> instances, DragonTree dragonTree) {
            if (dragonTree.isEmpty()) return;
            updateLabelProbability(instances, dragonTree.getRootNode());
        }

        private void updateLabelProbability(List<BinaryInstance> instances, DragonNode currentNode) {
            if (!currentNode.isValid()) return;
            if (currentNode.isLeaf()) {
                long numExamples = instances.size();
                long numPositiveExamples = instances.stream().filter(BinaryInstance::isTrue).count();
                double prob = currentNode.getLabel().isTrue() ? (double) numPositiveExamples / numExamples
                        : (double) (numExamples - numPositiveExamples) / numExamples;
                currentNode.getLabel().setProbability(Double.isNaN(prob) || Double.isInfinite(prob) ? 0d : prob);
            } else {
                DragonSplit split = currentNode.getSplit();
                List<BinaryInstance> leftInstances = instances.stream()
                        .filter(f -> f.getFeatures().get(split.getFeatureIndex()) < split.getFeatureValue())
                        .collect(Collectors.toList());
                List<BinaryInstance> rightInstances = instances.stream()
                        .filter(f -> f.getFeatures().get(split.getFeatureIndex()) >= split.getFeatureValue())
                        .collect(Collectors.toList());
                updateLabelProbability(leftInstances, currentNode.getLeftNode());
                updateLabelProbability(rightInstances, currentNode.getRightNode());
            }
        }

        public FitnessFunction getFitnessFunction() {
            return fitnessFunction;
        }

        public int getTreeMaxHeight() {
            return treeMaxHeight;
        }
    }

    private static class LocalFMeasureTrainer extends LocalDragonTrainer {
        private List<DragonSplit> initSplits;
        private DragonTree baseTree;
        private double lastFMeasure;

        public LocalFMeasureTrainer(FMeasureFunction fFunction, int treeMaxHeight) {
            super(fFunction, treeMaxHeight);
            this.lastFMeasure = 0d;
            this.baseTree = null;
        }

        @Override
        public DragonTree execute(List<BinaryInstance> binaryInstances, List<String> featureNames) {
            List<DragonCount> dragonCounts = count(generateDragonInstances(binaryInstances, featureNames));
            initSplits = initializeFMeasure(dragonCounts);
            DragonTree trainedTree = new DragonTree(buildRootNode(binaryInstances));
            updateLabelProbability(binaryInstances, trainedTree);
            return trainedTree.isEmpty() ? null : trainedTree;
        }

        private DragonNode buildRootNode(List<BinaryInstance> binaryInstances) {
            DragonSplit bestSplit = getBestSplit(binaryInstances);
            if (bestSplit == null || baseTree.getHeight() > getTreeMaxHeight()) {
                return null;
            }
            if (Double.compare(bestSplit.getSplitValue(), lastFMeasure) <= 0) return null;
            lastFMeasure = bestSplit.getSplitValue();
            DragonNode currentBest = baseTree.getRootNode().copy();
            DragonNode probeNode = buildRootNode(binaryInstances);
            return probeNode != null ? probeNode : currentBest;
        }

        private DragonSplit getBestSplit(List<BinaryInstance> binaryInstances) {
            if (initSplits.isEmpty()) return null;
            Optional<DragonSplit> bestSplit;
            double bestFScore = 0d;
            if (baseTree == null) {
                bestSplit = initSplits.stream().max(Comparator.comparing(DragonSplit::getSplitValue)
                        .thenComparing(DragonSplit::getFeatureIndex, Comparator.reverseOrder()));
                if (!bestSplit.isPresent()) return null;
                bestFScore = bestSplit.get().getSplitValue();
                baseTree = new DragonTree(new DragonNode(bestSplit.get()));
            } else {
                List<DragonTree> probeTrees = generateProbeTrees(baseTree);
                for (DragonTree probeTree : probeTrees) {
                    double probeFMeasure = probeFMeasure(binaryInstances, probeTree);
                    if (probeFMeasure > bestFScore) {
                        baseTree = probeTree.copy();
                        bestFScore = probeFMeasure;
                    }
                }
                bestSplit = baseTree.getAsList().stream()
                        .filter(f -> !f.isLeaf() && initSplits.contains(f.getSplit()))
                        .map(DragonNode::getSplit).findAny();
            }
            if (bestSplit.isPresent()) {
                initSplits.remove(bestSplit.get());
                bestSplit.get().setSplitValue(bestFScore);
            }
            return bestSplit.orElse(null);
        }

        private double probeFMeasure(List<BinaryInstance> binaryInstances, DragonTree tree) {
            int tp = 0;
            int fp = 0;
            int fn = 0;
            for (BinaryInstance binaryInstance : binaryInstances) {
                boolean groundTruthLabel = binaryInstance.isTrue();
                boolean predictedLabel = DragonPredictor.predict(binaryInstance, tree).isTrue();
                if (groundTruthLabel) {
                    if (predictedLabel) {
                        tp++;
                    } else {
                        fn++;
                    }
                } else {
                    if (predictedLabel) fp++;
                }
            }
            double fMeasure = tp / (tp + 0.5d * (fp + fn));
            return Double.isNaN(fMeasure) ? 0d : fMeasure;
        }

        private List<DragonTree> generateProbeTrees(DragonTree baseTree) {
            List<DragonTree> probeTrees = new ArrayList<>();
            List<DragonNode> leavesFromBaseTree = baseTree.getAsList().stream()
                    .filter(DragonNode::isLeaf).collect(Collectors.toList());
            for (DragonSplit split : initSplits) {
                for (DragonNode leaf : leavesFromBaseTree) {
                    boolean leafLabel = leaf.getLabel().isTrue();
                    leaf.setSplit(split);
                    probeTrees.add(baseTree.copy());
                    leaf.setLabel(new DragonLabel(leafLabel));
                }
            }
            return probeTrees;
        }

        private List<DragonSplit> initializeFMeasure(List<DragonCount> dragonCounts) {
            FMeasureFunction fFunction = (FMeasureFunction) getFitnessFunction();
            double lt = fFunction.getLowerThreshold();
            double ltr = fFunction.getLowerThresholdRate();
            int lms = fFunction.getLeafMinSamples();
            List<DragonSplit> initializedSplits = new ArrayList<>();
            dragonCounts.stream().filter(f -> f.getFeatureValue() >= lt && f.hasMinSamples(lms))
                    .collect(Collectors.groupingBy(DragonCount::getFeatureIndex))
                    .forEach((k, v) -> {
                        DragonSplit bestSplit = null;
                        for (DragonCount dragonCount : v) {
                            for (double threshold = 1.0d; threshold >= lt; threshold -= ltr) {
                                if (dragonCount.getFeatureValue() >= threshold) {
                                    DragonSplit split = fFunction.parseCountToSplit(dragonCount);
                                    if (bestSplit == null || bestSplit.getSplitValue() < split.getSplitValue()) {
                                        bestSplit = split.copy();
                                        bestSplit.setFeatureValue(threshold);
                                        break;
                                    }
                                }
                            }
                        }
                        if (bestSplit != null) initializedSplits.add(bestSplit);
                    });
            return initializedSplits;
        }
    }

    private static class LocalGiniTrainer extends LocalDragonTrainer {
        public final static String LEFT_INSTANCES = "LEFT_INSTANCES";
        public final static String RIGHT_INSTANCES = "RIGHT_INSTANCES";

        public LocalGiniTrainer(GiniIndexFunction gFunction, int treeMaxHeight) {
            super(gFunction, treeMaxHeight);
        }

        @Override
        public DragonTree execute(List<BinaryInstance> binaryInstances, List<String> featureNames) {
            List<DragonInstance> dragonInstances = generateDragonInstances(binaryInstances, featureNames);
            double lowerThreshold = getFitnessFunction().getLowerThreshold();
            for (DragonInstance dragonInstance : dragonInstances) {
                if (dragonInstance.getFeatureValue() < lowerThreshold) {
                    dragonInstance.setFeatureValue(0d);
                }
            }
            DragonTree trainedTree = new DragonTree(buildRootNode(dragonInstances, new DragonNode()));
            updateLabelProbability(binaryInstances, trainedTree);
            return trainedTree.isEmpty() ? null : trainedTree;
        }

        private DragonNode buildRootNode(List<DragonInstance> dragonInstances, DragonNode currentNode) {
            DragonSplit bestSplit = getBestSplit(dragonInstances);
            currentNode.setSplit(bestSplit);
            if (bestSplit == null || currentNode.getDepth() >= getTreeMaxHeight()) {
                return null;
            }
            if (bestSplit.getSplitValue() <= 1.0d) return currentNode;
            Map<String, List<DragonInstance>> updatedData = getUpdatedData(dragonInstances, bestSplit);
            List<DragonInstance> leftData = updatedData.get(LEFT_INSTANCES);
            List<DragonInstance> rightData = updatedData.get(RIGHT_INSTANCES);
            currentNode.setLeftNode(buildRootNode(leftData, currentNode.getLeftNode()));
            if (!currentNode.hasLeftNode()) currentNode.setLeftNode(new DragonNode(new DragonLabel(false)));
            currentNode.setRightNode(buildRootNode(rightData, currentNode.getRightNode()));
            if (!currentNode.hasRightNode()) currentNode.setRightNode(new DragonNode(new DragonLabel(true)));
            return currentNode;
        }

        DragonSplit getBestSplit(List<DragonInstance> dragonInstances) {
            return count(dragonInstances).stream()
                    .filter(f -> f.hasMinSamples(getFitnessFunction().getLeafMinSamples()))
                    .min(Comparator.comparingDouble(DragonCount::getAverageGiniIndex)
                            .thenComparing(DragonCount::getLeftPositive))
                    .map(getFitnessFunction()::parseCountToSplit)
                    .orElse(null);
        }

        private Map<String, List<DragonInstance>> getUpdatedData(List<DragonInstance> instances, DragonSplit split) {
            List<DragonInstance> leftData = new ArrayList<>();
            List<DragonInstance> rightData = new ArrayList<>();
            for (DragonInstance dragonInstance : instances) {
                if (dragonInstance.getFeatureIndex() != split.getFeatureIndex()) {
                    if (dragonInstance.getFeatureValue() < split.getFeatureValue()) {
                        leftData.add(dragonInstance);
                    } else {
                        rightData.add(dragonInstance);
                    }
                }
            }
            return new HashMap<String, List<DragonInstance>>() {{
                put(LEFT_INSTANCES, leftData);
                put(RIGHT_INSTANCES, rightData);
            }};
        }
    }

    public static void main(String[] args) throws Exception {
        File binFile = new File("/home/duongtrungduong/Desktop/MA/test/tmp/dblp-acm.bin");
        List<BinaryInstance> instances = new ArrayList<>();
        List<String> featureNames = new ArrayList<>();
        try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(binFile))) {
            ArrayList<?> objects = (ArrayList<?>) is.readObject();
            for (Object object : objects) {
                instances.add((BinaryInstance) object);
                featureNames.add("a");
            }
        }

        ExecutionEnvironment env = ExecutionEnvironment.createLocalEnvironment();

        LocalGiniTrainer trainer = new LocalGiniTrainer(
                new GiniIndexFunction(0.05, 50), 2);
        DragonTree tree = trainer.execute(instances, featureNames);
        System.out.println(tree);

//        ErrorEstimatePruner pruner = new ErrorEstimatePruner(0.75);
//        DragonTree tree2 = pruner.prune(env.fromCollection(instances), tree);
//        System.out.println(tree);
//        System.out.println(tree2);
//        System.out.println(tree.getHeight());
//        System.out.println(tree2.getHeight());
    }

//        File binFile = new File("/home/duongtrungduong/Desktop/MA/test/tmp/dblp-acm.bin");
//        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
//        String homeURI = "file:///home/duongtrungduong/Desktop/MA/";
//        URI trainDataSetUri = URI.create(homeURI + "test/libsvm/dblp-acm");
//        List<BinaryInstance> instances = LibsvmUtils.readFromLibsvm(env, trainDataSetUri)
//                .map(m -> new BinaryInstance(m.getFeatures(),m.getLabel() == TRUE, m.getProbability()))
//                .collect();
//        System.out.println(instances.size());
//        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(binFile))) {
//            os.writeObject(instances);
//            os.close();
//            System.out.println("done writing");
//        }

//        File binFile = new File("/home/duongtrungduong/Desktop/MA/test/tmp/dblp-acm.bin");
//        List<BinaryInstance> instances = new ArrayList<>();
//        try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(binFile))) {
//            ArrayList<?> objects = (ArrayList<?>) is.readObject();
//            for (Object object : objects) {
//                instances.add((BinaryInstance) object);
//            }
//        }
}