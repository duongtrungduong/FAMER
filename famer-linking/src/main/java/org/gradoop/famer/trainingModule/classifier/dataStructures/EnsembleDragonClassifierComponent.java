package org.gradoop.famer.trainingModule.classifier.dataStructures;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FMeasureFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.GiniIndexFunction;

import java.util.List;

public class EnsembleDragonClassifierComponent extends ClassifierComponent {
    private final GiniIndexFunction giniIndexFunction;
    private final FMeasureFunction fMeasureFunction;
    private final int numBootstraps;
    private final double sampleFraction;
    private final int bootstrapFeatureSize;
    private final int treeMaxHeight;
    private final JSONArray trainingSimilarityConfigs;

    public EnsembleDragonClassifierComponent(JSONObject jsonConfig) {
        super(jsonConfig);
        try {
            JSONObject giniIndexFunctionConfig = jsonConfig.getJSONObject("giniIndexFunction");
            double lowerThreshold = giniIndexFunctionConfig.getDouble("lowerThreshold");
            int leafMinSamples = giniIndexFunctionConfig.getInt("leafMinSamples");
            this.giniIndexFunction = new GiniIndexFunction(lowerThreshold, leafMinSamples);
        } catch (Exception ex) {
            throw new UnsupportedOperationException("EnsembleDragonClassifierComponent: value for giniIndexFunction" +
                    " could not be found or parsed.", ex);
        }
        try {
            JSONObject fMeasureFunctionConfig = jsonConfig.getJSONObject("fMeasureFunction");
            double lowerThreshold = fMeasureFunctionConfig.getDouble("lowerThreshold");
            double lowerThresholdRate = fMeasureFunctionConfig.getDouble("lowerThresholdRate");
            int leafMinSamples = fMeasureFunctionConfig.getInt("leafMinSamples");
            this.fMeasureFunction = new FMeasureFunction(lowerThreshold, lowerThresholdRate, leafMinSamples);
        } catch (Exception ex) {
            throw new UnsupportedOperationException("EnsembleDragonClassifierComponent: value for giniIndexFunction" +
                    " could not be found or parsed.", ex);
        }
        try {
            this.numBootstraps = jsonConfig.getInt("numBootstraps");
        } catch (Exception ex) {
            throw new UnsupportedOperationException("EnsembleDragonClassifierComponent: value for numBootstraps" +
                    " could not be found or parsed to integer.", ex);
        }
        try {
            this.sampleFraction = jsonConfig.getDouble("sampleFraction");
        } catch (Exception ex) {
            throw new UnsupportedOperationException("EnsembleDragonClassifierComponent: value for sampleFraction" +
                            " could not be found or parsed to double.", ex);
        }
        try {
            this.bootstrapFeatureSize = jsonConfig.getInt("bootstrapFeatureSize");
        } catch (Exception ex) {
            throw new UnsupportedOperationException("EnsembleDragonClassifierComponent: value for bootstrapFeatureSize" +
                    " could not be found or parsed to integer.", ex);
        }
        try {
            this.treeMaxHeight = jsonConfig.getInt("treeMaxHeight");
        } catch (Exception ex) {
            throw new UnsupportedOperationException("EnsembleDragonClassifierComponent: value for treeMaxHeight" +
                            " could not be found or parsed to integer.", ex);
        }
        try {
            this.trainingSimilarityConfigs = jsonConfig.getJSONArray("trainingSimilarityConfigs");
            checkFeatureNames(getFeatureNames(), trainingSimilarityConfigs);
        } catch (Exception ex) {
            throw new UnsupportedOperationException("EnsembleDragonClassifierComponent: value for" +
                    " trainingSimilarityConfigs could not be found or parsed.", ex);
        }
    }

    public EnsembleDragonClassifierComponent(ClassifierComponentBaseConfig baseConfig,
            GiniIndexFunction giniIndexFunction, FMeasureFunction fMeasureFunction,
            int numBootstraps, double sampleFraction, int bootstrapFeatureSize, int treeMaxHeight,
            JSONArray trainingSimilarityConfigs) {
        super(baseConfig);
        this.giniIndexFunction = giniIndexFunction;
        this.fMeasureFunction = fMeasureFunction;
        this.numBootstraps = numBootstraps;
        this.sampleFraction = sampleFraction;
        this.bootstrapFeatureSize = bootstrapFeatureSize;
        this.treeMaxHeight = treeMaxHeight;
        this.trainingSimilarityConfigs = trainingSimilarityConfigs;
        checkFeatureNames(getFeatureNames(), trainingSimilarityConfigs);
    }

    public JSONObject getAsJson() throws JSONException {
        return super.getAsJson().accumulate("giniIndexFunction", getGiniIndexFunction().getAsJson())
                .accumulate("fMeasureFunction", getFMeasureFunction().getAsJson())
                .accumulate("numBootstraps", getNumBootstraps())
                .accumulate("sampleFraction", getSampleFraction())
                .accumulate("bootstrapFeatureSize", getBootstrapFeatureSize())
                .accumulate("treeMaxHeight", getTreeMaxHeight())
                .accumulate("trainingSimilarityConfigs", getTrainingSimilarityConfigs());
    }

    public GiniIndexFunction getGiniIndexFunction() {
        return giniIndexFunction;
    }

    public FMeasureFunction getFMeasureFunction() {
        return fMeasureFunction;
    }

    public int getNumBootstraps() {
        return numBootstraps;
    }

    public double getSampleFraction() {
        return sampleFraction;
    }

    public int getBootstrapFeatureSize() {
        return bootstrapFeatureSize;
    }

    public int getTreeMaxHeight() {
        return treeMaxHeight;
    }

    public JSONArray getTrainingSimilarityConfigs() {
        return trainingSimilarityConfigs;
    }

    private static void checkFeatureNames(List<String> featureNames, JSONArray trainingSimilarityConfigs) {
        try {
            if (trainingSimilarityConfigs.length() != featureNames.size()) {
                featureNames.clear();
                for (int i = 0; i < trainingSimilarityConfigs.length(); i++) {
                    featureNames.add(trainingSimilarityConfigs.getJSONObject(i).getString("id"));
                }
            }
        } catch (Exception ex) {
            throw new UnsupportedOperationException("DragonClassifierComponent: value for trainingSimilarityConfigs " +
                    "could not be found or parsed.", ex);
        }
    }
}
