package org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.Serializable;
import java.util.Objects;

public class DragonNode implements Serializable {
    private DragonNode parentNode;
    private DragonNode leftNode;
    private DragonNode rightNode;
    private DragonSplit split;
    private DragonLabel label;

    public DragonNode() {
    }

    public DragonNode(JSONObject jsonNode) {
        try {
            DragonNode nodeFromJson = getFromJson(jsonNode, new DragonNode());
            setLeftNode(nodeFromJson.getLeftNode());
            setRightNode(nodeFromJson.getRightNode());
            setSplit(nodeFromJson.getSplit());
        } catch (JSONException ex) {
            throw new UnsupportedOperationException("DragonNode: node could not be parse from Json.", ex);
        }
    }

    public DragonNode(DragonSplit split) {
        this.setSplit(split);
    }

    public DragonNode(DragonNode leftNode, DragonNode rightNode, DragonSplit split) {
        this.setLeftNode(leftNode);
        this.setRightNode(rightNode);
        this.setSplit(split);
    }

    public DragonNode(DragonLabel label) {
        this.setLabel(label);
    }

    public String getAsString() {
        if (!isValid()) return null;
        StringBuilder sb = new StringBuilder();
        getAsString(sb, "", this);
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    private void getAsString(StringBuilder sb, String prefix, DragonNode currentNode) {
        if (currentNode == null || !currentNode.isValid()) return;
        if (currentNode.isLeaf()) {
            sb.append(currentNode.getLabel()).append("\n");
        } else {
            sb.append(currentNode.getSplit()).append("\n");
        }
        if (currentNode.hasLeftNode()) {
            sb.append(prefix).append("├── L: ");
            getAsString(sb, prefix + ("|   "), currentNode.getLeftNode());
        }
        if (currentNode.hasRightNode()) {
            sb.append(prefix).append("└── R: ");
            getAsString(sb, prefix + ("    "), currentNode.getRightNode());
        }
    }

    public JSONObject getAsJson() throws JSONException {
        if (!isValid()) return null;
        return getAsJson(this, new JSONObject());
    }

    private JSONObject getAsJson(DragonNode currentNode, JSONObject currentObject) throws JSONException {
        if (currentNode == null || !currentNode.isValid()) return currentObject;
        if (currentNode.isLeaf()) {
            currentObject.accumulate("label", currentNode.getLabel().getAsJson());
        } else {
            currentObject.accumulate("split", currentNode.getSplit().getAsJson());
        }
        if (currentNode.hasLeftNode()) {
            currentObject.accumulate("leftNode", getAsJson(currentNode.getLeftNode(), new JSONObject()));
        }
        if (currentNode.hasRightNode()) {
            currentObject.accumulate("rightNode", getAsJson(currentNode.getRightNode(), new JSONObject()));
        }
        return currentObject;
    }

    private DragonNode getFromJson(JSONObject currentJsonNode, DragonNode currentNode) throws JSONException {
        if (currentJsonNode.has("split")) {
            currentNode.setSplit(new DragonSplit(currentJsonNode.getJSONObject("split")));
        } else if (currentJsonNode.has("label")) {
            currentNode.setLabel(new DragonLabel(currentJsonNode.getJSONObject("label")));
        }
        if (currentJsonNode.has("leftNode")) {
            currentNode.setLeftNode(getFromJson(currentJsonNode.getJSONObject("leftNode"), new DragonNode()));
        }
        if (currentJsonNode.has("rightNode")) {
            currentNode.setRightNode(getFromJson(currentJsonNode.getJSONObject("rightNode"), new DragonNode()));
        }
        return currentNode;
    }

    public DragonNode getParentNode() {
        return parentNode;
    }

    public void setParentNode(DragonNode parentNode) {
        this.parentNode = parentNode;
    }

    public DragonNode getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(DragonNode leftNode) {
        this.leftNode = leftNode;
        if (leftNode != null) this.leftNode.setParentNode(this);
    }

    public DragonNode getRightNode() {
        return rightNode;
    }

    public void setRightNode(DragonNode rightNode) {
        this.rightNode = rightNode;
        if (rightNode != null) this.rightNode.setParentNode(this);
    }

    public DragonSplit getSplit() {
        return split;
    }

    public void setSplit(DragonSplit split) {
        this.split = split;
        if (split == null) {
            this.removeLeftNode();
            this.removeRightNode();
        } else {
            this.label = null;
            if (!this.hasLeftNode()) this.setLeftNode(new DragonNode(new DragonLabel(false)));
            if (!this.hasRightNode()) this.setRightNode(new DragonNode(new DragonLabel(true)));
        }
    }

    public DragonLabel getLabel() {
        return label;
    }

    public void setLabel(DragonLabel label) {
        this.label = label;
        setSplit(null);
    }

    public boolean hasLeftNode() {
        return leftNode != null;
    }

    public boolean hasRightNode() {
        return rightNode != null;
    }

    public void removeSplit() {
        setSplit(null);
    }

    public void removeLeftNode() {
        setLeftNode(null);
    }

    public void removeRightNode() {
        setRightNode(null);
    }

    public boolean isValid() {
        return (split != null && label == null) || (split == null && label != null);
    }

    public boolean isLeaf() {
        return (split == null);
    }

    public boolean isRoot() {
        return getParentNode() == null;
    }

    public int getHeight() {
        return getHeight(this);
    }

    private int getHeight(DragonNode node) {
        return node == null || !node.isValid() ? -1
                : 1 + Math.max(getHeight(node.getLeftNode()), getHeight(node.getRightNode()));
    }

    public int getDepth() {
        return getDepth(this);
    }

    private int getDepth(DragonNode node) {
        return node == null || !node.isValid() ? -1 : 1 + getDepth(node.getParentNode());
    }

    public DragonNode copy() {
        DragonNode copyNode = new DragonNode();
        if (!isValid()) return copyNode;
        if (isLeaf()) {
            copyNode.setLabel(label.copy());
        } else {
            copyNode.setSplit(split.copy());
        }
        if (hasLeftNode()) copyNode.setLeftNode(leftNode.copy());
        if (hasRightNode()) copyNode.setRightNode(rightNode.copy());
        return copyNode;
    }

    @Override
    public String toString() {
        return getAsString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DragonNode)) return false;

        DragonNode that = (DragonNode) o;

	    if (!Objects.equals(split, that.split)) return false;
        if (!Objects.equals(label, that.label)) return false;
        if (!Objects.equals(leftNode, that.leftNode)) return false;
        if (!Objects.equals(rightNode, that.rightNode)) return false;
        if (!Objects.equals(this.getDepth(), that.getDepth())) return false;
        return Objects.equals(this.getHeight(), that.getHeight());
    }

    @Override
    public int hashCode() {
        int result = leftNode != null ? leftNode.hashCode() : 0;
        result = 31 * result + (rightNode != null ? rightNode.hashCode() : 0);
        result = 31 * result + (split != null ? split.hashCode() : 0);
        result = 31 * result + (label != null ? label.hashCode() : 0);
        return result;
    }
}
