package org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DragonTree implements Serializable {
    private DragonNode rootNode;

    public DragonTree() {
    }

    public DragonTree(JSONObject jsonTree) {
        try {
            this.rootNode = new DragonNode(jsonTree.getJSONObject("rootNode"));
        } catch (JSONException ex) {
            throw new UnsupportedOperationException("DragonTree: tree could not be parsed from Json.", ex);
        }
    }

    public DragonTree(DragonNode rootNode) {
        this.rootNode = rootNode;
    }

    public String getAsString() {
        if (isEmpty()) return null;
        String[] tokens = rootNode.getAsString().split("\n");
        for (int i = 0; i < tokens.length; i++) {
            tokens[i] = (i == 0 ? "ROOT: " : "      ") + tokens[i];
        }
        return String.join("\n", tokens);
    }

    public JSONObject getAsJson() throws JSONException {
        if (isEmpty()) return null;
        return new JSONObject().accumulate("rootNode", rootNode.getAsJson());
    }

    public List<DragonNode> getAsList() {
        List<DragonNode> nodes = new ArrayList<>();
        if (isEmpty()) return nodes;
        getAsList(rootNode, nodes);
        return nodes;
    }

    private void getAsList(DragonNode currentNode, List<DragonNode> nodeList) {
        if (currentNode == null) return;
        nodeList.add(currentNode);
        getAsList(currentNode.getLeftNode(), nodeList);
        getAsList(currentNode.getRightNode(), nodeList);
    }

    public int getHeight() {
        return isEmpty() ? -1 : rootNode.getHeight();
    }

    public boolean isEmpty() {
        return rootNode == null || !rootNode.isValid();
    }

    public DragonNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(DragonNode rootNode) {
        this.rootNode = rootNode;
    }

    public DragonTree copy() {
        return isEmpty() ? new DragonTree() : new DragonTree(rootNode.copy());
    }

    @Override
    public String toString() {
        return getAsString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DragonTree)) return false;

        DragonTree that = (DragonTree) o;

        return Objects.equals(rootNode, that.rootNode);
    }

    @Override
    public int hashCode() {
        return rootNode != null ? rootNode.hashCode() : 0;
    }
}
