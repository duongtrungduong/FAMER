package org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures;

import org.apache.flink.api.java.tuple.Tuple5;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;

/**
 * Dragon instance used for training dragon-decision-tree.
 * This is the enriched version of {@link BinaryInstance} by adding feature name and splitting the feature vector.
 * Note that this class is extended {@link Tuple5} to easily uses the groupBy and minBy functions.
 *
 * <pre>
 * f0: group id
 * f1: feature name
 * f2: feature index
 * f3: feature value
 * f4: label
 * </pre>
 */
public class DragonInstance extends Tuple5<Long, String, Integer, Double, Boolean> {
    /**
     * Default constructor without parameter
     */
    public DragonInstance() {
    }

    /**
     * Constructor with given parameters
     * @param groupId group id
     * @param featureName name of the feature
     * @param featureIndex index of the feature
     * @param featureValue value of the feature
     * @param label label of the group
     */
    public DragonInstance(Long groupId, String featureName, int featureIndex, double featureValue, boolean label) {
        super(groupId, featureName, featureIndex, featureValue, label);
    }

    /**
     * Get group id
     * @return group id
     */
    public Long getGroupId() {
        return f0;
    }

    /**
     * Set group id
     * @param groupId a given group id
     */
    public void setGroupId(Long groupId) {
        f0 = groupId;
    }

    /**
     * Get name of the feature
     * @return feature name
     */
    public String getFeatureName() {
        return f1;
    }

    /**
     * Set name of the feature
     * @param featureName a given feature name
     */
    public void setFeatureName(String featureName) {
        f1 = featureName;
    }

    /**
     * Get index of the feature
     * @return feature index
     */
    public int getFeatureIndex() {
        return f2;
    }

    /**
     * Set name of the feature
     * @param featureIndex a given feature name
     */
    public void setFeatureIndex(int featureIndex) {
        f2 = featureIndex;
    }

    /**
     * Get feature value
     * @return feature value
     */
    public double getFeatureValue() {
        return f3;
    }

    /**
     * Set value of the feature
     * @param featureValue a given value of the feature
     */
    public void setFeatureValue(double featureValue) {
        f3 = featureValue;
    }

    /**
     * Get the label of the group
     * @return label of the group
     */
    public boolean getLabel() {
        return f4;
    }

    /**
     * Set label of the group
     * @param label label of the group
     */
    public void setLabel(boolean label) {
        f4 = label;
    }

    /**
     * Is the label of this dragon instance true or false
     * @return true if label is true, else false
     */
    public boolean isTrue() {
        return f4;
    }

    /**
     * Returns string representation of a DragonInstance.
     *
     * @return DragonInstance string representation.
     */
    @Override
    public String toString() {
        return "DragonInstance{" +
                "groupId=" + f0 +
                ", featureName=" + f1 +
                ", featureIndex=" + f2 +
                ", featureValue=" + f3 +
                ", label=" + f4 +
                '}';
    }

    public DragonInstance copy() {
        return new DragonInstance(getGroupId(), getFeatureName(), getFeatureIndex(), getFeatureValue(), getLabel());
    }
}
