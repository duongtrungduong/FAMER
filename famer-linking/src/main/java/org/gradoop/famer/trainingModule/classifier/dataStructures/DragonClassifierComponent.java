package org.gradoop.famer.trainingModule.classifier.dataStructures;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FMeasureFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FitnessFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FitnessFunction.FitnessFunctionType;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.GiniIndexFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.ErrorEstimatePruner;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.FMeasurePruner;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.NonePruner;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.PruningFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.PruningFunction.PruningMethod;

import java.util.List;

public class DragonClassifierComponent extends ClassifierComponent {
    private final FitnessFunction fitnessFunction;
    private final PruningFunction pruningFunction;
    private final int treeMaxHeight;
    private final JSONArray trainingSimilarityConfigs;

    public DragonClassifierComponent(JSONObject jsonConfig) {
        super(jsonConfig);
        try {
            JSONObject fitnessFunctionConfig = jsonConfig.getJSONObject("fitnessFunction");
            FitnessFunctionType fitnessFunctionType = FitnessFunctionType
                    .valueOf(fitnessFunctionConfig.getString("fitnessFunctionType"));
            double lowerThreshold = fitnessFunctionConfig.getDouble("lowerThreshold");
            int leafMinSamples = fitnessFunctionConfig.getInt("leafMinSamples");
            if (fitnessFunctionType.equals(FitnessFunctionType.GINI_INDEX)) {
                this.fitnessFunction = new GiniIndexFunction(lowerThreshold, leafMinSamples);
            } else {
                double lowerThresholdRate = fitnessFunctionConfig.getDouble("lowerThresholdRate");
                this.fitnessFunction = new FMeasureFunction(lowerThreshold, lowerThresholdRate, leafMinSamples);
            }
        } catch (JSONException ex) {
            throw new UnsupportedOperationException("DragonClassifierComponent: value for fitnessFunction could " +
                    "not be found or parsed.", ex);
        }
        try {
            JSONObject treePrunerConfig = jsonConfig.getJSONObject("pruningFunction");
            PruningMethod pruningMethod = PruningMethod.valueOf(treePrunerConfig.getString("pruningMethod"));
            if (pruningMethod.equals(PruningMethod.ERROR_ESTIMATE)) {
                this.pruningFunction = new ErrorEstimatePruner(treePrunerConfig.getDouble("confidenceLevel"));
            } else if (pruningMethod.equals(PruningMethod.F_MEASURE)) {
                this.pruningFunction = new FMeasurePruner();
            } else {
                this.pruningFunction = new NonePruner();
            }
        } catch (Exception ex) {
            throw new UnsupportedOperationException("DragonClassifierComponent: value for pruningMethod could " +
                    "not be found or parsed to string.", ex);
        }
        try {
            this.treeMaxHeight = jsonConfig.getInt("treeMaxHeight");
        } catch (Exception ex) {
            throw new UnsupportedOperationException("DragonClassifierComponent: value for treeMaxHeight could " +
                    "not be found or parsed to integer.", ex);
        }
        try {
            this.trainingSimilarityConfigs = jsonConfig.getJSONArray("trainingSimilarityConfigs");
            checkFeatureNames(getFeatureNames(), trainingSimilarityConfigs);
        } catch (Exception ex) {
            throw new UnsupportedOperationException("DragonClassifierComponent: value for trainingSimilarityConfigs " +
                    "could not be found or parsed.", ex);
        }
    }

    public DragonClassifierComponent(ClassifierComponentBaseConfig baseConfig, FitnessFunction fitnessFunction,
            PruningFunction pruningFunction, int treeMaxHeight, JSONArray trainingSimilarityConfigs) {
        super(baseConfig);
        this.fitnessFunction = fitnessFunction;
        this.pruningFunction = pruningFunction;
        this.treeMaxHeight = treeMaxHeight;
        this.trainingSimilarityConfigs = trainingSimilarityConfigs;
        checkFeatureNames(getFeatureNames(), trainingSimilarityConfigs);
    }

    public JSONObject getAsJson() throws JSONException {
        return super.getAsJson().accumulate("fitnessFunction", getFitnessFunction().getAsJson())
                .accumulate("pruningFunction", getPruningFunction().getAsJson())
                .accumulate("treeMaxHeight", getTreeMaxHeight())
                .accumulate("trainingSimilarityConfigs", getTrainingSimilarityConfigs());
    }

    public FitnessFunction getFitnessFunction() {
        return fitnessFunction;
    }

    public PruningFunction getPruningFunction() {
        return pruningFunction;
    }

    public int getTreeMaxHeight() {
        return treeMaxHeight;
    }

    public JSONArray getTrainingSimilarityConfigs() {
        return trainingSimilarityConfigs;
    }

    private static void checkFeatureNames(List<String> featureNames, JSONArray trainingSimilarityConfigs) {
        try {
            if (trainingSimilarityConfigs.length() != featureNames.size()) {
                featureNames.clear();
                for (int i = 0; i < trainingSimilarityConfigs.length(); i++) {
                    featureNames.add(trainingSimilarityConfigs.getJSONObject(i).getString("id"));
                }
            }
        } catch (Exception ex) {
            throw new UnsupportedOperationException("DragonClassifierComponent: value for trainingSimilarityConfigs " +
                    "could not be found or parsed.", ex);
        }
    }
}
