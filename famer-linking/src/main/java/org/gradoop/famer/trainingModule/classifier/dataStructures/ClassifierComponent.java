package org.gradoop.famer.trainingModule.classifier.dataStructures;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.common.functions.ClassBalancer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Basic data structure for classifier.
 */
public abstract class ClassifierComponent implements Serializable {

    /**
     * The names of the (numeric) features.
     */
    private final List<String> featureNames;

    /**
     * The name of the class (e.g. isMatch).
     */
    private final String className;

    /**
     * The values of the class (e.g. true/false).
     */
    private final List<String> classValues;

    /**
     * Define the balancing techniques to handle imbalanced data.
     */
    private final ClassBalancer classBalancer;

    public ClassifierComponent(JSONObject jsonConfig) {
        try {
            this.featureNames = new ArrayList<>();
            JSONArray jsonFeatureNames = jsonConfig.getJSONArray("featureNames");
            for (int i = 0; i < jsonFeatureNames.length(); i++) {
                this.featureNames.add(jsonFeatureNames.getString(i));
            }
        } catch (Exception ex) {
            throw new UnsupportedOperationException("Value for featureNames in classifier component could not be " +
                    "found or parsed to list of strings.", ex);
        }
        try {
            this.className = jsonConfig.getString("className");
        } catch (Exception ex) {
            throw new UnsupportedOperationException("Value for className in classifier component could not be " +
                    "found or parsed to string.", ex);
        }
        try {
            this.classValues = new ArrayList<>();
            JSONArray jsonClassValues = jsonConfig.getJSONArray("classValues");
            for (int i = 0; i < jsonClassValues.length(); i++) {
                this.classValues.add(jsonClassValues.getString(i));
            }
        } catch (Exception ex) {
            throw new UnsupportedOperationException("Value for classValues in classifier component could not be " +
                    "found or parsed to list of strings.", ex);
        }
        try {
            this.classBalancer = new ClassBalancer(jsonConfig.getJSONObject("classBalancer"));
        } catch (Exception ex) {
            throw new UnsupportedOperationException("Value for balanceMethod in classifier component could not be " +
                    "found or parsed to string.", ex);
        }
    }

    /**
     * Creates an instance of ClassifierComponent from the given {@link ClassifierComponent}.
     *
     * @param baseConfig The base configuration for the classifier component
     */
    public ClassifierComponent(ClassifierComponentBaseConfig baseConfig) {
        this.featureNames = baseConfig.getFeatureNames();
        this.className = baseConfig.getClassName();
        this.classValues = baseConfig.getClassValues();
        this.classBalancer = baseConfig.getClassBalancer();
    }

    public JSONObject getAsJson() throws JSONException {
        return new JSONObject().accumulate("classifierMethod", getName())
                .accumulate("featureNames", new JSONArray(featureNames))
                .accumulate("className", className)
                .accumulate("classValues", new JSONArray(classValues))
                .accumulate("classBalancer", classBalancer.getAsJson());
    }

    /**
     * Getter for featureNames.
     *
     * @return The names of the features.
     */
    public List<String> getFeatureNames() {
        return featureNames;
    }

    /**
     * Getter for className.
     *
     * @return The name of the class.
     */
    public String getClassName() {
        return className;
    }

    /**
     * Getter for classValues.
     *
     * @return The values of the class.
     */
    public List<String> getClassValues() {
        return classValues;
    }

    /**
     * Getter for classBalancer.
     *
     * @return The class balancer.
     */
    public ClassBalancer getClassBalancer() {
        return classBalancer;
    }

    /**
     * Get name of the classifier component
     *
     * @return The name of the classifier component
     */
    public String getName() {
        return getClass().getSimpleName();
    }
}
