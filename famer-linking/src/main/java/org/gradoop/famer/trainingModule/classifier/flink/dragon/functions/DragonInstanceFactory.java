package org.gradoop.famer.trainingModule.classifier.flink.dragon.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.DataSetUtils;
import org.apache.flink.util.Collector;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonInstance;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * Enriches training instances by flat-mapping {@link BinaryInstance} to {@link DragonInstance}.
 * For a given binary-instance with feature vector of size n, creates n dragon-instances with same group id.
 * Each dragon-instance receives then a feature value of the feature vector and a feature name respectively.
 * The group id is created by concatenating sub task index of this flat-map and a counter of processed elements
 */
public class DragonInstanceFactory {
    /**
     * Generate a dataset of {@link DragonInstance} from given dataset of {@link BinaryInstance}
     * @param binaryInstances input binary instances
     * @param featureNames input feature names
     * @return dataset of dragon instances
     */
    public static DataSet<DragonInstance> execute(DataSet<BinaryInstance> binaryInstances, List<String> featureNames) {
        return DataSetUtils.zipWithUniqueId(binaryInstances).flatMap(new DragonFactoryExecutor(featureNames));
    }

    /**
     * Generate a dataset of {@link DragonInstance} from given dataset of {@link BinaryInstance}
     * @param dragonInstances input dragon instances
     * @return dataset of binary instances
     */
    public static DataSet<BinaryInstance> reconstruct(DataSet<DragonInstance> dragonInstances) throws Exception {
        int featureSize = (int) dragonInstances.groupBy(0).first(1).count();
        return dragonInstances.groupBy(0).reduceGroup(new BinaryInstanceReconstruct(featureSize));
    }

    /**
     * The factory that creates dragon instances
     */
    private static class DragonFactoryExecutor implements FlatMapFunction<Tuple2<Long, BinaryInstance>,
            DragonInstance> {
        /**
         * List of feature names, which was used to generate training binary instances
         */
        private final List<String> featureNames;

        /**
         * Reused object
         */
        private final DragonInstance reusedObject;

        /**
         * Constructor with given parameter.
         *
         * @param featureNames list of feature names, which was used to generate training binary instances
         */
        public DragonFactoryExecutor(List<String> featureNames) {
            this.reusedObject = new DragonInstance();
            this.featureNames = featureNames;
        }

        @Override
        public void flatMap(Tuple2<Long, BinaryInstance> input, Collector<DragonInstance> collector) throws Exception {
            reusedObject.setGroupId(input.f0);
            reusedObject.setLabel(input.f1.isTrue());
            for (int i = 0; i < input.f1.getFeatures().size(); i++) {
                reusedObject.setFeatureName(featureNames.get(i));
                reusedObject.setFeatureIndex(i);
                reusedObject.setFeatureValue(input.f1.getFeatures().get(i));
                collector.collect(reusedObject);
            }
        }
    }

    private static class BinaryInstanceReconstruct implements GroupReduceFunction<DragonInstance, BinaryInstance> {
        private final int featureSize;
        private final BinaryInstance binaryInstance;
        private DragonInstance dragonInstance;

        public BinaryInstanceReconstruct(int featureSize) {
            this.binaryInstance = new BinaryInstance();
            this.dragonInstance = new DragonInstance();
            this.featureSize = featureSize;
        }

        @Override
        public void reduce(Iterable<DragonInstance> iterable, Collector<BinaryInstance> collector) throws Exception {
            if (featureSize == 0) return;
            Iterator<DragonInstance> iterator = iterable.iterator();
            Vector<Double> features = new Vector<>();
            features.setSize(featureSize);
            dragonInstance.setLabel(Boolean.FALSE);
            while (iterator.hasNext()) {
                dragonInstance = iterator.next();
                features.set(dragonInstance.getFeatureIndex(), dragonInstance.getFeatureValue());
            }
            binaryInstance.setFeatures(features);
            binaryInstance.setLabel(dragonInstance.getLabel() ? BinaryInstance.TRUE : BinaryInstance.FALSE);
            collector.collect(binaryInstance);
        }
    }
}
