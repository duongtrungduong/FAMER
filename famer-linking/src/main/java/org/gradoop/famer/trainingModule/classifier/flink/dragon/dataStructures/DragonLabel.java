package org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.Serializable;

public class DragonLabel implements Serializable {
    private boolean label;
    private double probability;

    public DragonLabel() {
    }

    public DragonLabel(JSONObject jsonLabel) {
        try {
            this.label = jsonLabel.getBoolean("label");
            this.probability = jsonLabel.getDouble("probability");
        } catch (JSONException ex) {
            throw new UnsupportedOperationException("DragonLabel: node could not be parse from Json.", ex);
        }
    }

    public DragonLabel(boolean label) {
        this.label = label;
    }

    public DragonLabel(boolean label, double probability) {
        this.label = label;
        this.probability = probability;
    }

    public DragonLabel(boolean label, int numPositiveExamples, int numNegativeExamples) {
        double probability = label ? (double) numPositiveExamples / (numPositiveExamples + numNegativeExamples) :
                (double) numNegativeExamples / (numPositiveExamples + numNegativeExamples);
        this.label = label;
        this.probability = Double.isNaN(probability) ? 0 : probability;
    }

    public JSONObject getAsJson() throws JSONException {
        return new JSONObject().accumulate("label", label)
                .accumulate("probability", getProbability())
                .accumulate("giniIndex", getGiniIndex())
                .accumulate("entropy", getEntropy());
    }

    public DragonLabel copy() {
        return new DragonLabel(label, probability);
    }

    public String getAsString() {
        return "DragonLabel{" +
                "label=" + label +
                ", probability=" + probability +
                ", giniIndex=" + getGiniIndex() +
                ", entropy=" + getEntropy() +
                '}';
    }

    public double getGiniIndex() {
        double p = probability;
        double q = 1d - p;
        return 1d - (Math.pow(p, 2) + Math.pow(q, 2));
    }

    public double getEntropy() {
        double p = probability;
        double q = 1d - p;
        if (p == 0 || q == 0) return 0d;
        double e = -p*Math.log(p)/Math.log(2) - q*Math.log(q)/Math.log(2);
        return Double.isInfinite(e) || Double.isNaN(e) ? 0d : e;
    }

    public boolean isTrue() {
        return label;
    }

    public double getProbability() {
        return probability;
    }

    public void setLabel(boolean label) {
        this.label = label;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

    @Override
    public String toString() {
        return getAsString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DragonLabel)) return false;

        DragonLabel that = (DragonLabel) o;

        if (label != that.label) return false;
        return Double.compare(that.probability, probability) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (label ? 1 : 0);
        temp = Double.doubleToLongBits(probability);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
