package org.gradoop.famer.trainingModule.classifier.methods;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.Utils;
import org.apache.flink.api.java.utils.DataSetUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.LibsvmUtils;
import org.gradoop.famer.trainingModule.classifier.dataStructures.DragonClassifierComponent;
import org.gradoop.famer.trainingModule.classifier.dataStructures.EnsembleDragonClassifierComponent;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FMeasureFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FitnessFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.GiniIndexFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.PruningFunction;
import org.gradoop.famer.trainingModule.classifier.flink.ensembleDragon.dataStructures.EnsembleElement;
import org.gradoop.famer.trainingModule.classifier.flink.ensembleDragon.functions.EnsembleDragonPredictor;
import org.gradoop.famer.trainingModule.classifier.flink.ensembleDragon.functions.EnsembleDragonTrainer;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;
import org.gradoop.famer.trainingModule.common.dataStructures.CommonInstance;
import org.gradoop.famer.trainingModule.common.dataStructures.TrainTestDataSet;
import org.gradoop.famer.trainingModule.common.functions.ClassBalancer;
import org.gradoop.famer.trainingModule.common.functions.TrainTestSplitter;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

public class EnsembleDragonClassifier extends CommonClassifier {
    private List<DragonClassifier> dragonClassifiers;

    public EnsembleDragonClassifier(JSONObject jsonClassifier) {
        super(jsonClassifier);
        if (jsonClassifier.has("dragonClassifiers")) {
            try {
                JSONArray jsonDragonClassifiers = jsonClassifier.optJSONArray("dragonClassifiers");
                this.dragonClassifiers = new ArrayList<>();
                for (int i = 0; i < jsonDragonClassifiers.length(); i++) {
                    dragonClassifiers.add(new DragonClassifier(jsonDragonClassifiers.getJSONObject(i)));
                }
            } catch (Exception ex) {
                throw new UnsupportedOperationException("EnsembleDragonClassifier: value for dragonClassifiers could " +
                        "not be found or parsed.", ex);
            }
        }
    }

    public EnsembleDragonClassifier(EnsembleDragonClassifierComponent classifierComponent) {
        super(classifierComponent);
    }

    @Override
    public void fit(DataSet<CommonInstance> trainingInstances) throws Exception {
//        List<CommonInstance> virtualSink = trainingInstances.collect();
//        trainingInstances = trainingInstances.getExecutionEnvironment().fromCollection(virtualSink);
        DataSet<BinaryInstance> binaryInstances = trainingInstances.map(m ->
                new BinaryInstance(m.getFeatures(),m.getLabel() == BinaryInstance.TRUE, m.getProbability()));
        EnsembleDragonTrainer ensembleDragonTrainer = new EnsembleDragonTrainer(getClassifierComponent());
        dragonClassifiers = ensembleDragonTrainer.trainEnsembleDragon(binaryInstances);
    }


    @Override
    public CommonInstance predict(CommonInstance unlabeledInstance) throws Exception {
        return EnsembleDragonPredictor.predict(unlabeledInstance, dragonClassifiers);
    }

    @Override
    public DataSet<CommonInstance> predict(DataSet<CommonInstance> unlabeledInstances) {
        return unlabeledInstances.map(m -> EnsembleDragonPredictor.predict(m, dragonClassifiers));
    }

    public JSONObject getAsJson() throws JSONException {
        EnsembleDragonClassifierComponent classifierComponent = getClassifierComponent();
        JSONArray jsonDragonClassifiers = new JSONArray();
        if (dragonClassifiers != null && !dragonClassifiers.isEmpty()) {
            for (DragonClassifier dragonClassifier : dragonClassifiers) {
                jsonDragonClassifiers.put(dragonClassifier.getAsJson());
            }
        }
        return new JSONObject().accumulate("classifierComponent", classifierComponent.getAsJson())
                .accumulate("dragonClassifiers", jsonDragonClassifiers);
    }

    public EnsembleDragonClassifierComponent getClassifierComponent() {
        return (EnsembleDragonClassifierComponent) super.getClassifierComponent();
    }

    public List<DragonClassifier> getDagonClassifiers() {
        return dragonClassifiers;
    }
}