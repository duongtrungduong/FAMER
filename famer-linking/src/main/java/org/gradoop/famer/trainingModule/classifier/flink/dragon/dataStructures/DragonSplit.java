package org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.Serializable;
import java.util.Objects;

public class DragonSplit implements Serializable {
    private String featureName;
    private int featureIndex;
    private double featureValue;
    private double splitValue;
    private String fitnessFunctionName;

    public DragonSplit() {
    }

    public DragonSplit(JSONObject jsonSplit) {
        try {
            this.featureName = jsonSplit.getString("featureName");
            this.featureIndex = jsonSplit.getInt("featureIndex");
            this.featureValue = jsonSplit.getDouble("featureValue");
            this.splitValue = jsonSplit.getDouble("splitValue");
            this.fitnessFunctionName = jsonSplit.getString("fitnessFunctionName");
        } catch (JSONException ex) {
            throw new UnsupportedOperationException("DragonNode: node could not be parse from Json.", ex);
        }
    }

    public DragonSplit(String featureName, int featureIndex, double featureValue, double splitValue,
                       String fitnessFunctionName) {
        this.featureName = featureName;
        this.featureIndex = featureIndex;
        this.featureValue = featureValue;
        this.splitValue = splitValue;
        this.fitnessFunctionName = fitnessFunctionName;
    }

    public String getAsString() {
        return "DragonSplit{" +
                "featureName='" + featureName + '\'' +
                ", featureIndex=" + featureIndex +
                ", featureValue=" + featureValue +
                ", splitValue=" + splitValue +
                ", fitnessFunctionName='" + fitnessFunctionName + '\'' +
                '}';
    }

    public JSONObject getAsJson() throws JSONException {
        return new JSONObject().accumulate("featureName", featureName)
                .accumulate("featureIndex", featureIndex)
                .accumulate("featureValue", featureValue)
                .accumulate("splitValue", splitValue)
                .accumulate("fitnessFunctionName", fitnessFunctionName);
    }

    public DragonSplit copy() {
        return new DragonSplit(featureName, featureIndex, featureValue, splitValue, fitnessFunctionName);
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public int getFeatureIndex() {
        return featureIndex;
    }

    public void setFeatureIndex(int featureIndex) {
        this.featureIndex = featureIndex;
    }

    public double getFeatureValue() {
        return featureValue;
    }

    public void setFeatureValue(double featureValue) {
        this.featureValue = featureValue;
    }

    public double getSplitValue() {
        return splitValue;
    }

    public void setSplitValue(double splitValue) {
        this.splitValue = splitValue;
    }

    public String getFitnessFunctionName() {
        return fitnessFunctionName;
    }

    public void setFitnessFunctionName(String fitnessFunctionName) {
        this.fitnessFunctionName = fitnessFunctionName;
    }

    @Override
    public String toString() {
        return getAsString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DragonSplit)) return false;

        DragonSplit that = (DragonSplit) o;

        if (featureIndex != that.featureIndex) return false;
        if (Double.compare(that.featureValue, featureValue) != 0) return false;
        if (Double.compare(that.splitValue, splitValue) != 0) return false;
        if (!Objects.equals(featureName, that.featureName)) return false;
        return Objects.equals(fitnessFunctionName, that.fitnessFunctionName);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = featureName != null ? featureName.hashCode() : 0;
        result = 31 * result + featureIndex;
        temp = Double.doubleToLongBits(featureValue);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(splitValue);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (fitnessFunctionName != null ? fitnessFunctionName.hashCode() : 0);
        return result;
    }
}