package org.gradoop.famer.trainingModule.classifier.methods;

import org.apache.flink.api.java.DataSet;
import org.gradoop.famer.trainingModule.classifier.dataStructures.ClassifierComponent;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;
import org.gradoop.famer.trainingModule.common.dataStructures.CommonInstance;

import java.io.Serializable;

public interface Classifier extends Serializable {
    void fit(DataSet<CommonInstance> trainingInstances) throws Exception;
    CommonInstance predict(CommonInstance unlabeledInstance) throws Exception;
    DataSet<CommonInstance> predict(DataSet<CommonInstance> unlabeledInstance) throws Exception;
}
