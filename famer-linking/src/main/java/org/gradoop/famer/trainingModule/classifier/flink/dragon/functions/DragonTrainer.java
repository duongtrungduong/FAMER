package org.gradoop.famer.trainingModule.classifier.flink.dragon.functions;

import org.apache.flink.api.java.DataSet;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.*;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FMeasureFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FitnessFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.GiniIndexFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.PruningFunction;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;

import java.util.List;
import java.util.Map;

public class DragonTrainer {
    private final FitnessFunction fitnessFunction;
    private final PruningFunction pruningFunction;
    private final int treeMaxHeight;

    public DragonTrainer(FitnessFunction fitnessFunction, PruningFunction pruningFunction, int treeMaxHeight) {
        this.fitnessFunction = fitnessFunction;
        this.pruningFunction = pruningFunction;
        this.treeMaxHeight = treeMaxHeight;
    }

    public DragonTree trainDragon(DataSet<BinaryInstance> binaryInstances, List<String> featureNames)
            throws Exception {
        DataSet<BinaryInstance> trainingInstances = fitnessFunction.initialize(binaryInstances, featureNames);
        DragonTree trainedTree = new DragonTree(buildRootNode(trainingInstances, new DragonNode()));
        DragonTree prunedTree = pruningFunction.prune(trainingInstances, trainedTree);
        updateLabelProbability(trainingInstances, prunedTree);
        return prunedTree;
    }

    private DragonNode buildRootNode(DataSet<BinaryInstance> trainingInstances, DragonNode currentNode)
            throws Exception {
        return fitnessFunction instanceof GiniIndexFunction ? buildGiniRootNode(trainingInstances, currentNode)
                : buildFMeasureRootNode(trainingInstances);
    }

    private DragonNode buildGiniRootNode(DataSet<BinaryInstance> trainingInstances, DragonNode currentNode)
            throws Exception {
        DragonSplit bestSplit = fitnessFunction.getBestSplitFeature(trainingInstances);
        currentNode.setSplit(bestSplit);

        if (bestSplit == null || currentNode.getDepth() >= treeMaxHeight) return null;

        if (fitnessFunction.checkStopCondition(bestSplit)) return currentNode;

        Map<String, DataSet<BinaryInstance>> splitInstances =
                ((GiniIndexFunction) fitnessFunction).updateTrainingInstances(trainingInstances, bestSplit);

        DataSet<BinaryInstance> leftInstances = splitInstances.get(GiniIndexFunction.LEFT_INSTANCES);
        currentNode.setLeftNode(buildGiniRootNode(leftInstances, currentNode.getLeftNode()));
        if (!currentNode.hasLeftNode()) currentNode.setLeftNode(new DragonNode(new DragonLabel(false)));

        DataSet<BinaryInstance> rightInstances = splitInstances.get(GiniIndexFunction.RIGHT_INSTANCES);
        currentNode.setRightNode(buildGiniRootNode(rightInstances, currentNode.getRightNode()));
        if (!currentNode.hasRightNode()) currentNode.setRightNode(new DragonNode(new DragonLabel(true)));

        return currentNode;
    }

    private DragonNode buildFMeasureRootNode(DataSet<BinaryInstance> dragonInstances) throws Exception {
        DragonSplit bestSplit = fitnessFunction.getBestSplitFeature(dragonInstances);
        DragonNode currentNode = ((FMeasureFunction) fitnessFunction).getBaseTree().getRootNode();

        if (bestSplit == null || currentNode.getHeight() > treeMaxHeight) return null;

        if (fitnessFunction.checkStopCondition(bestSplit)) return null;

        DragonNode currentBest = currentNode.copy();
        DragonNode probeNode = buildFMeasureRootNode(dragonInstances);
        return probeNode != null ? probeNode : currentBest;
    }

    /**
     * Update the probability label on each node of the dragon-decision-tree
     *
     * @param instances  the binary instances which were used to train the tree
     * @param dragonTree the trained dragon-decision-tree
     * @throws Exception runtime exception
     */
    private void updateLabelProbability(DataSet<BinaryInstance> instances, DragonTree dragonTree) throws Exception {
        if (dragonTree.isEmpty()) return;
        updateLabelProbability(instances, dragonTree.getRootNode());
    }

    /**
     * Update the probability label on each leaf node of the dragon-decision-tree
     *
     * @param instances   the binary instances which were used to train the tree
     * @param currentNode the current node that should be updated
     * @throws Exception flink runtime exception
     */
    private void updateLabelProbability(DataSet<BinaryInstance> instances, DragonNode currentNode)
            throws Exception {
        if (!currentNode.isValid()) return;
        if (currentNode.isLeaf()) {
            long numExams = instances.count();
            long numPos = instances.filter(BinaryInstance::isTrue).count();
            numPos = Math.min(numPos, numExams);
            double prob = currentNode.getLabel().isTrue() ? (double) numPos / numExams
                    : (double) (numExams - numPos) / numExams;
            currentNode.getLabel().setProbability(Double.isNaN(prob) || Double.isInfinite(prob) ? 0d : prob);
        } else {
            DragonSplit split = currentNode.getSplit();
            DataSet<BinaryInstance> leftInstances =
                    instances.filter(f -> f.getFeatures().get(split.getFeatureIndex()) < split.getFeatureValue());
            DataSet<BinaryInstance> rightInstances =
                    instances.filter(f -> f.getFeatures().get(split.getFeatureIndex()) >= split.getFeatureValue());
            updateLabelProbability(leftInstances, currentNode.getLeftNode());
            updateLabelProbability(rightInstances, currentNode.getRightNode());
        }
    }
}