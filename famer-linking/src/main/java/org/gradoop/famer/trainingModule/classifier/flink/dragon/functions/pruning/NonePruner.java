package org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning;

import org.apache.flink.api.java.DataSet;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonTree;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;

public class NonePruner extends PruningFunction {

    @Override
    public DragonTree prune(DataSet<BinaryInstance> trainingInstances, DragonTree tree) throws Exception {
        return tree;
    }

    @Override
    public JSONObject getAsJson() throws JSONException {
        return new JSONObject().accumulate("pruningMethod", PruningMethod.NONE);
    }
}