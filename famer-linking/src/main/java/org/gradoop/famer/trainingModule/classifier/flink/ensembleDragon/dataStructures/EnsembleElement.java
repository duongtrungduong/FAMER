package org.gradoop.famer.trainingModule.classifier.flink.ensembleDragon.dataStructures;

import org.apache.flink.api.java.tuple.Tuple4;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.classifier.methods.DragonClassifier;

import java.util.Arrays;
import java.util.stream.Collectors;

public class EnsembleElement extends Tuple4<String, int[], DragonClassifier, DragonClassifier> {
    private static final String ID_CONCAT_CHAR = ":";

    public EnsembleElement() {
    }

    public EnsembleElement(int[] indices, DragonClassifier giniIndexClassifier, DragonClassifier fMeasureClassifier) {
        super(generateId(indices), indices, giniIndexClassifier, fMeasureClassifier);
    }

    public void setFamilyId(String id) {
        f0 = id;
    }

    public String getFamilyId() {
        return f0;
    }

    public void setIndices(int[] indices) {
        f1 = indices;
    }

    public int[] getIndices() {
        return f1;
    }

    public void setGiniIndexClassifier(DragonClassifier giniIndexClassifier) {
        f2 = giniIndexClassifier;
    }

    public DragonClassifier getGiniIndexClassifier() {
        return f2;
    }

    public void setFMeasureClassifier(DragonClassifier fMeasureClassifier) {
        f3 = fMeasureClassifier;
    }

    public DragonClassifier getFMeasureClassifier() {
        return f3;
    }

    private static String generateId(int[] indices) {
        return Arrays.stream(indices).boxed().map(String::valueOf).collect(Collectors.joining(ID_CONCAT_CHAR));
    }

    public JSONObject getAsJson() throws JSONException {
        return new JSONObject().accumulate("familyId", f0)
                .accumulate("giniIndexClassifier", f2.getAsJson())
                .accumulate("fMeasureClassifier", f3.getAsJson());
    }
}
