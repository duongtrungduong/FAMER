package org.gradoop.famer.trainingModule.classifier.flink.dragon.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.functions.FunctionAnnotation.ForwardedFields;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.util.Collector;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonCount;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonInstance;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class DragonCounter {
    private static final String AGGREGATED_VALUES_BROADCAST_NAME = "AGGREGATED_VALUES";

    public static DataSet<DragonCount> count(DataSet<DragonInstance> dragonInstances) {
        DataSet<DragonAggregate> dragonAggregates =
                dragonInstances.groupBy(2, 3).reduceGroup(new AggregateDragonExecutor());
        return dragonAggregates.map(new CountDragonExecutor())
                .withBroadcastSet(dragonAggregates, AGGREGATED_VALUES_BROADCAST_NAME);
    }

    public static class DragonAggregate extends Tuple5<String, Integer, Double, Integer, Integer> {

        public DragonAggregate() {
        }

        public DragonAggregate(String featureName, Integer featureIndex, Double featureValue,
                Integer negativeCount, Integer positiveCount) {
            super(featureName, featureIndex, featureValue, negativeCount, positiveCount);
        }

        public String getFeatureName() {
            return f0;
        }

        public void setFeatureName(String featureName) {
            f0 = featureName;
        }

        public int getFeatureIndex() {
            return f1;
        }

        public void setFeatureIndex(int featureIndex) {
            f1 = featureIndex;
        }

        public double getFeatureValue() {
            return f2;
        }

        public void setFeatureValue(double featureValue) {
            f2 = featureValue;
        }

        public int getNegativeCount() {
            return f3;
        }

        public void setNegativeCount(int negativeCount) {
            f3 = negativeCount;
        }

        public int getPositiveCount() {
            return f4;
        }

        public void setPositiveCount(int positiveCount) {
            f4 = positiveCount;
        }
    }

    @ForwardedFields("f1->f0; f2->f1; f3->f2")
    private static class AggregateDragonExecutor implements GroupReduceFunction<DragonInstance, DragonAggregate> {
        @Override
        public void reduce(Iterable<DragonInstance> iterable, Collector<DragonAggregate> collector) throws Exception {
            Iterator<DragonInstance> iterator = iterable.iterator();
            DragonInstance instance = null;
            int negativeCount = 0;
            int positiveCount = 0;
            while (iterator.hasNext()) {
                instance = iterator.next();
                if (instance.isTrue()) {
                    positiveCount++;
                } else {
                    negativeCount++;
                }
            }
            if (Objects.nonNull(instance)) {
                collector.collect(new DragonAggregate(instance.getFeatureName(), instance.getFeatureIndex(),
                        instance.getFeatureValue(), negativeCount, positiveCount));
            }
        }
    }

    @ForwardedFields("f0; f1; f2")
    private static class CountDragonExecutor extends RichMapFunction<DragonAggregate, DragonCount> {
        /**
         * Feature index and its list of aggregated values
         */
        private Map<Integer, List<DragonAggregate>> aggregateByIndex;
        /**
         * Reused object
         */
        private final DragonCount dragonCount;
        /**
         * Reused object
         */
        private int leftNegative;
        /**
         * Reused object
         */
        private int rightNegative;
        /**
         * Reused object
         */
        private int leftPositive;
        /**
         * Reused object
         */
        private int rightPositive;

        public CountDragonExecutor() {
            this.dragonCount = new DragonCount();
        }

        @Override
        public void open(Configuration parameters) {
            List<DragonAggregate> aggregatedDragons =
                    getRuntimeContext().getBroadcastVariable(AGGREGATED_VALUES_BROADCAST_NAME);
            aggregateByIndex = aggregatedDragons.stream()
                    .collect(Collectors.groupingBy(DragonAggregate::getFeatureIndex, Collectors.toList()));
        }

        @Override
        public DragonCount map(DragonAggregate dragonAggregate) throws Exception {
            count(aggregateByIndex.get(dragonAggregate.getFeatureIndex()), dragonAggregate.getFeatureValue());
            dragonCount.f0 = dragonAggregate.f0;
            dragonCount.f1 = dragonAggregate.f1;
            dragonCount.f2 = dragonAggregate.f2;
            dragonCount.setLeftNegative(leftNegative);
            dragonCount.setLeftPositive(leftPositive);
            dragonCount.setRightNegative(rightNegative);
            dragonCount.setRightPositive(rightPositive);
            return dragonCount;
        }

        private void count(List<DragonAggregate> dragonAggregates, double featureValue) {
            leftNegative = rightNegative = leftPositive = rightPositive = 0;
            for (DragonAggregate dragonAggregate : dragonAggregates) {
                if (Double.compare(dragonAggregate.getFeatureValue(), featureValue) < 0) {
                    leftNegative += dragonAggregate.getNegativeCount();
                    leftPositive += dragonAggregate.getPositiveCount();
                } else {
                    rightNegative += dragonAggregate.getNegativeCount();
                    rightPositive += dragonAggregate.getPositiveCount();
                }
            }
        }
    }
}
