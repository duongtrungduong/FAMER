package org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple4;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonCount;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonInstance;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonSplit;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.DragonCounter;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.DragonInstanceFactory;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;

import java.util.*;

/**
 * Fitness function that used gini measure
 */
public class GiniIndexFunction extends FitnessFunction {
    /**
     * Label for the left training instances after updating the training instances
     */
    public final static String LEFT_INSTANCES = "LEFT_INSTANCES";

    /**
     * Label for the right training instances after updating the training instances
     */
    public final static String RIGHT_INSTANCES = "RIGHT_INSTANCES";

    private List<String> featureNames;

    /**
     * Constructor with given parameters
     *
     * @param lowerThreshold lower threshold of the gini-index function
     */
    public GiniIndexFunction(double lowerThreshold, int leafMinSamples) {
        super(lowerThreshold, leafMinSamples);
    }

    @Override
    public DataSet<BinaryInstance> initialize(DataSet<BinaryInstance> binaryInstances, List<String> featureNames)
            throws Exception {
        this.featureNames = featureNames;
        return binaryInstances.map(new GiniInitializer(getLowerThreshold()));
    }

    @Override
    public DragonSplit getBestSplitFeature(DataSet<BinaryInstance> trainingInstances) throws Exception {
        DataSet<DragonInstance> dragonInstances = DragonInstanceFactory.execute(trainingInstances, featureNames)
                .filter(f -> f.getFeatureValue() > 0);
        return DragonCounter.count(dragonInstances)
                .filter(f -> f.hasMinSamples(getLeafMinSamples()))
                .map(m -> Tuple4.of(m, m.getAverageGiniIndex(), m.getLeftPositive(), m.getFeatureIndex()))
                .returns(new TypeHint<Tuple4<DragonCount, Double, Integer, Integer>>() { })
                .minBy(1, 2, 3)
                .map(m -> parseCountToSplit(m.f0))
                .collect().stream().findFirst().orElse(null);
    }

    @Override
    public boolean checkStopCondition(DragonSplit split) {
        return split.getSplitValue() <= 1.0d;
    }

    /**
     * The implementation of gini-fitness-functions using Flink
     * If the similarity value less an a given threshold then set this value to zero
     */
    private static class GiniInitializer implements MapFunction<BinaryInstance, BinaryInstance> {
        /**
         * Lower threshold of the gini-index function
         */
        private final double lowerThreshold;

        /**
         * Constructor with given parameters
         *
         * @param lowerThreshold lower threshold of the gini-index function
         */
        public GiniInitializer(double lowerThreshold) {
            this.lowerThreshold = lowerThreshold;
        }

        @Override
        public BinaryInstance map(BinaryInstance binaryInstance) {
            BinaryInstance initInstance = binaryInstance.duplicate();
            for (int i = 0; i < initInstance.getFeatures().size(); i++) {
                if (Double.compare(initInstance.getFeatures().get(i), lowerThreshold) < 0) {
                    initInstance.getFeatures().set(i, 0d);
                }
            }
            return initInstance;
        }
    }

    /**
     * Function that is used to update the training dataset
     *
     * @param currentInstances current training dataset
     * @param split the last best split feature
     * @return two new training dataset, one for training the left node and the other for training the right node
     * @throws Exception flink runtime exception
     */
    public Map<String, DataSet<BinaryInstance>> updateTrainingInstances(DataSet<BinaryInstance> currentInstances,
            DragonSplit split) throws Exception {
        DataSet<Tuple2<String, BinaryInstance>> splitInstances = currentInstances
                .map(new LeftRightLabelMaker(split.getFeatureIndex(), split.getFeatureValue()));

        DataSet<BinaryInstance> leftInstances = splitInstances
                .filter(f -> f.f0.equals(LEFT_INSTANCES)).map(m -> m.f1);
        DataSet<BinaryInstance> rightInstances = splitInstances
                .filter(f -> f.f0.equals(RIGHT_INSTANCES)).map(m -> m.f1);

        return new HashMap<String, DataSet<BinaryInstance>>() {{
            put(LEFT_INSTANCES, leftInstances);
            put(RIGHT_INSTANCES, rightInstances);
        }};

    }

    /**
     * The label maker for labeling which dataset after update is used to trained the left/right node
     */
    private static class LeftRightLabelMaker implements MapFunction<BinaryInstance, Tuple2<String,BinaryInstance>> {
        /**
         * Feature index of the last best feature split
         */
        private final int splitFeatureIndex;

        /**
         * Feature value of the last best feature split
         */
        private final double splitFeatureValue;

        /**
         * Constructor with given parameters
         *
         * @param splitFeatureIndex feature index of the last best feature split
         * @param splitFeatureValue feature value of the last best feature split
         */
        public LeftRightLabelMaker(int splitFeatureIndex, double splitFeatureValue) {
            this.splitFeatureIndex = splitFeatureIndex;
            this.splitFeatureValue = splitFeatureValue;
        }

        @Override
        public Tuple2<String, BinaryInstance> map(BinaryInstance binaryInstance) throws Exception {
            String leftOrRight = binaryInstance.getFeatures().get(splitFeatureIndex) < splitFeatureValue
                    ? LEFT_INSTANCES : RIGHT_INSTANCES;
            binaryInstance.getFeatures().set(splitFeatureIndex, 0d);
            return Tuple2.of(leftOrRight, binaryInstance);
        }
    }

    @Override
    public JSONObject getAsJson() throws JSONException {
        return new JSONObject().accumulate("fitnessFunctionType", FitnessFunctionType.GINI_INDEX)
                .accumulate("lowerThreshold", getLowerThreshold())
                .accumulate("leafMinSamples", getLeafMinSamples());
    }

    @Override
    public FitnessFunction copy() {
        return new GiniIndexFunction(getLowerThreshold(), getLeafMinSamples());
    }
}