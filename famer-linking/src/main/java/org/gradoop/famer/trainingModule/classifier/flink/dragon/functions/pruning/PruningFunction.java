package org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning;

import org.apache.flink.api.java.DataSet;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonTree;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;

import java.io.Serializable;

/**
 * Abstract class all pruning function classes that used to prune dragon-decision-tree
 * Currently implementation: {@link ErrorEstimatePruner}, {@link FMeasurePruner}
 */
public abstract class PruningFunction implements Serializable {
    /**
     * Available pruning methods for dragon-decision-tree
     */
    public enum PruningMethod {
        /**
         * No pruning
         */
        NONE,
        /**
         * Use error estimate pruning
         */
        ERROR_ESTIMATE,
        /**
         * Use global F1-score pruning
         */
        F_MEASURE
    }

    public abstract DragonTree prune(DataSet<BinaryInstance> trainingInstances, DragonTree tree) throws Exception;

    /**
     * Get the pruning method config as Json
     * @return the Json config the this pruner
     * @throws JSONException Json parsing exception
     */
    public abstract JSONObject getAsJson() throws JSONException;

    /**
     * Return the name of this pruning method
     * @return the name of this pruning method
     */
    public String getName() {
        return getClass().getSimpleName();
    }
}
