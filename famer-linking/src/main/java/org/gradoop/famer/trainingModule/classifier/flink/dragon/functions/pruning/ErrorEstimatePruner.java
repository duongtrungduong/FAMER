package org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning;

import org.apache.commons.math3.distribution.FDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.util.FastMath;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.util.Collector;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonLabel;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonNode;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonSplit;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonTree;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class ErrorEstimatePruner extends PruningFunction {
    private final double confidenceLevel;

    public ErrorEstimatePruner(double confidenceLevel) {
        this.confidenceLevel = confidenceLevel;
    }

    @Override
    public DragonTree prune(DataSet<BinaryInstance> trainingInstances, DragonTree tree) throws Exception {
        if (tree.isEmpty()) return tree;
        for (;;) {
            DragonTree prunedTree = getPrunedTree(trainingInstances, tree.copy());
            if (tree.equals(prunedTree)) return tree;
            tree = prunedTree.copy();
        }
    }

    private DragonTree getPrunedTree(DataSet<BinaryInstance> trainingInstances, DragonTree tree) throws Exception {
        List<Tuple3<DragonNode, Integer, Integer>> leavesErrorCount = trainingInstances
                .map(new CountErrorAtLeaf(tree))
                .groupBy(0)
                .reduceGroup(new CountErrorAtLeaf(tree))
                .collect();

        Map<DragonNode, List<Tuple3<DragonNode, Integer, Integer>>> childrenAndParentError = leavesErrorCount
                .stream().map(m -> Tuple4.of(m.f0.getParentNode(), m.f0, m.f1, m.f2))
                .filter(f -> Objects.nonNull(f.f0))
                .collect(Collectors.groupingBy(g -> g.f0, Collectors.mapping(m -> Tuple3.of(m.f1, m.f2, m.f3),
                        Collectors.toList())));

        List<DragonNode> dragonNodes = tree.getAsList();
        childrenAndParentError.forEach((k, v) -> {
            Tuple3<DragonNode, Integer, Integer> lChildError = v.stream().filter(f -> !f.f0.getLabel().isTrue())
                    .findAny().orElse(null);
            Tuple3<DragonNode, Integer, Integer> rChildError = v.stream().filter(f -> f.f0.getLabel().isTrue())
                    .findAny().orElse(null);
            if (lChildError != null && rChildError != null) {
                int ln = lChildError.f1;
                int lnd = lChildError.f2;
                int rn = rChildError.f1;
                int rnd = rChildError.f2;
                int n = ln + rn;
                int nd = lnd + rnd;
                double lEstimateError = calculateWilsonUpperBound(ln, lnd, confidenceLevel);
                double rEstimateError = calculateWilsonUpperBound(rn, rnd, confidenceLevel);
                double pEstimateError = calculateWilsonUpperBound(n, nd, confidenceLevel);
                double combineEstimateError = (ln * lEstimateError + rn * rEstimateError) / (ln + rn);
                if (pEstimateError < combineEstimateError) pruneChildren(dragonNodes, k);
            }
        });
        return tree;
    }

    private void pruneChildren(List<DragonNode> dragonNodes, DragonNode nodeBePruned) {
        if (nodeBePruned.isRoot()) return;
        if (nodeBePruned.getParentNode().getLeftNode().equals(nodeBePruned)) {
            dragonNodes.get(dragonNodes.indexOf(nodeBePruned)).setLabel(new DragonLabel(false));
        } else {
            dragonNodes.get(dragonNodes.indexOf(nodeBePruned)).setLabel(new DragonLabel(true));
        }
    }

    private static class CountErrorAtLeaf implements MapFunction<BinaryInstance, Tuple2<Integer, Integer>>,
            GroupReduceFunction<Tuple2<Integer, Integer>, Tuple3<DragonNode, Integer, Integer>> {
        private final DragonTree dragonTree;
        private final Map<DragonNode, Integer> nodeAndIndex;

        public CountErrorAtLeaf(DragonTree dragonTree) {
            this.dragonTree = dragonTree;
            this.nodeAndIndex = new HashMap<>();
            List<DragonNode> dragonNodes = dragonTree.getAsList();
            for (int i = 0; i < dragonNodes.size(); i++) {
                nodeAndIndex.put(dragonNodes.get(i), i);
            }
        }

        @Override
        public Tuple2<Integer, Integer> map(BinaryInstance instance) throws Exception {
            return countError(instance, dragonTree.getRootNode());
        }

        private Tuple2<Integer, Integer> countError(BinaryInstance instance, DragonNode currentNode) {
            if (currentNode.isLeaf()) {
                if (currentNode.getLabel().isTrue() == instance.isTrue()) {
                    return Tuple2.of(nodeAndIndex.get(currentNode), 0);
                } else {
                    return Tuple2.of(nodeAndIndex.get(currentNode), 1);
                }
            }
            DragonSplit split = currentNode.getSplit();
            return (Double.compare(instance.getFeatures().get(split.getFeatureIndex()), split.getFeatureValue()) < 0)
                    ? countError(instance, currentNode.getLeftNode())
                    : countError(instance, currentNode.getRightNode());
        }

        @Override
        public void reduce(Iterable<Tuple2<Integer, Integer>> iterable,
                Collector<Tuple3<DragonNode, Integer, Integer>> collector) throws Exception {
            int numError = 0;
            int numExamples = 0;
            int nodeIndex = -1;
            for (Tuple2<Integer, Integer> values : iterable) {
                numError += values.f1;
                numExamples++;
                if (nodeIndex == -1) nodeIndex = values.f0;
            }
            List<DragonNode> dragonNodes = dragonTree.getAsList();
            if (nodeIndex > 0 && nodeIndex < dragonNodes.size()) {
                collector.collect(Tuple3.of(dragonNodes.get(nodeIndex), numExamples, numError));
            }
        }
    }

    private double calculateWilsonUpperBound(int numSamples, int numDefects, double confidenceLevel) {
        int numSuccesses = numSamples - numDefects;
        NormalDistribution normalDistribution = new NormalDistribution();
        double z = normalDistribution.inverseCumulativeProbability(1.0D - confidenceLevel);
        double zSquared = FastMath.pow(z, 2);
        double mean = (double) numSuccesses / (double) numSamples;
        double factor = 1.0D / (1.0D + 1.0D / (double) numSamples * zSquared);
        double modifiedSuccessRatio = mean + 1.0D / (double) (2 * numSamples) * zSquared;
        double difference = z * FastMath
                .sqrt(1.0D / (double) numSamples * mean * (1.0D - mean) + 1.0D / (4.0D * FastMath
                        .pow(numSamples, 2)) * zSquared);
        return factor * (modifiedSuccessRatio + difference);
    }

    private double calculateClopperPearsonUpperBound(int numSamples, int numDefects, double confidenceLevel) {
        if (numDefects == 0) return 1d - Math.pow(1d - confidenceLevel, 1d / numSamples);
        if (numDefects == numSamples) return 1d;
        double upperBound = 0.0D;
        double alpha = (1.0D - confidenceLevel);
        FDistribution distributionUpperBound = new FDistribution(2 * (numDefects + 1), 2 * (numSamples - numDefects));
        double fValueUpperBound = distributionUpperBound.inverseCumulativeProbability(1.0D - alpha);
        if (numDefects > 0) {
            upperBound = (double) (numDefects + 1) * fValueUpperBound
                    / ((double) (numSamples - numDefects) + (double) (numDefects + 1) * fValueUpperBound);
        }
        return upperBound;
    }

    @Override
    public JSONObject getAsJson() throws JSONException {
        return new JSONObject().accumulate("pruningMethod", PruningMethod.ERROR_ESTIMATE)
                .accumulate("confidenceLevel", confidenceLevel);
    }
}