package org.gradoop.famer.trainingModule.classifier.flink.dragon.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonLabel;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonNode;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonSplit;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonTree;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;
import org.gradoop.famer.trainingModule.common.dataStructures.CommonInstance;

import java.util.Vector;

public class DragonPredictor implements MapFunction<CommonInstance, CommonInstance> {
    private final DragonTree dragonTree;

    public DragonPredictor(DragonTree dragonTree) {
        this.dragonTree = dragonTree;
    }

    @Override
    public CommonInstance map(CommonInstance unlabeledInstance) throws Exception {
        return predict(unlabeledInstance, dragonTree);
    }

    public static BinaryInstance predict(CommonInstance unlabeledInstance, DragonTree dragonTree) {
        if (dragonTree == null || dragonTree.isEmpty()) {
            throw new IllegalStateException("Dragon decision tree is null or empty.");
        }
        return predict(unlabeledInstance.getFeatures(), dragonTree.getRootNode());
    }

    private static BinaryInstance predict(Vector<Double> featureValues, DragonNode currentNode) {
        if (currentNode.isLeaf()) {
            DragonLabel dragonLabel = currentNode.getLabel();
            return new BinaryInstance(featureValues, dragonLabel.isTrue(), dragonLabel.getProbability());
        }
        DragonSplit split = currentNode.getSplit();
        return (Double.compare(featureValues.get(split.getFeatureIndex()), split.getFeatureValue()) < 0)
                ? predict(featureValues, currentNode.getLeftNode())
                : predict(featureValues, currentNode.getRightNode());
    }
}
