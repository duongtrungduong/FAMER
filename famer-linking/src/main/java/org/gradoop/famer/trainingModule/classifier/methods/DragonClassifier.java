package org.gradoop.famer.trainingModule.classifier.methods;

import org.apache.flink.api.java.DataSet;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.classifier.dataStructures.DragonClassifierComponent;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonTree;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.DragonPredictor;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.DragonTrainer;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FitnessFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.PruningFunction;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;
import org.gradoop.famer.trainingModule.common.dataStructures.CommonInstance;

import java.util.List;

public class DragonClassifier extends CommonClassifier {
    private DragonTree dragonTree;

    public DragonClassifier(JSONObject jsonClassifier) {
        super(jsonClassifier);
        JSONObject jsonTree = jsonClassifier.optJSONObject("dragonTree");
        if (jsonTree != null) {
            this.dragonTree = new DragonTree(jsonTree);
        }
    }

    public DragonClassifier(DragonClassifierComponent classifierComponent) {
        super(classifierComponent);
    }

    @Override
    public void fit(DataSet<CommonInstance> trainingInstances) throws Exception {
        FitnessFunction fitnessFunction = getClassifierComponent().getFitnessFunction();
        PruningFunction pruningFunction = getClassifierComponent().getPruningFunction();
        List<String> featureNames = getClassifierComponent().getFeatureNames();
        int treeMaxHeight = getClassifierComponent().getTreeMaxHeight();

        DataSet<BinaryInstance> binaryInstances = trainingInstances.map(m ->
                new BinaryInstance(m.getFeatures(),m.getLabel() == BinaryInstance.TRUE, m.getProbability()));

        DragonTrainer dragonTrainer = new DragonTrainer(fitnessFunction, pruningFunction, treeMaxHeight);
        dragonTree = dragonTrainer.trainDragon(binaryInstances, featureNames);
    }

    @Override
    public CommonInstance predict(CommonInstance unlabeledInstance) throws Exception {
        return DragonPredictor.predict(unlabeledInstance, dragonTree);
    }

    @Override
    public DataSet<CommonInstance> predict(DataSet<CommonInstance> unlabeledInstances) {
        return unlabeledInstances.map(new DragonPredictor(dragonTree));
    }

    public JSONObject getAsJson() throws JSONException {
        DragonClassifierComponent dragonClassifierComponent = getClassifierComponent();
        return new JSONObject().accumulate("dragonTree", dragonTree.getAsJson())
                .accumulate("classifierComponent", dragonClassifierComponent.getAsJson());
    }

    public DragonClassifierComponent getClassifierComponent() {
        return (DragonClassifierComponent) super.getClassifierComponent();
    }

    public void setDragonTree(DragonTree dragonTree) {
        this.dragonTree = dragonTree;
    }

    public DragonTree getDragonTree() {
        return dragonTree;
    }
}
