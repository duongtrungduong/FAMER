package org.gradoop.famer.trainingModule.common.dataStructures;

import java.io.Serializable;
import java.util.Vector;

/***
 * A machine learning instance. This instance stores the features and their associated label.
 */
public class CommonInstance implements Serializable {

  /**
   * The label of the instance.
   */
  private double label;

  /**
   * The features of the instance.
   */
  private Vector<Double> features;

  /**
   * The probability of the instance.
   */
  private double probability;

  /**
   * Default constructor for Flink POJO
   */
  public CommonInstance() {
  }

  /**
   * Construct a new instance for the given features.
   *
   * @param features The features of the instance.
   */
  public CommonInstance(Vector<Double> features) {
    this(features, Double.NaN, Double.NaN);
  }

  /**
   * Construct a new instance for the given features, label and probability.
   *
   * @param features The features of the instance.
   * @param label The label of the instance.
   * @param probability The probability of the instance.
   */
  public CommonInstance(Vector<Double> features, double label, double probability) {
    this.label = label;
    this.features = features;
    this.probability = probability;
  }

  /**
   * Check if the instance has a label.
   *
   * @return True, if a label is set.
   */
  public boolean isLabeled() {
    return !Double.isNaN(label);
  }

  /**
   * Create a unlabeled copy of the instance.
   *
   * @return The copy of the instance.
   */
  public CommonInstance getUnlabeledCopy() {
    return new CommonInstance(this.getFeatures());
  }

  /**
   * Getter for label.
   *
   * @return The label of the instance.
   */
  public double getLabel() {
    return label;
  }

  /**
   * Setter for label.
   *
   * @param label The label of the instance.
   */
  public void setLabel(double label) {
    this.label = label;
  }

  /**
   * Getter for the features.
   *
   * @return The features of the instance.
   */
  public Vector<Double> getFeatures() {
    return features;
  }

  /**
   * Setter for the features.
   *
   * @param features The features of the instance.
   */
  public void setFeatures(Vector<Double> features) {
    this.features = features;
  }

  /**
   * Getter for the probability.
   *
   * @return The probability of the instance.
   */
  public double getProbability() {
    return probability;
  }

  /**
   * Setter for the probability.
   *
   * @param probability The probability of the instance.
   */
  public void setProbability(double probability) {
    this.probability = probability;
  }

  /**
   * Create a copy of the instance with all attributes.
   *
   * @return One identical copy.
   */
  public CommonInstance duplicate() {
    return new CommonInstance(new Vector<>(features), label, probability);
  }
}
