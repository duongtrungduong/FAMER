package org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness;

import org.apache.flink.api.java.DataSet;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.*;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;

import java.io.Serializable;
import java.util.List;

/**
 * Abstract class all fitness function classes that used to train dragon-decision-tree
 * Currently implementation: {@link GiniIndexFunction} and {@link FMeasureFunction}
 */
public abstract class FitnessFunction implements Serializable {
    /**
     * Available fitness functions for dragon-decision-tree
     */
    public enum FitnessFunctionType {
        /**
         * Gini-Index fitness function
         */
        GINI_INDEX,
        /**
         * Global F1-score fitness function
         */
        F_MEASURE
    }

    /**
     * Lower bound threshold
     */
    private final double lowerThreshold;

    /**
     * Min number of samples at leaf
     */
    private final int leafMinSamples;

    public FitnessFunction(double lowerThreshold, int leafMinSamples) {
        this.lowerThreshold = lowerThreshold;
        this.leafMinSamples = leafMinSamples;
    }

    /**
     * Abstract function for initialization the fitness function
     * @param trainingInstances binary training instances
     * @param featureNames name of features of the binary training instances
     * @return an enriched dataset of binary instances used for efficiently training (see {@link DragonInstance})
     * @throws Exception flink runtime exception
     */
    public abstract DataSet<BinaryInstance> initialize(DataSet<BinaryInstance> trainingInstances,
            List<String> featureNames) throws Exception;

    /**
     * Abstract function for getting the best split feature. This best split is used to build the dragon-decision-tree
     * @param trainingInstances binary training instances which were generated in the initialization phase
     * @return the best feature split for building the dragon-decision-tree
     * @throws Exception flink runtime exception
     */
    public abstract DragonSplit getBestSplitFeature(DataSet<BinaryInstance> trainingInstances) throws Exception;

    /**
     * Abstract class for checking whether the training should be stopped
     * @param split current best split feature
     * @return true if the training should be stopped, else false
     */
    public abstract boolean checkStopCondition(DragonSplit split);

    /**
     * Function that is used to create a split from the counted information
     * @param dragonCount the counted information
     * @return a split corresponding to the given counted information and fitness function
     */
    public DragonSplit parseCountToSplit(DragonCount dragonCount) {
        DragonSplit dragonSplit = new DragonSplit();
        dragonSplit.setFeatureName(dragonCount.getFeatureName());
        dragonSplit.setFeatureIndex(dragonCount.getFeatureIndex());
        dragonSplit.setFeatureValue(dragonCount.getFeatureValue());
        if (this instanceof GiniIndexFunction) {
            dragonSplit.setSplitValue(dragonCount.getAverageGiniIndex());
        } else if (this instanceof FMeasureFunction) {
            dragonSplit.setSplitValue(dragonCount.getFMeasure());
        }
        dragonSplit.setFitnessFunctionName(getName());
        return dragonSplit;
    }

    /**
     * Get the fitness function config as Json
     * @return the Json config the this fitness function
     * @throws JSONException Json parsing exception
     */
    public abstract JSONObject getAsJson() throws JSONException;

    /**
     * Return the name of this fitness function
     * @return the name of this fitness function
     */
    public String getName() {
        return getClass().getSimpleName();
    }

    /**
     * Get a copy of this fitness function
     * @return a copy of this fitness function
     */
    public abstract FitnessFunction copy();

    /**
     * Get the lower threshold
     *
     * @return lower threshold
     */
    public double getLowerThreshold() {
        return lowerThreshold;
    }

    /**
     * Get the min number of samples at leaf
     * @return min number of samples at leaf
     */
    public int getLeafMinSamples() {
        return leafMinSamples;
    }
}
