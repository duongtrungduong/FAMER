package org.gradoop.famer.preprocessing.io.benchmarks.dblp.acm;

import com.google.common.collect.Lists;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.configuration.ConfigConstants;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.RestOptions;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.api.entities.GraphHead;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMElement;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.preprocessing.io.benchmarks.musicbrainz.MusicBrainzReader;
import org.gradoop.flink.io.impl.csv.CSVDataSink;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.util.GradoopFlinkConfig;

import java.io.IOException;
import java.util.List;

public class LogicalGraphGenerator {
//    public static void main(String[] args) throws Exception {
//        Configuration conf = new Configuration();
//        conf.setBoolean(ConfigConstants.LOCAL_START_WEBSERVER, true);
//        conf.setInteger(RestOptions.PORT, 8082);
//        ExecutionEnvironment env = ExecutionEnvironment.createLocalEnvironmentWithWebUI(conf);
//        env.setParallelism(1);
//        GradoopFlinkConfig gradoopFlinkConfig = GradoopFlinkConfig.createConfig(env);
//
//        DblpAcmReader dblpAcmReader = new DblpAcmReader();
//        GraphCollection gc =
//                dblpAcmReader.getBenchmarkDataAsGraphCollection("/home/duongtrungduong/Desktop/MA/test/csv/dblpAcmMini/");
//
//        GradoopId dblpID = gc.getGraphHeads().filter(f -> f.getLabel().equals("DBLP")).map(EPGMElement::getId).collect().get(0);
//        GradoopId acmID = gc.getGraphHeads().filter(f -> f.getLabel().equals("ACM")).map(EPGMElement::getId).collect().get(0);
//        CSVDataSink dblpDataSink = new CSVDataSink("/home/duongtrungduong/Desktop/MA/test/lg/mini/dblp", gradoopFlinkConfig);
//        CSVDataSink acmDataSink = new CSVDataSink("/home/duongtrungduong/Desktop/MA/test/lg/mini/acm", gradoopFlinkConfig);
//
//        gc.getConfig().getExecutionEnvironment().setParallelism(1);
//        gc.getGraph(dblpID).writeTo(dblpDataSink, true);
//        gc.getGraph(acmID).writeTo(acmDataSink, true);
//
//        gc.print();
//    }

    public static void main(String[] args) throws Exception {
        ExecutionEnvironment env = ExecutionEnvironment.createLocalEnvironment();
        env.setParallelism(12);
        GradoopFlinkConfig gradoopFlinkConfig = GradoopFlinkConfig.createConfig(env);

        MusicBrainzReader reader = new MusicBrainzReader();
        GraphCollection gc = reader.getBenchmarkDataAsGraphCollection(
                "/home/duongtrungduong/Desktop/MA/test/csv/musicbrainz20K/20K/");

        CSVDataSink dataSink = new CSVDataSink("/home/duongtrungduong/Desktop/MA/test/lg/musicbrainz20K", gradoopFlinkConfig);

        gc.getConfig().getExecutionEnvironment();
        gc.writeTo(dataSink, true);

        System.out.println(gc.getVertices().map(m->m.getPropertyValue("id")).distinct().count());

//        gc.getVertices().map(f -> f.getPropertyValue("graphLabel")).distinct().print();

        gc.getVertices().groupBy(new KeySelector<EPGMVertex, Long>() {
            @Override
            public Long getKey(EPGMVertex v) throws Exception {
                return v.getPropertyValue("CID").getLong();
            }
        }).reduceGroup(new GroupReduceFunction<EPGMVertex, String>() {
            @Override
            public void reduce(Iterable<EPGMVertex> iterable, Collector<String> collector) throws Exception {
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@");
                List<EPGMVertex> vertices = Lists.newArrayList(iterable);
                for (int i =0; i<vertices.size()-1; i++) {
                    for (int j =i+1; j<vertices.size(); j++) {
                        System.out.println(vertices.get(i).getPropertyValue("id").toString() + ","
                                + vertices.get(j).getPropertyValue("id").toString());
                    }
                }
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@");
            }
        }).collect();
    }
}