/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test class for {@link TruncateBegin}
 */
public class TruncateBeginTest {

  private TruncateBegin truncateBegin = new TruncateBegin(2);

  @Test(expected = Exception.class)
  public void testComputeSimilarityForFirstNullValue() throws Exception {
    truncateBegin.computeSimilarity(null, "");
  }

  @Test(expected = Exception.class)
  public void testComputeSimilarityForSecondNullValue() throws Exception {
    truncateBegin.computeSimilarity("", null);
  }

  @Test
  public void testComputeSimilarity() throws Exception {
    double result1 = truncateBegin.computeSimilarity("ABC", "");

    assertEquals(0d, result1, 0d);

    double result2 = truncateBegin.computeSimilarity("ABC", "ACB");

    assertEquals(0d, result2, 0d);

    double result3 = truncateBegin.computeSimilarity("ABC", "ABC");

    assertEquals(1d, result3, 0d);

    double result4 = truncateBegin.computeSimilarity("ABC", "ABD");

    assertEquals(1d, result4, 0d);
  }
}
