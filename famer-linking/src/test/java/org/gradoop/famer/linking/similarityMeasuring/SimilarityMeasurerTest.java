/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring;

import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponentBaseConfig;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityField;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityFieldList;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.TruncateBeginComponent;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.gradoop.famer.linking.linking.Linker.GRAPH_LABEL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Test class for {@link SimilarityMeasurer}
 */
public class SimilarityMeasurerTest extends GradoopFlinkTestBase {

  @Test
  public void testSimilarityMeasuringWithTwoTruncateBeginComponents() throws Exception {
    String label = "person";
    String nameAttr = "name";
    String surNameAttr = "surname";

    EPGMVertex vertexA = createVertex(label, "A", "ABC", "ABC");
    EPGMVertex vertexB = createVertex(label, "B", "ABD", "ABD");
    EPGMVertex vertexC = createVertex(label, "C", "ACB", "ABE");

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> blockedVertices = getExecutionEnvironment().fromElements(
      Tuple2.of(vertexA, vertexB), Tuple2.of(vertexA, vertexC), Tuple2.of(vertexB, vertexC));

    List<SimilarityComponent> similarityComponents = new ArrayList<>();

    SimilarityComponentBaseConfig baseConfigSimName = new SimilarityComponentBaseConfig("simName",
      "*", label, nameAttr, "*", label, nameAttr, 1.0);
    SimilarityComponentBaseConfig baseConfigSimSurname = new SimilarityComponentBaseConfig("simSurName",
      "*", label, surNameAttr, "*", label, surNameAttr, 1.0);

    similarityComponents.add(new TruncateBeginComponent(baseConfigSimName, 2));
    similarityComponents.add(new TruncateBeginComponent(baseConfigSimSurname, 2));

    SimilarityMeasurer similarityMeasurer = new SimilarityMeasurer(similarityComponents);

    DataSet<Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList>> vertexVertexSimFieldList =
      blockedVertices.flatMap(similarityMeasurer);

    Map<String, List<Double>> results = new HashMap<>();

    vertexVertexSimFieldList.map(tuple -> {
      assertFalse(tuple.f2.getSimilarityFields().isEmpty());
      assertEquals(2, tuple.f2.getSimilarityFields().size());

      String id1 = tuple.f0.getPropertyValue("id").toString();
      String id2 = tuple.f1.getPropertyValue("id").toString();
      if (id1.compareTo(id2) < 0) {
        return Tuple2.of(id1 + id2, tuple.f2);
      } else {
        return Tuple2.of(id2 + id1, tuple.f2);
      }
    }).returns(new TypeHint<Tuple2<String, SimilarityFieldList>>() { })
      .collect().forEach(tuple -> results.put(tuple.f0, tuple.f1.getSimilarityFields().stream()
      .map(SimilarityField::getSimilarityValue).collect(Collectors.toList())));

    // expected values
    List<Double> similaritiesAB = Arrays.asList(1d, 1d);
    List<Double> similaritiesAC = Arrays.asList(0d, 1d);
    List<Double> similaritiesBC = Arrays.asList(0d, 1d);

    assertEquals(similaritiesAB, results.get("AB"));
    assertEquals(similaritiesAC, results.get("AC"));
    assertEquals(similaritiesBC, results.get("BC"));
  }

  private EPGMVertex createVertex(String label, String id, String name, String surname) {

    Properties properties = new Properties();
    properties.set("id", id);
    properties.set("name", name);
    properties.set("surname", surname);
    properties.set(GRAPH_LABEL, "graphLabel");

    EPGMVertex vertex = new EPGMVertex();
    vertex.setId(GradoopId.get());
    vertex.setLabel(label);
    vertex.setProperties(properties);

    return vertex;
  }
}
