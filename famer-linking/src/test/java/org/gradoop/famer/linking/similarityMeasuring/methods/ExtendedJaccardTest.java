/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test class for {@link ExtendedJaccard}
 */
public class ExtendedJaccardTest {

  private ExtendedJaccard extendedJaccard = new ExtendedJaccard(" ", 0.8, 0.5);

  @Test(expected = Exception.class)
  public void testComputeSimilarityForFirstNullValue() throws Exception {
    extendedJaccard.computeSimilarity(null, "");
  }

  @Test(expected = Exception.class)
  public void testComputeSimilarityForSecondNullValue() throws Exception {
    extendedJaccard.computeSimilarity("", null);
  }

  @Test
  public void testComputeSimilarity() throws Exception {
    double result1 = extendedJaccard.computeSimilarity("My test string", "test My implementation");
    assertEquals(0.5, result1, 0.0001);

    double result2 = extendedJaccard.computeSimilarity("My second test string", "test My second implementation");
    assertEquals(0.6, result2, 0.0001);

    double result3 = extendedJaccard.computeSimilarity("ABCDEF", "");
    assertEquals(0.0, result3, 0d);

  }
}
