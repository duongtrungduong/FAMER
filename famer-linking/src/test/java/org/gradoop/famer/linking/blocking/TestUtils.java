/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TestUtils {

  /**
   * Collects the tuples of paired vertices to a list and transforms them to sorted tuples with extracted
   * property value of the vertices
   *
   * @param pairedVertices The paired vertices
   * @param propertyKey The property to extract
   *
   * @return A list with the name tuples
   *
   * @throws Exception Thrown on errors while collecting from DataSet
   */
  public static List<Tuple2<String, String>> collectAndTransformResultTuples(
    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices, String propertyKey) throws Exception {

    return pairedVertices.collect().stream().map(tuple -> {
      String propVal1 = tuple.f0.getPropertyValue(propertyKey).getString();
      String propVal2 = tuple.f1.getPropertyValue(propertyKey).getString();
      if (propVal1.compareTo(propVal2) < 0) {
        return Tuple2.of(propVal1, propVal2);
      } else {
        return Tuple2.of(propVal2, propVal1);
      }
    }).sorted(Comparator.comparing(tuple -> tuple.f0 + tuple.f1)).collect(Collectors.toList());
  }
}
