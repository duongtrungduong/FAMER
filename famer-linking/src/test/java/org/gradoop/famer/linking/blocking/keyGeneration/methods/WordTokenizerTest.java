/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.keyGeneration.methods;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Test class for {@link WordTokenizer}
 */
public class WordTokenizerTest {

  private final String attribute = "test attribute for Word-Tokenizer";

  @Test
  public void testGenerateKeyForUnknownTokenizer() {
    List<String> result = new WordTokenizer("_").generateKey(attribute);

    assertFalse(result.isEmpty());
    assertEquals(1, result.size());
    assertEquals(attribute, result.get(0));
  }

  @Test
  public void testGenerateKeyForWhitespaceTokenizer() {
    List<String> expectedResult = Arrays.asList("test", "attribute", "for", "Word-Tokenizer");

    List<String> result = new WordTokenizer(" ").generateKey(attribute);

    assertFalse(result.isEmpty());
    assertEquals(4, result.size());
    assertEquals(expectedResult, result);
  }
}
