package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.EditDistanceComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.QGramsComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponentBaseConfig;
import org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions.GoldGraphCreator;
import org.gradoop.famer.linking.similarityMeasuring.dragonPreparation.functions.TrainDataCreator;
import org.gradoop.famer.trainingModule.classifier.methods.DragonClassifier;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DragonTest extends GradoopFlinkTestBase {
    private static final String sourceGraph = "DBLP";
    private static final String targetGraph = "ACM";
    private static final String sourceLabel = "*";
    private static final String targetLabel = "*";
    private GraphCollection inputGraphs;
    private DataSet<Tuple2<String, String>> groundTruth;

    @Before
    public void setUp() throws Exception {
        String inputGDLGraphs = "DBLP [" +
                "(v_DBLP_0:DBLP {graphLabel:\"DBLP\",venue:\"SIGMOD Record\",year:1998," +
                "id:\"journals/sigmod/ReichB98\",title:\"DA Componentized Architecture for Dynamic Electronic " +
                "Markets\"," +
                "authors:\"Israel Ben-Shaul, Benny Reich\"})" +

                "(v_DBLP_2:DBLP {graphLabel:\"DBLP\",venue:\"SIGMOD Conference\",year:1999," +
                "id:\"conf/sigmod/BraumandlKK99\",title:\"Database Patchwork on the Internet\",authors:\"Reinhard " +
                "Braumandl, Alfons Kemper, Donald Kossmann\"})" +
                "]" +

                "ACM [" +
                "(v_ACM_5:ACM {graphLabel:\"ACM\",venue:\"ACM SIGMOD Record \",year:1998,id:306115L,title:\"A " +
                "componentized architecture for dynamic electronic markets\",authors:\"Benny Reich, Israel " +
                "Ben-Shaul\"})" +

                "(v_ACM_6:ACM {graphLabel:\"ACM\",venue:\"International Conference on Management of Data\"," +
                "year:1999,id:304573L,title:\"Database patchwork on the Internet\",authors:\"Reinhard Braumandl, " +
                "Alfons Kemper, Donald Kossmann\"})" +
                "]";
        groundTruth = getExecutionEnvironment().fromCollection(Arrays.asList(
                Tuple2.of("conf/sigmod/BraumandlKK99", "304573"), Tuple2.of("journals/sigmod/ReichB98", "306115")));
        inputGraphs = getLoaderFromString(inputGDLGraphs).getGraphCollectionByVariables(sourceGraph, targetGraph);
    }

    @Test
    public void testGoldGraphCreator() throws Exception {
        LogicalGraph goldGraph = GoldGraphCreator.getGoldGraph(inputGraphs, groundTruth, getConfig());
        assertEquals(4L, goldGraph.getVertices().count());
        assertEquals(2L, goldGraph.getEdges().count());
    }

    @Test
    public void testTrainDataCreator() throws Exception {
        LogicalGraph goldGraph = GoldGraphCreator.getGoldGraph(inputGraphs, groundTruth, getConfig());
        List<SimilarityComponent> similarityComponents = Arrays.asList(
                new EditDistanceComponent(
                        createSimilarityComponentBaseConfig("EditDistance Title-Title", "title", "title")
                ),
                new QGramsComponent(
                        createSimilarityComponentBaseConfig("QGrams Title-Title", "title", "title"),
                        2, true, QGrams.SecondMethod.DICE
                ),
                new QGramsComponent(
                        createSimilarityComponentBaseConfig("QGrams Authors-Authors", "authors", "authors"),
                        2, true, QGrams.SecondMethod.DICE
                )
        );
        DataSet<BinaryInstance> trainingDataSet = TrainDataCreator
                .getTrainingInstances(goldGraph, similarityComponents);
        System.out.println(trainingDataSet.count());
        assertEquals(2, trainingDataSet.filter(BinaryInstance::isTrue).count());
        assertEquals(2, trainingDataSet.filter(f -> !f.isTrue()).count());
        assertEquals(4, trainingDataSet.filter(f -> f.getFeatures().size() == 3).count());
    }

    @Test
    public void testComputeSimilarity() throws Exception {
        String jsonClassifier = "{" +
                "  \"dragonTree\": {\"rootNode\": {" +
                "    \"split\": {" +
                "      \"featureName\": \"EditDistance Title-Title\"," +
                "      \"featureIndex\": 0," +
                "      \"featureValue\": 0.8," +
                "      \"splitValue\": 0.8," +
                "      \"fitnessFunctionName\": \"FMeasureFunction\"" +
                "    }," +
                "    \"leftNode\": {" +
                "      \"split\": {" +
                "        \"featureName\": \"QGrams Authors-Authors DICE\"," +
                "        \"featureIndex\": 2," +
                "        \"featureValue\": 0.7," +
                "        \"splitValue\": 0.9," +
                "        \"fitnessFunctionName\": \"FMeasureFunction\"" +
                "      }," +
                "      \"leftNode\": {\"label\": {" +
                "        \"label\": false," +
                "        \"probability\": 0.91," +
                "        \"giniIndex\": 0.0," +
                "        \"entropy\": 0.0" +
                "      }}," +
                "      \"rightNode\": {\"label\": {" +
                "        \"label\": true," +
                "        \"probability\": 0.8," +
                "        \"giniIndex\": 0.0," +
                "        \"entropy\": 0.0" +
                "      }}" +
                "    }," +
                "    \"rightNode\": {\"label\": {" +
                "      \"label\": true," +
                "      \"probability\": 0.95," +
                "      \"giniIndex\": 0.0," +
                "      \"entropy\": 0.0" +
                "    }}" +
                "  }}," +
                "  \"classifierComponent\": {" +
                "    \"classifierMethod\": \"DragonClassifierComponent\"," +
                "    \"featureNames\": [" +
                "      \"EditDistance Title-Title\"," +
                "      \"QGrams Title-Title DICE\"," +
                "      \"QGrams Authors-Authors DICE\"" +
                "    ]," +
                "    \"className\": \"isMatch\"," +
                "    \"classValues\": [" +
                "      \"true\"," +
                "      \"false\"" +
                "    ]," +
                "    \"classBalancer\": {\"classBalanceMethod\": \"NONE\"}," +
                "    \"fitnessFunction\": {" +
                "      \"fitnessFunctionType\": \"GINI_INDEX\"," +
                "      \"lowerThreshold\": 0.05," +
                "      \"leafMinSamples\": 0" +
                "    }," +
                "    \"pruningFunction\": {\"pruningMethod\": \"NONE\"}," +
                "    \"treeMaxHeight\": 3," +
                "    \"trainingSimilarityConfigs\": [" +
                "      {" +
                "        \"id\": \"EditDistance Title-Title\"," +
                "        \"sourceGraph\": \"DBLP\"," +
                "        \"targetGraph\": \"ACM\"," +
                "        \"sourceLabel\": \"GoldGraphVertexLabel\"," +
                "        \"targetLabel\": \"GoldGraphVertexLabel\"," +
                "        \"sourceAttribute\": \"title\"," +
                "        \"targetAttribute\": \"title\"," +
                "        \"weight\": 1," +
                "        \"similarityMethod\": \"EditDistanceComponent\"" +
                "      }," +
                "      {" +
                "        \"id\": \"QGrams Title-Title DICE\"," +
                "        \"sourceGraph\": \"DBLP\"," +
                "        \"targetGraph\": \"ACM\"," +
                "        \"sourceLabel\": \"GoldGraphVertexLabel\"," +
                "        \"targetLabel\": \"GoldGraphVertexLabel\"," +
                "        \"sourceAttribute\": \"title\"," +
                "        \"targetAttribute\": \"title\"," +
                "        \"weight\": 1," +
                "        \"length\": 3," +
                "        \"padding\": true," +
                "        \"secondMethod\": \"DICE\"," +
                "        \"similarityMethod\": \"QGramsComponent\"" +
                "      }," +
                "      {" +
                "        \"id\": \"QGrams Authors-Authors DICE\"," +
                "        \"sourceGraph\": \"DBLP\"," +
                "        \"targetGraph\": \"ACM\"," +
                "        \"sourceLabel\": \"GoldGraphVertexLabel\"," +
                "        \"targetLabel\": \"GoldGraphVertexLabel\"," +
                "        \"sourceAttribute\": \"authors\"," +
                "        \"targetAttribute\": \"authors\"," +
                "        \"weight\": 1," +
                "        \"length\": 3," +
                "        \"padding\": true," +
                "        \"secondMethod\": \"DICE\"," +
                "        \"similarityMethod\": \"QGramsComponent\"" +
                "      }" +
                "    ]" +
                "  }" +
                "}";
        DragonClassifier classifier = new DragonClassifier(new JSONObject(jsonClassifier));
        Dragon dragon = new Dragon(classifier);
        List<PropertyValue> list1;
        List<PropertyValue> list2;

        list1 = Arrays.asList(PropertyValue.create("title"), PropertyValue.create("authors"));
        list2 = Arrays.asList(PropertyValue.create("title"), PropertyValue.create("authors"));
        double result1 = dragon.computeSimilarity(list1, list2);
        assertEquals(0.95d, result1, 0d);

        list1 = Arrays.asList(PropertyValue.create("title"), PropertyValue.create("authors"));
        list2 = Arrays.asList(PropertyValue.create("ti-t-le"), PropertyValue.create("authors"));
        double result2 = dragon.computeSimilarity(list1, list2);
        assertEquals(0.8d, result2, 0d);

        list1 = Arrays.asList(PropertyValue.create("title"), PropertyValue.create("authors"));
        list2 = Arrays.asList(PropertyValue.create("title"), PropertyValue.create("au-t-h-or-s"));
        double result3 = dragon.computeSimilarity(list1, list2);
        assertEquals(0.95d, result3, 0d);

        list1 = Arrays.asList(PropertyValue.create("title"), PropertyValue.create("authors"));
        list2 = Arrays.asList(PropertyValue.create("abc"), PropertyValue.create("efg"));
        double result4 = dragon.computeSimilarity(list1, list2);
        assertEquals(0.09d, result4, 0.001d);

        list1 = Collections.singletonList(PropertyValue.create("title"));
        list2 = Collections.singletonList(PropertyValue.create("title"));
        double result5 = dragon.computeSimilarity(list1, list2);
        assertEquals(0d, result5, 0d);
    }

    private static SimilarityComponentBaseConfig createSimilarityComponentBaseConfig(String simId,
            String sourceAttribute, String targetAttribute) {
        return new SimilarityComponentBaseConfig(simId,
                sourceGraph, sourceLabel, sourceAttribute,
                targetGraph, targetLabel, targetAttribute,
                1.0);
    }
}
