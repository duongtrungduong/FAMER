/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.linking;

import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.keyGeneration.BlockingKeyGenerator;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.FullAttributeComponent;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.KeyGeneratorComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponentBaseConfig;
import org.gradoop.famer.linking.blocking.methods.dataStructures.CartesianProductComponent;
import org.gradoop.famer.linking.linking.dataStructures.LinkerComponent;
import org.gradoop.famer.linking.selection.dataStructures.SelectionComponent;
import org.gradoop.famer.linking.selection.dataStructures.aggregatorRule.AggregatorRule;
import org.gradoop.famer.linking.selection.dataStructures.aggregatorRule.AggregatorRuleComponent;
import org.gradoop.famer.linking.selection.dataStructures.aggregatorRule.AggregatorRuleComponentType;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.Condition;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.ConditionOperator;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRule;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRuleComponent;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRuleComponentType;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponentBaseConfig;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.TruncateBeginComponent;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.incremental.common.PropertyNames.NEIGHBOR;
import static org.gradoop.famer.incremental.common.PropertyNames.NEW_LINK;
import static org.gradoop.famer.linking.linking.Linker.GRAPH_LABEL;
import static org.gradoop.famer.linking.linking.Linker.SIM_VALUE;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test class for {@link Linker}
 */
public class LinkerTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  private List<BlockingComponent> blockingComponents;

  private List<SimilarityComponent> similarityComponents;

  private SelectionComponent selectionComponent;

  @Before
  public void setUp() throws Exception {
    // create graph
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\", key: \"alice\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(allan:Person {id:2, name:\"Allan\", key: \"allan\", " + GRAPH_LABEL + ":\"g2\"})" +
      "(bob:Person {id:3, name:\"Bob\", key: \"bob\", " + GRAPH_LABEL + ":\"g3\", " + CLUSTER_ID + ":\"c3\"})" +
      "(carol:Person {id:4, name:\"Carol\", key: \"carol\", " + GRAPH_LABEL + ":\"g4\"})" +
      "(bob:Person {})-[]->(carol:Person)" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    // create blocking components: use full attribute for key generation and cartesian product for blocking
    KeyGeneratorComponent keyGeneratorComponent = new FullAttributeComponent("key");
    BlockingKeyGenerator blockingKeyGenerator = new BlockingKeyGenerator(keyGeneratorComponent);
    Map<String, Set<String>> graphPairs = new HashMap<>();
    Set<String> values = new HashSet<>();
    values.add("*");
    graphPairs.put("*", values);
    Map<String, Set<String>> categoryPairs = new HashMap<>();
    categoryPairs.put("*", values);
    BlockingComponentBaseConfig blockingBaseConfig =
      new BlockingComponentBaseConfig(blockingKeyGenerator, graphPairs, categoryPairs);
    blockingComponents = Collections.singletonList(new CartesianProductComponent(blockingBaseConfig));

    // create similarity components: compute similarity on first two letters of name property
    SimilarityComponentBaseConfig simBaseConfig =
      new SimilarityComponentBaseConfig("simName", "*", "Person",
        "name", "*", "Person", "name", 1.0);
    similarityComponents = Collections.singletonList(new TruncateBeginComponent(simBaseConfig, 2));

    // create selection rule: keep similarities >= 1.0
    List<Condition> conditions = Collections.singletonList(
      new Condition("greaterEqual", "simName", ConditionOperator.GREATER_EQUAL, 1.0));
    List<SelectionRuleComponent> selectionRuleComponents = Collections.singletonList(
      new SelectionRuleComponent(SelectionRuleComponentType.CONDITION, "greaterEqual"));
    SelectionRule selectionRule = new SelectionRule(conditions, selectionRuleComponents);

    // create and aggregation rule: add 1.0 to simValue
    List<AggregatorRuleComponent> aggregatorRuleComponents = Arrays.asList(
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "simName"),
      new AggregatorRuleComponent(AggregatorRuleComponentType.ARITHMETIC_OPERATOR, "PLUS"),
      new AggregatorRuleComponent(AggregatorRuleComponentType.CONSTANT, "1"));
    AggregatorRule aggregatorRule = new AggregatorRule(aggregatorRuleComponents, 0d);

    // create selection component
    selectionComponent = new SelectionComponent(selectionRule, aggregatorRule);
  }

  @Test
  public void testLinkerForGraphCollectionWithoutKeepingCurrentEdges() throws Exception {
    LinkerComponent linkerComponent =
      new LinkerComponent(blockingComponents, similarityComponents, selectionComponent, false, false);

    // run linking for input graph
    GraphCollection inputAsCollection = getConfig().getGraphCollectionFactory().fromGraph(inputGraph);

    evaluateLinkingWithoutKeepingCurrentEdges(new Linker(linkerComponent).executeLinking(inputAsCollection));
  }

  @Test
  public void testLinkerForLogicalGraphWithoutKeepingCurrentEdges() throws Exception {
    LinkerComponent linkerComponent =
      new LinkerComponent(blockingComponents, similarityComponents, selectionComponent, false, false);

    // run linking for input graph
    evaluateLinkingWithoutKeepingCurrentEdges(new Linker(linkerComponent).executeLinking(inputGraph));
  }

  private void evaluateLinkingWithoutKeepingCurrentEdges(LogicalGraph resultGraph) throws Exception {
    // expect 1 similarity edge between Alice with simValue 2.0
    assertEquals(4L, resultGraph.getVertices().count());
    assertEquals(1L, resultGraph.getEdges().count());

    EPGMEdge edge = resultGraph.getEdges().collect().get(0);

    assertEquals(2d, edge.getPropertyValue(SIM_VALUE).getDouble(), 0d);

    List<EPGMVertex> vertices = resultGraph.getVertices().collect();
    EPGMVertex source = vertices.stream().filter(v -> v.getId().equals(edge.getSourceId()))
      .collect(Collectors.toList()).get(0);
    EPGMVertex target = vertices.stream().filter(v -> v.getId().equals(edge.getTargetId()))
      .collect(Collectors.toList()).get(0);

    assertEquals("Alice", source.getPropertyValue("name").getString());
    assertEquals("Allan", target.getPropertyValue("name").getString());
  }


  @Test
  public void testLinkerForGraphCollectionWithKeepingCurrentEdges() throws Exception {
    LinkerComponent linkerComponent =
      new LinkerComponent(blockingComponents, similarityComponents, selectionComponent, true, false);

    // run linking for input graph
    GraphCollection inputAsCollection = getConfig().getGraphCollectionFactory().fromGraph(inputGraph);

    evaluateLinkingWithKeepingCurrentEdges(new Linker(linkerComponent).executeLinking(inputAsCollection));
  }

  @Test
  public void testLinkerForLogicalGraphWithKeepingCurrentEdges() throws Exception {
    LinkerComponent linkerComponent =
      new LinkerComponent(blockingComponents, similarityComponents, selectionComponent, true, false);

    // run linking for input graph
    evaluateLinkingWithKeepingCurrentEdges(new Linker(linkerComponent).executeLinking(inputGraph));
  }

  private void evaluateLinkingWithKeepingCurrentEdges(LogicalGraph resultGraph) throws Exception {
    // expect 1 similarity edge between Alice with simValue 2.0 and 1 old edge between Bob and Carol
    assertEquals(4L, resultGraph.getVertices().count());
    assertEquals(2L, resultGraph.getEdges().count());

    List<EPGMEdge> edges = resultGraph.getEdges().collect();

    EPGMEdge simEdge =
      edges.stream().filter(e -> e.hasProperty(SIM_VALUE)).collect(Collectors.toList()).get(0);

    assertEquals(2d, simEdge.getPropertyValue(SIM_VALUE).getDouble(), 0d);

    List<EPGMVertex> vertices = resultGraph.getVertices().collect();
    EPGMVertex source = vertices.stream().filter(v -> v.getId().equals(simEdge.getSourceId()))
      .collect(Collectors.toList()).get(0);
    EPGMVertex target = vertices.stream().filter(v -> v.getId().equals(simEdge.getTargetId()))
      .collect(Collectors.toList()).get(0);

    assertEquals("Alice", source.getPropertyValue("name").getString());
    assertEquals("Allan", target.getPropertyValue("name").getString());
  }

  @Test
  public void testLinkerForGraphCollectionWithKeepingCurrentEdgesAndRecomputedSimilarity() throws Exception {
    LinkerComponent linkerComponent =
      new LinkerComponent(blockingComponents, similarityComponents, selectionComponent, true, true);

    // run linking for input graph
    GraphCollection inputAsCollection = getConfig().getGraphCollectionFactory().fromGraph(inputGraph);

    evaluateLinkingWithKeepingCurrentEdgesAndRecomputedSimilarity(new Linker(linkerComponent).executeLinking(inputAsCollection));
  }

  @Test
  public void testLinkerForLogicalGraphWithKeepingCurrentEdgesAndRecomputedSimilarity() throws Exception {
    LinkerComponent linkerComponent =
      new LinkerComponent(blockingComponents, similarityComponents, selectionComponent, true, true);

    // run linking for input graph
    evaluateLinkingWithKeepingCurrentEdgesAndRecomputedSimilarity(new Linker(linkerComponent).executeLinking(inputGraph));
  }

  private void evaluateLinkingWithKeepingCurrentEdgesAndRecomputedSimilarity(LogicalGraph resultGraph) throws Exception {
    // expect 1 similarity edge between Alice with simValue 2.0
    assertEquals(4L, resultGraph.getVertices().count());
    assertEquals(1L, resultGraph.getEdges().count());

    EPGMEdge edge = resultGraph.getEdges().collect().get(0);

    assertEquals(2d, edge.getPropertyValue(SIM_VALUE).getDouble(), 0d);

    List<EPGMVertex> vertices = resultGraph.getVertices().collect();
    EPGMVertex source = vertices.stream().filter(v -> v.getId().equals(edge.getSourceId()))
      .collect(Collectors.toList()).get(0);
    EPGMVertex target = vertices.stream().filter(v -> v.getId().equals(edge.getTargetId()))
      .collect(Collectors.toList()).get(0);

    assertEquals("Alice", source.getPropertyValue("name").getString());
    assertEquals("Allan", target.getPropertyValue("name").getString());
  }

  /* tests regarding linking with the incremental module */

  @Test
  public void testLinkerForLogicalGraphWithTagNewNewLink() throws Exception {
    LinkerComponent linkerComponent = new LinkerComponent(blockingComponents, similarityComponents,
      selectionComponent, false, false, false, true);

    // run linking for input graph
    LogicalGraph resultGraph = new Linker(linkerComponent).executeLinking(inputGraph);
    // expect the edge to have a new link property
    assertTrue(resultGraph.getEdges().collect().get(0).hasProperty(NEW_LINK));
  }

  @Test
  public void testLinkerForLogicalGraphWithTagNeighborCluster() throws Exception {
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\", key: \"alice\", " + GRAPH_LABEL + ":\"g1\", " + CLUSTER_ID + ":\"c1\"})" +
      "(allan:Person {id:2, name:\"Allan\", key: \"allan\", " + GRAPH_LABEL + ":\"g2\"})" +
      "(bob:Person {id:3, name:\"Bob\", key: \"bob\", " + GRAPH_LABEL + ":\"g3\"})" +
      "(carol:Person {id:4, name:\"Carol\", key: \"carol\", " + GRAPH_LABEL + ":\"g4\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    LinkerComponent linkerComponent = new LinkerComponent(blockingComponents, similarityComponents,
      selectionComponent, false, false, true, false);

    // run linking for input graph
    LogicalGraph resultGraph = new Linker(linkerComponent).executeLinking(inputGraph);
    // expect the edge to have a neighbor property with the value 'c'
    assertTrue(resultGraph.getEdges().collect().get(0).hasProperty(NEIGHBOR));
    assertEquals("c1", resultGraph.getEdges().collect().get(0).getPropertyValue(NEIGHBOR).getString());
  }
}
