/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.standardBlocking;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.api.entities.VertexFactory;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.keyGeneration.BlockingKeyGenerator;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.FullAttributeComponent;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.KeyGeneratorComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponentBaseConfig;
import org.gradoop.famer.linking.blocking.methods.dataStructures.StandardBlockingComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.StandardBlockingEmptyKeyStrategy;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.gradoop.famer.linking.blocking.TestUtils.collectAndTransformResultTuples;
import static org.gradoop.famer.linking.linking.Linker.GRAPH_LABEL;
import static org.junit.Assert.*;

/**
 * Test class for {@link StandardBlocking}
 */
public class StandardBlockingTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  private BlockingComponentBaseConfig baseConfig;

  private final int parallelismDegree = 4;

  private VertexFactory<EPGMVertex> vertexFactory;

  @Before
  public void setUp() {
    KeyGeneratorComponent keyGeneratorComponent = new FullAttributeComponent("key");
    BlockingKeyGenerator blockingKeyGenerator = new BlockingKeyGenerator(keyGeneratorComponent);
    Map<String, Set<String>> graphPairs = new HashMap<>();
    Set<String> values = new HashSet<>();
    values.add("*");
    graphPairs.put("*", values);
    Map<String, Set<String>> categoryPairs = new HashMap<>();
    categoryPairs.put("*", values);
    baseConfig = new BlockingComponentBaseConfig(blockingKeyGenerator, graphPairs, categoryPairs);
    vertexFactory = getConfig().getLogicalGraphFactory().getVertexFactory();
  }

  /**
   * Tests if load balancing bug is fixed
   *
   * @throws Exception
   */
  @Test
  public void testLoadBalancingProblem() throws Exception {
    String wantedKey = "b";

    String graphString =
      "input[" + "(alice:Person {id:1, name:\"Alice\", key: \"" + wantedKey + "\", " + GRAPH_LABEL +
        ":\"g1\"})" + "(bob:Person {id:2, name:\"Bob\", key: \"" + wantedKey + "\", " + GRAPH_LABEL +
        ":\"g1\"})" + "(carol:Person {id:3, name:\"Carol\", key: \"a\", " + GRAPH_LABEL +
        ":\"g1\"})" + "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    StandardBlockingComponent standardBlockingComponent =
      new StandardBlockingComponent(baseConfig, parallelismDegree);

    //Perform blocking
    List<Tuple2<EPGMVertex, EPGMVertex>> blockedVertices =
      new StandardBlocking(standardBlockingComponent).generatePairedVertices(inputGraph.getVertices())
        .collect();

    //Get vertices with wanted key
    List<EPGMVertex> wantedVertices =
      inputGraph.getVertices().filter(v -> v.getPropertyValue("key").toString().equals(wantedKey)).collect();
    List<Tuple2<EPGMVertex, EPGMVertex>> expectedBlock =
      Collections.singletonList(new Tuple2<>(wantedVertices.get(0), wantedVertices.get(1)));

    Assert.assertEquals(expectedBlock, blockedVertices);
  }

  /**
   * Test if StandardBlocking works correct for empty keys with {@link StandardBlockingEmptyKeyStrategy}
   * {@code EMPTY_BLOCK}
   */
  @Test
  public void testGeneratePairedVerticesForEmptyKeyWithEmptyBlockStrategy() throws Exception {
    String graphString =
      "input[" + "(alice:Person {id:1, name:\"Alice\", key: \"a\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(bob:Person {id:2, name:\"Bob\", key: \"a\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(carol:Person {id:3, name:\"Carol\", key: \"\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(dave:Person {id:4, name:\"Dave\", key: \"\", " + GRAPH_LABEL + ":\"g1\"})" + "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    StandardBlockingEmptyKeyStrategy emptyKeyStrategy = StandardBlockingEmptyKeyStrategy.EMPTY_BLOCK;

    StandardBlockingComponent standardBlockingComponent =
      new StandardBlockingComponent(baseConfig, parallelismDegree, emptyKeyStrategy);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new StandardBlocking(standardBlockingComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult =
      Arrays.asList(Tuple2.of("Alice", "Bob"), Tuple2.of("Carol", "Dave"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(2, result.size());
    assertEquals(expectedResult, result);
  }

  /**
   * Test if StandardBlocking works correct for empty keys with {@link StandardBlockingEmptyKeyStrategy}
   * {@code REMOVE}
   */
  @Test
  public void testGeneratePairedVerticesForEmptyKeyWithRemoveStrategy() throws Exception {
    String graphString =
      "input[" + "(alice:Person {id:1, name:\"Alice\", key: \"a\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(bob:Person {id:2, name:\"Bob\", key: \"a\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(carol:Person {id:3, name:\"Carol\", key: \"\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(dave:Person {id:4, name:\"Dave\", key: \"\", " + GRAPH_LABEL + ":\"g1\"})" + "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    StandardBlockingEmptyKeyStrategy emptyKeyStrategy = StandardBlockingEmptyKeyStrategy.REMOVE;

    StandardBlockingComponent standardBlockingComponent =
      new StandardBlockingComponent(baseConfig, parallelismDegree, emptyKeyStrategy);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new StandardBlocking(standardBlockingComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Collections.singletonList(Tuple2.of("Alice", "Bob"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(1, result.size());
    assertEquals(expectedResult, result);
  }

  /**
   * Test if StandardBlocking works correct for empty keys with {@link StandardBlockingEmptyKeyStrategy}
   * {@code ADD_TO_ALL}
   */
  @Test
  public void testGeneratePairedVerticesForEmptyKeyWithAddToAllStrategy() throws Exception {
    String graphString =
      "input[" + "(alice:Person {id:1, name:\"Alice\", key: \"a\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(bob:Person {id:2, name:\"Bob\", key: \"b\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(carol:Person {id:3, name:\"Carol\", key: \"\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(dave:Person {id:4, name:\"Dave\", key: \"\", " + GRAPH_LABEL + ":\"g1\"})" + "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    StandardBlockingEmptyKeyStrategy emptyKeyStrategy = StandardBlockingEmptyKeyStrategy.ADD_TO_ALL;

    StandardBlockingComponent standardBlockingComponent =
      new StandardBlockingComponent(baseConfig, parallelismDegree, emptyKeyStrategy);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new StandardBlocking(standardBlockingComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays
      .asList(Tuple2.of("Alice", "Carol"), Tuple2.of("Alice", "Dave"), Tuple2.of("Bob", "Carol"),
        Tuple2.of("Bob", "Dave"), Tuple2.of("Carol", "Dave"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(5, result.size());
    assertEquals(expectedResult, result);
  }

  /**
   * Test if StandardBlocking works correct when input "allowed graph pairs" is limited
   */
  @Test
  public void testGeneratePairedVerticesWithLimitedGraphPairs() throws Exception {
    String graphString =
      "input[" + "(alice:Person {id:1, name:\"Alice\", key: \"a\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(bob:Person {id:2, name:\"Bob\", key: \"a\", " + GRAPH_LABEL + ":\"g2\"})" +
        "(carol:Person {id:3, name:\"Carol\", key: \"a\", " + GRAPH_LABEL + ":\"g3\"})" +
        "(dave:Person {id:4, name:\"Dave\", key: \"b\", " + GRAPH_LABEL + ":\"g2\"})" + "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    Map<String, Set<String>> limitedGraphPairs = new HashMap<>();
    // g1 is limited to g2
    Set<String> value1 = new HashSet<>();
    value1.add("g2");
    limitedGraphPairs.put("g1", value1);
    // g2 is limited to g1
    Set<String> value2 = new HashSet<>();
    value2.add("g1");
    limitedGraphPairs.put("g2", value2);

    baseConfig.setGraphPairs(limitedGraphPairs);

    StandardBlockingComponent standardBlockingComponent =
      new StandardBlockingComponent(baseConfig, parallelismDegree);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new StandardBlocking(standardBlockingComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Collections.singletonList(Tuple2.of("Alice", "Bob"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(1, result.size());
    assertEquals(expectedResult, result);
  }

  /**
   * Test if StandardBlocking works correct when input "allowed category pairs" is limited
   */
  @Test
  public void testGeneratePairedVerticesWithLimitedCategoryPairs() throws Exception {
    String graphString =
      "input[" + "(alice:Person {id:1, name:\"Alice\", key: \"a\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(bob:Teacher {id:2, name:\"Bob\", key: \"a\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(carol:Other {id:3, name:\"Carol\", key: \"a\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(dave:Person {id:4, name:\"Dave\", key: \"b\", " + GRAPH_LABEL + ":\"g1\"})" + "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    Map<String, Set<String>> limitedCategoryPairs = new HashMap<>();
    // Person is limited to Teacher
    Set<String> value1 = new HashSet<>();
    value1.add("Teacher");
    limitedCategoryPairs.put("Person", value1);
    // Teacher is limited to Person
    Set<String> value2 = new HashSet<>();
    value2.add("Person");
    limitedCategoryPairs.put("Teacher", value2);

    baseConfig.setCategoryPairs(limitedCategoryPairs);

    StandardBlockingComponent standardBlockingComponent =
      new StandardBlockingComponent(baseConfig, parallelismDegree);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new StandardBlocking(standardBlockingComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Collections.singletonList(Tuple2.of("Alice", "Bob"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(1, result.size());
    assertEquals(expectedResult, result);
  }

  /**
   * Test if StandardBlocking works correct for no matching keys
   */
  @Test
  public void testGeneratePairedVerticesForUnmatchedKeys() throws Exception {
    String graphString =
      "input[" + "(alice:Person {id:1, name:\"Alice\", key: \"a\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(bob:Person {id:2, name:\"Bob\", key: \"b\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(carol:Person {id:3, name:\"Carol\", key: \"c\", " + GRAPH_LABEL + ":\"g1\"})" +
        "(dave:Person {id:4, name:\"Dave\", key: \"d\", " + GRAPH_LABEL + ":\"g1\"})" + "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    StandardBlockingComponent standardBlockingComponent =
      new StandardBlockingComponent(baseConfig, parallelismDegree);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new StandardBlocking(standardBlockingComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<EPGMVertex, EPGMVertex>> result = pairedVertices.collect();

    assertTrue(result.isEmpty());
  }

  /**
   * Test if StandardBlocking works correct for an empty graph
   */
  @Test
  public void testGeneratePairedVerticesForEmptyGraph() throws Exception {
    inputGraph = getLoaderFromString("input[]").getLogicalGraphByVariable("input");

    StandardBlockingComponent standardBlockingComponent =
      new StandardBlockingComponent(baseConfig, parallelismDegree);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new StandardBlocking(standardBlockingComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<EPGMVertex, EPGMVertex>> result = pairedVertices.collect();

    assertTrue(result.isEmpty());
  }

  /**
   * Test if StandardBlocking works correct for one vertex
   */
  @Test
  public void testGeneratePairedVerticesForOneVertex() throws Exception {
    inputGraph = getLoaderFromString("input[(alice:Person {id:1, name:\"Alice\", key:\"a\"})]")
      .getLogicalGraphByVariable("input");

    StandardBlockingComponent standardBlockingComponent =
      new StandardBlockingComponent(baseConfig, parallelismDegree);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new StandardBlocking(standardBlockingComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<EPGMVertex, EPGMVertex>> result = pairedVertices.collect();

    assertTrue(result.isEmpty());
  }

  /**
   * Test if StandardBlocking throws exception when vertices miss the graphLabel property
   */
  @Test(expected = Exception.class)
  public void testGeneratePairedVerticesForMissingGraphLabelProperty() throws Exception {
    String graphString =
      "input[" + "(alice:Person {id:1, name:\"Alice\"})" + "(bob:Person {id:2, name:\"Bob\"})" +
        "(carol:Person {id:3, name:\"Carol\"})" + "(dave:Person {id:4, name:\"Dave\"})" + "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    StandardBlockingComponent standardBlockingComponent =
      new StandardBlockingComponent(baseConfig, parallelismDegree);

    new StandardBlocking(standardBlockingComponent).generatePairedVertices(inputGraph.getVertices())
      .collect();
  }
}
