/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.keyGeneration.methods;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Test class for {@link QGrams}
 */
public class QGramsTest {

  private final String attribute = "test";

  private final int q = 2;

  @Test(expected = Exception.class)
  public void testGenerateKeyForInvalidThreshold() {
    new QGrams(q, 1.5).generateKey(attribute);
  }

  @Test
  public void testGenerateKeyForThreshold0() {
    List<String> expectedResult = Arrays.asList("st", "es", "esst", "te", "test", "tees", "teesst");

    List<String> result = new QGrams(q, 0).generateKey(attribute);

    assertFalse(result.isEmpty());
    assertEquals(expectedResult.size(), result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testGenerateKeyForThreshold1() {
    List<String> expectedResult = Collections.singletonList("teesst");

    List<String> result = new QGrams(q, 1).generateKey(attribute);

    assertFalse(result.isEmpty());
    assertEquals(expectedResult.size(), result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testGenerateKeyForMaxQ() {
    List<String> result = new QGrams(Integer.MAX_VALUE, 1).generateKey(attribute);

    assertFalse(result.isEmpty());
    assertEquals(1, result.size());
    assertEquals(attribute, result.get(0));
  }
}
