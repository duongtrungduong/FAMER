/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test class for {@link SimilarityComputation}
 */
public class SimilarityComputationDefaultTest {

  private SimilarityComputation similarityComputation = Mockito.spy(SimilarityComputation.class);

  @SuppressWarnings("unchecked")
  @Test
  public void testDefaultComputeSimilarity() throws Exception {
    PropertyValue propertyValue1 = new PropertyValue();
    PropertyValue propertyValue2 = new PropertyValue();

    Object parsedObject1 = new Object();
    Object parsedObject2 = new Object();

    double expected = 0d;

    Mockito.when(similarityComputation.parsePropertyValue(propertyValue1)).thenReturn(parsedObject1);
    Mockito.when(similarityComputation.parsePropertyValue(propertyValue2)).thenReturn(parsedObject2);
    Mockito.when(similarityComputation.computeSimilarity(parsedObject1, parsedObject2)).thenReturn(expected);

    double result = similarityComputation.computeSimilarity(propertyValue1, propertyValue2);

    assertEquals(expected, result, 0d);
  }

  @Test
  public void testDefaultGetString() {
    String result = similarityComputation.getString(PropertyValue.create(" test  String "));

    assertEquals("test string", result);
  }

  @Test
  public void testDefaultGetNumber() {
    Number testNumber = 1L;
    Number result1 = similarityComputation.getNumber(PropertyValue.create(testNumber));

    assertNotNull(result1);
    assertEquals(1L, result1.longValue());

    String testString = "1.0";
    Number result2 = similarityComputation.getNumber(PropertyValue.create(testString));

    assertNotNull(result2);
    assertEquals(1d, result2.doubleValue(), 0d);
  }

  @Test(expected = Exception.class)
  public void testDefaultGetNumberForInvalidFormat() {
    String testString = "A";
    similarityComputation.getNumber(PropertyValue.create(testString));
  }
}
