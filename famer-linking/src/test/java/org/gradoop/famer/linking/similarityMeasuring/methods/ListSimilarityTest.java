/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * Test class for {@link ListSimilarity}
 */
public class ListSimilarityTest {

  private ListSimilarity listSimilarity = new ListSimilarity(
    new EditDistanceLevenshtein(), new ListSimilarity.AggregationMethodMin());

  @Test(expected = Exception.class)
  public void testComputeSimilarityForFirstNullValue() throws Exception {
    listSimilarity.computeSimilarity(null, new ArrayList<>());
  }

  @Test(expected = Exception.class)
  public void testComputeSimilarityForSecondNullValue() throws Exception {
    listSimilarity.computeSimilarity(new ArrayList<>(), null);
  }

  @Test
  public void testComputeSimilarityForAggregationMin() throws Exception {
    List<String> list1 = Arrays.asList("ABC", "ABC");
    List<String> list2 = Arrays.asList("ABC", "ABC");
    List<PropertyValue> propList1 = list1.stream().map(PropertyValue::create).collect(Collectors.toList());
    List<PropertyValue> propList2 = list2.stream().map(PropertyValue::create).collect(Collectors.toList());

    double result1 = listSimilarity.computeSimilarity(propList1, propList2);

    assertEquals(1d, result1, 0d);

    list1 = Arrays.asList("ABC", "ABC");
    list2 = Arrays.asList("", "");
    propList1 = list1.stream().map(PropertyValue::create).collect(Collectors.toList());
    propList2 = list2.stream().map(PropertyValue::create).collect(Collectors.toList());

    double result2 = listSimilarity.computeSimilarity(propList1, propList2);

    assertEquals(0d, result2, 0d);

    list1 = Arrays.asList("ABC", "ABC");
    list2 = Arrays.asList("A", "ABC");
    propList1 = list1.stream().map(PropertyValue::create).collect(Collectors.toList());
    propList2 = list2.stream().map(PropertyValue::create).collect(Collectors.toList());

    double result3 = listSimilarity.computeSimilarity(propList1, propList2);

    assertEquals(1d / 3d, result3, 0.0001);
  }

  @Test
  public void testComputeSimilarityForAggregationMax() throws Exception {
    listSimilarity = new ListSimilarity(
      new EditDistanceLevenshtein(), new ListSimilarity.AggregationMethodMax());

    List<String> list1 = Arrays.asList("ABC", "ABC");
    List<String> list2 = Arrays.asList("ABC", "ABC");
    List<PropertyValue> propList1 = list1.stream().map(PropertyValue::create).collect(Collectors.toList());
    List<PropertyValue> propList2 = list2.stream().map(PropertyValue::create).collect(Collectors.toList());

    double result1 = listSimilarity.computeSimilarity(propList1, propList2);

    assertEquals(1d, result1, 0d);

    list1 = Arrays.asList("ABC", "ABC");
    list2 = Arrays.asList("", "");
    propList1 = list1.stream().map(PropertyValue::create).collect(Collectors.toList());
    propList2 = list2.stream().map(PropertyValue::create).collect(Collectors.toList());

    double result2 = listSimilarity.computeSimilarity(propList1, propList2);

    assertEquals(0d, result2, 0d);

    list1 = Arrays.asList("ABC", "ABC");
    list2 = Arrays.asList("A", "ABC");
    propList1 = list1.stream().map(PropertyValue::create).collect(Collectors.toList());
    propList2 = list2.stream().map(PropertyValue::create).collect(Collectors.toList());

    double result3 = listSimilarity.computeSimilarity(propList1, propList2);

    assertEquals(1d, result3, 0d);
  }

  @Test
  public void testComputeSimilarityForAggregationAverage() throws Exception {
    listSimilarity = new ListSimilarity(
      new EditDistanceLevenshtein(), new ListSimilarity.AggregationMethodAverage());

    List<String> list1 = Arrays.asList("ABC", "ABC");
    List<String> list2 = Arrays.asList("ABC", "ABC");
    List<PropertyValue> propList1 = list1.stream().map(PropertyValue::create).collect(Collectors.toList());
    List<PropertyValue> propList2 = list2.stream().map(PropertyValue::create).collect(Collectors.toList());

    double result1 = listSimilarity.computeSimilarity(propList1, propList2);

    assertEquals(1d, result1, 0d);

    list1 = Arrays.asList("ABC", "ABC");
    list2 = Arrays.asList("", "");
    propList1 = list1.stream().map(PropertyValue::create).collect(Collectors.toList());
    propList2 = list2.stream().map(PropertyValue::create).collect(Collectors.toList());

    double result2 = listSimilarity.computeSimilarity(propList1, propList2);

    assertEquals(0d, result2, 0d);

    list1 = Arrays.asList("ABC", "ABC");
    list2 = Arrays.asList("A", "ABC");
    propList1 = list1.stream().map(PropertyValue::create).collect(Collectors.toList());
    propList2 = list2.stream().map(PropertyValue::create).collect(Collectors.toList());

    double result3 = listSimilarity.computeSimilarity(propList1, propList2);

    assertEquals(2d / 3d, result3, 0.0001d);
  }

  @Test
  public void testComputeSimilarityForNumericalSimilarityComputation() throws Exception {
    listSimilarity = new ListSimilarity(
      new NumericalSimilarityWithMaxDistance(10.0), new ListSimilarity.AggregationMethodMax());

    List<Integer> list1 = Arrays.asList(100, 200);
    List<Integer> list2 = Arrays.asList(104, 206);
    List<PropertyValue> propList1 = list1.stream().map(PropertyValue::create).collect(Collectors.toList());
    List<PropertyValue> propList2 = list2.stream().map(PropertyValue::create).collect(Collectors.toList());

    double result1 = listSimilarity.computeSimilarity(propList1, propList2);

    assertEquals(0.6, result1, 0d);

    List<Double> list3 = Arrays.asList(100.0, 200.0);
    List<Double> list4 = Arrays.asList(206.0, 115.0);
    propList1 = list3.stream().map(PropertyValue::create).collect(Collectors.toList());
    propList2 = list4.stream().map(PropertyValue::create).collect(Collectors.toList());

    double result2 = listSimilarity.computeSimilarity(propList1, propList2);

    assertEquals(0.4, result2, 0d);

    list3 = Arrays.asList(100.0, 200.0);
    list4 = Arrays.asList(0.0, 0.0);
    propList1 = list3.stream().map(PropertyValue::create).collect(Collectors.toList());
    propList2 = list4.stream().map(PropertyValue::create).collect(Collectors.toList());

    double result3 = listSimilarity.computeSimilarity(propList1, propList2);

    assertEquals(0.0, result3, 0d);
  }
}
