/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test class for {@link LongestCommonSubstring}
 */
public class LongestCommonSubstringTest {

  private LongestCommonSubstring longestCommonSubstring =
    new LongestCommonSubstring(LongestCommonSubstring.SecondMethod.OVERLAP);

  @Test(expected = Exception.class)
  public void testComputeSimilarityForFirstNullValue() throws Exception {
    longestCommonSubstring.computeSimilarity(null, "");
  }

  @Test(expected = Exception.class)
  public void testComputeSimilarityForSecondNullValue() throws Exception {
    longestCommonSubstring.computeSimilarity("", null);
  }

  @Test
  public void testComputeSimilarity() throws Exception {
    double result1 = longestCommonSubstring.computeSimilarity("ABC", "");

    assertEquals(0d, result1, 0d);

    double result2 = longestCommonSubstring.computeSimilarity("ABC", "ABC");

    assertEquals(1d, result2, 0d);

    double result3 = longestCommonSubstring.computeSimilarity("ABC", "ADE");

    assertEquals(1d / 3d, result3, 0d);

    double result4 = longestCommonSubstring.computeSimilarity("ABC", "EBC");

    assertEquals(2d / 3d, result4, 0d);
  }
}
