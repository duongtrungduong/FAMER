package org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonNode;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonSplit;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonTree;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ErrorEstimatePrunerTest {
    private final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
    private final ErrorEstimatePruner pruner = new ErrorEstimatePruner(0.75);

    @Test
    public void testPrune() throws Exception {
        String jsonTree = "{\"rootNode\": {" +
                "  \"split\": {" +
                "    \"featureName\": \"Name 2\"," +
                "    \"featureIndex\": 2," +
                "    \"featureValue\": 0.8," +
                "    \"splitValue\": 0," +
                "    \"fitnessFunctionName\": \"test\"" +
                "  }," +
                "  \"leftNode\": {\"label\": {" +
                "    \"label\": false," +
                "    \"probability\": 0," +
                "    \"giniIndex\": 0," +
                "    \"entropy\": 0" +
                "  }}," +
                "  \"rightNode\": {\"label\": {" +
                "    \"label\": true," +
                "    \"probability\": 0," +
                "    \"giniIndex\": 0," +
                "    \"entropy\": 0" +
                "  }}" +
                "}}";

        // add (redundancy) node
        DragonTree tree = new DragonTree(new JSONObject(jsonTree));
        DragonNode leftRedundancy = new DragonNode(new DragonSplit("Name 0", 0,
                0.6, 0.0, "test"));
        tree.getRootNode().setLeftNode(leftRedundancy);

//         data which generates error on redundancy node
        List<BinaryInstance> data0 = new ArrayList<>();
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.98, 0.91, 0.92)), true, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(1.0, 1.0, 1.0)), true, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.4, 1.0, 0.92)), true, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(1.0, 1.0, 1.0)), true, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.90, 0.91, 0.92)), true, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.60, 0.56, 0.72)), false, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.64, 0.74, 0.34)), false, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.73, 0.77, 0.24)), false, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.31, 0.22, 0.11)), false, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.55, 0.33, 0.24)), false, 0));
        DataSet<BinaryInstance> binaryInstances0 = env.fromCollection(data0);
        DragonTree prunedTree0 = pruner.prune(binaryInstances0, tree);
        assertEquals(1, prunedTree0.getHeight());
        assertTrue(prunedTree0.getRootNode().getLeftNode().isLeaf());
        assertTrue(prunedTree0.getRootNode().getRightNode().isLeaf());

        // data which does not generates error on redundancy node
        List<BinaryInstance> data1 = new ArrayList<>();
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(0.98, 0.91, 0.92)), true, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(1.0, 1.0, 1.0)), true, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(0.4, 1.0, 0.92)), true, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(1.0, 1.0, 1.0)), true, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(0.90, 0.91, 0.92)), true, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(0.50, 0.91, 0.72)), false, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(0.50, 0.74, 0.34)), false, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(0.50, 0.77, 0.24)), false, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(0.31, 0.22, 0.11)), false, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(0.55, 0.33, 0.24)), false, 0));
        DataSet<BinaryInstance> binaryInstances1 = env.fromCollection(data1);
        DragonTree prunedTree1 = pruner.prune(binaryInstances1, tree);
        assertEquals(tree, prunedTree1);
    }
}
