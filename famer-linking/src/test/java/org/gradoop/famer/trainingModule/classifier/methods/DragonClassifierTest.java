package org.gradoop.famer.trainingModule.classifier.methods;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.trainingModule.classifier.dataStructures.ClassifierComponentBaseConfig;
import org.gradoop.famer.trainingModule.classifier.dataStructures.DragonClassifierComponent;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonTree;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.FMeasureFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness.GiniIndexFunction;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.pruning.NonePruner;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;
import org.gradoop.famer.trainingModule.common.dataStructures.CommonInstance;
import org.gradoop.famer.trainingModule.common.functions.ClassBalancer;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import static org.junit.Assert.*;

public class DragonClassifierTest {
    private final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
    private DragonClassifierComponent gComponent;
    private DragonClassifierComponent fComponent;
    private DragonClassifier classifier;

    @Before
    public void setUp() throws Exception {
        String jsonSimConfig = "[" +
                "    {" +
                "      \"id\": \"EditDistance Title-Title\"," +
                "      \"sourceGraph\": \"DBLP\"," +
                "      \"targetGraph\": \"ACM\"," +
                "      \"sourceLabel\": \"GoldGraphVertexLabel\"," +
                "      \"targetLabel\": \"GoldGraphVertexLabel\"," +
                "      \"sourceAttribute\": \"title\"," +
                "      \"targetAttribute\": \"title\"," +
                "      \"weight\": 1," +
                "      \"similarityMethod\": \"EditDistanceComponent\"" +
                "    }," +
                "    {" +
                "      \"id\": \"MongeElkan Title-Title\"," +
                "      \"sourceGraph\": \"DBLP\"," +
                "      \"targetGraph\": \"ACM\"," +
                "      \"sourceLabel\": \"GoldGraphVertexLabel\"," +
                "      \"targetLabel\": \"GoldGraphVertexLabel\"," +
                "      \"sourceAttribute\": \"title\"," +
                "      \"targetAttribute\": \"title\"," +
                "      \"weight\": 1," +
                "      \"tokenizer\": \" \"," +
                "      \"jaroWinklerThreshold\": 0.5," +
                "      \"similarityMethod\": \"MongeElkanComponent\"" +
                "    }," +
                "    {" +
                "      \"id\": \"EditDistance Authors-Authors\"," +
                "      \"sourceGraph\": \"DBLP\"," +
                "      \"targetGraph\": \"ACM\"," +
                "      \"sourceLabel\": \"GoldGraphVertexLabel\"," +
                "      \"targetLabel\": \"GoldGraphVertexLabel\"," +
                "      \"sourceAttribute\": \"authors\"," +
                "      \"targetAttribute\": \"authors\"," +
                "      \"weight\": 1," +
                "      \"similarityMethod\": \"EditDistanceComponent\"" +
                "    }" +
                "]";
        List<String> featureNames = Arrays.asList("sim1", "sim2", "sim3");
        String className = "isMatch";
        List<String> classValues = Arrays.asList("true", "false");
        ClassBalancer classBalancer = new ClassBalancer(ClassBalancer.ClassBalanceMethod.NONE);
        ClassifierComponentBaseConfig baseConfig = new ClassifierComponentBaseConfig(featureNames, className,
                classValues, classBalancer);

        GiniIndexFunction gFunction = new GiniIndexFunction(0.1, 0);
        FMeasureFunction fFunction = new FMeasureFunction(0.1, 0.1, 0);

        int treeMaxHeight = 3;
        JSONArray simConfig = new JSONArray(jsonSimConfig);

        gComponent = new DragonClassifierComponent(baseConfig, gFunction, new NonePruner(), treeMaxHeight, simConfig);
        fComponent = new DragonClassifierComponent(baseConfig, fFunction, new NonePruner(), treeMaxHeight, simConfig);
    }

    @Test
    public void testTraining() throws Exception {
        List<BinaryInstance> data0 = new ArrayList<>();
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.98, 0.91, 0.92)), true, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(1.0, 1.0, 1.0)), true, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.4, 1.0, 0.92)), true, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(1.0, 1.0, 1.0)), true, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.90, 0.91, 0.92)), true, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.60, 0.56, 0.72)), false, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.64, 0.74, 0.34)), false, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.73, 0.77, 0.24)), false, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.31, 0.22, 0.11)), false, 0));
        data0.add(new BinaryInstance(new Vector<>(Arrays.asList(0.55, 0.33, 0.24)), false, 0));
        DataSet<CommonInstance> trainingInstances = env.fromCollection(data0).map(m -> m);

        classifier = new DragonClassifier(gComponent);
        classifier.fit(trainingInstances);
        assertNotEquals(null, classifier.getDragonTree());

        classifier = new DragonClassifier(fComponent);
        classifier.fit(trainingInstances);
        assertNotEquals(null, classifier.getDragonTree());
    }

    @Test
    public void testPredicting() throws Exception {
        String jsonTree = "{\"rootNode\":{\"split\":{\"featureName\":\"Name 2\",\"featureIndex\":2,\"featureValue\":0" +
                ".9,\"splitValue\":0,\"fitnessFunctionName\":\"test\"}," +
                "\"leftNode\":{\"split\":{\"featureName\":\"Name 0\",\"featureIndex\":0,\"featureValue\":0.6," +
                "\"splitValue\":0,\"fitnessFunctionName\":\"test\"},\"leftNode\":{\"label\":{\"label\":false," +
                "\"probability\":0,\"giniIndex\":0,\"entropy\":0}},\"rightNode\":{\"label\":{\"label\":true," +
                "\"probability\":0,\"giniIndex\":0,\"entropy\":0}}},\"rightNode\":{\"split\":{\"featureName\":\"Name " +
                "1\",\"featureIndex\":1,\"featureValue\":0.7,\"splitValue\":0,\"fitnessFunctionName\":\"test\"}," +
                "\"leftNode\":{\"label\":{\"label\":false,\"probability\":0,\"giniIndex\":0,\"entropy\":0}}," +
                "\"rightNode\":{\"label\":{\"label\":true,\"probability\":0,\"giniIndex\":0,\"entropy\":0}}}}}";

        List<CommonInstance> unlabeledInstances = new ArrayList<>();
        unlabeledInstances.add(new BinaryInstance(new Vector<>(Arrays.asList(0.98, 0.99, 0.99))));
        unlabeledInstances.add(new BinaryInstance(new Vector<>(Arrays.asList(0.2, 0.3, 0.4))));

        DragonTree tree = new DragonTree(new JSONObject(jsonTree));
        classifier = new DragonClassifier(gComponent);
        classifier.setDragonTree(tree);

        assertEquals(BinaryInstance.TRUE, classifier.predict(unlabeledInstances.get(0)).getLabel(), 0d);
        assertEquals(BinaryInstance.FALSE, classifier.predict(unlabeledInstances.get(1)).getLabel(), 0d);
    }
}
