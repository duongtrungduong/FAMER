package org.gradoop.famer.trainingModule.classifier.flink.dragon.functions.fitness;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonInstance;
import org.gradoop.famer.trainingModule.classifier.flink.dragon.dataStructures.DragonSplit;
import org.gradoop.famer.trainingModule.common.dataStructures.BinaryInstance;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class FMeasureFunctionTest {
    private final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
    private final FMeasureFunction fMeasureFunction = new FMeasureFunction(0.1, 0.1, 0);

    @Test
    public void testGetBestSplit() throws Exception {
        List<String> featureNames0 = Collections.singletonList("sim0");
        List<BinaryInstance> data0 = new ArrayList<>();
        data0.add(new BinaryInstance(new Vector<>(Collections.singletonList(0.0)), false, 0));
        data0.add(new BinaryInstance(new Vector<>(Collections.singletonList(0.0)), false, 0));
        data0.add(new BinaryInstance(new Vector<>(Collections.singletonList(0.707)), true, 0));
        data0.add(new BinaryInstance(new Vector<>(Collections.singletonList(0.816)), true, 0));
        data0.add(new BinaryInstance(new Vector<>(Collections.singletonList(1.0)), true, 0));
        DataSet<BinaryInstance> binaryInstances0 = env.fromCollection(data0);
        binaryInstances0 = fMeasureFunction.initialize(binaryInstances0, featureNames0);

        DragonSplit split0 = fMeasureFunction.getBestSplitFeature(binaryInstances0);
        assertEquals(1.0d, split0.getSplitValue(), 0.001d);
        assertEquals(0.7d, split0.getFeatureValue(), 0.001d);
        assertEquals(0, split0.getFeatureIndex());

        List<String> featureNames1 = Arrays.asList("sim0", "sim1");
        List<BinaryInstance> data1 = new ArrayList<>();
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(0d, 0.33d)), true, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(0d, 0.54d)), true, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(0d, 0.56d)), true, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(0d, 0.42d)), true, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(1d, 0.50d)), true, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(1d, 0.55d)), false, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(1d, 0.31d)), false, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(0d, 0.04d)), false, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(1d, 0.02d)), false, 0));
        data1.add(new BinaryInstance(new Vector<>(Arrays.asList(0d, 0.49d)), false, 0));
        DataSet<BinaryInstance> binaryInstances1 = env.fromCollection(data1);
        binaryInstances1 = fMeasureFunction.initialize(binaryInstances1, featureNames1);

        DragonSplit split1 = fMeasureFunction.getBestSplitFeature(binaryInstances1);
        assertEquals(0.833d, split1.getSplitValue(), 0.001d);
        assertEquals(0.3d, split1.getFeatureValue(), 0.001d);
        assertEquals(1, split1.getFeatureIndex());
    }
}
