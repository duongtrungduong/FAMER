/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.aggregation.Aggregations;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple6;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.functions.VertexToVertexClusterId;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.AnnotateValueAtIndexWithFlag;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.ComputeGroupCount;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.MapVertexToVertexData;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.PerfectCompleteClusterReducer;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.SumReduceValuesAtIndex;
import org.gradoop.famer.postprocessing.quality.functions.AnnotateValueWithFlag;
import org.gradoop.famer.postprocessing.quality.functions.MapVertexToIdAndPropertyValue;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.tuple.Project2To1;
import org.gradoop.flink.model.impl.operators.count.Count;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.clustering.common.PropertyNames.GRAPH_LABEL;

/**
 * It is used when there is no Golden Truth file and only vertices with the same ids are true positives.
 * Computes Precision, Recall, and F-Measure of the input, which can be a GraphCollection or a LogicalGraph.
 */
public class ClusteringQualityMeasuresWithSameIdsOneSource extends AbstractClusteringQualityMeasures {

  /**
   * TODO: define
   */
  private final String vertexIdLabel;

  /**
   * Whether clusters are overlapping
   */
  private final boolean hasOverlap;

  /**
   * Number of graph sources
   */
  private final int sourceNo;

  /**
   * The input clustered graph
   */
  private final LogicalGraph inputGraph;

  /**
   * The input clustered graph collection
   */
  private final GraphCollection inputGraphCollection;

  /**
   * The Flink execution environment
   */
  private final ExecutionEnvironment env;

  /**
   * Creates an instance of ClusteringQualityMeasuresWithSameIdsOneSource
   *
   * @param clusteredGraphCollection The input clustered graph collection
   * @param vertexIdLabel TODO: define
   * @param hasOverlap Whether clusters are overlapping
   */
  public ClusteringQualityMeasuresWithSameIdsOneSource(GraphCollection clusteredGraphCollection,
    String vertexIdLabel, boolean hasOverlap) {
    this(clusteredGraphCollection, vertexIdLabel, hasOverlap, 5);
  }

  /**
   * Creates an instance of ClusteringQualityMeasuresWithSameIdsOneSource
   *
   * @param clusteredGraph The input clustered graph
   * @param vertexIdLabel TODO: define
   * @param hasOverlap Whether clusters are overlapping
   */
  public ClusteringQualityMeasuresWithSameIdsOneSource(LogicalGraph clusteredGraph, String vertexIdLabel,
    boolean hasOverlap) {
    this(clusteredGraph, vertexIdLabel, hasOverlap, 5);
  }

  /**
   * Creates an instance of ClusteringQualityMeasuresWithSameIdsOneSource
   *
   * @param clusteredGraph The input clustered graph
   * @param vertexIdLabel TODO: define
   * @param hasOverlap Whether clusters are overlapping
   * @param sourceNo Number of graph sources
   */
  public ClusteringQualityMeasuresWithSameIdsOneSource(LogicalGraph clusteredGraph, String vertexIdLabel,
    boolean hasOverlap, int sourceNo) {
    super();
    this.inputGraphCollection = null;
    this.inputGraph = clusteredGraph;
    this.vertexIdLabel = vertexIdLabel.equals("") ? "clsId" : vertexIdLabel;
    this.hasOverlap = hasOverlap;
    this.sourceNo = sourceNo;
    this.env = clusteredGraph.getConfig().getExecutionEnvironment();
  }

  /**
   * Creates an instance of ClusteringQualityMeasuresWithSameIdsOneSource
   *
   * @param clusteredGraphCollection The input clustered graph collection
   * @param vertexIdLabel TODO: define
   * @param hasOverlap Whether clusters are overlapping
   * @param sourceNo Number of graph sources
   */
  public ClusteringQualityMeasuresWithSameIdsOneSource(GraphCollection clusteredGraphCollection,
    String vertexIdLabel, boolean hasOverlap, int sourceNo) {
    super();
    this.inputGraphCollection = clusteredGraphCollection;
    this.inputGraph = null;
    this.vertexIdLabel = vertexIdLabel.equals("") ? "clsId" : vertexIdLabel;
    this.hasOverlap = hasOverlap;
    this.sourceNo = sourceNo;
    this.env = clusteredGraphCollection.getConfig().getExecutionEnvironment();
  }

  @Override
  @SuppressWarnings({"LineLength"})
  public void computeSets() {
    DataSet<EPGMVertex> resultVertices;
    if (inputGraphCollection != null) {
      resultVertices = inputGraphCollection.getVertices();
    } else {
      resultVertices = inputGraph.getVertices();
    }

    DataSet<Tuple2<GradoopId, String>> vertexIdPubId =
      resultVertices.map(new MapVertexToIdAndPropertyValue(vertexIdLabel));

    gtRecordNoSet = vertexIdPubId.groupBy(1)
      .reduceGroup((GroupReduceFunction<Tuple2<GradoopId, String>, Tuple2<String, Long>>) (values, out) -> {
        long count = 0L;
        for (Tuple2<GradoopId, String> v : values) {
          count++;
        }
        out.collect(Tuple2.of(GT_FLAG, count * (count - 1) / 2));
      }).returns(new TypeHint<Tuple2<String, Long>>() { })
      .reduce((ReduceFunction<Tuple2<String, Long>>) (in1, in2) -> Tuple2.of(GT_FLAG, in1.f1 + in2.f1));

    repAllPositivesSet = env.fromElements(Tuple2.of(REP_AP_FLAG, 0L));
    repTruePositivesSet = env.fromElements(Tuple2.of(REP_TP_FLAG, 0L));
    if (hasOverlap) {
      DataSet<Tuple3<String, String, String>> vertexIdPubIdClusterId =
        resultVertices.flatMap((FlatMapFunction<EPGMVertex, Tuple3<String, String, String>>)
          (vertex, out) -> {
            if (vertex.getPropertyValue(CLUSTER_ID).toString().contains(",")) {
              String[] clusterIds = vertex.getPropertyValue(CLUSTER_ID).toString().split(",");
              String recId = vertex.getPropertyValue(vertexIdLabel).toString();
              for (String clusterId : clusterIds) {
                out.collect(Tuple3.of(vertex.getId().toString(), recId, clusterId));
              }
            }
          });

      DataSet<Tuple2<String, String>> idPairPubId1PubId2 = vertexIdPubIdClusterId.join(vertexIdPubIdClusterId)
        .where(2).equalTo(2)
        .with((JoinFunction<Tuple3<String, String, String>, Tuple3<String, String, String>, Tuple6<String, String, String, String, String, String>>) (in1, in2) ->
          Tuple6.of(in1.f0, in1.f1, in1.f2, in2.f0, in2.f1, in2.f2))
        .flatMap((FlatMapFunction<Tuple6<String, String, String, String, String, String>, Tuple2<String, String>>) (in, out) -> {
          if (in.f0.compareTo(in.f3) < 0) {
            out.collect(Tuple2.of(in.f0 + "," + in.f3, in.f1 + "," + in.f4));
          }
        });

      repAllPositivesSet = idPairPubId1PubId2.groupBy(0)
        .reduceGroup(new ComputeGroupCount<>())
        .reduce((ReduceFunction<Long>) Long::sum)
        .map(new AnnotateValueWithFlag<>(REP_AP_FLAG));

      idPairPubId1PubId2 =
        idPairPubId1PubId2.flatMap((FlatMapFunction<Tuple2<String, String>, Tuple2<String, String>>)
          (in, out) -> {
            if (in.f1.split(",")[0].equals(in.f1.split(",")[1])) {
              out.collect(in);
            }
          });

      repTruePositivesSet = idPairPubId1PubId2.groupBy(0)
        .reduceGroup(new ComputeGroupCount<>())
        .reduce((ReduceFunction<Long>) Long::sum)
        .map(new AnnotateValueWithFlag<>(REP_TP_FLAG));

      resultVertices = resultVertices.flatMap((FlatMapFunction<EPGMVertex, EPGMVertex>) (vertex, out) -> {
        String clusterId = vertex.getPropertyValue(CLUSTER_ID).toString();
        if (clusterId.contains(",")) {
          String[] clusterIds = clusterId.split(",");
          for (String id : clusterIds) {
            vertex.setProperty(CLUSTER_ID, id);
            out.collect(vertex);
          }
        } else {
          out.collect(vertex);
        }
      });
    }

    DataSet<Tuple3<String, String, String>> idLabelsClusterIdsSourceIds =
      resultVertices.flatMap(new MapVertexToVertexData(vertexIdLabel, CLUSTER_ID, GRAPH_LABEL));

    DataSet<Tuple2<Long, Long>> perfectCompleteClusters = idLabelsClusterIdsSourceIds.groupBy(1)
      .reduceGroup(new PerfectCompleteClusterReducer(sourceNo));

    perfectClusterNoSet = perfectCompleteClusters.reduce(new SumReduceValuesAtIndex(0))
      .map(new AnnotateValueAtIndexWithFlag<>(PCN_FLAG, 0));

    perfectCompleteClusterNoSet = perfectCompleteClusters.reduce(new SumReduceValuesAtIndex(1))
      .map(new AnnotateValueAtIndexWithFlag<>(PCCN_FLAG, 1));

    // get phase distribution set as cluster id prefix by CLIP algorithm
    DataSet<Integer> phaseDistribution =
      idLabelsClusterIdsSourceIds.map((MapFunction<Tuple3<String, String, String>, Integer>) in -> {
        if (in.f1.split("-")[0].contains("1")) {
          return 1;
        }
        if (in.f1.split("-")[0].contains("2")) {
          return 2;
        }
        if (in.f1.split("-")[0].contains("3")) {
          return 3;
        }
        return 0;
      });

    DataSet<Integer> phase1 = phaseDistribution.filter(value -> value.equals(1));
    DataSet<Integer> phase2 = phaseDistribution.filter(value -> value.equals(2));
    DataSet<Integer> phase3 = phaseDistribution.filter(value -> value.equals(3));

    // all positives
    DataSet<Tuple1<String>> clusterIds =
      resultVertices.flatMap(new VertexToVertexClusterId(CLUSTER_ID, true)).map(new Project2To1<>());

    DataSet<Tuple2<Long, Long>> clusterSizeAPNo = clusterIds.groupBy(0)
      .reduceGroup((GroupReduceFunction<Tuple1<String>, Tuple2<Long, Long>>) (in, out) -> {
        long count = 0L;
        for (Tuple1<String> i : in) {
          count++;
        }
        out.collect(Tuple2.of(count, count * (count - 1) / 2));
      }).returns(new TypeHint<Tuple2<Long, Long>>() { });

    singletonSet = clusterSizeAPNo.filter((FilterFunction<Tuple2<Long, Long>>) tuple -> tuple.f0 == 1L)
      .reduce(new SumReduceValuesAtIndex(0)).map(new AnnotateValueAtIndexWithFlag<>(SINGLE_FLAG, 0));

    allPositivesSet = clusterSizeAPNo.reduce(new SumReduceValuesAtIndex(1))
      .map(new AnnotateValueAtIndexWithFlag<>(AP_FLAG, 1));

    clusterSizeSet = clusterSizeAPNo.reduce(new SumReduceValuesAtIndex(0))
      .map(new AnnotateValueAtIndexWithFlag<>(CS_FLAG, 0));

    maxClusterSizeSet = clusterSizeAPNo.aggregate(Aggregations.MAX, 0)
      .map(new AnnotateValueAtIndexWithFlag<>(MAX_CS_FLAG, 0));

    minClusterSizeSet = clusterSizeAPNo.aggregate(Aggregations.MIN, 0)
      .map(new AnnotateValueAtIndexWithFlag<>(MIN_CS_FLAG, 0));

    clusterNoSet = Count.count(clusterSizeAPNo).map(new AnnotateValueWithFlag<>(CN_FLAG));

    phase1VertexNoSet = Count.count(phase1).map(new AnnotateValueWithFlag<>(PHASE_1_FLAG));
    phase2VertexNoSet = Count.count(phase2).map(new AnnotateValueWithFlag<>(PHASE_2_FLAG));
    phase3VertexNoSet = Count.count(phase3).map(new AnnotateValueWithFlag<>(PHASE_3_FLAG));

    // true positives
    // f1=CLUSTER_ID, f0=vertexIdLabel
    DataSet<Tuple1<String>> clusterIdPubId = idLabelsClusterIdsSourceIds
      .map((MapFunction<Tuple3<String, String, String>, Tuple1<String>>) in -> Tuple1.of(in.f1 + "," + in.f0))
      .returns(new TypeHint<Tuple1<String>>() { });

    truePositivesSet = clusterIdPubId.groupBy(0).
      reduceGroup((GroupReduceFunction<Tuple1<String>, Tuple2<String, Long>>) (group, out) -> {
        long count = 0L;
        for (Tuple1<String> i : group) {
          count++;
        }
        out.collect(Tuple2.of(TP_FLAG, count * (count - 1) / 2));
      }).returns(new TypeHint<Tuple2<String, Long>>() { })
      .reduce((ReduceFunction<Tuple2<String, Long>>) (in1, in2) -> Tuple2.of(TP_FLAG, in1.f1 + in2.f1));
  }

  @Override
  protected void computeValues() throws Exception {
    if (allPositivesSet == null) {
      computeSets();
    }

    DataSet<Tuple2<String, Long>> sets = truePositivesSet.union(allPositivesSet).union(gtRecordNoSet)
      .union(repAllPositivesSet).union(repTruePositivesSet).union(maxClusterSizeSet).union(minClusterSizeSet)
      .union(clusterNoSet).union(clusterSizeSet).union(singletonSet).union(perfectClusterNoSet)
      .union(perfectCompleteClusterNoSet).union(phase1VertexNoSet).union(phase2VertexNoSet)
      .union(phase3VertexNoSet);

    repAllPositiveNo = 0L;
    repTruePositiveNo = 0L;

    for (Tuple2<String, Long> setCountTuple : sets.collect()) {
      switch (setCountTuple.f0) {
      case AP_FLAG:
        allPositives = setCountTuple.f1;
        break;
      case TP_FLAG:
        truePositives = setCountTuple.f1;
        break;
      case GT_FLAG:
        gtRecordNo = setCountTuple.f1;
        break;
      case REP_AP_FLAG:
        repAllPositiveNo = setCountTuple.f1;
        break;
      case REP_TP_FLAG:
        repTruePositiveNo = setCountTuple.f1;
        break;
      case MAX_CS_FLAG:
        maxClusterSize = setCountTuple.f1;
        break;
      case MIN_CS_FLAG:
        minClusterSize = setCountTuple.f1;
        break;
      case CS_FLAG:
        clusterSize = setCountTuple.f1;
        break;
      case CN_FLAG:
        clusterNo = setCountTuple.f1;
        break;
      case PCN_FLAG:
        perfectClusterNo = setCountTuple.f1;
        break;
      case PCCN_FLAG:
        perfectCompleteClusterNo = setCountTuple.f1;
        break;
      case SINGLE_FLAG:
        singletons = setCountTuple.f1;
        break;
      case PHASE_1_FLAG:
        phase1VertexNo = setCountTuple.f1;
        break;
      case PHASE_2_FLAG:
        phase2VertexNo = setCountTuple.f1;
        break;
      case PHASE_3_FLAG:
        phase3VertexNo = setCountTuple.f1;
        break;
      default:
        break;
      }
    }

    if (hasOverlap) {
      allPositives -= repAllPositiveNo;
      truePositives -= repTruePositiveNo;
    }

    averageClusterSize = (double) clusterSize / clusterNo;
  }
}

