/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.inputGraph;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.postprocessing.quality.inputGraph.functions.MapEdgeToSourceIdTargetId;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.operators.count.Count;


/**
 * Computes precision, recall, and fmeasure of the input graph. It does not consider transitive closure.
 * Number of positives (true positives + false positives) is the number of edges.
 * When there is no Golden Truth file but vertices with the same ids are true positives, methods of this
 * class must be used.
 */

public class SimilarityGraphQualityMeasuresWithSameIds extends AbstractSimilarityGraphQualityMeasures {

  /**
   * The input similarity graph
   */
  private LogicalGraph inputGraph;

  /**
   * TODO: define
   */
  private final String vertexIdLabel;

  /**
   * Creates an instance of SimilarityGraphQualityMeasuresWithSameIds
   *
   * @param inputGraph The input similarity graph
   */
  public SimilarityGraphQualityMeasuresWithSameIds(LogicalGraph inputGraph) {
    super();
    this.inputGraph = inputGraph;
    this.vertexIdLabel = "recId";
  }

  @Override
  protected void computeSets() {
    DataSet<Tuple2<GradoopId, String>> vertexIdPubId = inputGraph.getVertices().map(
      (MapFunction<EPGMVertex, Tuple2<GradoopId, String>>) in -> {
        String recId = in.getPropertyValue(vertexIdLabel).toString().split("s")[0];
        return Tuple2.of(in.getId(), recId);
      });

    gtRecordNoSet = vertexIdPubId.groupBy(1)
      .reduceGroup((GroupReduceFunction<Tuple2<GradoopId, String>, Tuple2<String, Long>>) (group, out) -> {
        long count = 0L;
        for (Tuple2<GradoopId, String> v : group) {
          count++;
        }
        out.collect(Tuple2.of(GT_FLAG, count * (count - 1) / 2));
      }).reduce((ReduceFunction<Tuple2<String, Long>>) (in1, in2) -> Tuple2.of(GT_FLAG, in1.f1 + in2.f1));

    DataSet<Tuple2<GradoopId, GradoopId>> sourceIdTargetId =
      inputGraph.getEdges().map(new MapEdgeToSourceIdTargetId());

    DataSet<Tuple2<String, String>> sourcePubIdTargetPubId = vertexIdPubId.join(sourceIdTargetId)
      .where(0).equalTo(0)
      .with((JoinFunction<Tuple2<GradoopId, String>, Tuple2<GradoopId, GradoopId>, Tuple2<String, GradoopId>>)
        (in1, in2) -> Tuple2.of(in1.f1, in2.f1))
      .join(vertexIdPubId)
      .where(1).equalTo(0)
      .with((JoinFunction<Tuple2<String, GradoopId>, Tuple2<GradoopId, String>, Tuple2<String, String>>)
        (in1, in2) -> Tuple2.of(in1.f0, in2.f1));

    truePositivesSet =
      sourcePubIdTargetPubId.map((MapFunction<Tuple2<String, String>, Tuple2<String, Long>>) in -> {
        if (in.f0.equals(in.f1)) {
          return Tuple2.of(TP_FLAG, 1L);
        }
        return Tuple2.of(TP_FLAG, 0L);
      }).reduce((ReduceFunction<Tuple2<String, Long>>) (in1, in2) -> Tuple2.of(TP_FLAG, in1.f1 + in2.f1));

    allPositivesSet = Count.count(inputGraph.getEdges()).map(
      (MapFunction<Long, Tuple2<String, Long>>) in -> Tuple2.of(AP_FLAG, in));
  }
}
