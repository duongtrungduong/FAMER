/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph;

import org.apache.flink.api.java.DataSet;
import org.gradoop.famer.clustering.common.dataStructures.ClusterCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * Compares two clustered logical graphs by comparing their clusters. Adds
 */
public class CompareClustering {

  /**
   * The first clustered logical graph
   */
  private final LogicalGraph clusteredGraph1;

  /**
   * The second clustered logical graph
   */
  private final LogicalGraph clusteredGraph2;

  /**
   * The number of different clusters
   */
  private long difference;

  /**
   * Creates an instance of CompareClustering
   *
   * @param clusteredGraph1 The first clustered logical graph
   * @param clusteredGraph2 The second clustered logical graph
   */
  public CompareClustering(LogicalGraph clusteredGraph1, LogicalGraph clusteredGraph2) {
    difference = -1L;
    this.clusteredGraph1 = clusteredGraph1;
    this.clusteredGraph2 = clusteredGraph2;
  }

  /**
   * Computes and returns the difference between the two clustered logical graphs
   *
   * @return The number of different clusters
   * @throws Exception Thrown, if comparison fails
   */
  public long getDifference() throws Exception {
    if (difference == -1L) {
      ClusterCollection clusterCollection1 = new ClusterCollection(clusteredGraph1);
      ClusterCollection clusterCollection2 = new ClusterCollection(clusteredGraph2);
      DataSet<Long> resultDataset = clusterCollection1.compareClusterCollections(clusterCollection2);
      difference = resultDataset.collect().get(0);
    }
    return difference;
  }
}
