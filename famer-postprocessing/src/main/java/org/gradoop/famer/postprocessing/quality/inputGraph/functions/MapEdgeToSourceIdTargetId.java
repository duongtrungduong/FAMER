/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.inputGraph.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;

/**
 * Maps an edge to a {@code Tuple2<SourceId, TargetId>}
 */
public class MapEdgeToSourceIdTargetId implements MapFunction<EPGMEdge, Tuple2<GradoopId, GradoopId>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple2<GradoopId, GradoopId> reuseTuple = new Tuple2<>();

  @Override
  public Tuple2<GradoopId, GradoopId> map(EPGMEdge edge) throws Exception {
    reuseTuple.f0 = edge.getSourceId();
    reuseTuple.f1 = edge.getTargetId();
    return reuseTuple;
  }
}
