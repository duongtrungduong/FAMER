/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;

/**
 * Compares two Strings from the input tuple and concatenates them with the smaller String on the first
 * position.
 */
public class ConcatStringsInOrder implements MapFunction<Tuple2<String, String>, Tuple1<String>> {

  @Override
  public Tuple1<String> map(Tuple2<String, String> value) throws Exception {
    if (value.f0.compareTo(value.f1) < 0) {
      return Tuple1.of(value.f0 + "," + value.f1);
    } else {
      return Tuple1.of(value.f1 + "," + value.f0);
    }
  }
}
