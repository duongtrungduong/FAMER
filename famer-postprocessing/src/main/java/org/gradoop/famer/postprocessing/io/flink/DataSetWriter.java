/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.io.flink;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.core.fs.FileSystem;

/**
 * Class to write Flinks {@link DataSet} with arbitrary entry types as CSV to disk.
 *
 * @param <T> The {@link DataSet} entry type
 */
public class DataSetWriter<T> {

  /**
   * Delimiter for each written row
   */
  private static final String ROW_DELIMITER = System.getProperty("line.separator");

  /**
   * Delimiter for each written field in a row
   */
  private static final String FIELD_DELIMITER = ",";

  /**
   * Writes a {@link DataSet} of type {@code <T>} as CSV to the given target path, overwrites existing data.
   *
   * @param dataSet The {@link DataSet} to write
   * @param targetPath The target path
   */
  public void writeDataSetAsCSV(DataSet<T> dataSet, String targetPath) {
    writeDataSetAsCSV(dataSet, targetPath, true);
  }

  /**
   * Writes a {@link DataSet} of type {@code <T>} as CSV to the given target path.
   *
   * @param dataSet The {@link DataSet} to write
   * @param targetPath The target path
   * @param overwrite Whether to overwrite existing data
   */
  public void writeDataSetAsCSV(DataSet<T> dataSet, String targetPath, boolean overwrite) {
    FileSystem.WriteMode writeMode = overwrite ?
      FileSystem.WriteMode.OVERWRITE : FileSystem.WriteMode.NO_OVERWRITE;

    dataSet.writeAsCsv(targetPath, ROW_DELIMITER, FIELD_DELIMITER, writeMode);
  }
}
