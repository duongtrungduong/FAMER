/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Maps a vertex to a {@code Tuple3<VertexIdLabel, ClusterId, GraphSource>} with the vertex data.
 */
public class MapVertexToVertexData implements FlatMapFunction<EPGMVertex, Tuple3<String, String, String>> {

  /**
   * TODO: define
   */
  private final String vertexIdLabel;

  /**
   * Property key to access the cluster id
   */
  private final String clusterIdKey;

  /**
   * Property key to access the graph source
   */
  private final String graphLabelKey;

  /**
   * Reduce object instantiation
   */
  private final Tuple3<String, String, String> reuseTuple;

  /**
   * Creates an instance
   *
   * @param vertexIdLabel TODO: define
   * @param clusterIdKey Property key to access the cluster id
   * @param graphLabelKey Property key to access the graph source
   */
  public MapVertexToVertexData(String vertexIdLabel, String clusterIdKey, String graphLabelKey) {
    this.vertexIdLabel = vertexIdLabel;
    this.clusterIdKey = clusterIdKey;
    this.graphLabelKey = graphLabelKey;
    this.reuseTuple = new Tuple3<>();
  }

  @Override
  public void flatMap(EPGMVertex vertex, Collector<Tuple3<String, String, String>> out) {
    if (vertex.hasProperty(clusterIdKey) && vertex.hasProperty(vertexIdLabel) &&
      vertex.hasProperty(graphLabelKey)) {
      reuseTuple.f0 = vertex.getPropertyValue(vertexIdLabel).toString();
      reuseTuple.f2 = vertex.getPropertyValue(graphLabelKey).toString();

      String clusterId = vertex.getPropertyValue(clusterIdKey).toString();
      if (clusterId.contains(",")) {
        String[] clusterIds = clusterId.split(",");
        for (String id : clusterIds) {
          reuseTuple.f1 = id;
          out.collect(reuseTuple);
        }
      } else {
        reuseTuple.f1 = clusterId;
        out.collect(reuseTuple);
      }
    }
  }
}
