/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.MapVertexToVertexData;
import org.gradoop.famer.postprocessing.quality.functions.AnnotateValueWithFlag;
import org.gradoop.famer.postprocessing.quality.functions.MapVertexToIdAndPropertyValue;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.util.ArrayList;
import java.util.List;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.clustering.common.PropertyNames.GRAPH_LABEL;

/**
 * It is used when there is no Golden Truth file and noly vertices with the same ids are true positives
 * Computes Precision, Recall, and FMeasue of the input
 * The input can be a GraphCollection or a LogicalGraph.
 */
public class ClusteringQualityMeasuresWithSameIdsForWDI extends AbstractClusteringQualityMeasures {

  /**
   * TODO: define
   */
  private final String vertexIdLabel;

  /**
   * Whether clusters are overlapping
   */
  private final boolean hasOverlap;

  /**
   * The input clustered graph
   */
  private final LogicalGraph inputGraph;

  /**
   * The input clustered graph collection
   */
  private final GraphCollection inputGraphCollection;

  /**
   * The Flink execution environment
   */
  private final ExecutionEnvironment env;

  /**
   *
   * @param clusteredGraphCollection The input clustered graph collection
   * @param vertexIdLabel TODO: define
   * @param hasOverlap Whether clusters are overlapping
   */
  public ClusteringQualityMeasuresWithSameIdsForWDI(GraphCollection clusteredGraphCollection,
    String vertexIdLabel, boolean hasOverlap) {
    super();
    this.inputGraphCollection = clusteredGraphCollection;
    this.inputGraph = null;
    this.vertexIdLabel = vertexIdLabel.equals("") ? "recId" : vertexIdLabel;
    this.hasOverlap = hasOverlap;
    this.env = clusteredGraphCollection.getConfig().getExecutionEnvironment();
  }

  /**
   *
   * @param clusteredGraph The input clustered graph
   * @param vertexIdLabel TODO: define
   * @param hasOverlap Whether clusters are overlapping
   */
  public ClusteringQualityMeasuresWithSameIdsForWDI(LogicalGraph clusteredGraph, String vertexIdLabel,
    boolean hasOverlap) {
    super();
    this.inputGraphCollection = null;
    this.inputGraph = clusteredGraph;
    this.vertexIdLabel = vertexIdLabel.equals("") ? "recId" : vertexIdLabel;
    this.hasOverlap = hasOverlap;
    this.env = clusteredGraph.getConfig().getExecutionEnvironment();
  }

  @Override
  protected void computeSets() {
    DataSet<EPGMVertex> resultVertices;
    if (inputGraphCollection != null) {
      resultVertices = inputGraphCollection.getVertices();
    } else {
      resultVertices = inputGraph.getVertices();
    }

    DataSet<Tuple2<GradoopId, String>> vertexIdPubId =
      resultVertices.map(new MapVertexToIdAndPropertyValue(vertexIdLabel));

    gtRecordNoSet = vertexIdPubId.groupBy(1)
      .reduceGroup((GroupReduceFunction<Tuple2<GradoopId, String>, Tuple2<String, Long>>) (values, out) -> {
        long count = 0L;
        for (Tuple2<GradoopId, String> v : values) {
          count++;
        }
        out.collect(Tuple2.of(GT_FLAG, count * (count - 1) / 2));
      }).returns(new TypeHint<Tuple2<String, Long>>() { })
      .reduce((ReduceFunction<Tuple2<String, Long>>) (in1, in2) -> Tuple2.of(GT_FLAG, in1.f1 + in2.f1));

    if (hasOverlap) {
      resultVertices = resultVertices.flatMap((FlatMapFunction<EPGMVertex, EPGMVertex>) (vertex, out) -> {
        String clusterId = vertex.getPropertyValue(CLUSTER_ID).toString();
        if (clusterId.contains(",")) {
          String[] clusterIds = clusterId.split(",");
          for (String id : clusterIds) {
            vertex.setProperty(CLUSTER_ID, id);
            out.collect(vertex);
          }
        } else {
          out.collect(vertex);
        }
      });
    }

    // all positives
    DataSet<Tuple2<String, String>> sourceIdClusterId =
      resultVertices.map((MapFunction<EPGMVertex, Tuple2<String, String>>) in ->
        Tuple2.of(in.getPropertyValue(GRAPH_LABEL).toString(), in.getPropertyValue(CLUSTER_ID).toString()))
        .returns(new TypeHint<Tuple2<String, String>>() { });

    DataSet<Tuple3<String, String, Long>> sourceIdClusterIdCount = sourceIdClusterId.groupBy(0, 1)
      .reduceGroup((GroupReduceFunction<Tuple2<String, String>, Tuple3<String, String, Long>>)
        (group, out) -> {
          long count = 0L;
          String s1 = "";
          String s2 = "";
          for (Tuple2<String, String> i : group) {
            count++;
            s1 = i.f0;
            s2 = i.f1;
          }
          out.collect(Tuple3.of(s1, s2, count));
        }).returns(new TypeHint<Tuple3<String, String, Long>>() { });

    allPositivesSet = sourceIdClusterIdCount.groupBy(1)
      .reduceGroup((GroupReduceFunction<Tuple3<String, String, Long>, Long>) (group, out) -> {
        long count = 0L;
        List<Long> list = new ArrayList<>();
        for (Tuple3<String, String, Long> i : group) {
          list.add(i.f2);
        }
        Long[] listArray = list.toArray(new Long[list.size()]);
        for (int i = 0; i < listArray.length; i++) {
          for (int j = i + 1; j < listArray.length; j++) {
            count += listArray[i] * listArray[j];
          }
        }
        out.collect(count);
      }).returns(new TypeHint<Long>() { })
      .reduce((ReduceFunction<Long>) Long::sum)
      .map(new AnnotateValueWithFlag<>(AP_FLAG));

    // true positives
    DataSet<Tuple3<String, String, String>> idLabelsClusterIdsSourceIds =
      resultVertices.flatMap(new MapVertexToVertexData(vertexIdLabel, CLUSTER_ID, GRAPH_LABEL));

    // group on clusterId first and vertexIdLabel second
    truePositivesSet = idLabelsClusterIdsSourceIds.groupBy(1, 0)
      .reduceGroup((GroupReduceFunction<Tuple3<String, String, String>, Long>) (group, out) -> {
        long count = 0L;
        for (Tuple3<String, String, String> i : group) {
          count++;
        }
        out.collect(count * (count - 1) / 2);
      }).returns(new TypeHint<Long>() { })
      .reduce((ReduceFunction<Long>) Long::sum)
      .map(new AnnotateValueWithFlag<>(TP_FLAG));
  }

  @Override
  protected void computeValues() throws Exception {
    if (allPositivesSet == null) {
      computeSets();
    }

    DataSet<Tuple2<String, Long>> sets = truePositivesSet.union(allPositivesSet).union(gtRecordNoSet);

    for (Tuple2<String, Long> setCountTuple : sets.collect()) {
      switch (setCountTuple.f0) {
      case AP_FLAG:
        allPositives = setCountTuple.f1;
        break;
      case TP_FLAG:
        truePositives = setCountTuple.f1;
        break;
      case GT_FLAG:
        gtRecordNo = setCountTuple.f1;
        break;
      default:
        break;
      }
    }
  }
}

