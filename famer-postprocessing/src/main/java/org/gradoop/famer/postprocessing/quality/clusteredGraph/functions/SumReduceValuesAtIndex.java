/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph.functions;

import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;

/**
 * Reduces a group of {@code Tuple2<Long, Long>} by adding up either the first or the second field, with the
 * field index given in {@link #index}
 */
public class SumReduceValuesAtIndex implements ReduceFunction<Tuple2<Long, Long>> {

  /**
   * Index of the field to add up
   */
  private final int index;

  /**
   * Reduce object instantiation
   */
  private final Tuple2<Long, Long> reuseTuple;

  /**
   * Creates an instance of SumReduceValuesAtIndex
   *
   * @param index Index of the field to add up
   */
  public SumReduceValuesAtIndex(int index) {
    this.index = index;
    this.reuseTuple = new Tuple2<>();
  }

  @Override
  public Tuple2<Long, Long> reduce(Tuple2<Long, Long> in1, Tuple2<Long, Long> in2) throws Exception {
    if (index == 0) {
      reuseTuple.f0 = in1.f0 + in2.f0;
      reuseTuple.f1 = 0L;
      return reuseTuple;
    } else {
      reuseTuple.f0 = 0L;
      reuseTuple.f1 = in1.f1 + in2.f1;
      return reuseTuple;
    }
  }
}
