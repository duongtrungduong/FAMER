/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple2;

/**
 * Maps a value to an annotated {@code Tuple2<AnnotationFlag, Value>}
 *
 * @param <T> Value type
 */
@FunctionAnnotation.ForwardedFields("*->f1")
public class AnnotateValueWithFlag<T> implements MapFunction<T, Tuple2<String, T>> {

  /**
   * Flag for annotation
   */
  private final String flag;

  /**
   * Reduce object instantiation
   */
  private final Tuple2<String, T> reuseTuple;

  /**
   * Creates an instance of AnnotateValueWithFlag
   *
   * @param flag Flag for annotation
   */
  public AnnotateValueWithFlag(String flag) {
    this.flag = flag;
    this.reuseTuple = new Tuple2<>();
  }

  @Override
  public Tuple2<String, T> map(T value) throws Exception {
    reuseTuple.f0 = flag;
    reuseTuple.f1 = value;
    return reuseTuple;
  }
}
