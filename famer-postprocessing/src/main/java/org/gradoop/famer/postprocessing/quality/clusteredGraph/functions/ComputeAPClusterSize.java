/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.List;

/**
 * For a group of {@code Tuple3<IdLabel, ClusterId, SourceId>}, grouped on ClusterId, computes the number of
 * clusters with the same graph source (all positives) and the number of group clusters. Returns a
 * {@code Tuuple2<AllPositivesCount, ClusterCount>} for the group.
 */
public class ComputeAPClusterSize implements
  GroupReduceFunction<Tuple3<String, String, String>, Tuple2<Long, Long>> {

  /**
   * TODO: define
   */
  private final boolean isWithinDataSetMatch;

  /**
   * Reduce object instantiation
   */
  private final Tuple2<Long, Long> reuseTuple;

  /**
   * Creates an instance of ComputeAPClusterSize
   *
   * @param isWithinDataSetMatch TODO: define
   */
  public ComputeAPClusterSize(boolean isWithinDataSetMatch) {
    this.isWithinDataSetMatch = isWithinDataSetMatch;
    this.reuseTuple = new Tuple2<>();
  }

  @Override
  public void reduce(Iterable<Tuple3<String, String, String>> group, Collector<Tuple2<Long, Long>> out)
    throws Exception {
    long clusterCount = 0L;
    long apCount = 0L;

    if (!isWithinDataSetMatch) {
      List<String> types = new ArrayList<>();
      for (Tuple3<String, String, String> groupItem : group) {
        clusterCount++;
        types.add(groupItem.f2);
      }
      String[] typesArray = types.toArray(new String[types.size()]);
      for (int i = 0; i < typesArray.length; i++) {
        for (int j = i + 1; j < typesArray.length; j++) {
          if (!typesArray[i].equals(typesArray[j])) {
            apCount++;
          }
        }
      }
    } else {
      for (Tuple3<String, String, String> i : group) {
        clusterCount++;
      }
      apCount = (clusterCount * (clusterCount - 1)) / 2;
    }
    reuseTuple.f0 = apCount;
    reuseTuple.f1 = clusterCount;
    out.collect(reuseTuple);
  }
}
