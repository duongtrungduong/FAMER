/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.inputGraph;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.DataSetUtils;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.postprocessing.quality.functions.AnnotateValueWithFlag;
import org.gradoop.famer.postprocessing.quality.inputGraph.functions.MapEdgeToSourceIdTargetId;
import org.gradoop.famer.postprocessing.quality.functions.MapGroundTruthLineToTuple;
import org.gradoop.famer.postprocessing.quality.functions.MapVertexToIdAndPropertyValue;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.operators.count.Count;

/**
 * Computes precision, recall, and f-measure of the input graph. It does not consider transitive closure.
 * Number of positives (true positives + false positives) is the number of edges.
 */
public class SimilarityGraphQualityMeasures extends AbstractSimilarityGraphQualityMeasures {

  /**
   * Path to the ground truth file
   */
  private final String groundTruthFilePath;

  /**
   * Tokenizer for the ground truth file records
   */
  private final String groundTruthSplitter;

  /**
   * The input similarity graph
   */
  private LogicalGraph inputGraph;

  /**
   * TODO: define
   */
  private String vertexIdLabel;

  /**
   * The Flink execution environment
   */
  private final ExecutionEnvironment env;

  /**
   * Creates an instance of SimilarityGraphQualityMeasures
   *
   * @param groundTruthFilepath Path to the ground truth file
   * @param groundTruthSplitter Tokenizer for the ground truth file records
   * @param inputGraph The input similarity graph
   * @param vertexIdLabel TODO: define
   */
  public SimilarityGraphQualityMeasures(String groundTruthFilepath, String groundTruthSplitter,
    LogicalGraph inputGraph, String vertexIdLabel) {
    super();
    this.groundTruthFilePath = groundTruthFilepath.trim();
    this.groundTruthSplitter = groundTruthSplitter;
    this.inputGraph = inputGraph;
    this.vertexIdLabel = vertexIdLabel;
    this.env = inputGraph.getConfig().getExecutionEnvironment();
  }

  @Override
  protected void computeSets() {
    DataSet<Tuple2<String, String>> groundTruthFile = env.readTextFile(groundTruthFilePath)
      .map(new MapGroundTruthLineToTuple(groundTruthSplitter));

    DataSet<Tuple2<GradoopId, String>> vertexIdPubId = inputGraph.getVertices()
      .map(new MapVertexToIdAndPropertyValue(vertexIdLabel));

    DataSet<Tuple2<GradoopId, String>> vertexIdClusterId =
      DataSetUtils.zipWithUniqueId(inputGraph.getEdges().map(new MapEdgeToSourceIdTargetId()))
        .flatMap((FlatMapFunction<Tuple2<Long, Tuple2<GradoopId, GradoopId>>, Tuple2<GradoopId, String>>)
          (in, out) -> {
          out.collect(Tuple2.of(in.f1.f0, "c" + in.f0.toString()));
          out.collect(Tuple2.of(in.f1.f1, "c" + in.f0.toString()));
        });

    DataSet<Tuple2<String, String>> pubIdClusterId = vertexIdPubId.join(vertexIdClusterId)
      .where(0).equalTo(0)
      .with((JoinFunction<Tuple2<GradoopId, String>, Tuple2<GradoopId, String>, Tuple2<String, String>>)
        (in1, in2) -> Tuple2.of(in1.f1, in2.f1));

    DataSet<Tuple2<String, String>> temp = pubIdClusterId.join(groundTruthFile)
      .where(0).equalTo(0)
      .with((JoinFunction<Tuple2<String, String>, Tuple2<String, String>, Tuple2<String, String>>)
        (in1, in2) -> Tuple2.of(in1.f1, in2.f1));

    truePositivesSet = temp.join(pubIdClusterId)
      .where(1).equalTo(0)
      .with((JoinFunction<Tuple2<String, String>, Tuple2<String, String>, Tuple2<String, String>>)
        (in1, in2) -> Tuple2.of(in1.f0, in2.f1))
      .flatMap((FlatMapFunction<Tuple2<String, String>, Tuple2<String, Long>>) (in, out) -> {
        if (in.f0.equals(in.f1)) {
          out.collect(Tuple2.of(TP_FLAG, 1L));
        }
      }).reduce((ReduceFunction<Tuple2<String, Long>>) (in1, in2) -> Tuple2.of(TP_FLAG, in1.f1 + in2.f1));

    gtRecordNoSet = Count.count(groundTruthFile).map(new AnnotateValueWithFlag<>(GT_FLAG));

    allPositivesSet = Count.count(inputGraph.getEdges()).map(new AnnotateValueWithFlag<>(AP_FLAG));
  }
}
