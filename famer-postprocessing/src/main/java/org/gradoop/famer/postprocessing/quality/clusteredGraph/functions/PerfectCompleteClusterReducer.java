/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.List;

/**
 * For a group of {@code Tuple3<VertexLabelId, ClusterId, GraphSource>}, grouped on ClusterId, computes
 * whether the cluster is perfect, perfect but incomplete or perfect and complete.
 * Returns a {@code Tuple2<IsPerfect, IsComplete>} for the cluster with a long value (either 0 or 1)
 * indicating the perfect and complete status.
 */
public class PerfectCompleteClusterReducer implements
  GroupReduceFunction<Tuple3<String, String, String>, Tuple2<Long, Long>> {

  /**
   * Number of graph sources
   */
  private final int sourceNo;

  /**
   * Reduce object instantiation
   */
  private final Tuple2<Long, Long> reuseTuple;

  /**
   * Creates an instance of PerfectCompleteClusterReducer
   *
   * @param sourceNo Number of graph sources
   */
  public PerfectCompleteClusterReducer(int sourceNo) {
    this.sourceNo = sourceNo;
    this.reuseTuple = new Tuple2<>();
  }

  @Override
  public void reduce(Iterable<Tuple3<String, String, String>> group, Collector<Tuple2<Long, Long>> out)
    throws Exception {
    List<String> types = new ArrayList<>();
    boolean isPerfect = true;

    for (Tuple3<String, String, String> i : group) {
      if (types.contains(i.f2)) {
        isPerfect = false;
        break;
      }
      types.add(i.f2);
    }

    if (!isPerfect) {
      // cluster is not perfect and incomplete
      reuseTuple.f0 = 0L;
      reuseTuple.f1 = 0L;
      out.collect(reuseTuple);
    } else {
      if (types.size() != sourceNo) {
        // cluster is perfect but incomplete
        reuseTuple.f0 = 1L;
        reuseTuple.f1 = 0L;
        out.collect(reuseTuple);
      } else {
        // cluster is perfect and complete
        reuseTuple.f0 = 1L;
        reuseTuple.f1 = 1L;
        out.collect(reuseTuple);
      }
    }
  }
}
