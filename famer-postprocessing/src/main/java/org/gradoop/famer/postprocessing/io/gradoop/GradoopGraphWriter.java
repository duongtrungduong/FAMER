/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.io.gradoop;

import org.gradoop.flink.io.impl.csv.CSVDataSink;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * Class for writing a Gradoop {@link LogicalGraph} or {@link GraphCollection} as CSV to disk
 */
public class GradoopGraphWriter {

  /**
   * Writes a {@link LogicalGraph} as CSV to the given target folder, overwrites existing data.
   *
   * @param graph The {@link LogicalGraph} to write
   * @param targetPath The path to the target folder
   *
   * @throws Exception Exception thrown on write errors
   */
  public void writeLogicalGraphAsCSV(LogicalGraph graph, String targetPath) throws Exception {
    writeLogicalGraphAsCSV(graph, targetPath, true);
  }

  /**
   * Writes a {@link LogicalGraph} as CSV to the given target folder
   *
   * @param graph The {@link LogicalGraph} to write
   * @param targetPath The path to the target folder
   * @param overwrite Whether to overwrite existing data
   *
   * @throws Exception Exception thrown on write errors
   */
  public void writeLogicalGraphAsCSV(LogicalGraph graph, String targetPath, boolean overwrite)
    throws Exception {
    CSVDataSink sink = new CSVDataSink(targetPath, graph.getConfig());
    sink.write(graph, overwrite);
    graph.getConfig().getExecutionEnvironment().execute();
  }

  /**
   * Writes a {@link GraphCollection} as CSV to the given target folder, overwrites existing data.
   *
   * @param graph The {@link GraphCollection} to write
   * @param targetPath The path to the target folder
   *
   * @throws Exception Exception thrown on write errors
   */
  public void writeGraphCollectionAsCSV(GraphCollection graph, String targetPath) throws Exception {
    writeGraphCollectionAsCSV(graph, targetPath, true);
  }

  /**
   * Writes a {@link GraphCollection} as CSV to the given target folder
   *
   * @param graph The {@link GraphCollection} to write
   * @param targetPath The path to the target folder
   * @param overwrite Whether to overwrite existing data
   *
   * @throws Exception Exception thrown on write errors
   */
  public void writeGraphCollectionAsCSV(GraphCollection graph, String targetPath, boolean overwrite)
    throws Exception {
    CSVDataSink sink = new CSVDataSink(targetPath, graph.getConfig());
    sink.write(graph, overwrite);
    graph.getConfig().getExecutionEnvironment().execute();
  }
}
