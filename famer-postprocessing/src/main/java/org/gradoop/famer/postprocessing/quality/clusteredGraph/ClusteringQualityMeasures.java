/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.aggregation.Aggregations;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple6;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.AnnotateValueAtIndexWithFlag;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.ComputeAPClusterSize;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.ComputeGroupCount;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.ConcatStringsInOrder;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.MapVertexToVertexData;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.PerfectCompleteClusterReducer;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.SumReduceValuesAtIndex;
import org.gradoop.famer.postprocessing.quality.functions.AnnotateValueWithFlag;
import org.gradoop.famer.postprocessing.quality.functions.MapGroundTruthLineToTuple;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.utils.LeftSide;
import org.gradoop.flink.model.impl.operators.count.Count;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.clustering.common.PropertyNames.GRAPH_LABEL;

/**
 * Computes Precision, Recall, and F-Measure of the input, which can be a GraphCollection or a LogicalGraph.
 * It is implemented using both {@code groupby} and {@code join}. The codes related to the join are currently
 * commented. In the case of using {@code groupby}, cluster size is computed as well.
 */
public class ClusteringQualityMeasures extends AbstractClusteringQualityMeasures {

  /**
   * Path to the ground truth file
   */
  private final String groundTruthFilePath;

  /**
   * Tokenizer for the ground truth file records
   */
  private final String groundTruthSplitter;

  /**
   * TODO: define
   */
  private final String vertexIdLabel;

  /**
   * TODO: define
   */
  private final boolean isGroundTruthFull;

  /**
   * TODO: define
   */
  private final boolean isWithinDataSetMatch;

  /**
   * Whether clusters are overlapping
   */
  private final boolean hasOverlap;

  /**
   * Number of graph sources
   */
  private final int sourceNo;

  /**
   * The input clustered graph
   */
  private final LogicalGraph inputGraph;

  /**
   * The input clustered graph collection
   */
  private final GraphCollection inputGraphCollection;

  /**
   * The Flink execution environment
   */
  private final ExecutionEnvironment env;

  /**
   * Creates an instance of ClusteringQualityMeasures
   *
   * @param clusteredGraph The input clustered graph
   * @param groundTruthFilepath Path to the ground truth file
   * @param groundTruthSplitter Tokenizer for the ground truth file records
   * @param vertexIdLabel TODO: define
   * @param isGroundTruthFull TODO: define
   * @param isWithinDataSetMatch TODO: define
   * @param hasOverlap Whether clusters are overlapping
   * @param sourceNo Number of graph sources
   */
  public ClusteringQualityMeasures(LogicalGraph clusteredGraph, String groundTruthFilepath,
    String groundTruthSplitter, String vertexIdLabel, boolean isGroundTruthFull,
    boolean isWithinDataSetMatch, boolean hasOverlap, int sourceNo) {
    super();
    this.groundTruthFilePath = groundTruthFilepath.trim();
    this.groundTruthSplitter = groundTruthSplitter;
    this.vertexIdLabel = vertexIdLabel.equals("") ? "recId" : vertexIdLabel;
    this.isGroundTruthFull = isGroundTruthFull;
    this.isWithinDataSetMatch = isWithinDataSetMatch;
    this.hasOverlap = hasOverlap;
    this.sourceNo = sourceNo;
    this.inputGraphCollection = null;
    this.inputGraph = clusteredGraph;
    this.env = clusteredGraph.getConfig().getExecutionEnvironment();
  }

  /**
   * Creates an instance of ClusteringQualityMeasures
   *
   * @param clusteredGraphCollection The input clustered graph collection
   * @param groundTruthFilepath Path to the ground truth file
   * @param groundTruthSplitter Tokenizer for the ground truth file records
   * @param vertexIdLabel TODO: define
   * @param isGroundTruthFull TODO: define
   * @param isWithinDataSetMatch TODO: define
   * @param hasOverlap Whether clusters are overlapping
   * @param sourceNo Number of graph sources
   */
  public ClusteringQualityMeasures(GraphCollection clusteredGraphCollection,
    String groundTruthFilepath, String groundTruthSplitter, String vertexIdLabel, boolean isGroundTruthFull,
    boolean isWithinDataSetMatch, boolean hasOverlap, int sourceNo) {
    super();
    this.groundTruthFilePath = groundTruthFilepath.trim();
    this.groundTruthSplitter = groundTruthSplitter;
    this.vertexIdLabel = vertexIdLabel.equals("") ? "recId" : vertexIdLabel;
    this.isGroundTruthFull = isGroundTruthFull;
    this.isWithinDataSetMatch = isWithinDataSetMatch;
    this.hasOverlap = hasOverlap;
    this.sourceNo = sourceNo;
    this.inputGraphCollection = clusteredGraphCollection;
    this.inputGraph = null;
    this.env = clusteredGraphCollection.getConfig().getExecutionEnvironment();
  }

  @Override
  @SuppressWarnings({"LineLength"})
  protected void computeSets() {
    DataSet<Tuple2<String, String>> groundTruthFile = env.readTextFile(groundTruthFilePath)
      .map(new MapGroundTruthLineToTuple(groundTruthSplitter));

    DataSet<EPGMVertex> resultVertices;
    if (inputGraphCollection != null) {
      resultVertices = inputGraphCollection.getVertices();
    } else {
      resultVertices = inputGraph.getVertices();
    }

    DataSet<Tuple3<String, String, String>> idLabelsClusterIdsSourceIds =
      resultVertices.flatMap(new MapVertexToVertexData(vertexIdLabel, CLUSTER_ID, GRAPH_LABEL));

    // get phase distribution set as cluster id prefix by CLIP algorithm
    DataSet<Integer> phaseDistribution =
      idLabelsClusterIdsSourceIds.map((MapFunction<Tuple3<String, String, String>, Integer>) in -> {
        if (in.f1.split("-")[0].contains("1")) {
          return 1;
        }
        if (in.f1.split("-")[0].contains("2")) {
          return 2;
        }
        if (in.f1.split("-")[0].contains("3")) {
          return 3;
        }
        return 0;
      });

    DataSet<Integer> phase1 = phaseDistribution.filter(value -> value.equals(1));
    DataSet<Integer> phase2 = phaseDistribution.filter(value -> value.equals(2));
    DataSet<Integer> phase3 = phaseDistribution.filter(value -> value.equals(3));

    DataSet<Long> tmpTP = idLabelsClusterIdsSourceIds.join(groundTruthFile)
      .where(0).equalTo(0)
      .with((JoinFunction<Tuple3<String, String, String>, Tuple2<String, String>, Tuple2<String, String>>)
        (in1, in2) -> Tuple2.of(in1.f1, in2.f1))
      .returns(new TypeHint<Tuple2<String, String>>() { })
      .join(idLabelsClusterIdsSourceIds)
      .where(1).equalTo(0)
      .with((JoinFunction<Tuple2<String, String>, Tuple3<String, String, String>, Tuple2<String, String>>)
        (in1, in2) -> Tuple2.of(in1.f0, in2.f1))
      .returns(new TypeHint<Tuple2<String, String>>() { })
      .flatMap((FlatMapFunction<Tuple2<String, String>, Long>) (in, out) -> {
        if (in.f0.equals(in.f1)) {
          out.collect(1L);
        }
      }).returns(new TypeHint<Long>() { });

////////////////////////////////////////////////////////////////////////////////////
///////////////// join instead of group by
//        DataSet<Long> tmpAP;
//
//        DataSet<Tuple4<String, String, String, String>> tmpResult =
//          idLabelsClusterIdsSourceIds.join(idLabelsClusterIdsSourceIds)
//          .where(1).equalTo(1)
//          .with((JoinFunction<Tuple3<String, String, String>, Tuple3<String, String, String>,
//            Tuple4<String, String, String, String>>)
//            (first, second) -> Tuple4.of(first.f0, second.f0, first.f2, second.f2));
//
//        if (isWithinDataSetMatch) {
//            tmpAP = tmpResult.flatMap(
//              (FlatMapFunction<Tuple4<String, String, String, String>, Long>) (value, out) -> {
//                if (!value.f0.equals(value.f1)) {
//                  out.collect(1L);
//                }
//              });
//        } else {
//            tmpAP = tmpResult.flatMap(
//              (FlatMapFunction<Tuple4<String, String, String, String>, Long>) (value, out) -> {
//                if (!value.f0.equals(value.f1) && !value.f2.equals(value.f3)) {
//                  out.collect(1L);
//                }
//              });
//        }
//        apSet = Count.count(tmpAP).map(new AnnotateValueWithFlag<>(AP_FLAG));
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
///////////////// group by instead of join
    DataSet<Tuple2<Long, Long>> apClusterSize;

    apClusterSize = idLabelsClusterIdsSourceIds.groupBy(1)
      .reduceGroup(new ComputeAPClusterSize(isWithinDataSetMatch));

    allPositivesSet = apClusterSize.reduce(new SumReduceValuesAtIndex(0))
      .map(new AnnotateValueAtIndexWithFlag<>(AP_FLAG, 0));

    maxClusterSizeSet = apClusterSize.aggregate(Aggregations.MAX, 1)
      .map(new AnnotateValueAtIndexWithFlag<>(MAX_CS_FLAG, 1));

    minClusterSizeSet = apClusterSize.aggregate(Aggregations.MIN, 1)
      .map(new AnnotateValueAtIndexWithFlag<>(MIN_CS_FLAG, 1));

    clusterNoSet = Count.count(apClusterSize).map(new AnnotateValueWithFlag<>(CN_FLAG));

    clusterSizeSet = apClusterSize.reduce(new SumReduceValuesAtIndex(1))
      .map(new AnnotateValueAtIndexWithFlag<>(CS_FLAG, 1));

    phase1VertexNoSet = Count.count(phase1).map(new AnnotateValueWithFlag<>(PHASE_1_FLAG));
    phase2VertexNoSet = Count.count(phase2).map(new AnnotateValueWithFlag<>(PHASE_2_FLAG));
    phase3VertexNoSet = Count.count(phase3).map(new AnnotateValueWithFlag<>(PHASE_3_FLAG));

    DataSet<Tuple2<Long, Long>> perfectCompleteClusters = idLabelsClusterIdsSourceIds.groupBy(1)
      .reduceGroup(new PerfectCompleteClusterReducer(sourceNo));

    perfectClusterNoSet = perfectCompleteClusters.reduce(new SumReduceValuesAtIndex(0))
      .map(new AnnotateValueAtIndexWithFlag<>(PCN_FLAG, 0));

    perfectCompleteClusterNoSet = perfectCompleteClusters.reduce(new SumReduceValuesAtIndex(1))
      .map(new AnnotateValueAtIndexWithFlag<>(PCCN_FLAG, 1));

    singletonSet = idLabelsClusterIdsSourceIds.groupBy(1)
      .reduceGroup((GroupReduceFunction<Tuple3<String, String, String>, Tuple2<String, Long>>)
        (group, out) -> {
          int count = 0;
          for (Tuple3<String, String, String> value : group) {
            count++;
          }
          if (count == 1) {
            out.collect(Tuple2.of(SINGLE_FLAG, 1L));
          } else {
            out.collect(Tuple2.of(SINGLE_FLAG, 0L));
          }
        })
      .returns(new TypeHint<Tuple2<String, Long>>() { })
      .reduce((ReduceFunction<Tuple2<String, Long>>) (in1, in2) -> Tuple2.of(in1.f0, in1.f1 + in2.f1))
      .returns(new TypeHint<Tuple2<String, Long>>() { });
////////////////////////////////////////////////////////////////////////////////////

    truePositivesSet = Count.count(tmpTP).map(new AnnotateValueWithFlag<>(TP_FLAG));

    gtRecordNoSet = Count.count(groundTruthFile).map(new AnnotateValueWithFlag<>(GT_FLAG));

    repAllPositivesSet = env.fromElements(Tuple2.of(REP_AP_FLAG, 0L));
    repTruePositivesSet = env.fromElements(Tuple2.of(REP_TP_FLAG, 0L));
    if (hasOverlap) {
      DataSet<Tuple2<String, String>> pairedVerticesClusterId =
        idLabelsClusterIdsSourceIds.join(idLabelsClusterIdsSourceIds)
          .where(1).equalTo(1)
          .with((JoinFunction<Tuple3<String, String, String>, Tuple3<String, String, String>, Tuple6<String, String, String, String, String, String>>) (in1, in2) ->
            Tuple6.of(in1.f0, in1.f1, in1.f2, in2.f0, in2.f1, in2.f2))
          .flatMap((FlatMapFunction<Tuple6<String, String, String, String, String, String>, Tuple2<String, String>>) (in, out) -> {
            if (!in.f2.equals(in.f5) || isWithinDataSetMatch) {
              if (in.f0.compareTo(in.f3) < 0) {
                out.collect(Tuple2.of(in.f0 + "," + in.f3, in.f1));
              }
            }
          });

      repAllPositivesSet = pairedVerticesClusterId.groupBy(0)
        .reduceGroup(new ComputeGroupCount<>())
        .reduce((ReduceFunction<Long>) Long::sum)
        .map(new AnnotateValueWithFlag<>(REP_AP_FLAG));

      DataSet<Tuple1<String>> tempGroundTruth = groundTruthFile.map(new ConcatStringsInOrder());

      repTruePositivesSet = pairedVerticesClusterId.join(tempGroundTruth)
        .where(0).equalTo(0)
        .with(new LeftSide<>())
        .groupBy(0)
        .reduceGroup(new ComputeGroupCount<>())
        .reduce((ReduceFunction<Long>) Long::sum)
        .map(new AnnotateValueWithFlag<>(REP_TP_FLAG));
    }
  }

  @Override
  protected void computeValues() throws Exception {
    if (allPositivesSet == null) {
      computeSets();
    }

    DataSet<Tuple2<String, Long>> sets = truePositivesSet.union(allPositivesSet).union(gtRecordNoSet)
      .union(repAllPositivesSet).union(repTruePositivesSet).union(maxClusterSizeSet).union(minClusterSizeSet)
      .union(clusterNoSet).union(clusterSizeSet).union(perfectClusterNoSet).union(singletonSet)
      .union(perfectCompleteClusterNoSet).union(phase1VertexNoSet).union(phase2VertexNoSet)
      .union(phase3VertexNoSet);

////used with join
//    DataSet<Tuple2<String, Long>> sets =
//      tpSet.union(apSet).union(gtRecordNoSet).union(repAPSet).union(repTPSet);

    for (Tuple2<String, Long> setCountTuple : sets.collect()) {
      switch (setCountTuple.f0) {
      case AP_FLAG:
        allPositives = setCountTuple.f1;
        break;
      case TP_FLAG:
        truePositives = setCountTuple.f1;
        break;
      case GT_FLAG:
        gtRecordNo = setCountTuple.f1;
        break;
      case REP_AP_FLAG:
        repAllPositiveNo = setCountTuple.f1;
        break;
      case REP_TP_FLAG:
        repTruePositiveNo = setCountTuple.f1;
        break;
      case MAX_CS_FLAG:
        maxClusterSize = setCountTuple.f1;
        break;
      case MIN_CS_FLAG:
        minClusterSize = setCountTuple.f1;
        break;
      case CS_FLAG:
        clusterSize = setCountTuple.f1;
        break;
      case CN_FLAG:
        clusterNo = setCountTuple.f1;
        break;
      case PCN_FLAG:
        perfectClusterNo = setCountTuple.f1;
        break;
      case PCCN_FLAG:
        perfectCompleteClusterNo = setCountTuple.f1;
        break;
      case SINGLE_FLAG:
        singletons = setCountTuple.f1;
        break;
      case PHASE_1_FLAG:
        phase1VertexNo = setCountTuple.f1;
        break;
      case PHASE_2_FLAG:
        phase2VertexNo = setCountTuple.f1;
        break;
      case PHASE_3_FLAG:
        phase3VertexNo = setCountTuple.f1;
        break;
      default:
        break;
      }
    }

    if (!isGroundTruthFull) {
      repAllPositiveNo /= 2;
//////used with join
//      ap /= 2;
    }

    if (hasOverlap) {
      allPositives -= repAllPositiveNo;
      truePositives -= repTruePositiveNo;
    }

    averageClusterSize = (double) clusterSize / clusterNo;
  }
}

