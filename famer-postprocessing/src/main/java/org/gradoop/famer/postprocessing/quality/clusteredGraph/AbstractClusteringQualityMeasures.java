/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;

/**
 * Abstract class all classes for quality measuring on clustered graphs are inheriting from.
 */
public abstract class AbstractClusteringQualityMeasures {

  /**
   * Flag to mark tuples with the value for true positives
   */
  protected static final String TP_FLAG = "tp";
  /**
   * Flag to mark tuples with the value for all positives
   */
  protected static final String AP_FLAG = "ap";
  /**
   * Flag to mark tuples with the value for ground truth record numbers
   */
  protected static final String GT_FLAG = "gt";
  /**
   * Flag to mark tuples with the value for repaired all positives
   */
  protected static final String REP_AP_FLAG = "repAP";
  /**
   * Flag to mark tuples with the value for repaired true positives
   */
  protected static final String REP_TP_FLAG = "repTP";
  /**
   * Flag to mark tuples with the value for max cluster size
   */
  protected static final String MAX_CS_FLAG = "maxCs";
  /**
   * Flag to mark tuples with the value for min cluster size
   */
  protected static final String MIN_CS_FLAG = "minCs";
  /**
   * Flag to mark tuples with the value for cluster size
   */
  protected static final String CS_FLAG = "cs";
  /**
   * Flag to mark tuples with the value for cluster number
   */
  protected static final String CN_FLAG = "cn";
  /**
   * Flag to mark tuples with the value for perfect cluster number
   */
  protected static final String PCN_FLAG = "pcn";
  /**
   * Flag to mark tuples with the value for perfect complete cluster number
   */
  protected static final String PCCN_FLAG = "pccn";
  /**
   * Flag to mark tuples with the value for singleton cluster number
   */
  protected static final String SINGLE_FLAG = "single";
  /**
   * Flag to mark tuples with value for vertices clustered in phase 1
   */
  protected static final String PHASE_1_FLAG = "ph1";
  /**
   * Flag to mark tuples with value for vertices clustered in phase 2
   */
  protected static final String PHASE_2_FLAG = "ph2";
  /**
   * Flag to mark tuples with value for vertices clustered in phase 3
   */
  protected static final String PHASE_3_FLAG = "ph3";

  /**
   * DataSet with {@code Tuple2<TP_FLAG, true positives count>}
   */
  protected DataSet<Tuple2<String, Long>> truePositivesSet;
  /**
   * DataSet with {@code Tuple2<AP_FLAG, all positives count>}
   */
  protected DataSet<Tuple2<String, Long>> allPositivesSet;
  /**
   * DataSet with {@code Tuple2<GT_FLAG, ground truth record number count>}
   */
  protected DataSet<Tuple2<String, Long>> gtRecordNoSet;
  /**
   * DataSet with {@code Tuple2<CS_FLAG, cluster size>}
   */
  protected DataSet<Tuple2<String, Long>> clusterSizeSet;
  /**
   * DataSet with {@code Tuple2<CN_FLAG, clusters count>}
   */
  protected DataSet<Tuple2<String, Long>> clusterNoSet;
  /**
   * DataSet with {@code Tuple2<MIN_CS_FLAG, min cluster size>}
   */
  protected DataSet<Tuple2<String, Long>> minClusterSizeSet;
  /**
   * DataSet with {@code Tuple2<MAX_CS_FLAG, max cluster size>}
   */
  protected DataSet<Tuple2<String, Long>> maxClusterSizeSet;
  /**
   * DataSet with {@code Tuple2<PCN_FLAG, perfect clusters count>}
   */
  protected DataSet<Tuple2<String, Long>> perfectClusterNoSet;
  /**
   * DataSet with {@code Tuple2<PCCN_FLAG, perfect complete clusters count>}
   */
  protected DataSet<Tuple2<String, Long>> perfectCompleteClusterNoSet;
  /**
   * DataSet with {@code Tuple2<SINGLE_FLAG, singleton clusters count>}
   */
  protected DataSet<Tuple2<String, Long>> singletonSet;
  /**
   * DataSet with {@code Tuple2<PHASE_1_FLAG, vertex count clustered in phase 1>}
   */
  protected DataSet<Tuple2<String, Long>> phase1VertexNoSet;
  /**
   * DataSet with {@code Tuple2<PHASE_2_FLAG, vertex count clustered in phase 2>}
   */
  protected DataSet<Tuple2<String, Long>> phase2VertexNoSet;
  /**
   * DataSet with {@code Tuple2<PHASE_3_FLAG, vertex count clustered in phase 3>}
   */
  protected DataSet<Tuple2<String, Long>> phase3VertexNoSet;
  /**
   * DataSet with {@code Tuple2<REP_AP_FLAG, repaired all positives count>}
   */
  protected DataSet<Tuple2<String, Long>> repAllPositivesSet;
  /**
   * DataSet with {@code Tuple2<REP_TP_FLAG, repaired true positives count>}
   */
  protected DataSet<Tuple2<String, Long>> repTruePositivesSet;

  /**
   * Number of true positives
   */
  protected long truePositives;
  /**
   * Number of all positives
   */
  protected long allPositives;
  /**
   * Number of ground truth records
   */
  protected long gtRecordNo;
  /**
   * Number of clusters
   */
  protected long clusterNo;
  /**
   * Cluster size
   */
  protected long clusterSize;
  /**
   * Min cluster size
   */
  protected long minClusterSize;
  /**
   * Max cluster size
   */
  protected long maxClusterSize;
  /**
   * Average cluster size
   */
  protected double averageClusterSize;
  /**
   * Number of perfect clusters
   */
  protected long perfectClusterNo;
  /**
   * Number of perfect complete clusters
   */
  protected long perfectCompleteClusterNo;
  /**
   * Number of singleton clusters
   */
  protected long singletons;
  /**
   * Number of repaired all positives
   */
  protected long repAllPositiveNo;
  /**
   * Number of repaired true positives
   */
  protected long repTruePositiveNo;
  /**
   * Number of vertices clustered in phase 1
   */
  protected long phase1VertexNo;
  /**
   * Number of vertices clustered in phase 2
   */
  protected long phase2VertexNo;
  /**
   * Number of vertices clustered in phase 3
   */
  protected long phase3VertexNo;

  /**
   * Creates an instance of AbstractClusteringQualityMeasures and initializes sets and values
   */
  public AbstractClusteringQualityMeasures() {
    truePositivesSet = null;
    allPositivesSet = null;
    gtRecordNoSet = null;
    clusterSizeSet = null;
    truePositives = -1L;
    allPositives = -1L;
    gtRecordNo = -1L;
    clusterNo = -1L;
    clusterSize = -1L;
    minClusterSize = -1L;
    maxClusterSize = -1L;
    averageClusterSize = -1d;
    perfectClusterNo = -1L;
    perfectCompleteClusterNo = -1L;
    repAllPositiveNo = -1L;
    repTruePositiveNo = -1L;
    singletons = -1L;
    phase1VertexNo = -1L;
    phase2VertexNo = -1L;
    phase3VertexNo = -1L;
  }

  /**
   * Returns the number of true positives, calls {@link #computeValues} if necessary
   *
   * @return Number of true positives
   *
   * @throws Exception thrown if value computation fails
   */
  public long getTruePositives() throws Exception {
    if (truePositives == -1) {
      computeValues();
    }
    return truePositives;
  }

  /**
   * Returns the number of all positives, calls {@link #computeValues} if necessary
   *
   * @return Number of all positives
   *
   * @throws Exception thrown if value computation fails
   */
  public long getAllPositives() throws Exception {
    if (allPositives == -1) {
      computeValues();
    }
    return allPositives;
  }

  /**
   * Returns the number of ground truth records, calls {@link #computeValues} if necessary
   *
   * @return Number of ground truth records
   *
   * @throws Exception thrown if value computation fails
   */
  public long getGtRecordNo() throws Exception {
    if (gtRecordNo == -1) {
      computeValues();
    }
    return gtRecordNo;
  }

  /**
   * Returns the number of clusters, calls {@link #computeValues} if necessary
   *
   * @return Number of clusters
   *
   * @throws Exception thrown if value computation fails
   */
  public long getClusterNo() throws Exception {
    if (clusterNo == -1) {
      computeValues();
    }
    return clusterNo;
  }

  /**
   * Computes and returns the precision, calls {@link #computeValues} if necessary
   *
   * @return precision
   *
   * @throws Exception thrown if value computation fails
   */
  public double computePrecision() throws Exception {
    if (truePositives == -1) {
      computeValues();
    }
    double precision = (double) truePositives / allPositives;
    return Double.isNaN(precision) ? 0d : precision;
  }

  /**
   * Computes and returns the recall, calls {@link #computeValues} if necessary
   *
   * @return recall
   *
   * @throws Exception thrown if value computation fails
   */
  public double computeRecall() throws Exception {
    if (truePositives == -1) {
      computeValues();
    }
    double recall = (double) truePositives / gtRecordNo;
    return Double.isNaN(recall) ? 0d : recall;
  }

  /**
   * Computes and returns the f-measure, calls {@link #computeValues} if necessary
   *
   * @return f-measure
   *
   * @throws Exception thrown if value computation fails
   */
  public double computeFMeasure() throws Exception {
    if (truePositives == -1) {
      computeValues();
    }
    double pr = computePrecision();
    double re = computeRecall();
    double fMeasure = 2d * pr * re / (pr + re);
    return Double.isNaN(fMeasure) ? 0d : fMeasure;
  }

  /**
   * Returns the min cluster size, calls {@link #computeValues} if necessary
   *
   * @return Min cluster size
   *
   * @throws Exception thrown if value computation fails
   */
  public long getMinClusterSize() throws Exception {
    if (minClusterSize == -1) {
      computeValues();
    }
    return minClusterSize;
  }

  /**
   * Returns the max cluster size, calls {@link #computeValues} if necessary
   *
   * @return Max cluster size
   *
   * @throws Exception thrown if value computation fails
   */
  public long getMaxClusterSize() throws Exception {
    if (maxClusterSize == -1) {
      computeValues();
    }
    return maxClusterSize;
  }

  /**
   * Returns the sum cluster size, calls {@link #computeValues} if necessary
   *
   * @return Sum cluster size
   *
   * @throws Exception thrown if value computation fails
   */
  public long getSumClusterSize() throws Exception {
    if (clusterSize == -1) {
      computeValues();
    }
    return clusterSize;
  }

  /**
   * Returns the average cluster size, calls {@link #computeValues} if necessary
   *
   * @return Average cluster size
   *
   * @throws Exception thrown if value computation fails
   */
  public double getAverageClusterSize() throws Exception {
    if (averageClusterSize < 0d) {
      computeValues();
    }
    return averageClusterSize;
  }

  /**
   * Returns the number of perfect clusters, calls {@link #computeValues} if necessary
   *
   * @return The number of perfect clusters
   *
   * @throws Exception thrown if value computation fails
   */
  public long getPerfectClusterNo() throws Exception {
    if (perfectClusterNo == -1) {
      computeValues();
    }
    return perfectClusterNo;
  }

  /**
   * Returns the number of perfect complete clusters, calls {@link #computeValues} if necessary
   *
   * @return The number of perfect complete clusters
   *
   * @throws Exception thrown if value computation fails
   */
  public long getPerfectCompleteClusterNo() throws Exception {
    if (perfectCompleteClusterNo == -1) {
      computeValues();
    }
    return perfectCompleteClusterNo;
  }

  /**
   * Returns the number of singleton clusters, calls {@link #computeValues} if necessary
   *
   * @return The number of singleton clusters
   *
   * @throws Exception thrown if value computation fails
   */
  public long getSingletons() throws Exception {
    if (singletons == -1) {
      computeValues();
    }
    return singletons;
  }

  /**
   * Returns the number of vertices clustered in phase 1, calls {@link #computeValues} if necessary
   *
   * @return The number of vertices clustered in phase 1
   *
   * @throws Exception thrown if value computation fails
   */
  public long getPhase1ClusteredVertices() throws Exception {
    if (phase1VertexNo == -1) {
      computeValues();
    }
    return phase1VertexNo;
  }

  /**
   * Returns the number of vertices clustered in phase 2, calls {@link #computeValues} if necessary
   *
   * @return The number of vertices clustered in phase 2
   *
   * @throws Exception thrown if value computation fails
   */
  public long getPhase2ClusteredVertices() throws Exception {
    if (phase2VertexNo == -1) {
      computeValues();
    }
    return phase2VertexNo;
  }

  /**
   * Returns the number of vertices clustered in phase 3, calls {@link #computeValues} if necessary
   *
   * @return The number of vertices clustered in phase 3
   *
   * @throws Exception thrown if value computation fails
   */
  public long getPhase3ClusteredVertices() throws Exception {
    if (phase3VertexNo == -1) {
      computeValues();
    }
    return phase3VertexNo;
  }

  /**
   * Computes all datasets, must be implemented by inheriting classes
   */
  protected abstract void computeSets();

  /**
   * Computes all values, must be implemented by inheriting classes
   */
  protected abstract void computeValues() throws Exception;
}
