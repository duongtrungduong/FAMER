/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.inputGraph;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;

/**
 * Abstract class all classes for quality measuring on similarity graphs are inheriting from.
 */
public abstract class AbstractSimilarityGraphQualityMeasures {

  /**
   * Flag to mark tuples with the value for true positives
   */
  protected static final String TP_FLAG = "tp";
  /**
   * Flag to mark tuples with the value for all positives
   */
  protected static final String AP_FLAG = "ap";
  /**
   * Flag to mark tuples with the value for ground truth
   */
  protected static final String GT_FLAG = "gt";

  /**
   * DataSet with {@code Tuple2<TP_FLAG, true positives count>}
   */
  protected DataSet<Tuple2<String, Long>> truePositivesSet;
  /**
   * DataSet with {@code Tuple2<AP_FLAG, all positives count>}
   */
  protected DataSet<Tuple2<String, Long>> allPositivesSet;
  /**
   * DataSet with {@code Tuple2<GT_FLAG, ground truth record number count>}
   */
  protected DataSet<Tuple2<String, Long>> gtRecordNoSet;

  /**
   * Number of true positives
   */
  protected long truePositives;
  /**
   * Number of all positives
   */
  protected long allPositives;
  /**
   * Number of ground truth records
   */
  protected long gtRecordNo;

  /**
   * Creates an instance of AbstractSimilarityGraphQualityMeasures and initializes sets and values
   */
  public AbstractSimilarityGraphQualityMeasures() {
    truePositivesSet = null;
    allPositivesSet = null;
    gtRecordNoSet = null;
    allPositives = -1L;
    truePositives = -1L;
    gtRecordNo = -1L;
  }

  /**
   * Returns the number of true positives, calls {@link #computeValues} if necessary
   *
   * @return Number of true positives
   *
   * @throws Exception thrown if value computation fails
   */
  public long getTruePositives() throws Exception {
    if (truePositives == -1) {
      computeValues();
    }
    return truePositives;
  }

  /**
   * Returns the number of all positives, calls {@link #computeValues} if necessary
   *
   * @return Number of all positives
   *
   * @throws Exception thrown if value computation fails
   */
  public long getAllPositives() throws Exception {
    if (allPositives == -1) {
      computeValues();
    }
    return allPositives;
  }

  /**
   * Returns the number of ground truth records, calls {@link #computeValues} if necessary
   *
   * @return Number of ground truth records
   *
   * @throws Exception thrown if value computation fails
   */
  public long getGtRecordsNo() throws Exception {
    if (gtRecordNo == -1) {
      computeValues();
    }
    return gtRecordNo;
  }

  /**
   * Computes and returns the precision, calls {@link #computeValues} if necessary
   *
   * @return precision
   *
   * @throws Exception thrown if value computation fails
   */
  public double computePrecision() throws Exception {
    if (truePositives == -1) {
      computeValues();
    }
    double precision = (double) truePositives / allPositives;
    return Double.isNaN(precision) ? 0d : precision;
  }

  /**
   * Computes and returns the recall, calls {@link #computeValues} if necessary
   *
   * @return recall
   *
   * @throws Exception thrown if value computation fails
   */
  public double computeRecall() throws Exception {
    if (truePositives == -1) {
      computeValues();
    }
    double recall = (double) truePositives / gtRecordNo;
    return Double.isNaN(recall) ? 0d : recall;
  }

  /**
   * Computes and returns the f-measure, calls {@link #computeValues} if necessary
   *
   * @return f-measure
   *
   * @throws Exception thrown if value computation fails
   */
  public double computeFMeasure() throws Exception {
    if (truePositives == -1) {
      computeValues();
    }
    double pr = computePrecision();
    double re = computeRecall();
    double fMeasure = 2d * pr * re / (pr + re);
    return Double.isNaN(fMeasure) ? 0d : fMeasure;
  }

  /**
   * Computes the values for true positives, all positives and ground truth records
   *
   * @throws Exception thrown if value computation fails
   */
  private void computeValues() throws Exception {
    if (allPositivesSet == null) {
      computeSets();
    }
    DataSet<Tuple2<String, Long>> sets = truePositivesSet.union(allPositivesSet).union(gtRecordNoSet);
    for (Tuple2<String, Long> i : sets.collect()) {
      if (i.f0.equals(AP_FLAG)) {
        allPositives = i.f1;
      } else if (i.f0.equals(TP_FLAG)) {
        truePositives = i.f1;
      } else if (i.f0.equals(GT_FLAG)) {
        gtRecordNo = i.f1;
      }
    }
  }

  /**
   * Computes all datasets, must be implemented by inheriting classes
   */
  protected abstract void computeSets();
}
