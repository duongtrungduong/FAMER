/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.util.Collector;

import java.util.stream.StreamSupport;

/**
 * Reduces a group and returns the number of items in the group.
 *
 * @param <T> Group item type
 */
public class ComputeGroupCount<T> implements GroupReduceFunction<T, Long> {

  @Override
  public void reduce(Iterable<T> group, Collector<Long> out) throws Exception {
    out.collect(StreamSupport.stream(group.spliterator(), false).count());
  }
}
