/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.center;

import com.google.common.collect.Lists;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.clustering.common.PropertyNames.GRAPH_LABEL;
import static org.gradoop.famer.clustering.common.PropertyNames.IS_CENTER;
import static org.gradoop.famer.clustering.common.PropertyNames.IS_NON_CENTER;
import static org.gradoop.famer.clustering.common.PropertyNames.SIM_VALUE;
import static org.gradoop.famer.clustering.common.PropertyNames.VERTEX_PRIORITY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

/**
 * Test class for the parallel {@link Center} algorithm.
 */
public class CenterTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  @Before
  public void setUpGraph() throws Exception {
    String graphString = "similarityGraph[" +
      "/* cluster 1 */" +
      "(v0 {id:0, " + GRAPH_LABEL + ":\"A\"})" +
      "(v1 {id:1, " + GRAPH_LABEL + ":\"B\"})" +
      "(v0)-[e0 {" + SIM_VALUE + ":0.9}]->(v1)" +
      "/* cluster 2 */" +
      "(v2 {id:2, " + GRAPH_LABEL + ":\"B\"})" +
      "(v3 {id:3, " + GRAPH_LABEL + ":\"B\"})" +
      "(v4 {id:4, " + GRAPH_LABEL + ":\"B\"})" +
      "(v0)-[e1 {" + SIM_VALUE + ":0.1}]->(v2)" +
      "(v0)-[e2 {" + SIM_VALUE + ":0.01}]->(v3)" +
      "(v2)-[e3 {" + SIM_VALUE + ":0.5}]->(v3)" +
      "(v2)-[e4 {" + SIM_VALUE + ":0.5}]->(v4)" +
      "(v3)-[e5 {" + SIM_VALUE + ":0.7}]->(v4)" +
      "/* cluster 3 */" +
      "(v5 {id:5, " + GRAPH_LABEL + ":\"A\"})" +
      "(v6 {id:6, " + GRAPH_LABEL + ":\"A\"})" +
      "(v7 {id:7, " + GRAPH_LABEL + ":\"B\"})" +
      "(v5)-[e6 {" + SIM_VALUE + ":0.8}]->(v6)" +
      "(v5)-[e7 {" + SIM_VALUE + ":0.6}]->(v7)" +
      "(v6)-[e8 {" + SIM_VALUE + ":0.5}]->(v7)" +
      "/* cluster 4 */" +
      "(v8 {id:8, " + GRAPH_LABEL + ":\"A\"})" +
      "(v9 {id:9, " + GRAPH_LABEL + ":\"A\"})" +
      "(v8)-[e9 {" + SIM_VALUE + ":0.8}]->(v9)" +
      "/* cluster 5 - single vertex */" +
      "(v10 {id:10, " + GRAPH_LABEL + ":\"B\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");
  }

  @Test
  public void testClusteringForPrioritySelectionMax() throws Exception {
    LogicalGraph clusteredGraph = new Center(PrioritySelection.MAX, false,
      ClusteringOutputType.GRAPH, Integer.MAX_VALUE).execute(inputGraph);

    createClusterListsAndRunCommonTests(clusteredGraph);
  }

  @Test
  public void testClusteringForPrioritySelectionMin() throws Exception {
    LogicalGraph clusteredGraph = new Center(PrioritySelection.MIN, false,
      ClusteringOutputType.GRAPH, Integer.MAX_VALUE).execute(inputGraph);

    createClusterListsAndRunCommonTests(clusteredGraph);
  }

  /**
   * Creates a {@code List<Vertex>} for each cluster, holding the cluster vertices and collects them in a
   * list. Checks if all vertices have the properties for clusterId, isCenter, isNonCenter and vertexPriority.
   * Checks if a vertex defined as center has its own vertex priority as cluster id. Checks if the expected
   * number of clusters was created and checks if each cluster only have one vertex marked as cluster
   * center. Checks if not no overlapping clusters are created.
   *
   * @param clusteredGraph The clustered logical graph
   *
   * @throws Exception Thrown on errors while collecting the vertices from their DataSets
   */
  private void createClusterListsAndRunCommonTests(LogicalGraph clusteredGraph) throws Exception {
    // check vertices and extract cluster id
    DataSet<Tuple2<Long, EPGMVertex>> vertices = clusteredGraph.getVertices().flatMap(
      (FlatMapFunction<EPGMVertex, Tuple2<Long, EPGMVertex>>) (vertex, out) -> {
        assertNotNull("vertex has no clusterId property",
          vertex.getPropertyValue(CLUSTER_ID));
        assertFalse("vertex has overlapping clusters, but shouldn't",
          vertex.getPropertyValue(CLUSTER_ID).toString().contains(","));
        assertNotNull("vertex has no isCenter property",
          vertex.getPropertyValue(IS_CENTER));
        assertNotNull("vertex has no isNonCenter property",
          vertex.getPropertyValue(IS_NON_CENTER));
        assertNotNull("vertex has no vertexPriority property",
          vertex.getPropertyValue(VERTEX_PRIORITY));
        long clusterId = vertex.getPropertyValue(CLUSTER_ID).getLong();
        if (vertex.getPropertyValue(IS_CENTER).getBoolean()) {
          assertEquals("vertex is marked as center but its clusterId doesn't equal its vertexPriority",
            vertex.getPropertyValue(VERTEX_PRIORITY).getLong(), clusterId);
        }
        out.collect(Tuple2.of(clusterId, vertex));
      })
      .returns(new TypeHint<Tuple2<Long, EPGMVertex>>() { });

    // collect cluster lists
    List<List<EPGMVertex>> clusterLists = vertices.groupBy(0)
      .reduceGroup((GroupReduceFunction<Tuple2<Long, EPGMVertex>, List<EPGMVertex>>) (group, out) ->
        out.collect(Lists.newArrayList(group).stream().map(tuple -> tuple.f1).collect(Collectors.toList())))
      .returns(new TypeHint<List<EPGMVertex>>() { })
      .collect();

    // check clusters
    assertEquals("expected 5 clusters, but found " + clusterLists.size(),
      5, clusterLists.size());
    for (List<EPGMVertex> cluster : clusterLists) {
      assertEquals("one cluster center expected per cluster, found none or more than one",
        1L, cluster.stream().filter(v -> v.getPropertyValue(IS_CENTER).getBoolean()).count());
    }
  }
}
