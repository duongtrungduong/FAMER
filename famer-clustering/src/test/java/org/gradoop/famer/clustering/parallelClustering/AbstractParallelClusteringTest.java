/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

/**
 * JUnit tests for {@link AbstractParallelClustering}.
 */
public class AbstractParallelClusteringTest {

  private final AbstractParallelClustering abstractParallelClustering =
    Mockito.mock(AbstractParallelClustering.class, Mockito.CALLS_REAL_METHODS);

  @Test
  public void testGetClusteringOutputTypeFromJsonConfig() throws JSONException {
    String json = "{\n" +
      "   \"clusteringOutputType\":\"GRAPH\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    ClusteringOutputType result =
      abstractParallelClustering.getClusteringOutputTypeFromJsonConfig(clusterConfigJson);

    assertNotNull(result);
    assertEquals(ClusteringOutputType.GRAPH, result);
  }

  @Test(expected = RuntimeException.class)
  public void testGetClusteringOutputTypeFromJsonConfigForMissingValue() throws JSONException {
    String json = "{}";
    JSONObject clusterConfigJson = new JSONObject(json);

    abstractParallelClustering.getClusteringOutputTypeFromJsonConfig(clusterConfigJson);
  }

  @Test(expected = RuntimeException.class)
  public void testGetClusteringOutputTypeFromJsonConfigForWrongValue() throws JSONException {
    String json = "{\n" +
      "   \"clusteringOutputType\":\"a\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    abstractParallelClustering.getClusteringOutputTypeFromJsonConfig(clusterConfigJson);
  }

  @Test
  public void testGetMaxIterationFromJsonConfigForMaxValue() throws JSONException {
    String json = "{\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    int result = abstractParallelClustering.getMaxIterationFromJsonConfig(clusterConfigJson);

    assertEquals(Integer.MAX_VALUE, result);
  }

  @Test
  public void testGetMaxIterationFromJsonConfigForCustomValue() throws JSONException {
    String json = "{\n" +
      "   \"maxIteration\":\"1\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    int result = abstractParallelClustering.getMaxIterationFromJsonConfig(clusterConfigJson);

    assertEquals(1, result);
  }

  @Test(expected = RuntimeException.class)
  public void testGetMaxIterationFromJsonConfigForMissingValue() throws JSONException {
    String json = "{}";
    JSONObject clusterConfigJson = new JSONObject(json);

    abstractParallelClustering.getMaxIterationFromJsonConfig(clusterConfigJson);
  }

  @Test(expected = RuntimeException.class)
  public void testGetMaxIterationFromJsonConfigForWrongValue() throws JSONException {
    String json = "{\n" +
      "   \"maxIteration\":\"a\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    abstractParallelClustering.getMaxIterationFromJsonConfig(clusterConfigJson);
  }

  @Test
  public void testGetIsEdgesBiDirectedFromJsonConfig() throws JSONException {
    String json = "{\n" +
      "   \"isEdgesBiDirected\":false\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    boolean result =
      abstractParallelClustering.getIsEdgesBiDirectedFromJsonConfig(clusterConfigJson);

    assertFalse(result);
  }

  @Test(expected = RuntimeException.class)
  public void testGetIsEdgesBiDirectedFromJsonConfigForMissingValue() throws JSONException {
    String json = "{}";
    JSONObject clusterConfigJson = new JSONObject(json);

    abstractParallelClustering.getIsEdgesBiDirectedFromJsonConfig(clusterConfigJson);
  }

  @Test(expected = RuntimeException.class)
  public void testGetIsEdgesBiDirectedFromJsonConfigForWrongValue() throws JSONException {
    String json = "{\n" +
      "   \"isEdgesBiDirected\":a\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    abstractParallelClustering.getIsEdgesBiDirectedFromJsonConfig(clusterConfigJson);
  }

  @Test
  public void testGetPrioritySelectionFromJsonConfig() throws JSONException {
    String json = "{\n" +
      "   \"prioritySelection\":\"MIN\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    PrioritySelection result =
      abstractParallelClustering.getPrioritySelectionFromJsonConfig(clusterConfigJson);

    assertNotNull(result);
    assertEquals(PrioritySelection.MIN, result);
  }

  @Test(expected = RuntimeException.class)
  public void testGetPrioritySelectionFromJsonConfigForMissingValue() throws JSONException {
    String json = "{}";
    JSONObject clusterConfigJson = new JSONObject(json);

    abstractParallelClustering.getPrioritySelectionFromJsonConfig(clusterConfigJson);
  }

  @Test(expected = RuntimeException.class)
  public void testGetPrioritySelectionFromJsonConfigForWrongValue() throws JSONException {
    String json = "{\n" +
      "   \"prioritySelection\":\"a\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    abstractParallelClustering.getPrioritySelectionFromJsonConfig(clusterConfigJson);
  }
}
