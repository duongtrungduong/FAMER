/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.serialClustering;

import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test class for {@link AbstractSerialClustering}. Uses Mockito to mock the abstract class with usage of
 * its real methods.
 */
public class AbstractSerialClusteringTest {

  private final AbstractSerialClustering abstractSerialClustering =
    Mockito.mock(AbstractSerialClustering.class, Mockito.CALLS_REAL_METHODS);

  @Test
  public void testGetLinesFromFilePath() throws IOException {
    String filePath = new File(AbstractSerialClusteringTest.class.getResource(
      "/serialClustering/abstractSerialClustering/testReadLines.csv").getFile()).getAbsolutePath();
    Stream<String> lines = abstractSerialClustering.getLinesFromFilePath(filePath);

    List<String> linesList = lines.collect(Collectors.toList());

    assertEquals("expected 3 lines, but found " + linesList.size(), 3, linesList.size());
    assertEquals("first line read with errors", "1,one", linesList.get(0));
    assertEquals("second line read with errors", "2,two", linesList.get(1));
    assertEquals("third line read with errors", "3,three", linesList.get(2));
  }

  @Test
  public void testWriteLinesToFilePath() throws IOException {
    File tempOutFile = File.createTempFile("famerSerialClusteringTestOutput", ".csv");
    tempOutFile.deleteOnExit();

    List<String> linesToWrite = Arrays.asList("1,one", "2,two", "3,three");
    abstractSerialClustering.writeLinesToFilePath(tempOutFile.getPath(), linesToWrite);

    assertTrue(Files.exists(Paths.get(tempOutFile.getPath())));

    List<String> writtenLinesList =
      Files.lines(Paths.get(tempOutFile.getPath()), StandardCharsets.UTF_8).collect(Collectors.toList());

    assertEquals("expected 3 lines, but found " + writtenLinesList.size(), 3, writtenLinesList.size());
    assertEquals("first line written with errors", "1,one", writtenLinesList.get(0));
    assertEquals("second line written with errors", "2,two", writtenLinesList.get(1));
    assertEquals("third line written with errors", "3,three", writtenLinesList.get(2));
  }
}
