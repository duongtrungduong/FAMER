/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.gradoop.flink.model.api.operators.UnaryGraphToGraphOperator;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * The abstract class all parallel clustering algorithms inheriting from. Implements the Gradoop
 * {@link UnaryGraphToGraphOperator} to be executable as operator on a {@link LogicalGraph}.
 */
public abstract class AbstractParallelClustering implements UnaryGraphToGraphOperator {

  /**
   * Creates an instance of AbstractParallelClustering
   */
  public AbstractParallelClustering() {
  }

  /**
   * Gets the {@link ClusteringOutputType} from the clustering json configuration part.
   *
   * @param jsonConfig The clustering json configuration
   *
   * @return The {@link ClusteringOutputType} for clustering
   */
  protected ClusteringOutputType getClusteringOutputTypeFromJsonConfig(JSONObject jsonConfig) {
    try {
      return ClusteringOutputType.valueOf(jsonConfig.getString("clusteringOutputType"));
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Clustering: value for clusteringOutputType could not be " +
        "found or parsed", ex);
    }
  }

  /**
   * Gets the maxIteration value from the clustering json configuration part.
   *
   * @param jsonConfig The clustering json configuration
   *
   * @return The maxIteration value for clustering
   */
  protected int getMaxIterationFromJsonConfig(JSONObject jsonConfig) {
    try {
      if (jsonConfig.getString("maxIteration").equals("MAX_VALUE")) {
        return Integer.MAX_VALUE;
      } else {
        return jsonConfig.getInt("maxIteration");
      }
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Clustering: value for maxIteration could not be found or " +
        "parsed", ex);
    }
  }

  /**
   * Gets the isEdgesBiDirected value from the clustering json configuration part.
   *
   * @param jsonConfig The clustering json configuration
   *
   * @return The isEdgesBiDirected value for clustering
   */
  protected boolean getIsEdgesBiDirectedFromJsonConfig(JSONObject jsonConfig) {
    try {
      if (jsonConfig.getString("isEdgesBiDirected").equals("true") ||
        jsonConfig.getString("isEdgesBiDirected").equals("false")) {
        return jsonConfig.getBoolean("isEdgesBiDirected");
      } else {
        throw new UnsupportedOperationException("Clustering: value for isEdgesBiDirected is not 'true' " +
          "or 'false'");
      }
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Clustering: value for isEdgesBiDirected could not be found " +
        "or parsed", ex);
    }
  }

  /**
   * Gets the {@link PrioritySelection} from the clustering json configuration part.
   *
   * @param jsonConfig The clustering json configuration
   *
   * @return The {@link PrioritySelection} for clustering
   */
  protected PrioritySelection getPrioritySelectionFromJsonConfig(JSONObject jsonConfig) {
    try {
      return PrioritySelection.valueOf(jsonConfig.getString("prioritySelection"));
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Clustering: value for prioritySelection could not be found " +
        "or parsed", ex);
    }
  }

  @Override
  public LogicalGraph execute(LogicalGraph graph) {
    return this.runClustering(graph);
  }

  /**
   * Executes the parallel clustering algorithm. Needs to be implemented by inheriting classes.
   *
   * @param graph The input logical graph
   *
   * @return The logical graph with the clustering result
   */
  protected abstract LogicalGraph runClustering(LogicalGraph graph);
}
