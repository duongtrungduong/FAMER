/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.center.functions;

import com.google.common.collect.Lists;
import org.apache.flink.api.common.aggregators.LongSumAggregator;
import org.apache.flink.graph.spargel.GatherFunction;
import org.apache.flink.graph.spargel.MessageIterator;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.CenterMessage;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;

import java.util.Arrays;
import java.util.List;

/**
 * Gather function used in Center clustering algorithm
 */
public class CenterGatherFunction extends GatherFunction<GradoopId, EPGMVertex, CenterMessage> {
  /**
   * The aggregator name
   */
  public static final String HAS_FINISHED_AGGREGATOR = "hasFinished";
  /**
   * The aggregator to determine if cluster identification has finished
   */
  private LongSumAggregator hasFinishedAggregator = new LongSumAggregator();
  /**
   * Which vertex to select as center based on its vertex priority, see {@link PrioritySelection}
   */
  private final PrioritySelection prioritySelection;

  /**
   * Creates an instance of CenterGatherFunction
   *
   * @param prioritySelection Which vertex to select as center based on its vertex priority
   */
  public CenterGatherFunction(PrioritySelection prioritySelection) {
    this.prioritySelection = prioritySelection;
  }

  @Override
  public void preSuperstep() {
    hasFinishedAggregator = getIterationAggregator(HAS_FINISHED_AGGREGATOR);
  }

  @Override
  public void updateVertex(org.apache.flink.graph.Vertex<GradoopId, EPGMVertex> vertex,
    MessageIterator<CenterMessage> messages) {
    int superstepMod2 = getSuperstepNumber() % 2;
    if (superstepMod2 == 1) { // for all odd supersteps
      String deciding = "";
      if (!vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean() &&
        !vertex.f1.getPropertyValue(PropertyNames.IS_NON_CENTER).getBoolean()) {
        int state =
          Integer.parseInt(vertex.f1.getPropertyValue(PropertyNames.STATE).toString());
        CenterMessage[] msgArray = fillAndSortMsgArray(messages);
        if (state < msgArray.length) {
          if (msgArray[state].getSimDegree() != -1.0) {
            deciding += msgArray[state].getClusterId() + "," +
              vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).toString();
          } else {
            // handle unconnected vertices, process message from itself and mark it as center
            vertex.f1.setProperty(PropertyNames.IS_CENTER, true);
            vertex.f1.setProperty(PropertyNames.CLUSTER_ID,
              vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong());
          }
        }
      }
      hasFinishedAggregator.aggregate(1L);
      vertex.f1.setProperty(PropertyNames.DECIDING, deciding);
      setNewVertexValue(vertex.f1);
    } else if (superstepMod2 == 0) {
      if (!vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean() &&
        !vertex.f1.getPropertyValue(PropertyNames.IS_NON_CENTER).getBoolean()) {
        CenterMessage[] msgArray = fillAndSortMsgArray(messages);
        int state =
          Integer.parseInt(vertex.f1.getPropertyValue(PropertyNames.STATE).toString());
        for (int i = state; i < msgArray.length; i++) {
          if (msgArray[i].getAdditionalInfo().contains(PropertyNames.IS_CENTER)) {
            vertex.f1.setProperty(PropertyNames.CLUSTER_ID, msgArray[i].getClusterId());
            vertex.f1.setProperty(PropertyNames.IS_NON_CENTER, true);
            break;
          }
          if (msgArray[i].getAdditionalInfo().contains(PropertyNames.IS_NON_CENTER)) {
            vertex.f1.setProperty(PropertyNames.STATE, state + 1);
          } else {
            hasFinishedAggregator.aggregate(1L);
            long selectedClusterId = Long.parseLong(msgArray[i].getAdditionalInfo().split(",")[0]);
            long origin = Long.parseLong(msgArray[i].getAdditionalInfo().split(",")[1]);
            long vertexPriority =
              vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();
            if (selectedClusterId == vertexPriority) {
              if (((origin > vertexPriority) && (prioritySelection == PrioritySelection.MIN)) ||
                ((origin < vertexPriority) && (prioritySelection == PrioritySelection.MAX))) {
                vertex.f1.setProperty(PropertyNames.IS_CENTER, true);
                vertex.f1.setProperty(PropertyNames.CLUSTER_ID, vertexPriority);
              }
              break;
            }
            break;
          }
        }
      }
      setNewVertexValue(vertex.f1);
    }
  }

  /**
   * Creates an array of the incoming messages and sorts it based on the similarity degree or, if this
   * is equal, on the vertex priority selection.
   *
   * @param messages The incoming messages
   *
   * @return The sorted array of messages
   */
  private CenterMessage[] fillAndSortMsgArray(MessageIterator<CenterMessage> messages) {
    List<CenterMessage> msgList = Lists.newArrayList(messages.iterator());
    CenterMessage[] msgArray = msgList.toArray(new CenterMessage[msgList.size()]);
    Arrays.sort(msgArray, (in1, in2) -> {
      if (in1.getSimDegree() > in2.getSimDegree()) {
        return -1;
      }
      if (in1.getSimDegree() < in2.getSimDegree()) {
        return 1;
      }
      if ((in1.getClusterId() < in2.getClusterId()) && (prioritySelection == PrioritySelection.MIN)) {
        return -1;
      }
      if ((in1.getClusterId() > in2.getClusterId()) && (prioritySelection == PrioritySelection.MAX)) {
        return -1;
      }
      if ((in1.getClusterId() > in2.getClusterId()) && (prioritySelection == PrioritySelection.MIN)) {
        return 1;
      }
      if ((in1.getClusterId() < in2.getClusterId()) && (prioritySelection == PrioritySelection.MAX)) {
        return 1;
      }
      return 0;
    });
    return msgArray;
  }
}
