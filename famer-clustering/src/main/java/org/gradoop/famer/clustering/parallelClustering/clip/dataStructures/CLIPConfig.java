/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.clip.dataStructures;

/**
 * CLIP Algorithm configuration parameters
 */
public class CLIPConfig {

  /**
   * The allowed similarity loss
   */
  private double delta;

  /**
   * Number of sources
   */
  private int sourceNumber;

  /**
   * Whether to remove all source consistent vertices
   */
  private boolean removeSourceConsistentVertices;

  /**
   * The similarity value coefficient
   */
  private double simValueCoef;

  /**
   * The degree coefficient
   */
  private double degreeCoef;

  /**
   * The strength coefficient
   */
  private double strengthCoef;

  /**
   * Creates an instance of CLIPConfig with default values
   */
  public CLIPConfig() {
    this.delta = 0.0;
    this.sourceNumber = 1;
    this.removeSourceConsistentVertices = false;
    this.simValueCoef = 0.5;
    this.degreeCoef = 0.2;
    this.strengthCoef = 0.3;
  }

  /**
   * Creates an instance of CLIPConfig
   *
   * @param delta The allowed similarity loss
   * @param sourceNumber Number of sources
   * @param removeSourceConsistentVertices Whether to remove all source consistent vertices
   * @param simValueCoef The similarity value coefficient
   * @param degreeCoef The degree coefficient
   * @param strengthCoef The strength coefficient
   */
  public CLIPConfig(double delta, int sourceNumber, boolean removeSourceConsistentVertices,
    double simValueCoef, double degreeCoef, double strengthCoef) {
    this.delta = delta;
    this.sourceNumber = sourceNumber;
    this.removeSourceConsistentVertices = removeSourceConsistentVertices;
    this.simValueCoef = simValueCoef;
    this.degreeCoef = degreeCoef;
    this.strengthCoef = strengthCoef;
  }

  public double getDelta() {
    return delta;
  }

  public int getSourceNumber() {
    return sourceNumber;
  }

  public boolean isRemoveSourceConsistentVertices() {
    return removeSourceConsistentVertices;
  }

  public double getSimValueCoef() {
    return simValueCoef;
  }

  public double getDegreeCoef() {
    return degreeCoef;
  }

  public double getStrengthCoef() {
    return strengthCoef;
  }
}
