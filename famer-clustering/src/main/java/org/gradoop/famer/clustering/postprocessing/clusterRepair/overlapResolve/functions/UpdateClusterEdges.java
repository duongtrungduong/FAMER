/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.common.dataStructures.Cluster;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Reduces a group of {@code Tuple2<Cluster, clusterId>}, grouped by the clusterId, to a new Cluster with
 * updated intra- and inter-cluster edges
 */
public class UpdateClusterEdges implements GroupReduceFunction<Tuple2<Cluster, String>, Cluster> {

  @Override
  public void reduce(Iterable<Tuple2<Cluster, String>> group, Collector<Cluster> out) throws Exception {
    List<EPGMVertex> vertices = new ArrayList<>();
    List<EPGMEdge> edges = new ArrayList<>();
    List<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>> vertexPairs = new ArrayList<>();
    List<EPGMEdge> intraClusterEdges = new ArrayList<>();
    List<EPGMEdge> interClusterEdges = new ArrayList<>();
    String clusterId = "";

    for (Tuple2<Cluster, String> groupItem : group) {
      for (EPGMVertex vertex : groupItem.f0.getVertices()) {
        if (!vertices.contains(vertex)) {
          vertices.add(vertex);
        }
      }
      for (EPGMEdge edge : groupItem.f0.getInterLinks()) {
        if (!edges.contains(edge)) {
          edges.add(edge);
        }
      }
      for (EPGMEdge edge : groupItem.f0.getIntraEdges()) {
        if (!edges.contains(edge)) {
          edges.add(edge);
        }
      }
      clusterId = groupItem.f1;
    }

    for (EPGMEdge edge : edges) {
      EPGMVertex vertex0 = null;
      EPGMVertex vertex1 = null;
      for (EPGMVertex vertex : vertices) {
        if (vertex.getId().equals(edge.getSourceId())) {
          vertex0 = vertex;
        } else if (vertex.getId().equals(edge.getTargetId())) {
          vertex1 = vertex;
        }
      }
      vertexPairs.add(Tuple3.of(edge, vertex0, vertex1));
    }
    for (Tuple3<EPGMEdge, EPGMVertex, EPGMVertex> vertexPair : vertexPairs) {
      if ((vertexPair.f1 == null) && (vertexPair.f2 == null)) {
        continue;
      } else if ((vertexPair.f1 == null) || (vertexPair.f2 == null)) {
        interClusterEdges.add(vertexPair.f0);
      } else {
//        String[] f1ClusterIds = vertexPair.f1.getPropertyValue("ClusterId").toString().split(",");
//        String[] f2ClusterIds = vertexPair.f2.getPropertyValue("ClusterId").toString().split(",");
//        if (hasIntersection(f1ClusterIds, f2ClusterIds)) {
//          interClusterEdges.add(vertexPair.f0);
//        } else {
//          intraClusterEdges.add(vertexPair.f0);
//        }
        List<String> sourceClusterIds = Arrays.asList(vertexPair.f1.getPropertyValue(
          PropertyNames.CLUSTER_ID).toString().split(","));
        List<String> targetClusterIds = Arrays.asList(vertexPair.f2.getPropertyValue(
          PropertyNames.CLUSTER_ID).toString().split(","));

        if (sourceClusterIds.contains(clusterId) && targetClusterIds.contains(clusterId)) {
          intraClusterEdges.add(vertexPair.f0);
          if ((sourceClusterIds.size() > 1) || (targetClusterIds.size() > 1)) {
            interClusterEdges.add(vertexPair.f0);
          }
        } else if ((sourceClusterIds.contains(clusterId) && !targetClusterIds.contains(clusterId)) ||
          (!sourceClusterIds.contains(clusterId) && targetClusterIds.contains(clusterId))) {
          interClusterEdges.add(vertexPair.f0);
        }
      }
    }
    out.collect(new Cluster(vertices, intraClusterEdges, interClusterEdges, clusterId, ""));
  }
}
