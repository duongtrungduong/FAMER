/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Joins a Gradoop vertex with a Gelly vertex on their id to transfer the result value from the connected
 * component algorithm to the Gradoop vertex as property.
 */
public class JoinVertexWithGellyResultVertex implements
  JoinFunction<org.apache.flink.graph.Vertex<GradoopId, Long>, EPGMVertex, EPGMVertex> {
  /**
   * The used cluster id prefix
   */
  private final String clusterIdPrefix;
  /**
   * The property to store the connected component cluster id
   */
  private final String clusterIdProperty;

  /**
   * Creates an instance of JoinVertexWithGellyResultVertex
   *
   * @param clusterIdPrefix The used cluster id prefix
   * @param clusterIdProperty The property to store the connected component cluster id
   */
  public JoinVertexWithGellyResultVertex(String clusterIdPrefix, String clusterIdProperty) {
    this.clusterIdPrefix = clusterIdPrefix;
    this.clusterIdProperty = clusterIdProperty;
  }

  @Override
  public EPGMVertex join(org.apache.flink.graph.Vertex<GradoopId, Long> gellyVertex, EPGMVertex vertex) {
    vertex.setProperty(clusterIdProperty, clusterIdPrefix + gellyVertex.getValue().toString());
    return vertex;
  }
}
