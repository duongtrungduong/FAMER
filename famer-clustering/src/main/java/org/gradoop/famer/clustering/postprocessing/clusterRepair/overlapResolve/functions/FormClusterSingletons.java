/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.common.dataStructures.Cluster;

import java.util.ArrayList;
import java.util.List;

/**
 * For a {@code Tuple3<Vertex, oldClusterId, newClusterId>}, returns a new Cluster holding only the Vertex if
 * the Vertex is marked as a single vertex
 */
public class FormClusterSingletons implements FlatMapFunction<Tuple3<EPGMVertex, String, String>, Cluster> {

  @Override
  public void flatMap(Tuple3<EPGMVertex, String, String> vertexOldClusterIdNewClusterId,
    Collector<Cluster> out) throws Exception {
    if (vertexOldClusterIdNewClusterId.f2.contains("s")) {
      List<EPGMVertex> vertices = new ArrayList<>();
      vertexOldClusterIdNewClusterId.f0.setProperty(PropertyNames.CLUSTER_ID,
        vertexOldClusterIdNewClusterId.f2);
      vertices.add(vertexOldClusterIdNewClusterId.f0);
      out.collect(new Cluster(vertices, vertexOldClusterIdNewClusterId.f2));
    }
  }
}
