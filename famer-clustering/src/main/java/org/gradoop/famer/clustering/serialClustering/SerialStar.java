/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.serialClustering;

import org.gradoop.famer.clustering.serialClustering.dataStructures.PrioritySelection;
import org.gradoop.famer.clustering.serialClustering.dataStructures.SerialVertexComponent;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The serial implementation of Star algorithm
 */
public class SerialStar extends AbstractSerialClustering {

  /**
   * Enumeration for the type of Star algorithm
   */
  public enum StarType {
    /**
     * Start type 1
     */
    ONE,
    /**
     * Star type 2
     */
    TWO
  }

  /**
   * Which vertex to select as center based on its vertex priority
   */
  private final PrioritySelection prioritySelection;

  /**
   * The type of Star algorithm
   */
  private final StarType type;

  /**
   * Creates an instance of SerialStar
   *
   * @param prioritySelection Which vertex to select as center based on its vertex priority
   * @param type The type of Star algorithm
   * @param verticesInputPath File path to read the vertices from
   * @param edgesInputPath File path to read the edges from
   * @param outputDir Directory for writing the clustering results
   */
  public SerialStar(PrioritySelection prioritySelection, StarType type, String verticesInputPath,
    String edgesInputPath, String outputDir) {
    super(verticesInputPath, edgesInputPath, outputDir);
    this.prioritySelection = prioritySelection;
    this.type = type;
  }

  @Override
  protected void initializeSerialGraphComponents() throws IOException {
    getLinesFromFilePath(verticesInputPath).forEach(line -> {
      String[] lineData = line.split(",");
      SerialVertexComponent svc =
        new SerialVertexComponent(Long.parseLong(lineData[1]), Long.parseLong(lineData[1]), false);
      svc.setId(lineData[0]);
      serialVertexComponentMap.put(lineData[0], svc);
    });
    getLinesFromFilePath(edgesInputPath).forEach(line -> {
      String[] lineData = line.split(",");
      String sourceId = lineData[0];
      String targetId = lineData[1];
      double degree = Double.parseDouble(lineData[2]);
      // update svc for sourceId
      SerialVertexComponent svc = serialVertexComponentMap.get(sourceId);
      svc.addToNeighborList(targetId);
      svc.addToDegree(1);
      svc.addToSimDegree(degree);
      serialVertexComponentMap.put(sourceId, svc);
      // update svc for targetId
      svc = serialVertexComponentMap.get(targetId);
      svc.addToNeighborList(sourceId);
      svc.addToDegree(1);
      svc.addToSimDegree(degree);
      serialVertexComponentMap.put(targetId, svc);
    });
  }

  @Override
  protected void writeClusteringResultsToFile() throws IOException {
    List<String> clusteringResults = serialVertexComponentMap.entrySet().stream()
      .map(entry -> entry.getKey() + "," + entry.getValue().getClusterIds())
      .collect(Collectors.toList());

    writeLinesToFilePath(outputPath, clusteringResults);
  }

  @Override
  protected void doSerialClustering() {
    SerialVertexComponent[] svcArray = new SerialVertexComponent[serialVertexComponentMap.size()];
    Set<Map.Entry<String, SerialVertexComponent>> entries = serialVertexComponentMap.entrySet();
    Iterator<Map.Entry<String, SerialVertexComponent>> entriesIterator = entries.iterator();
    int i = 0;
    while (entriesIterator.hasNext()) {
      Map.Entry<String, SerialVertexComponent> mapping = entriesIterator.next();
      svcArray[i] = mapping.getValue();
      double sumSimDegree = svcArray[i].getSimDegree();
      int sumDegree = svcArray[i].getDegree();
      if (type == StarType.ONE) {
        svcArray[i].setSimDegree(sumDegree);
      } else if (type == StarType.TWO) {
        if (sumDegree != 0) {
          svcArray[i].setSimDegree(sumSimDegree / (double) sumDegree);
        } else {
          svcArray[i].setSimDegree(sumSimDegree);
        }
      }
      i++;
    }
    Arrays.sort(svcArray, (in1, in2) -> {
      if (in1.getSimDegree() > in2.getSimDegree()) {
        return -1;
      }
      if (in1.getSimDegree() < in2.getSimDegree()) {
        return 1;
      }
      if (((in1.getClusterId() > in2.getClusterId()) && (prioritySelection == PrioritySelection.MAX)) ||
        ((in1.getClusterId() < in2.getClusterId()) && (prioritySelection == PrioritySelection.MIN))) {
        return -1;
      }
      if (((in1.getClusterId() > in2.getClusterId()) && (prioritySelection == PrioritySelection.MIN)) ||
        ((in1.getClusterId() < in2.getClusterId()) && (prioritySelection == PrioritySelection.MAX))) {
        return 1;
      }
      return 0;
    });
    for (i = 0; i < svcArray.length; i++) {
      SerialVertexComponent center = svcArray[i];
      if (serialVertexComponentMap.get(svcArray[i].getId()).getClusterIds().equals("")) {
        String clusterId = Long.toString(svcArray[i].getVertexPrio());
        center.setClusterIds(clusterId);
        serialVertexComponentMap.put(svcArray[i].getId(), center);
        List<String> neighborList = svcArray[i].getNeighborList();
        for (String neighborId : neighborList) {
          SerialVertexComponent svc = serialVertexComponentMap.get(neighborId);
          svc.setClusterIds(clusterId);
          serialVertexComponentMap.put(neighborId, svc);
        }
      }
    }
  }
}


