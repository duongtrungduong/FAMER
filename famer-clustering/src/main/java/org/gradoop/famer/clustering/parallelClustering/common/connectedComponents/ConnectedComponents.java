/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common.connectedComponents;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.graph.Graph;
import org.apache.flink.graph.library.GSAConnectedComponents;
import org.apache.flink.types.NullValue;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.common.GenerateOutput;
import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.functions.EdgeToGellyEdgeOnSimilarityLabel;
import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.functions.JoinVertexWithGellyResultVertex;
import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.functions.VertexToGellyVertexWithPriorityValue;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.functions.ModifyLogicalGraphForClustering;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.epgm.Id;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;

/**
 * Implementation of the Connected Component algorithm used in the CLIP clustering algorithm, based on
 * Flink/Gellys {@link GSAConnectedComponents}. A directed input graph is transformed to an undirected
 * graph for computation.
 */
public class ConnectedComponents extends AbstractParallelClustering {

  /**
   * Maximum number of iterations for the Gelly Connected Components algorithm
   */
  private final int maxIteration;

  /**
   * Prefix string for the cluster id
   */
  private final String clusterIdPrefix;

  /**
   * The output type for the clustering result
   */
  private final ClusteringOutputType clusteringOutputType;

  /**
   * Label of the similarity edges to filter on before clustering
   */
  private final String similarityEdgeLabel;

  /**
   * Creates an instance of ConnectedComponentsForClustering
   *
   * @param maxIteration Maximum number of iterations for the Gelly Connected Components algorithm
   */
  public ConnectedComponents(int maxIteration) {
    this(maxIteration, "", null, ClusteringOutputType.GRAPH);
  }

  /**
   * Creates an instance of ConnectedComponentsForClustering
   *
   * @param maxIteration Maximum number of iterations for the Gelly Connected Components algorithm
   * @param clusterIdPrefix The used prefix string for the cluster id
   */
  public ConnectedComponents(int maxIteration, String clusterIdPrefix) {
    this(maxIteration, clusterIdPrefix, null, ClusteringOutputType.GRAPH);
  }

  /**
   * Creates an instance of ConnectedComponentsForClustering
   *
   * @param maxIteration Maximum number of iterations for the Gelly Connected Components algorithm
   * @param clusterIdPrefix The used prefix string for the cluster id
   * @param similarityEdgeLabel Label of the similarity edges to filter on before clustering
   */
  public ConnectedComponents(int maxIteration, String clusterIdPrefix,
    String similarityEdgeLabel) {
    this(maxIteration, clusterIdPrefix, similarityEdgeLabel, ClusteringOutputType.GRAPH);
  }

  /**
   * Creates an instance of ConnectedComponentsForClustering
   *
   * @param maxIteration Maximum number of iterations for the Gelly Connected Components algorithm
   * @param clusterIdPrefix The used prefix string for the cluster id
   * @param similarityEdgeLabel Label of the similarity edges to filter on before clustering
   * @param clusteringOutputType The output type for the clustering result
   */
  public ConnectedComponents(int maxIteration, String clusterIdPrefix,
    String similarityEdgeLabel, ClusteringOutputType clusteringOutputType) {
    this.maxIteration = maxIteration;
    this.clusterIdPrefix = clusterIdPrefix;
    this.similarityEdgeLabel = similarityEdgeLabel;
    this.clusteringOutputType = clusteringOutputType;
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The clustering algorithm json config part
   */
  public ConnectedComponents(JSONObject jsonConfig) {
    String clusterIdPrefixElem = jsonConfig.optString("clusterIdPrefix");
    if (clusterIdPrefixElem.equals("")) {
      this.clusterIdPrefix = "";
    } else {
      try {
        this.clusterIdPrefix = jsonConfig.getString("clusterIdPrefix");
      } catch (Exception ex) {
        throw new UnsupportedOperationException("Clustering: value for clusterIdPrefix could not be " +
          "parsed", ex);
      }
    }
    String similarityEdgeLabelElem = jsonConfig.optString("similarityEdgeLabel");
    if (similarityEdgeLabelElem.equals("")) {
      this.similarityEdgeLabel = null;
    } else {
      try {
        this.similarityEdgeLabel = jsonConfig.getString("similarityEdgeLabel");
      } catch (Exception ex) {
        throw new UnsupportedOperationException("Clustering: value for similarityEdgeLabel could not be " +
          "parsed", ex);
      }
    }
    this.clusteringOutputType = getClusteringOutputTypeFromJsonConfig(jsonConfig);
    this.maxIteration = getMaxIterationFromJsonConfig(jsonConfig);
  }

  public int getMaxIteration() {
    return maxIteration;
  }

  public String getClusterIdPrefix() {
    return clusterIdPrefix;
  }

  public ClusteringOutputType getClusteringOutputType() {
    return clusteringOutputType;
  }

  public String getSimilarityEdgeLabel() {
    return similarityEdgeLabel;
  }

  @Override
  protected LogicalGraph runClustering(LogicalGraph inputGraph) {
    inputGraph = inputGraph.callForGraph(new ModifyLogicalGraphForClustering());
    Graph<GradoopId, Long, NullValue> gellyGraph = Graph.fromDataSet(
      inputGraph.getVertices().map(new VertexToGellyVertexWithPriorityValue()),
      inputGraph.getEdges().flatMap(new EdgeToGellyEdgeOnSimilarityLabel(similarityEdgeLabel)),
      inputGraph.getConfig().getExecutionEnvironment());

    try {
      DataSet<EPGMVertex> annotatedVertices =
        new org.apache.flink.graph.library.ConnectedComponents<GradoopId, Long, NullValue>(maxIteration)
          .run(gellyGraph)
          .join(inputGraph.getVertices())
          .where(0).equalTo(new Id<>())
          .with(new JoinVertexWithGellyResultVertex(clusterIdPrefix, CLUSTER_ID));

      LogicalGraph resultLogicalGraph = inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(
        inputGraph.getGraphHead(), annotatedVertices, inputGraph.getEdges());

      return resultLogicalGraph.callForGraph(new GenerateOutput(clusteringOutputType));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  @Override
  public String getName() {
    return ConnectedComponents.class.getName();
  }
}
