/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.clip.clipResolver.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple5;
import org.gradoop.common.model.impl.id.GradoopId;

/**
 * Joins a {@code Tuple3<VertexId, Type, ClusterId>} with a {@code Tuple4<EdgeId, SourceId, TargetId,
 * PrioValue>} where {@code VertexId == SourceId} and returns a {@code Tuple5<Type, EdgeId, SourceId,
 * TargetId, PrioValue>}
 */
@FunctionAnnotation.ForwardedFieldsFirst("f1->f0")
@FunctionAnnotation.ForwardedFieldsSecond("f0->f1;f1->f2;f2->f3;f3->f4")
public class FirstJoinOfVertexAndEdgeInfo implements
  JoinFunction<Tuple3<GradoopId, String, String>, Tuple4<GradoopId, GradoopId, GradoopId, Double>,
    Tuple5<String, GradoopId, GradoopId, GradoopId, Double>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple5<String, GradoopId, GradoopId, GradoopId, Double> reuseTuple = new Tuple5<>();

  @Override
  public Tuple5<String, GradoopId, GradoopId, GradoopId, Double> join(
    Tuple3<GradoopId, String, String> vertexInfo,
    Tuple4<GradoopId, GradoopId, GradoopId, Double> edgeInfo) throws Exception {
    reuseTuple.f0 = vertexInfo.f1;
    reuseTuple.f1 = edgeInfo.f0;
    reuseTuple.f2 = edgeInfo.f1;
    reuseTuple.f3 = edgeInfo.f2;
    reuseTuple.f4 = edgeInfo.f3;
    return reuseTuple;
  }
}
