/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Reduces a group of {@code Tuple3<Vertex, VertexId, Identifier>} grouped by the VertexId and returns the
 * vertex depending on the number of group items and the Identifier.
 */
public class MinusVertexGroupReducer implements
  GroupReduceFunction<Tuple3<EPGMVertex, GradoopId, String>, EPGMVertex> {

  @Override
  public void reduce(Iterable<Tuple3<EPGMVertex, GradoopId, String>> group, Collector<EPGMVertex> out)
    throws Exception {
    int count = 0;
    EPGMVertex vertex = null;
    String type = "";

    for (Tuple3<EPGMVertex, GradoopId, String> groupItem : group) {
      vertex = groupItem.f0;
      type = groupItem.f2;
      count++;
    }

    if ((count == 1) && type.equals(Minus.FIRST) && (vertex != null)) {
      out.collect(vertex);
    }
  }
}
