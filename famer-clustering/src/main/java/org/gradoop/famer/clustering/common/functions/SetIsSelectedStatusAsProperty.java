/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Reduces a group of {@code Tuple3<Edge, EdgeId, IsSelectedCount>} grouped by the EdgeId and returns the Edge
 * with an isSelected property set depending on the aggregated isSelectedCount values from the group.
 */
public class SetIsSelectedStatusAsProperty implements
  GroupReduceFunction<Tuple3<EPGMEdge, GradoopId, Integer>, EPGMEdge> {

  @Override
  public void reduce(Iterable<Tuple3<EPGMEdge, GradoopId, Integer>> group, Collector<EPGMEdge> out)
    throws Exception {
    int isSelectedCount = 0;
    EPGMEdge edge = new EPGMEdge();

    for (Tuple3<EPGMEdge, GradoopId, Integer> groupItem : group) {
      edge = groupItem.f0;
      isSelectedCount += groupItem.f2;
    }

    if (isSelectedCount == 2) {
      edge.setProperty(PropertyNames.IS_SELECTED, 2);
    } else if (isSelectedCount == 1) {
      edge.setProperty(PropertyNames.IS_SELECTED, 1);
    } else {
      edge.setProperty(PropertyNames.IS_SELECTED, 0);
    }

    out.collect(edge);
  }
}
