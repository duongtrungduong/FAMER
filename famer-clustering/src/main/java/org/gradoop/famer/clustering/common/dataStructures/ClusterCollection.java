/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.dataStructures;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.common.functions.ClassifyEdges;
import org.gradoop.famer.clustering.common.functions.ClusterToClusterClusterId;
import org.gradoop.famer.clustering.common.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.famer.clustering.common.functions.EdgesToEdgeList;
import org.gradoop.famer.clustering.common.functions.JoinVerticesAndEdgesToCluster;
import org.gradoop.famer.clustering.common.functions.VertexToVertexClusterId;
import org.gradoop.famer.clustering.common.functions.VerticesToVertexList;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.util.List;

/**
 * Structure to hold a DataSet of {@link Cluster}s.
 */
public class ClusterCollection {

  /**
   * The DataSet of clusters
   */
  private DataSet<Cluster> clusters;

  /**
   * Creates an instance ClusterCollection
   *
   * @param clusters The DataSet of clusters
   */
  public ClusterCollection(DataSet<Cluster> clusters) {
    this.clusters = clusters;
  }

  /**
   * Creates an instance ClusterCollection
   *
   * @param clusteredLogicalGraph The logical graph to derive the clusters from
   */
  public ClusterCollection(LogicalGraph clusteredLogicalGraph) {
    clusters = fromLogicalGraph(clusteredLogicalGraph);
  }

  /**
   * Creates an instance ClusterCollection
   *
   * @param vertices The DataSet of vertices to derive the clusters from
   * @param edges The DataSet of edges to drive the clusters from
   */
  public ClusterCollection(DataSet<EPGMVertex> vertices, DataSet<EPGMEdge> edges) {
    clusters = fromLogicalGraph(vertices, edges);
  }

  /**
   * Returns the DataSet of clusters
   *
   * @return The clusters
   */
  public DataSet<Cluster> getClusters() {
    return clusters;
  }

  /**
   * Derives the clusters from a logical graph
   *
   * @param clusteredLogicalGraph The logical graph to derive the clusters from
   *
   * @return The DataSet of clusters
   */
  private DataSet<Cluster> fromLogicalGraph(LogicalGraph clusteredLogicalGraph) {
    return fromLogicalGraph(clusteredLogicalGraph.getVertices(), clusteredLogicalGraph.getEdges());
  }

  /**
   * Derives the clusters from a logical graph
   *
   * @param inputVertices The DataSet of vertices to derive the clusters from
   * @param inputEdges The DataSet of edges to drive the clusters from
   * @return The DataSet of clusters
   */
  private DataSet<Cluster> fromLogicalGraph(DataSet<EPGMVertex> inputVertices, DataSet<EPGMEdge> inputEdges) {
    // cluster edges
    DataSet<Tuple2<List<Tuple2<EPGMEdge, Boolean>>, String>> edges =
      new EdgeToEdgeSourceVertexTargetVertex(inputVertices, inputEdges).execute()
        .flatMap(new ClassifyEdges())
        .groupBy(1)
        .reduceGroup(new EdgesToEdgeList());

    // cluster vertices
    DataSet<Tuple2<List<EPGMVertex>, String>> vertices = inputVertices
      .flatMap(new VertexToVertexClusterId(PropertyNames.CLUSTER_ID, true))
      .groupBy(1)
      .reduceGroup(new VerticesToVertexList());

    // creating clusters
    return vertices.leftOuterJoin(edges)
      .where(1).equalTo(1)
      .with(new JoinVerticesAndEdgesToCluster());
  }

  /**
   * Compares this cluster collection to another cluster collection by comparing clusters with the same
   * cluster id and counting the differences between each two clusters.
   *
   * @param other The other cluster collection
   *
   * @return A {@code DataSet<Long>} with the aggregated difference count
   */
  public DataSet<Long> compareClusterCollections(ClusterCollection other) {

    DataSet<Tuple2<Cluster, String>> clusterClusterId = clusters.map(new ClusterToClusterClusterId());
    DataSet<Tuple2<Cluster, String>> otherClusterClusterId =
      other.clusters.map(new ClusterToClusterClusterId());

    DataSet<Long> output = otherClusterClusterId.union(clusterClusterId)
      .groupBy(1)
      .reduceGroup((GroupReduceFunction<Tuple2<Cluster, String>, Long>) (group, out) -> {
        Cluster cluster1 = null;
        Cluster cluster2 = null;
        int count = 0;
        for (Tuple2<Cluster, String> groupItem : group) {
          if (count == 0) {
            cluster1 = groupItem.f0;
          } else {
            cluster2 = groupItem.f0;
          }
          count++;
        }
        if (count == 1) {
          out.collect(1L);
          return;
        }
        if ((cluster1 != null) && (cluster2 != null)) {
          if (cluster1.isDifferent(cluster2)) {
            out.collect(1L);
          } else {
            out.collect(0L);
          }
        }
      }).reduce((ReduceFunction<Long>) Long::sum);

    return output;
  }
}
