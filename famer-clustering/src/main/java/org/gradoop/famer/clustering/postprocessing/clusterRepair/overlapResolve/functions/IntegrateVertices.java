/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.common.dataStructures.Cluster;

import java.util.ArrayList;
import java.util.List;

/**
 * Reduces a group of {@code Tuple3<Vertex, oldClusterId, newClusterId>}, grouped by oldClusterId, to a
 * {@code Tuple3<Cluster, oldClusterId, newClusterId>} with the Cluster holding all grouped vertices.
 */
public class IntegrateVertices implements
  GroupReduceFunction<Tuple3<EPGMVertex, String, String>, Tuple3<Cluster, String, String>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<Cluster, String, String> reuseTuple = new Tuple3<>();

  @Override
  public void reduce(Iterable<Tuple3<EPGMVertex, String, String>> group,
    Collector<Tuple3<Cluster, String, String>> out) throws Exception {
    List<EPGMVertex> vertices = new ArrayList<>();
    String oldClusterId = "";
    String newClusterId = "";

    for (Tuple3<EPGMVertex, String, String> groupItem : group) {
      oldClusterId = groupItem.f1;
      if (groupItem.f0 != null) { // resolve
        groupItem.f0.setProperty(PropertyNames.CLUSTER_ID, groupItem.f2);
        vertices.add(groupItem.f0);
      } else { // merge case TODO: groupItem.f0 is NEVER null in this algorithm, keep this if-else?
        newClusterId = groupItem.f2;
      }
    }
    reuseTuple.f0 = new Cluster(vertices);
    reuseTuple.f1 = oldClusterId;
    reuseTuple.f2 = newClusterId;
    out.collect(reuseTuple);
  }
}
