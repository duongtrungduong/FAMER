/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMElement;
import org.gradoop.common.model.impl.id.GradoopId;

/**
 * Minus operator
 *
 * @param <E> EPGMElement
 */
public class Minus<E extends EPGMElement> {

  /**
   * Identifier for items of the first input dataset
   */
  static final String FIRST = "first";

  /**
   * Identifier for items of the second input dataset
   */
  static final String SECOND = "second";

  /**
   * The generic group reduce function for elements of type {@link E}
   */
  private final GroupReduceFunction<Tuple3<E, GradoopId, String>, E> groupReducer;

  /**
   * Creates an instance of CLIPMinus
   *
   * @param groupReducer The generic group reduce function to use
   */
  public Minus(GroupReduceFunction<Tuple3<E, GradoopId, String>, E> groupReducer) {
    this.groupReducer = groupReducer;
  }

  /**
   * Executes the operator
   *
   * @param first The first dataset
   * @param second The second dataset
   *
   * @return The condensed result dataset
   */
  public DataSet<E> execute(DataSet<Tuple2<E, GradoopId>> first, DataSet<Tuple2<E, GradoopId>> second) {
    DataSet<Tuple3<E, GradoopId, String>> firstIdentTuples =
      first.map(new CreateIdentificationTuple<>(FIRST));
    DataSet<Tuple3<E, GradoopId, String>> secondIdentTuples =
      second.map(new CreateIdentificationTuple<>(SECOND));

    return firstIdentTuples.union(secondIdentTuples).groupBy(1).reduceGroup(groupReducer);
  }
}
