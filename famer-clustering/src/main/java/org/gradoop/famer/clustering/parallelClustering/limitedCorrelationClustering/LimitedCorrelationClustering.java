/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.limitedCorrelationClustering;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.graph.Graph;
import org.apache.flink.graph.spargel.ScatterGatherConfiguration;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMElement;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.common.GenerateOutput;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.functions.EdgeToGellyEdgeCommon;
import org.gradoop.famer.clustering.parallelClustering.common.functions.GellyVertexToVertexCommon;
import org.gradoop.famer.clustering.parallelClustering.common.functions.LongMaxAggregator;
import org.gradoop.famer.clustering.parallelClustering.common.functions.ModifyLogicalGraphForClustering;
import org.gradoop.famer.clustering.parallelClustering.limitedCorrelationClustering.functions.FilterCenterTypeVertices;
import org.gradoop.famer.clustering.parallelClustering.limitedCorrelationClustering.functions.LimitedCorrelationGatherFunction;
import org.gradoop.famer.clustering.parallelClustering.limitedCorrelationClustering.functions.LimitedCorrelationScatterFunction;
import org.gradoop.famer.clustering.parallelClustering.limitedCorrelationClustering.functions.VertexToGellyVertexForLimitedCorrelation;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.utils.LeftSide;

import static org.gradoop.famer.clustering.parallelClustering.limitedCorrelationClustering.functions.LimitedCorrelationGatherFunction.MAX_DEGREE_AGGREGATOR;

/**
 * The Gradoop/Flink implementation of Limited Correlation Clustering (Pivot). Centers are selected only
 * from the specified data source, declared in the vertex property with key
 * {@link org.gradoop.famer.clustering.common.PropertyNames#GRAPH_LABEL}.
 */
public class LimitedCorrelationClustering extends AbstractParallelClustering {

  /**
   * Value between 0.0 and 1.0 used in the computation for the probability for a vertex to get selected as
   * cluster center
   */
  private final double epsilon;

  /**
   * Which data source a center can be selected from
   */
  private final String centerType;

  /**
   * Whether edges are bidirectional
   */
  private final boolean isEdgesBiDirected;

  /**
   * The output type for the clustering result
   */
  private final ClusteringOutputType clusteringOutputType;

  /**
   * Number of maximum iterations for the used scatter gather iteration
   */
  private final int maxIteration;

  /**
   * Creates an instance of LimitedCorrelationClustering with {@link #epsilon} set to 0.9
   *
   * @param centerType Which data source a center can be selected from
   * @param isEdgesBiDirected Whether edges are bidirectional
   * @param clusteringOutputType The output type for the clustering result
   * @param maxIteration Number of maximum iterations for the used scatter gather iteration
   */
  public LimitedCorrelationClustering(String centerType, boolean isEdgesBiDirected,
    ClusteringOutputType clusteringOutputType, int maxIteration) {
    this(0.9, centerType, isEdgesBiDirected, clusteringOutputType, maxIteration);
  }

  /**
   * Creates an instance of LimitedCorrelationClustering
   *
   * @param epsilon Value between 0.0 and 1.0 used in the computation for the probability for a vertex to
   *                get selected as cluster center
   * @param centerType Which data source a center can be selected from
   * @param isEdgesBiDirected Whether edges are bidirectional
   * @param clusteringOutputType The output type for the clustering result
   * @param maxIteration Number of maximum iterations for the used scatter gather iteration
   */
  public LimitedCorrelationClustering(double epsilon, String centerType, boolean isEdgesBiDirected,
    ClusteringOutputType clusteringOutputType, int maxIteration) {
    this.epsilon = epsilon;
    this.centerType = centerType;
    this.isEdgesBiDirected = isEdgesBiDirected;
    this.clusteringOutputType = clusteringOutputType;
    this.maxIteration = maxIteration;
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The clustering algorithm json config part
   */
  public LimitedCorrelationClustering(JSONObject jsonConfig) {
    try {
      this.epsilon = jsonConfig.getDouble("epsilon");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Clustering: value for epsilon could not be found or parsed",
        ex);
    }
    try {
      this.centerType = jsonConfig.getString("centerType");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Clustering: value for centerType could not be found or " +
        "parsed", ex);
    }
    this.isEdgesBiDirected = getIsEdgesBiDirectedFromJsonConfig(jsonConfig);
    this.clusteringOutputType = getClusteringOutputTypeFromJsonConfig(jsonConfig);
    this.maxIteration = getMaxIterationFromJsonConfig(jsonConfig);
  }

  public double getEpsilon() {
    return epsilon;
  }

  public String getCenterType() {
    return centerType;
  }

  public boolean isEdgesBiDirected() {
    return isEdgesBiDirected;
  }

  public ClusteringOutputType getClusteringOutputType() {
    return clusteringOutputType;
  }

  public int getMaxIteration() {
    return maxIteration;
  }

  @Override
  protected LogicalGraph runClustering(LogicalGraph inputGraph) {
    inputGraph = inputGraph.callForGraph(new ModifyLogicalGraphForClustering());

    // TODO: check if algorithm is really intended this way:
    //  - only edges adjacent to center type vertices are kept
    //  - all vertices not adjacent to center type vertices will become their own cluster

    // get center type vertices
    DataSet<EPGMVertex> centerTypeVertices = inputGraph.getVertices()
      .filter(new FilterCenterTypeVertices(centerType));
    // get edges with center type vertex as source
    DataSet<EPGMEdge> edgesWithCenterTypeSource = inputGraph.getEdges().join(centerTypeVertices)
      .where(EPGMEdge::getSourceId).equalTo(EPGMElement::getId)
      .with(new LeftSide<>());
    // get edges with center type vertex as target
    DataSet<EPGMEdge> edgesWithCenterTypeTarget = inputGraph.getEdges().join(centerTypeVertices)
      .where(EPGMEdge::getTargetId).equalTo(EPGMElement::getId)
      .with(new LeftSide<>());
    // combine edges with center type source and target to get all center type adjacent edges
    DataSet<EPGMEdge> edgesForCenterTypeVertices =
      edgesWithCenterTypeSource.union(edgesWithCenterTypeTarget);
    // group on source and target and remove duplicate edges
    edgesForCenterTypeVertices = edgesForCenterTypeVertices.map(edge ->
      Tuple3.of(edge.getSourceId(), edge.getTargetId(), edge))
      .returns(new TypeHint<Tuple3<GradoopId, GradoopId, EPGMEdge>>() { })
      .groupBy(0, 1)
      .reduceGroup((GroupReduceFunction<Tuple3<GradoopId, GradoopId, EPGMEdge>, EPGMEdge>) (group, out) ->
        out.collect(group.iterator().next().f2))
      .returns(new TypeHint<EPGMEdge>() { });

    Graph<GradoopId, EPGMVertex, Double> gellyGraph = Graph.fromDataSet(
      inputGraph.getVertices().map(new VertexToGellyVertexForLimitedCorrelation(centerType)),
      edgesForCenterTypeVertices.flatMap(new EdgeToGellyEdgeCommon(isEdgesBiDirected)),
      inputGraph.getConfig().getExecutionEnvironment());

    ScatterGatherConfiguration parameters = new ScatterGatherConfiguration();
    LongMaxAggregator longMaxAggregator = new LongMaxAggregator();
    parameters.registerAggregator(MAX_DEGREE_AGGREGATOR, longMaxAggregator);
    parameters.setSolutionSetUnmanagedMemory(true);

    Graph<GradoopId, EPGMVertex, Double> resultGraph = gellyGraph.runScatterGatherIteration(
      new LimitedCorrelationScatterFunction(epsilon), new LimitedCorrelationGatherFunction(), maxIteration,
      parameters);

    LogicalGraph resultLogicalGraph = inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(
      inputGraph.getGraphHead(), resultGraph.getVertices().map(new GellyVertexToVertexCommon()),
      inputGraph.getEdges());

    return resultLogicalGraph.callForGraph(new GenerateOutput(clusteringOutputType));
  }

  @Override
  public String getName() {
    return LimitedCorrelationClustering.class.getName();
  }
}
