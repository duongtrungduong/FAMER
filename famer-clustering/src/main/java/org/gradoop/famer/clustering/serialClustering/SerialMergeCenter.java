/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.serialClustering;

import org.gradoop.famer.clustering.serialClustering.dataStructures.PrioritySelection;
import org.gradoop.famer.clustering.serialClustering.dataStructures.SerialEdgeComponent;
import org.gradoop.famer.clustering.serialClustering.dataStructures.SerialVertexComponent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The serial implementation of Merge Center algorithm
 */
public class SerialMergeCenter extends AbstractSerialClustering {

  /**
   * Which vertex to select as center based on its vertex priority
   */
  private final PrioritySelection prioritySelection;

  /**
   * Threshold an edge component degree must reach or exceed
   */
  private final double threshold;

  /**
   * List for the {@link SerialEdgeComponent} used in this algorithm
   */
  private List<SerialEdgeComponent> serialEdgeComponentList;

  /**
   * Creates an instance of SerialMergeCenter
   *
   * @param prioritySelection Which vertex to select as center based on its vertex priority
   * @param threshold Threshold an edge component degree must reach or exceed
   * @param verticesInputPath File path to read the vertices from
   * @param edgesInputPath File path to read the edges from
   * @param outputDir Directory for writing the clustering results
   */
  SerialMergeCenter(PrioritySelection prioritySelection, double threshold, String verticesInputPath,
    String edgesInputPath, String outputDir) {
    super(verticesInputPath, edgesInputPath, outputDir);
    this.prioritySelection = prioritySelection;
    this.threshold = threshold;
    this.serialEdgeComponentList = new ArrayList<>();
  }

  @Override
  protected void initializeSerialGraphComponents() throws IOException {
    getLinesFromFilePath(verticesInputPath).forEach(line -> {
      String[] lineData = line.split(",");
      SerialVertexComponent svc =
        new SerialVertexComponent(Long.parseLong(lineData[1]), Long.parseLong(lineData[1]), false);
      serialVertexComponentMap.put(lineData[0], svc);
    });
    getLinesFromFilePath(edgesInputPath).forEach(line -> {
      String[] lineData = line.split(",");
      SerialEdgeComponent sec =
        new SerialEdgeComponent(lineData[0], lineData[1], Double.parseDouble(lineData[2]));
      serialEdgeComponentList.add(sec);
    });
  }

  @Override
  protected void writeClusteringResultsToFile() throws IOException {
    List<String> clusteringResults = serialVertexComponentMap.entrySet().stream()
      .map(entry -> entry.getKey() + "," + entry.getValue().getClusterId())
      .collect(Collectors.toList());

    writeLinesToFilePath(outputPath, clusteringResults);
  }

  @Override
  protected void doSerialClustering() {
    SerialEdgeComponent[] secArray =
      serialEdgeComponentList.toArray(new SerialEdgeComponent[serialEdgeComponentList.size()]);

    Arrays.sort(secArray, (in1, in2) -> Double.compare(in2.getDegree(), in1.getDegree()));

    for (int i = 0; i < secArray.length; i++) {
      SerialEdgeComponent sec = secArray[i];
      SerialVertexComponent source = serialVertexComponentMap.get(sec.getSourceId());
      SerialVertexComponent target = serialVertexComponentMap.get(sec.getTargetId());
      long sourceVertexPrio = source.getVertexPrio();
      long targetVertexPrio = target.getVertexPrio();
      if (!source.getIsAssigned() && !target.getIsAssigned()) {
        if (prioritySelection == PrioritySelection.MIN) {
          if (sourceVertexPrio < targetVertexPrio) {
            source.setIsCenter(true);
          } else {
            target.setIsCenter(true);
          }
        } else if (prioritySelection == PrioritySelection.MAX) {
          if (sourceVertexPrio > targetVertexPrio) {
            source.setIsCenter(true);
          } else {
            target.setIsCenter(true);
          }
        }
        if (source.getIsCenter()) {
          source.setClusterId(sourceVertexPrio);
          target.setClusterId(sourceVertexPrio);
        } else if (target.getIsCenter()) {
          source.setClusterId(targetVertexPrio);
          target.setClusterId(targetVertexPrio);
        }
        source.setIsAssigned(true);
        target.setIsAssigned(true);
        serialVertexComponentMap.put(sec.getSourceId(), source);
        serialVertexComponentMap.put(sec.getTargetId(), target);
      } else if (source.getIsCenter() && target.getIsCenter()) {
        long clusterId = 0L;
        long modifyingClusterId = 0L;
        if (prioritySelection == PrioritySelection.MIN) {
          if (source.getClusterId() < target.getClusterId()) {
            clusterId = source.getClusterId();
            modifyingClusterId = target.getClusterId();
          } else if (source.getClusterId() > target.getClusterId()) {
            clusterId = target.getClusterId();
            modifyingClusterId = source.getClusterId();
          }
        } else if (prioritySelection == PrioritySelection.MAX) {
          if (source.getClusterId() > target.getClusterId()) {
            clusterId = source.getClusterId();
            modifyingClusterId = target.getClusterId();
          } else if (source.getClusterId() < target.getClusterId()) {
            clusterId = target.getClusterId();
            modifyingClusterId = source.getClusterId();
          }
        }
        if ((modifyingClusterId != clusterId) && (sec.getDegree() >= threshold)) {
          source.setClusterId(clusterId);
          target.setClusterId(clusterId);
          for (Map.Entry<String, SerialVertexComponent> entry : serialVertexComponentMap.entrySet()) {
            if (entry.getValue().getClusterId() == modifyingClusterId) {
              SerialVertexComponent svc = entry.getValue();
              svc.setClusterId(clusterId);
              entry.setValue(svc);
            }
          }
          serialVertexComponentMap.put(sec.getSourceId(), source);
          serialVertexComponentMap.put(sec.getTargetId(), target);
        }
      } else if ((target.getIsAssigned() && source.getIsCenter()) ||
        (source.getIsAssigned() && target.getIsCenter())) {
        long clusterId;
        long modifyingClusterId;
        if (target.getIsCenter()) {
          clusterId = target.getClusterId();
          modifyingClusterId = source.getClusterId();
        } else {
          clusterId = source.getClusterId();
          modifyingClusterId = target.getClusterId();
        }
        if ((modifyingClusterId != clusterId) && (sec.getDegree() >= threshold)) {
          source.setClusterId(clusterId);
          target.setClusterId(clusterId);
          for (Map.Entry<String, SerialVertexComponent> entry : serialVertexComponentMap.entrySet()) {
            if (entry.getValue().getClusterId() == modifyingClusterId) {
              SerialVertexComponent svc = entry.getValue();
              svc.setClusterId(clusterId);
              entry.setValue(svc);
            }
          }
          serialVertexComponentMap.put(sec.getSourceId(), source);
          serialVertexComponentMap.put(sec.getTargetId(), target);
        }
      } else if ((!target.getIsAssigned() && source.getIsCenter()) ||
        (!source.getIsAssigned() && target.getIsCenter())) {
        if (target.getIsCenter()) {
          source.setClusterId(target.getClusterId());
          source.setIsAssigned(true);
          serialVertexComponentMap.put(sec.getSourceId(), source);
        } else {
          target.setClusterId(source.getClusterId());
          target.setIsAssigned(true);
          serialVertexComponentMap.put(sec.getTargetId(), target);
        }
      }
    }
  }
}


