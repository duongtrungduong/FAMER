/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.serialClustering.dataStructures;

import java.util.ArrayList;
import java.util.List;

/**
 * Vertex structure for serial clustering algorithms
 */
public class SerialVertexComponent {
  /**
   * Id for the vertex component
   */
  private String id;
  /**
   * Priority for the vertex component
   */
  private long vertexPrio;
  /**
   * Cluster id for the vertex component
   */
  private long clusterId;
  /**
   * Defines if this vertex component is a cluster center
   */
  private boolean isCenter;
  /**
   * Defines if this vertex component is assigned to a cluster
   */
  private boolean isAssigned;
  /**
   * Degree for the vertex component
   */
  private int degree;
  /**
   * Similarity degree for the vertex component
   */
  private double simDegree;
  /**
   * Cluster ids the vertex component is assigned to, as comma separated String
   */
  private String clusterIds;
  /**
   * List of neighbors for the vertex component
   */
  private List<String> neighborList;

  /**
   * Creates an instance of SerialVertexComponent
   *
   * @param vertexPrio Priority for the vertex component
   * @param clusterId Cluster id for the vertex component
   * @param isCenter Defines if this vertex component is a cluster center
   */
  public SerialVertexComponent(long vertexPrio, long clusterId, boolean isCenter) {
    this.vertexPrio = vertexPrio;
    this.clusterId = clusterId;
    this.isCenter = isCenter;
    this.isAssigned = false;
    this.degree = 0;
    this.simDegree = 0.0;
    this.clusterIds = "";
    this.neighborList = new ArrayList<>();
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public long getVertexPrio() {
    return vertexPrio;
  }

  public void setVertexPrio(long vertexPrio) {
    this.vertexPrio = vertexPrio;
  }

  public long getClusterId() {
    return clusterId;
  }

  public void setClusterId(long clusterId) {
    this.clusterId = clusterId;
  }

  public boolean getIsCenter() {
    return isCenter;
  }

  public void setIsCenter(boolean isCenter) {
    this.isCenter = isCenter;
  }

  public boolean getIsAssigned() {
    return isAssigned;
  }

  public void setIsAssigned(boolean isAssigned) {
    this.isAssigned = isAssigned;
  }

  public int getDegree() {
    return degree;
  }

  public void setDegree(int degree) {
    this.degree = degree;
  }

  /**
   * Increments the degree by the given value
   *
   * @param degree The degree value to add
   */
  public void addToDegree(int degree) {
    this.degree += degree;
  }

  public double getSimDegree() {
    return simDegree;
  }

  public void setSimDegree(double simDegree) {
    this.simDegree = simDegree;
  }

  /**
   * Increments the similarity degree by the given value
   *
   * @param simDegree The similarity value to add
   */
  public void addToSimDegree(double simDegree) {
    this.simDegree += simDegree;
  }

  public String getClusterIds() {
    return clusterIds;
  }

  /**
   * Sets the cluster ids as new cluster ids or, of cluster ids are already defined, concatenates them to
   * the cluster ids
   *
   * @param clusterIds The new cluster ids
   */
  public void setClusterIds(String clusterIds) {
    if (this.clusterIds.equals("")) {
      this.clusterIds = clusterIds;
    } else {
      addClusterIds(clusterIds);
    }
  }

  /**
   * Concatenates new cluster ids to the cluster ids
   *
   * @param clusterIds The new cluster ids
   */
  private void addClusterIds(String clusterIds) {
    this.clusterIds += "," + clusterIds;
  }

  public List<String> getNeighborList() {
    return neighborList;
  }

  public void setNeighborList(List<String> neighborList) {
    this.neighborList = neighborList;
  }

  /**
   * Adds a new neighbor id to the list of neighbors
   *
   * @param neighbor The new neighbor id
   */
  public void addToNeighborList(String neighbor) {
    neighborList.add(neighbor);
  }
}
