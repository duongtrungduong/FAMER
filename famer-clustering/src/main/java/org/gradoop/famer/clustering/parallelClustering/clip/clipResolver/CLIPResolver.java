/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.clip.clipResolver;

import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.famer.clustering.parallelClustering.clip.clipResolver.functions.EdgeToEdgeInfoTuple;
import org.gradoop.famer.clustering.parallelClustering.clip.clipResolver.functions.RetrieveEdgeFromJoinedTuple;
import org.gradoop.famer.clustering.parallelClustering.clip.clipResolver.functions.FirstJoinOfVertexAndEdgeInfo;
import org.gradoop.famer.clustering.parallelClustering.clip.clipResolver.functions.SecondJoinOfVertexAndEdgeInfo;
import org.gradoop.famer.clustering.parallelClustering.clip.clipResolver.functions.CLIPResolveReducer;
import org.gradoop.famer.clustering.parallelClustering.clip.clipResolver.functions.VertexToVertexInfoTuple;
import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.ConnectedComponents;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.api.operators.UnaryGraphToGraphOperator;

/**
 * Resolves the CLIP algorithm with phase 3
 */
public class CLIPResolver implements UnaryGraphToGraphOperator {

  /**
   * Cluster id prefix for phase 3 used in connected components algorithm
   */
  private static final String PREFIX_PHASE3 = "ph3-";

  /**
   * The similarity value coefficient from the configuration object
   */
  private final double simValueCoef;

  /**
   * The strength coefficient from the configuration object
   */
  private final double strengthCoef;

  /**
   * Number of maximal iteration for the used connected components algorithm
   */
  private final int maxIteration;

  /**
   * Creates an instance of CLIPResolver
   *
   * @param simValueCoef The similarity value coefficient from the configuration object
   * @param strengthCoef The strength coefficient from the configuration object
   * @param maxIteration Number of maximal iteration for the used connected components algorithm
   */
  public CLIPResolver(double simValueCoef, double strengthCoef, int maxIteration) {
    this.simValueCoef = simValueCoef;
    this.strengthCoef = strengthCoef;
    this.maxIteration = maxIteration;
  }

  @Override
  public LogicalGraph execute(LogicalGraph inputGraph) {
    inputGraph = inputGraph.callForGraph(new ConnectedComponents(maxIteration));

    DataSet<Tuple3<GradoopId, String, String>> vertexIdTypeConCompId =
      inputGraph.getVertices().map(new VertexToVertexInfoTuple());
    DataSet<Tuple4<GradoopId, GradoopId, GradoopId, Double>> edgeIdSourceIdTargetIdPrioValue =
      inputGraph.getEdges().map(new EdgeToEdgeInfoTuple(simValueCoef, strengthCoef));

    DataSet<Tuple1<GradoopId>> edgeIds = vertexIdTypeConCompId.join(edgeIdSourceIdTargetIdPrioValue)
      .where(0).equalTo(1)
      .with(new FirstJoinOfVertexAndEdgeInfo()).join(vertexIdTypeConCompId)
      .where(3).equalTo(0)
      .with(new SecondJoinOfVertexAndEdgeInfo())
      .groupBy(2)
      .sortGroup(6, Order.DESCENDING)
      .reduceGroup(new CLIPResolveReducer<>());

    DataSet<Tuple2<EPGMEdge, GradoopId>> edgeEdgeId = inputGraph.getEdges()
      .map(edge -> Tuple2.of(edge, edge.getId()))
      .returns(new TypeHint<Tuple2<EPGMEdge, GradoopId>>() { });

    DataSet<EPGMEdge> edges = edgeEdgeId.join(edgeIds)
      .where(1).equalTo(0)
      .with(new RetrieveEdgeFromJoinedTuple());

    LogicalGraph resultGraph = inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(
      inputGraph.getGraphHead(), inputGraph.getVertices(), edges);

    return resultGraph.callForGraph(new ConnectedComponents(maxIteration, PREFIX_PHASE3));
  }

  @Override
  public String getName() {
    return CLIPResolver.class.getName();
  }
}
