/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.serialClustering;

import org.gradoop.famer.clustering.serialClustering.dataStructures.SerialVertexComponent;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The serial implementation of Correlation Clustering algorithm
 */
public class SerialCorrelationClustering extends AbstractSerialClustering {

  /**
   * Defines if edges are bidirectional
   */
  private final boolean isEdgesBiDirected;

  /**
   * Maps a vertex priority to a {@link SerialVertexComponent}
   */
  private HashMap<Long, SerialVertexComponent> serialVPVertexComponentMap;

  /**
   * The maximum vertex priority
   */
  private long maxVP;

  /**
   * The minimum vertex priority
   */
  private long minVP;

  /**
   * Creates an instance of SerialCorrelationClustering
   *
   * @param isEdgesBiDirected Defines if edges are bidirectional
   * @param verticesInputPath File path to read the vertices from
   * @param edgesInputPath File path to read the edges from
   * @param outputDir Directory for writing the clustering results
   */
  SerialCorrelationClustering(boolean isEdgesBiDirected, String verticesInputPath, String edgesInputPath,
    String outputDir) {
    super(verticesInputPath, edgesInputPath, outputDir);
    this.isEdgesBiDirected = isEdgesBiDirected;
    this.serialVPVertexComponentMap = new HashMap<>();
    this.maxVP = Long.MIN_VALUE;
    this.minVP = Long.MAX_VALUE;
  }

  @Override
  public void initializeSerialGraphComponents() throws IOException {
    getLinesFromFilePath(verticesInputPath).forEach(line -> {
      String[] lineData = line.split(",");
      String id = lineData[0];
      long vp = Long.parseLong(lineData[1]);
      SerialVertexComponent svc = new SerialVertexComponent(vp, 0L, false);
      svc.setId(id);
      serialVertexComponentMap.put(id, svc);
      serialVPVertexComponentMap.put(vp, svc);
      if (vp < minVP) {
        minVP = vp;
      }
      if (vp > maxVP) {
        maxVP = vp;
      }
    });
    getLinesFromFilePath(edgesInputPath).forEach(line -> {
      String[] lineData = line.split(",");
      String sourceId = lineData[0];
      String targetId = lineData[1];
      SerialVertexComponent svc = serialVertexComponentMap.get(sourceId);
      svc.addToNeighborList(targetId);
      serialVertexComponentMap.put(sourceId, svc);
      if (!isEdgesBiDirected) {
        svc = serialVertexComponentMap.get(targetId);
        svc.addToNeighborList(sourceId);
        serialVertexComponentMap.put(targetId, svc);
      }
    });
  }

  @Override
  public void writeClusteringResultsToFile() throws IOException {
    List<String> clusteringResults = serialVertexComponentMap.entrySet().stream()
      .map(entry -> entry.getKey() + "," + entry.getValue().getClusterId())
      .collect(Collectors.toList());

    writeLinesToFilePath(outputPath, clusteringResults);
  }

  @Override
  public void doSerialClustering() {
    long interval = maxVP - minVP;
    long remaining = maxVP - minVP;
    while (remaining > 0) {
      boolean randGenerator = true;
      long randomNum = 0L;
      while (randGenerator) {
        randomNum = (long) Math.floor(Math.random() * interval + minVP);
        randGenerator =
          serialVertexComponentMap.get(serialVPVertexComponentMap.get(randomNum).getId()).getIsAssigned();
      }
      String centerId = serialVPVertexComponentMap.get(randomNum).getId();
      long centerClusterId = randomNum;
      SerialVertexComponent svc = serialVertexComponentMap.get(centerId);
      svc.setIsAssigned(true);
      svc.setClusterId(centerClusterId);
      serialVertexComponentMap.put(centerId, svc);
      remaining--;
      List<String> neighborList = serialVertexComponentMap.get(centerId).getNeighborList();
      for (String neighbor : neighborList) {
        if (!serialVertexComponentMap.get(neighbor).getIsAssigned()) {
          SerialVertexComponent neighborSvc = serialVertexComponentMap.get(neighbor);
          neighborSvc.setIsAssigned(true);
          neighborSvc.setClusterId(centerClusterId);
          serialVertexComponentMap.put(neighbor, neighborSvc);
          remaining--;
        }
      }
    }
  }
}


