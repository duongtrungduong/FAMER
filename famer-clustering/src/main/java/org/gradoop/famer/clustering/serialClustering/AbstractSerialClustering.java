/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.serialClustering;

import org.gradoop.famer.clustering.serialClustering.dataStructures.SerialVertexComponent;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

/**
 * The abstract class all serial clustering algorithms inheriting from.
 */
public abstract class AbstractSerialClustering implements SerialClustering {

  /**
   * File path to read the vertices from
   */
  protected final String verticesInputPath;

  /**
   * File path to read the edges from
   */
  protected final String edgesInputPath;

  /**
   * File path for writing the clustering results
   */
  protected final String outputPath;

  /**
   * Maps a vertex id to its {@link SerialVertexComponent}
   */
  protected HashMap<String, SerialVertexComponent> serialVertexComponentMap;

  /**
   * Creates an instance of AbstractSerialClustering
   *
   * @param verticesInputPath File path to read the vertices from
   * @param edgesInputPath File path to read the edges from
   * @param outputPath File path for writing the clustering results
   */
  AbstractSerialClustering(String verticesInputPath, String edgesInputPath, String outputPath) {
    this.verticesInputPath = verticesInputPath;
    this.edgesInputPath = edgesInputPath;
    this.outputPath = outputPath;
    this.serialVertexComponentMap = new HashMap<>();
  }

  /**
   * Reads the vertices and edges from files and initializes the graph components. Needs to be
   * implemented by inheriting classes.
   *
   * @throws IOException Exception thrown on errors while reading a file
   */
  protected abstract void initializeSerialGraphComponents() throws IOException;

  /**
   * Executes the serial clustering algorithm. Needs to be implemented by inheriting classes.
   */
  protected abstract void doSerialClustering();

  /**
   * Writes the clustering result. Needs to be implemented by inheriting classes.
   *
   * @throws IOException Exception thrown on errors while writing a file
   */
  protected abstract void writeClusteringResultsToFile() throws IOException;

  /**
   * Reads a file from the given path and returns the lines as {@link Stream<String>}
   *
   * @param filePath Path to file
   * @return Lines as {@link Stream<String>}
   *
   * @throws IOException Exception thrown on read errors
   */
  Stream<String> getLinesFromFilePath(String filePath) throws IOException {
    return Files.lines(Paths.get(filePath), StandardCharsets.UTF_8);
  }

  /**
   * Writes the lines to the given file path. If the file doesn't exist, creates and writes to it. If the
   * file exists, deletes the content and writes to it.
   *
   * @param filePath Path to file
   * @param lines Lines to write
   *
   * @throws IOException Exception thrown on write errors
   *
   * Note: suppressed findbugs warning for ignored result of {@code mkdirs()}
   */
  void writeLinesToFilePath(String filePath, List<String> lines) throws IOException {
    File path = new File(filePath);
    if (!path.exists()) {
      path.getParentFile().mkdirs();
    }
    Files.write(Paths.get(filePath), lines, StandardCharsets.UTF_8);
  }

  @Override
  public void execute() throws IOException {
    initializeSerialGraphComponents();
    doSerialClustering();
    writeClusteringResultsToFile();
  }
}
