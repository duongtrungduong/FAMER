/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * JoinFunction to replace an edge source id with its corresponding source vertex on a
 * {@code Tuple3<Edge, SourceId, TargetId>}
 */
@FunctionAnnotation.ForwardedFieldsFirst("f0;f2")
public class ReplaceSourceIdWithSourceVertex implements
  JoinFunction<Tuple3<EPGMEdge, GradoopId, GradoopId>, EPGMVertex, Tuple3<EPGMEdge, EPGMVertex, GradoopId>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<EPGMEdge, EPGMVertex, GradoopId> reuseTuple = new Tuple3<>();

  @Override
  public Tuple3<EPGMEdge, EPGMVertex, GradoopId> join(
    Tuple3<EPGMEdge, GradoopId, GradoopId> edgeSourceIdTargetId,
    EPGMVertex sourceVertex) throws Exception {
    reuseTuple.f0 = edgeSourceIdTargetId.f0;
    reuseTuple.f1 = sourceVertex;
    reuseTuple.f2 = edgeSourceIdTargetId.f2;
    return reuseTuple;
  }
}
