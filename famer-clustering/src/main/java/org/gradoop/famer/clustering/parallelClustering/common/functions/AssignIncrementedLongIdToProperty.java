/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Assigns a given long value as property with the given {@link #propertyKey} to a vertex. Increments this
 * long id by 1 to assure that the property value is always greater than 0.
 */
public class AssignIncrementedLongIdToProperty implements MapFunction<Tuple2<Long, EPGMVertex>, EPGMVertex> {

  /**
   * The property key
   */
  private final String propertyKey;

  /**
   * Creates an instance of AssignIncrementedLongIdToProperty
   *
   * @param propertyKey the property key
   */
  public AssignIncrementedLongIdToProperty(String propertyKey) {
    this.propertyKey = propertyKey;
  }

  @Override
  public EPGMVertex map(Tuple2<Long, EPGMVertex> longIdVertexTuple) throws Exception {
    longIdVertexTuple.f1.setProperty(propertyKey, longIdVertexTuple.f0 + 1L);
    return longIdVertexTuple.f1;
  }
}
