/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.common.dataStructures.Cluster;

import java.util.Arrays;

/**
 * Performs a left outer join for a {@code Tuple2<Cluster, clusterId>} (left) and a
 * {@code Tuple3<Cluster, oldClusterId, newClusterId>} (right), where the left clusterId matches the right
 * oldClusterId. Returns the unchanged left cluster if no join partner was found. Returns the updated left
 * cluster, otherwise.
 */
public class UpdateJoin implements
  JoinFunction<Tuple2<Cluster, String>, Tuple3<Cluster, String, String>, Cluster> {

  @Override
  public Cluster join(Tuple2<Cluster, String> clusterClusterId,
    Tuple3<Cluster, String, String> clusterOldClusterIdNewClusterId) throws Exception {
    Cluster cluster = clusterClusterId.f0;
    if (clusterOldClusterIdNewClusterId == null) { // no join partner
      return cluster;
    } else {
      Cluster newVertices = clusterOldClusterIdNewClusterId.f0;
      String newClusterId = clusterOldClusterIdNewClusterId.f2;
      String clusterId = clusterClusterId.f1;

      if (clusterId.equals("")) { // resolve case
        for (EPGMVertex v : newVertices.getVertices()) {
          String newVertexClusterId = v.getPropertyValue(PropertyNames.CLUSTER_ID).toString();
          cluster.removeFromVertices(v.getId());
          if (newVertexClusterId.equals(clusterId) ||
            (newVertexClusterId.contains(",") &&
              Arrays.asList(newVertexClusterId.split(",")).contains(clusterId))) {
            cluster.addToVertices(v);
          }
//          else if (newVertexClusterId.contains("s")) {
//            List<Vertex> vertices = new ArrayList<>();
//            vertices.add(v);
//            cluster = new Cluster(vertices, newVertexClusterId);
//          }
        }
      } else { // merge case
        for (EPGMVertex vertex : cluster.getVertices()) {
          vertex.setProperty(PropertyNames.CLUSTER_ID, newClusterId);
        }
        cluster.setClusterId(newClusterId);
      }
      return cluster;
    }
  }
}
