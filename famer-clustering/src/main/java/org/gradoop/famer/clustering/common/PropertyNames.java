/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common;

/**
 * All common property names used for clustering and cluster postprocessing
 */
public class PropertyNames {

  /**
   * Property key to access the graphLabel property
   */
  public static final String GRAPH_LABEL = "graphLabel";

  /**
   * Property key to access the cluster id property
   */
  public static final String CLUSTER_ID = "clusterId";

  /**
   * Property key to access the similarity value property
   */
  public static final String SIM_VALUE = "value";

  /**
   * Property key to access the vertex priority property
   */
  public static final String VERTEX_PRIORITY = "vertexPriority";

  /**
   * Property key to access the degree property
   */
  public static final String DEGREE = "degree";

  /**
   * Property key to access the isCenter property
   */
  public static final String IS_CENTER = "isCenter";

  /**
   * Property key to access the isNonCenter property
   */
  public static final String IS_NON_CENTER = "isNonCenter";

  /**
   * Property key to access the deciding property
   */
  public static final String DECIDING = "deciding";

  /**
   * Property key to access the state property
   */
  public static final String STATE = "state";

  /**
   * Property key to access the centerDegree property
   */
  public static final String CENTER_DEGREE = "centerDegree";

  /**
   * Property key to access the roundNo property
   */
  public static final String ROUND_NO = "roundNo";

  /**
   * Property key to access the isSelected property
   */
  public static final String IS_SELECTED = "isSelected";

  /**
   * Property key to access the centerSide property
   */
  public static final String CENTER_SIDE = "centerSide";

  /**
   * Property key to access the componentId property
   */
  public static final String COMPONENT_ID = "componentId";
}
