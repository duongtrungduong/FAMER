/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.mergeCenter;

import org.apache.flink.api.common.aggregators.LongSumAggregator;
import org.apache.flink.graph.Graph;
import org.apache.flink.graph.spargel.ScatterGatherConfiguration;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.common.GenerateOutput;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.gradoop.famer.clustering.parallelClustering.common.functions.EdgeToGellyEdgeCommon;
import org.gradoop.famer.clustering.parallelClustering.common.functions.GellyVertexToVertexCommon;
import org.gradoop.famer.clustering.parallelClustering.common.functions.ModifyLogicalGraphForClustering;
import org.gradoop.famer.clustering.parallelClustering.mergeCenter.functions.MergeCenterGatherFunction;
import org.gradoop.famer.clustering.parallelClustering.mergeCenter.functions.MergeCenterScatterFunction;
import org.gradoop.famer.clustering.parallelClustering.mergeCenter.functions.VertexToGellyVertexForMergeCenter;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import static org.gradoop.famer.clustering.parallelClustering.mergeCenter.functions.MergeCenterGatherFunction.HAS_FINISHED_AGGREGATOR;
import static org.gradoop.famer.clustering.parallelClustering.mergeCenter.functions.MergeCenterGatherFunction.PHASE_2_AGGREGATOR;
import static org.gradoop.famer.clustering.parallelClustering.mergeCenter.functions.MergeCenterGatherFunction.PHASE_3_AGGREGATOR;

/**
 * The Gradoop/Flink implementation of Merge Center algorithm
 */
public class MergeCenter extends AbstractParallelClustering {

  /**
   * Whether edges are bidirectional
   */
  private final boolean isEdgesBiDirected;

  /**
   * The output type of the clustering result
   */
  private final ClusteringOutputType clusteringOutputType;

  /**
   * Which vertex to select as center based on its vertex priority, see {@link PrioritySelection}
   */
  private final PrioritySelection prioritySelection;

  /**
   * Threshold for the similarity degree which decides if clusters are merged
   */
  private final double simDegMergeThreshold;

  /**
   * Number of maximum iterations for the used scatter gather iteration
   */
  private final int maxIteration;

  /**
   * Creates an instance of MergeCenter
   *
   * @param prioritySelection Which vertex to select as center based on its vertex priority
   * @param simDegMergeThreshold Threshold for the similarity degree which decides if clusters are merged
   * @param isEdgesBiDirected Whether edges are bidirectional
   * @param clusteringOutputType The output type of the clustering result
   * @param maxIteration Number of maximum iterations for the used scatter gather iteration
   */
  public MergeCenter(PrioritySelection prioritySelection, double simDegMergeThreshold,
    boolean isEdgesBiDirected, ClusteringOutputType clusteringOutputType, int maxIteration) {
    this.prioritySelection = prioritySelection;
    this.simDegMergeThreshold = simDegMergeThreshold;
    this.isEdgesBiDirected = isEdgesBiDirected;
    this.clusteringOutputType = clusteringOutputType;
    this.maxIteration = maxIteration;
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The clustering algorithm json config part
   */
  public MergeCenter(JSONObject jsonConfig) {
    this.prioritySelection = getPrioritySelectionFromJsonConfig(jsonConfig);
    try {
      this.simDegMergeThreshold = jsonConfig.getDouble("simDegMergeThreshold");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Clustering: value for simDegMergeThreshold could not be " +
        "found or parsed", ex);
    }
    this.isEdgesBiDirected = getIsEdgesBiDirectedFromJsonConfig(jsonConfig);
    this.clusteringOutputType = getClusteringOutputTypeFromJsonConfig(jsonConfig);
    this.maxIteration = getMaxIterationFromJsonConfig(jsonConfig);
  }

  public boolean isEdgesBiDirected() {
    return isEdgesBiDirected;
  }

  public ClusteringOutputType getClusteringOutputType() {
    return clusteringOutputType;
  }

  public PrioritySelection getPrioritySelection() {
    return prioritySelection;
  }

  public double getSimDegMergeThreshold() {
    return simDegMergeThreshold;
  }

  public int getMaxIteration() {
    return maxIteration;
  }

  @Override
  protected LogicalGraph runClustering(LogicalGraph inputGraph) {
    inputGraph = inputGraph.callForGraph(new ModifyLogicalGraphForClustering());
    Graph<GradoopId, EPGMVertex, Double> gellyGraph = Graph.fromDataSet(
      inputGraph.getVertices().map(new VertexToGellyVertexForMergeCenter()),
      inputGraph.getEdges().flatMap(new EdgeToGellyEdgeCommon(isEdgesBiDirected)),
      inputGraph.getConfig().getExecutionEnvironment());

    ScatterGatherConfiguration parameters = new ScatterGatherConfiguration();
    LongSumAggregator longSumAggregator = new LongSumAggregator();
    parameters.registerAggregator(HAS_FINISHED_AGGREGATOR, longSumAggregator);
    LongSumAggregator longSumAggregator2 = new LongSumAggregator();
    parameters.registerAggregator(PHASE_2_AGGREGATOR, longSumAggregator2);
    LongSumAggregator longSumAggregator3 = new LongSumAggregator();
    parameters.registerAggregator(PHASE_3_AGGREGATOR, longSumAggregator3);
    parameters.setSolutionSetUnmanagedMemory(true);

    Graph<GradoopId, EPGMVertex, Double>  resultGraph = gellyGraph.runScatterGatherIteration(
      new MergeCenterScatterFunction(),
      new MergeCenterGatherFunction(prioritySelection, simDegMergeThreshold),
      maxIteration, parameters);

    LogicalGraph resultLogicalGraph = inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(
      inputGraph.getGraphHead(), resultGraph.getVertices().map(new GellyVertexToVertexCommon()),
      inputGraph.getEdges());

    return resultLogicalGraph.callForGraph(new GenerateOutput(clusteringOutputType));
  }

  @Override
  public String getName() {
    return MergeCenter.class.getName();
  }
}




