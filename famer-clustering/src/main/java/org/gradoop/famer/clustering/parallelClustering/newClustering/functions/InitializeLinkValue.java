/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.newClustering.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.newClustering.dataStructures.LinkValue;

import java.util.ArrayList;
import java.util.List;

/**
 * GroupReduce function for new clustering algorithm
 * TODO: was implemented without any description or naming - what does it do, is it fully implemented?
 */
public class InitializeLinkValue implements GroupReduceFunction<Tuple3<EPGMEdge, GradoopId, String>,
    Tuple5<GradoopId, EPGMEdge, GradoopId, String, LinkValue>> {
  /**
   * Reduce object instantiation
   */
  private final Tuple5<GradoopId, EPGMEdge, GradoopId, String, LinkValue> reuseTuple = new Tuple5<>();

  @Override
  public void reduce(Iterable<Tuple3<EPGMEdge, GradoopId, String>> group,
    Collector<Tuple5<GradoopId, EPGMEdge, GradoopId, String, LinkValue>> out) throws Exception {
    List<Tuple4<EPGMEdge, Double, GradoopId, String>> edgeSimValueEndIdOtherEndTypeList = new ArrayList<>();

    for (Tuple3<EPGMEdge, GradoopId, String> groupItem : group) {
      Tuple4<EPGMEdge, Double, GradoopId, String> edgeSimValueEndIdOtherEndTypeTuple = new Tuple4<>();
      edgeSimValueEndIdOtherEndTypeTuple.f0 = groupItem.f0;
      edgeSimValueEndIdOtherEndTypeTuple.f1 =
        Double.parseDouble(groupItem.f0.getPropertyValue(PropertyNames.SIM_VALUE).toString());
      edgeSimValueEndIdOtherEndTypeTuple.f2 = groupItem.f1;
      edgeSimValueEndIdOtherEndTypeTuple.f3 = groupItem.f2;
      edgeSimValueEndIdOtherEndTypeList.add(edgeSimValueEndIdOtherEndTypeTuple);
    }

    if (edgeSimValueEndIdOtherEndTypeList.size() == 1) {
      Tuple4<EPGMEdge, Double, GradoopId, String> in = edgeSimValueEndIdOtherEndTypeList.get(0);
      reuseTuple.f0 = in.f0.getId();
      reuseTuple.f1 = in.f0;
      reuseTuple.f2 = in.f2;
      reuseTuple.f3 = in.f3;
      reuseTuple.f4 = new LinkValue(true, 1);
      out.collect(reuseTuple);
    } else {
      edgeSimValueEndIdOtherEndTypeList.sort((s1, s2) -> s2.f1.compareTo(s1.f1));
      int i = 1;
      for (Tuple4<EPGMEdge, Double, GradoopId, String> in : edgeSimValueEndIdOtherEndTypeList) {
        reuseTuple.f0 = in.f0.getId();
        reuseTuple.f1 = in.f0;
        reuseTuple.f2 = in.f2;
        reuseTuple.f3 = in.f3;
        reuseTuple.f4 = new LinkValue(false, i);
        out.collect(reuseTuple);
        i++;
      }
    }
  }
}


