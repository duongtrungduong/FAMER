/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common.dataStructures;

import java.io.Serializable;

/**
 * The message structure used in center algorithm scatter gather functions
 */
public class CenterMessage implements Serializable {

  /**
   * Cluster id for a vertex
   */
  private long clusterId;

  /**
   * Similarity degree
   */
  private double simDegree;

  /**
   * Additional information stored for a vertex
   */
  private String additionalInfo;

  /**
   * Cluster ids a vertex is assigned to, as comma separated String
   */
  private String clusterIds;

  /**
   * Creates an instance of CenterMessage
   *
   * @param clusterId Cluster id for a vertex
   * @param simDegree Similarity degree for a vertex
   */
  public CenterMessage(long clusterId, double simDegree) {
    this.clusterId = clusterId;
    this.simDegree = simDegree;
    additionalInfo = "";
    clusterIds = "";
  }

  public long getClusterId() {
    return clusterId;
  }

  public void setClusterId(long clusterId) {
    this.clusterId = clusterId;
  }

  public double getSimDegree() {
    return simDegree;
  }

  public void setSimDegree(double simDegree) {
    this.simDegree = simDegree;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  public String getClusterIds() {
    return clusterIds;
  }

  public void setClusterIds(String clusterIds) {
    this.clusterIds = clusterIds;
  }

}
