/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.correlationClustering.functions;

import org.apache.flink.graph.spargel.GatherFunction;
import org.apache.flink.graph.spargel.MessageIterator;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.common.functions.LongMaxAggregator;

/**
 * Gather function used in the Correlation algorithm.
 */
public class CorrelationGatherFunction extends GatherFunction<GradoopId, EPGMVertex, Long> {

  /**
   * The aggregator name
   */
  public static final String MAX_DEGREE_AGGREGATOR = "maxDegree";

  /**
   * The aggregator for the maximum degree
   */
  private LongMaxAggregator maxDegreeAggregator = new LongMaxAggregator();

  @Override
  public void preSuperstep() {
    maxDegreeAggregator = getIterationAggregator(MAX_DEGREE_AGGREGATOR);
  }

  @Override
  public void updateVertex(org.apache.flink.graph.Vertex<GradoopId, EPGMVertex> vertex,
    MessageIterator<Long> messages) {
    switch (getSuperstepNumber() % 3) {
    case 1: // find max degree
      long aggregatedVertexDegree = 0L;
      if (vertex.f1.getPropertyValue(PropertyNames.CLUSTER_ID).getLong() == 0L) {
        for (long msg : messages) {
          aggregatedVertexDegree += msg;
        }
        maxDegreeAggregator.aggregate(aggregatedVertexDegree);
      }
      setNewVertexValue(vertex.f1);
      break;
    case 2: // select centers
      int msgCount = 0;
      boolean sameVertex = false;
      long vertexPriority =
        vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();
      for (long msg : messages) {
        if (msg != 0L) { // ignore fake messages
          if (msg == vertexPriority) {
            sameVertex = true;
          } else {
            msgCount++;
          }
        }
      }
      if ((msgCount == 0) && sameVertex) {
        vertex.f1.setProperty(PropertyNames.IS_CENTER, true);
      }
      setNewVertexValue(vertex.f1);
      break;
    case 0: // grow cluster around centers
      vertex.f1.setProperty(PropertyNames.ROUND_NO, getSuperstepNumber() / 3);
      if (vertex.f1.getPropertyValue(PropertyNames.CLUSTER_ID).getLong() == 0L) {
        // if vertex is unassigned
        long minClusterId = Long.MAX_VALUE;
        for (long msg : messages) {
          if ((msg != 0L) && (msg < minClusterId)) { // ignore fake messages with msg != 0L
            minClusterId = msg; // concurrency rule no. 2
          }
        }
        if (minClusterId != Long.MAX_VALUE) {
          vertex.f1.setProperty(PropertyNames.CLUSTER_ID, minClusterId);
        }
        setNewVertexValue(vertex.f1);
      }
      break;
    default:
      break;
    }
  }
}
