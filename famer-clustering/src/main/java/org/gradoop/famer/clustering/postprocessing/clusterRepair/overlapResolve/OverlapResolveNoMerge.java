/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve;

import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.common.dataStructures.Cluster;
import org.gradoop.famer.clustering.common.dataStructures.ClusterCollection;
import org.gradoop.famer.clustering.common.functions.ClusterToClusterClusterId;
import org.gradoop.famer.clustering.common.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.famer.clustering.common.functions.FindMaxSimilarityEdges;
import org.gradoop.famer.clustering.common.functions.Minus;
import org.gradoop.famer.clustering.common.functions.MinusVertexGroupReducer;
import org.gradoop.famer.clustering.common.functions.RemoveInterClusterEdges;
import org.gradoop.famer.clustering.common.functions.SetIsSelectedStatusAsProperty;
import org.gradoop.famer.clustering.common.functions.VertexToVertexClusterId;
import org.gradoop.famer.clustering.parallelClustering.clip.functions.FilterEdgesOnSelectedStatus;
import org.gradoop.famer.clustering.postprocessing.AbstractClusterPostprocessing;
import org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.dataStructures.ResolveIteration;
import org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions.ClusterToVertexVertexId;
import org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions.EdgeToEdgeEndIdOtherEndType;
import org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions.FilterOverlappingVertices;
import org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions.FormClusterSingletons;
import org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions.IntegrateVertices;
import org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions.JoinVertexWithClusterOnId;
import org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions.NoMergeResolver;
import org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions.UpdateClusterEdges;
import org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions.UpdateJoin;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.tuple.Value0Of2;

/**
 * Operator to resolve overlapping clusters
 */
public class OverlapResolveNoMerge extends AbstractClusterPostprocessing {

  /**
   * The allowed similarity value loss
   */
  private final double delta;

  /**
   * Whether to run the second resolving phase
   */
  private final boolean runPhase2;

  /**
   * Creates an instance of OverlapResolveNoMerge
   *
   * @param delta The allowed similarity value loss
   */
  public OverlapResolveNoMerge(double delta) {
    this(delta, false);
  }

  /**
   * Creates an instance of OverlapResolveNoMerge
   *
   * @param delta The allowed similarity value loss
   * @param runPhase2 Whether to run the second resolving phase
   */
  public OverlapResolveNoMerge(double delta, boolean runPhase2) {
    this.delta = delta;
    this.runPhase2 = runPhase2;
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The clustering algorithm json config part
   */
  public OverlapResolveNoMerge(JSONObject jsonConfig) {
    try {
      this.delta = jsonConfig.getDouble("delta");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("ClusterPostprocessing: value for delta could not be " +
        "found or parsed", ex);
    }
    Object runPhase2Elem = jsonConfig.opt("runPhase2");
    if (runPhase2Elem == null) {
      this.runPhase2 = false;
    } else {
      try {
        if (jsonConfig.getString("runPhase2").equals("true") ||
          jsonConfig.getString("runPhase2").equals("false")) {
          this.runPhase2 = jsonConfig.getBoolean("runPhase2");
        } else {
          throw new UnsupportedOperationException("ClusterPostprocessing: value for runPhase2 is not 'true'" +
            " or 'false'");
        }
      } catch (Exception ex) {
        throw new UnsupportedOperationException("ClusterPostprocessing: value for runPhase2 could not be " +
          "parsed", ex);
      }
    }
  }

  public double getDelta() {
    return delta;
  }

  public boolean isRunPhase2() {
    return runPhase2;
  }

  @Override
  protected LogicalGraph runPostprocessing(LogicalGraph clusteredGraph) {
    DataSet<EPGMVertex> vertices = clusteredGraph.getVertices();
    // add selected status as property to edge
    DataSet<EPGMEdge> allEdgesWithSelectedStatus =
      new EdgeToEdgeSourceVertexTargetVertex(clusteredGraph).execute()
      .flatMap(new EdgeToEdgeEndIdOtherEndType())
      .groupBy(1, 2)
      .reduceGroup(new FindMaxSimilarityEdges(delta))
      .groupBy(1)
      .reduceGroup(new SetIsSelectedStatusAsProperty());
    // get strong edges (with selected status > 1)
    DataSet<EPGMEdge> strongEdges = allEdgesWithSelectedStatus.filter(new FilterEdgesOnSelectedStatus(1));

    // convert to cluster collection
    ClusterCollection clusterCollection = new ClusterCollection(vertices, strongEdges);
    DataSet<Cluster> cluster = clusterCollection.getClusters();

    /* Phase 1: */
    // find vertices for overlapping cluster
    DataSet<Tuple2<Cluster, String>> clusterClusterId = cluster.map(new ClusterToClusterClusterId());
    // process overlapped vertices
    DataSet<Tuple3<EPGMVertex, String, String>> vertexOldClusterIdNewClusterId =
      vertices.flatMap(new FilterOverlappingVertices())
      .flatMap(new VertexToVertexClusterId(PropertyNames.CLUSTER_ID, true)).join(clusterClusterId)
      .where(1).equalTo(1)
      .with(new JoinVertexWithClusterOnId())
      .groupBy(0)
      .reduceGroup(new NoMergeResolver(ResolveIteration.ITERATION1));
    // update cluster
    DataSet<Tuple3<Cluster, String, String>> updated = vertexOldClusterIdNewClusterId.groupBy(1)
      .reduceGroup(new IntegrateVertices());
    cluster = clusterClusterId.leftOuterJoin(updated)
      .where(1).equalTo(1)
      .with(new UpdateJoin())
      .union(vertexOldClusterIdNewClusterId.flatMap(new FormClusterSingletons()));
    cluster = cluster.map(new ClusterToClusterClusterId())
      .groupBy(1)
      .reduceGroup(new UpdateClusterEdges());
    // update vertices
    vertices = cluster.flatMap(new ClusterToVertexVertexId()).distinct(1).map(new Value0Of2<>());

    if (runPhase2) {
      /* Phase 2: */
      // find vertices for overlapping cluster
      clusterClusterId = cluster.map(new ClusterToClusterClusterId());
      // process overlapped vertices
      vertexOldClusterIdNewClusterId = vertices.flatMap(new FilterOverlappingVertices())
        .flatMap(new VertexToVertexClusterId(PropertyNames.CLUSTER_ID, true)).join(clusterClusterId)
        .where(1).equalTo(1)
        .with(new JoinVertexWithClusterOnId())
        .groupBy(0)
        .reduceGroup(new NoMergeResolver(ResolveIteration.ITERATION2));
      // update vertices
      DataSet<Tuple2<EPGMVertex, GradoopId>> updatedVertices = vertexOldClusterIdNewClusterId
        .map(vertexCIdCId -> Tuple2.of(vertexCIdCId.f0, vertexCIdCId.f0.getId()))
        .returns(new TypeHint<Tuple2<EPGMVertex, GradoopId>>() { })
        .distinct(1);
      DataSet<Tuple2<EPGMVertex, GradoopId>> allVertices = vertices
        .map(vertex -> Tuple2.of(vertex, vertex.getId()))
        .returns(new TypeHint<Tuple2<EPGMVertex, GradoopId>>() { });
      vertices = new Minus<>(new MinusVertexGroupReducer()).execute(allVertices, updatedVertices);
    }

    // find vertices for overlapping cluster
    clusterClusterId = cluster.map(new ClusterToClusterClusterId());
    // process overlapped vertices
    vertexOldClusterIdNewClusterId = vertices.flatMap(new FilterOverlappingVertices())
      .flatMap(new VertexToVertexClusterId(PropertyNames.CLUSTER_ID, true)).join(clusterClusterId)
      .where(1).equalTo(1)
      .with(new JoinVertexWithClusterOnId()).groupBy(0)
      .reduceGroup(new NoMergeResolver(ResolveIteration.ITERATION2));
    // update cluster
    updated = vertexOldClusterIdNewClusterId.groupBy(1).reduceGroup(new IntegrateVertices());
    cluster = clusterClusterId.leftOuterJoin(updated)
      .where(1).equalTo(1)
      .with(new UpdateJoin())
      .union(vertexOldClusterIdNewClusterId.flatMap(new FormClusterSingletons()));
    cluster = cluster.map(new ClusterToClusterClusterId())
      .groupBy(1)
      .reduceGroup(new UpdateClusterEdges());
    // update vertices
    vertices = cluster.flatMap(new ClusterToVertexVertexId()).distinct(1).map(new Value0Of2<>());

    // build result graph
    clusteredGraph = clusteredGraph.getConfig().getLogicalGraphFactory().fromDataSets(vertices, strongEdges);
    DataSet<EPGMEdge> reducedEdges = new EdgeToEdgeSourceVertexTargetVertex(clusteredGraph).execute()
        .flatMap(new RemoveInterClusterEdges());
    return clusteredGraph.getConfig().getLogicalGraphFactory().fromDataSets(vertices, reducedEdges);
  }

  @Override
  public String getName() {
    return OverlapResolveNoMerge.class.getName();
  }
}
