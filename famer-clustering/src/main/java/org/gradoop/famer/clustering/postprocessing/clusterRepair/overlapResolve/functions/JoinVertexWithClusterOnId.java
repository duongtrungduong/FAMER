/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.dataStructures.Cluster;

/**
 * Joins a {@code Tuple2<Vertex, clusterId>} with a {@code Tuple2<Cluster, clusterId>} where the clusterId
 * matches and returns a {@code Tuple3<vertexId, Vertex, Cluster>} built from the matched tuples.
 */
@FunctionAnnotation.ReadFieldsFirst("f0")
@FunctionAnnotation.ForwardedFieldsSecond("f0->f2")
public class JoinVertexWithClusterOnId implements
  JoinFunction<Tuple2<EPGMVertex, String>, Tuple2<Cluster, String>, Tuple3<GradoopId, EPGMVertex, Cluster>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<GradoopId, EPGMVertex, Cluster> reuseTuple = new Tuple3<>();

  @Override
  public Tuple3<GradoopId, EPGMVertex, Cluster> join(Tuple2<EPGMVertex, String> vertexClusterId,
    Tuple2<Cluster, String> clusterClusterId) throws Exception {
    reuseTuple.f0 = vertexClusterId.f0.getId();
    reuseTuple.f1 = vertexClusterId.f0;
    reuseTuple.f2 = clusterClusterId.f0;
    return reuseTuple;
  }
}
