/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.clip.clipResolver.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.java.tuple.Tuple7;
import org.gradoop.common.model.impl.id.GradoopId;

/**
 * Joins a {@code Tuple5<Type1, EdgeId, SourceId, TargetId, PrioValue>} with a {@code Tuple3<VertexId,
 * Type2, ClusterId>} where {@code TargetId == VertexId} and returns a {@code Tuple7<Type1, Type2, ClusterId,
 * EdgeId, SourceId, TargetId, PrioValue>}.
 */
@FunctionAnnotation.ForwardedFieldsFirst("f0->f0;f1->f3;f2->f4;f3->f5;f4->f6")
@FunctionAnnotation.ForwardedFieldsSecond("f1;f2")
public class SecondJoinOfVertexAndEdgeInfo implements
  JoinFunction<Tuple5<String, GradoopId, GradoopId, GradoopId, Double>, Tuple3<GradoopId, String, String>,
    Tuple7<String, String, String, GradoopId, GradoopId, GradoopId, Double>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple7<String, String, String, GradoopId, GradoopId, GradoopId, Double> reuseTuple =
    new Tuple7<>();

  @Override
  public Tuple7<String, String, String, GradoopId, GradoopId, GradoopId, Double> join(
    Tuple5<String, GradoopId, GradoopId, GradoopId, Double> joinedInfo,
    Tuple3<GradoopId, String, String> vertexInfo) throws Exception {
    reuseTuple.f0 = joinedInfo.f0;
    reuseTuple.f1 = vertexInfo.f1;
    reuseTuple.f2 = vertexInfo.f2;
    reuseTuple.f3 = joinedInfo.f1;
    reuseTuple.f4 = joinedInfo.f2;
    reuseTuple.f5 = joinedInfo.f3;
    reuseTuple.f6 = joinedInfo.f4;
    return reuseTuple;
  }
}
