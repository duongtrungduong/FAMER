/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.limitedCorrelationClustering.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Maps a Gradoop vertex to a Gelly vertex for the scatter gather iteration used in the Limited Correlation
 * algorithm.
 */
public final class VertexToGellyVertexForLimitedCorrelation implements
  MapFunction<EPGMVertex, org.apache.flink.graph.Vertex<GradoopId, EPGMVertex>> {

  /**
   * Reduce object instantiation
   */
  private final org.apache.flink.graph.Vertex<GradoopId, EPGMVertex> reuseVertex;

  /**
   * Which data source a center is selected from
   */
  private final String centerType;

  /**
   * Creates an instance of VertexToGellyVertexForLimitedCorrelation
   *
   * @param centerType Which data source a center is selected from
   */
  public VertexToGellyVertexForLimitedCorrelation(String centerType) {
    this.reuseVertex = new org.apache.flink.graph.Vertex<>();
    this.centerType = centerType;
  }

  @Override
  public org.apache.flink.graph.Vertex<GradoopId, EPGMVertex> map(EPGMVertex vertex) throws Exception {
    vertex.setProperty(PropertyNames.CLUSTER_ID, 0L);
    vertex.setProperty(PropertyNames.DEGREE, 0L);
    vertex.setProperty(PropertyNames.IS_CENTER, false);
    vertex.setProperty(PropertyNames.ROUND_NO, 0);
    if (vertex.getPropertyValue(PropertyNames.GRAPH_LABEL).toString().equals(centerType)) {
      vertex.setProperty(PropertyNames.CENTER_SIDE, true);
    } else {
      vertex.setProperty(PropertyNames.CENTER_SIDE, false);
    }
    reuseVertex.setId(vertex.getId());
    reuseVertex.setValue(vertex);
    return reuseVertex;
  }
}
