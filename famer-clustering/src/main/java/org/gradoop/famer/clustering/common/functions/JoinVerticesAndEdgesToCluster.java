/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.common.dataStructures.Cluster;

import java.util.ArrayList;
import java.util.List;

/**
 * Creates a cluster for a joined {@code Tuple2<List<Vertex>, ClusterId>} (left) and a
 * {@code Tuple2<List<Tuple2<Edge,IsInterEdge>>, ClusterId>} (right) if the ClusterId matches. If there is
 * no right join partner the cluster is created with an empty list of edges.
 */
public class JoinVerticesAndEdgesToCluster implements
  JoinFunction<Tuple2<List<EPGMVertex>, String>, Tuple2<List<Tuple2<EPGMEdge, Boolean>>, String>, Cluster> {

  @Override
  public Cluster join(Tuple2<List<EPGMVertex>, String> vertexListClusterId,
    Tuple2<List<Tuple2<EPGMEdge, Boolean>>, String> edgeListClusterId) throws Exception {

    List<EPGMEdge> intraLinks = new ArrayList<>();
    List<EPGMEdge> interLinks = new ArrayList<>();
    List<EPGMVertex> vertices = new ArrayList<>();

    if (edgeListClusterId != null) {
      for (Tuple2<EPGMEdge, Boolean> edge : edgeListClusterId.f0) {
        if (edge.f1) {
          interLinks.add(edge.f0);
        } else {
          intraLinks.add(edge.f0);
        }
      }
    }

    String componentId = "";
    if (vertexListClusterId.f0.get(0).hasProperty(PropertyNames.COMPONENT_ID)) {
      componentId = vertexListClusterId.f0.get(0).getPropertyValue(PropertyNames.COMPONENT_ID).toString();
    }
    for (EPGMVertex vertex : vertexListClusterId.f0) {
      if (!vertices.contains(vertex)) {
        vertices.add(vertex);
      }
    }

    return new Cluster(vertices, intraLinks, interLinks, vertexListClusterId.f1, componentId);
  }
}
