/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * Maps each edge of a {@link LogicalGraph} to a {@code Tuple3<Edge, SourceVertex, TargetVertex>}.
 */
public class EdgeToEdgeSourceVertexTargetVertex {

  /**
   * Vertices of the graph
   */
  private final DataSet<EPGMVertex> vertices;

  /**
   * Edges of the graph
   */
  private final DataSet<EPGMEdge> edges;

  /**
   * Creates an instance of EdgeToEdgeSourceVertexTargetVertex
   *
   * @param graph Graph to get the edge tuples from
   */
  public EdgeToEdgeSourceVertexTargetVertex(LogicalGraph graph) {
    this(graph.getVertices(), graph.getEdges());
  }

  /**
   * Creates an instance of EdgeToEdgeSourceVertexTargetVertex
   *
   * @param vertices Vertices used to compute the edge tuples
   * @param edges Edges used to compute the edge tuples
   */
  public EdgeToEdgeSourceVertexTargetVertex(DataSet<EPGMVertex> vertices, DataSet<EPGMEdge> edges) {
    this.vertices = vertices;
    this.edges = edges;
  }

  /**
   * Executes the mapping from an {@link EPGMEdge} to a {@code Tuple3<Edge, SourceVertex, TargetVertex>}
   *
   * @return The {@code Tuple3<Edge, SourceVertex, TargetVertex>} dataset
   */
  public DataSet<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>> execute() {
    DataSet<Tuple3<EPGMEdge, GradoopId, GradoopId>> edgeSourceIdTargetId = edges
      .map(edge -> Tuple3.of(edge, edge.getSourceId(), edge.getTargetId()))
      .returns(new TypeHint<Tuple3<EPGMEdge, GradoopId, GradoopId>>() { });

    return edgeSourceIdTargetId.join(vertices)
      .where(1).equalTo(EPGMVertex::getId)
      .with(new ReplaceSourceIdWithSourceVertex()).join(vertices)
      .where(2).equalTo(EPGMVertex::getId)
      .with(new ReplaceTargetIdWithTargetVertex());
  }


}
