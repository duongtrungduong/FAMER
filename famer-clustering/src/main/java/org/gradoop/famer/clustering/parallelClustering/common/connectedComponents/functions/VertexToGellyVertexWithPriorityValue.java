/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Maps a Gradoop vertex to a Gelly vertex with the vertex id as key and the vertex priority as long value.
 */
public class VertexToGellyVertexWithPriorityValue implements
  MapFunction<EPGMVertex, org.apache.flink.graph.Vertex<GradoopId, Long>> {

  /**
   * Reduce object instantiation
   */
  private final org.apache.flink.graph.Vertex<GradoopId, Long> reuseVertex =
    new org.apache.flink.graph.Vertex<>();

  @Override
  public org.apache.flink.graph.Vertex<GradoopId, Long> map(EPGMVertex vertex) throws Exception {
    reuseVertex.setId(vertex.getId());
    reuseVertex.setValue(vertex.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong());
    return reuseVertex;
  }
}
