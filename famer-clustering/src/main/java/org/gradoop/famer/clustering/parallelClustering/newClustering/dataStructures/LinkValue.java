/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.newClustering.dataStructures;

import java.io.Serializable;

/**
 * Data structure for new clustering algorithm
 * TODO: was implemented without any description or naming - what does it do, is it fully implemented?
 */
public class LinkValue implements Serializable {
  /**
   * TODO
   */
  private boolean isPositive;
  /**
   * TODO
   */
  private int rank;

  /**
   * Creates an instance of LinkValue
   */
  public LinkValue() {
    this(false, -1);
  }

  /**
   * Creates an instance of LinkValue
   *
   * @param isPositive TODO
   * @param rank TODO
   */
  public LinkValue(boolean isPositive, int rank) {
    this.isPositive = isPositive;
    this.rank = rank;
  }

  public boolean isPositive() {
    return isPositive;
  }

  public void setIsPositive(boolean positive) {
    isPositive = positive;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }
}
