/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.clip.functions;

import org.apache.flink.api.common.functions.FilterFunction;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Filter function to keep all edges where the isSelected property value is greater than the given
 * {@link #selectedStatus}.
 */
public class FilterEdgesOnSelectedStatus implements FilterFunction<EPGMEdge> {

  /**
   * The selected status on which an edge is filtered
   */
  private final int selectedStatus;

  /**
   * Creates an instance of FilterEdgesOnSelectedStatus
   *
   * @param selectedStatus The selected status on which an edge is filtered
   */
  public FilterEdgesOnSelectedStatus(int selectedStatus) {
    this.selectedStatus = selectedStatus;
  }

  @Override
  public boolean filter(EPGMEdge edge) throws Exception {
    if (edge.hasProperty(PropertyNames.IS_SELECTED)) {
      return edge.getPropertyValue(PropertyNames.IS_SELECTED).getInt() > selectedStatus;
    }
    return false;
  }
}
