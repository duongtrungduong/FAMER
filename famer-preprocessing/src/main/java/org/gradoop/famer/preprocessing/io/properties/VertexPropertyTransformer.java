/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.properties;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.preprocessing.io.properties.functions.CombinePropertiesMap;
import org.gradoop.famer.preprocessing.io.properties.functions.RenamePropertiesMap;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.util.List;

import static org.gradoop.famer.preprocessing.io.properties.GraphPropertyChecker.GRAPH_LABEL_PROPERTY;
import static org.gradoop.famer.preprocessing.io.properties.functions.CombinePropertiesMap.COMBINE_BROADCAST_SET;
import static org.gradoop.famer.preprocessing.io.properties.functions.RenamePropertiesMap.RENAME_BROADCAST_SET;

/**
 * Class for transformations on vertex properties for {@link GraphCollection} and {@link LogicalGraph}
 */
public class VertexPropertyTransformer {

  /**
   * <pre>
   * For {@link GraphCollection}:
   * Combines an arbitrary number of properties of a vertex into a new property, if - and only if - the
   * vertex has all given properties. The properties to combine are passed via {@code propertyTuples} as a
   * list of {@code Tuple3<String, String, List<Tuple2<String, Boolean>>>}, where
   *   t.f0 = graphLabel for a graph in the collection or empty string for application on all graphs
   *   t.f1 = name for the new combined property
   *   t.f2 = list of {@code Tuple2<String, Boolean>} of vertex properties for combination, where
   *            t.f0 = property key
   *            t.f1 = whether to keep the original property (true), or not (false)
   *            The properties are combined in the order of their appearance in the list
   * </pre>
   *
   * @param graphs The {@link GraphCollection} where to combine the vertex properties
   * @param propertyTuples A list of {@code Tuple3<String, String, List<Tuple2<String, Boolean>>>} with the
   *                      information for property combination
   *
   * @return A {@link GraphCollection} with the combined vertex properties
   */
  public GraphCollection combineProperties(GraphCollection graphs,
    List<Tuple3<String, String, List<Tuple2<String, Boolean>>>>  propertyTuples) {
    DataSet<Tuple3<String, String, List<Tuple2<String, Boolean>>>> broadcastSet =
      graphs.getConfig().getExecutionEnvironment().fromCollection(propertyTuples);

    DataSet<EPGMVertex> vertices = graphs.getVertices()
      .map(new CombinePropertiesMap(GRAPH_LABEL_PROPERTY))
      .withBroadcastSet(broadcastSet, COMBINE_BROADCAST_SET);

    return graphs.getConfig().getGraphCollectionFactory()
      .fromDataSets(graphs.getGraphHeads(), vertices, graphs.getEdges());
  }

  /**
   * <pre>
   * For {@link LogicalGraph}:
   * Combines an arbitrary number of properties of a vertex into a new property, if - and only if - the
   * vertex has all given properties. The properties to combine are passed via {@code propertyTuples} as a
   * list of {@code Tuple3<String, String, List<Tuple2<String, Boolean>>>}, where
   *   t.f0 = graphLabel for the logical graph or empty string
   *   t.f1 = name for the new combined property
   *   t.f2 = list of {@code Tuple2<String, Boolean>} of vertex properties for combination, where
   *            t.f0 = property key
   *            t.f1 = whether to keep the original property (true), or not (false)
   *            The properties are combined in the order of their appearance in the list
   * </pre>
   *
   * @param graph The {@link LogicalGraph} where to combine the vertex properties
   * @param propertyTuples A list of {@code Tuple2<String, List<Tuple2<String, Boolean>>>} with the
   *                       information for property combination
   *
   * @return A {@link LogicalGraph} with the combined vertex properties
   */
  public LogicalGraph combineProperties(LogicalGraph graph,
    List<Tuple3<String, String, List<Tuple2<String, Boolean>>>>  propertyTuples) {
    DataSet<Tuple3<String, String, List<Tuple2<String, Boolean>>>> broadcastSet =
      graph.getConfig().getExecutionEnvironment().fromCollection(propertyTuples);

    DataSet<EPGMVertex> vertices = graph.getVertices()
      .map(new CombinePropertiesMap(GRAPH_LABEL_PROPERTY))
      .withBroadcastSet(broadcastSet, COMBINE_BROADCAST_SET);

    return graph.getConfig().getLogicalGraphFactory()
      .fromDataSets(graph.getGraphHead(), vertices, graph.getEdges());
  }

  /**
   * <pre>
   * For {@link GraphCollection}:
   * Renames an arbitrary number of properties of a vertex. The properties to rename are passed via
   * {@code propertyTuples} as a list of {@code Tuple4<String, String, String, Boolean>>>}, where
   *    t.f0 = graphLabel for a graph in the graph collection or empty string for application on all graphs
   *    t.f1 = key ot the property for renaming
   *    t.f2 = name for the renamed property
   *    t.f3 = whether to keep the original property (true), or not (false)
   * The properties are renamed in the order of their appearance in the list
   * </pre>
   *
   * @param graphs The {@link GraphCollection} where to rename the vertex properties
   * @param propertyTuples A list of {@code Tuple3<String, String, Boolean>>>} with the information for
   *                      property renaming
   *
   * @return A {@link GraphCollection} with the renamed vertex properties
   */
  public GraphCollection renameProperties(GraphCollection graphs,
    List<Tuple4<String, String, String, Boolean>> propertyTuples) {
    DataSet<Tuple4<String, String, String, Boolean>> broadcastSet =
      graphs.getConfig().getExecutionEnvironment().fromCollection(propertyTuples);

    DataSet<EPGMVertex> vertices = graphs.getVertices()
      .map(new RenamePropertiesMap(GRAPH_LABEL_PROPERTY))
      .withBroadcastSet(broadcastSet, RENAME_BROADCAST_SET);

    return graphs.getConfig().getGraphCollectionFactory()
      .fromDataSets(graphs.getGraphHeads(), vertices, graphs.getEdges());
  }

  /**
   * <pre>
   * For {@link LogicalGraph}:
   * Renames an arbitrary number of properties of a vertex. The properties to rename are passed via
   * {@code propertyTuples} as a list of {@code Tuple3<String, String, Boolean>>>}, where
   *    t.f0 = graphLabel for the logical graph or empty string
   *    t.f1 = key ot the property for renaming
   *    t.f2 = name for the renamed property
   *    t.f3 = whether to keep the original property (true), or not (false)
   * The properties are renamed in the order of their appearance in the list
   * </pre>
   *
   * @param graph The {@link LogicalGraph} where to rename the vertex properties
   * @param propertyTuples A list of {@code Tuple3<String, String, Boolean>>>} with the information for
   *                      property renaming
   *
   * @return A {@link LogicalGraph} with the renamed vertex properties
   */
  public LogicalGraph renameProperties(LogicalGraph graph,
    List<Tuple4<String, String, String, Boolean>> propertyTuples) {
    DataSet<Tuple4<String, String, String, Boolean>> broadcastSet =
      graph.getConfig().getExecutionEnvironment().fromCollection(propertyTuples);

    DataSet<EPGMVertex> vertices = graph.getVertices()
      .map(new RenamePropertiesMap(GRAPH_LABEL_PROPERTY))
      .withBroadcastSet(broadcastSet, RENAME_BROADCAST_SET);

    return graph.getConfig().getLogicalGraphFactory()
      .fromDataSets(graph.getGraphHead(), vertices, graph.getEdges());
  }
}
