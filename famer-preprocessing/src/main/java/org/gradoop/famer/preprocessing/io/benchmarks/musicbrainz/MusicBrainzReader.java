/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.benchmarks.musicbrainz;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple12;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.hadoop.shaded.com.google.common.collect.Lists;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.famer.preprocessing.io.benchmarks.BenchmarkSetReaderBase;
import org.gradoop.famer.preprocessing.io.common.GraphToGraphCollection;
import org.gradoop.flink.io.impl.graph.GraphDataSource;
import org.gradoop.flink.io.impl.graph.tuples.ImportEdge;
import org.gradoop.flink.io.impl.graph.tuples.ImportVertex;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.bool.False;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class to read the Music Brainz benchmark data.
 * {@see https://dbs.uni-leipzig.de/research/projects/object_matching/benchmark_datasets_for_entity_resolution}
 */
public class MusicBrainzReader extends BenchmarkSetReaderBase<DataSet<Tuple2<Long, Long>>> {

  @Override
  public DataSet<Tuple2<Long, Long>> getPerfectMapping(String folderPath) {

    return getEnv().readCsvFile(folderPath)
      .ignoreFirstLine()
      .parseQuotedStrings('"')
      .fieldDelimiter(",")
      .types(Long.class, Long.class, Long.class, String.class, String.class, String.class, String.class,
        String.class, String.class, String.class, String.class, String.class)
      .map(t -> Tuple2.of(t.f1, t.f0))
      .returns(new TypeHint<Tuple2<Long, Long>>() { })
      .groupBy(0)
      .reduceGroup((GroupReduceFunction<Tuple2<Long, Long>, Tuple2<Long, Long>>) (group, out) -> {
        ArrayList<Tuple2<Long, Long>> groupList = Lists.newArrayList(group);
        for (int i = 0; i < groupList.size(); i++) {
          for (int j = i + 1; j < groupList.size(); j++) {
            out.collect(Tuple2.of(groupList.get(i).f1, groupList.get(j).f1));
          }
        }
      }).returns(new TypeHint<Tuple2<Long, Long>>() { });
  }

  @Override
  @SuppressWarnings("checkstyle:WhitespaceAfter")
  public GraphCollection getBenchmarkDataAsGraphCollection(String folderPath) {

    DataSet<Tuple12<Long, Long, Long, String, String, String, String, String, String, String, String, String>>
      tuples = getEnv().readCsvFile(folderPath)
      .ignoreFirstLine()
      .parseQuotedStrings('"')
      .fieldDelimiter(",")
      .types(Long.class, Long.class, Long.class, String.class, String.class, String.class, String.class,
        String.class, String.class, String.class, String.class, String.class);

    List<LogicalGraph> graphs = new ArrayList<>();

    DataSet<ImportVertex<Long>> importVertices = tuples.map(
      (MapFunction<Tuple12<Long, Long, Long, String, String, String, String, String, String, String, String,
        String>, ImportVertex<Long>>) tuple -> {
        Map<String, Object> properties = new HashMap<>();
        properties.put("CID", tuple.f1);
        properties.put("CTID", tuple.f2);
        properties.put("SourceID", tuple.f3);
        properties.put("id", tuple.f4);
        properties.put("number", tuple.f5);
        properties.put("title", tuple.f6);
        properties.put("length", tuple.f7);
        properties.put("artist", tuple.f8);
        properties.put("album", tuple.f9);
        properties.put("year", tuple.f10);
        properties.put("language", tuple.f11);
        properties.put(GRAPH_LABEL_PROPERTY, tuple.f3);
        return new ImportVertex<>(tuple.f0, tuple.f3, Properties.createFromMap(properties));
      }).returns(new TypeHint<ImportVertex<Long>>() { });

    graphs.add(createLogicalGraph(importVertices.filter(vertex -> vertex.getLabel().equals("1")), "1"));
    graphs.add(createLogicalGraph(importVertices.filter(vertex -> vertex.getLabel().equals("2")), "2"));
    graphs.add(createLogicalGraph(importVertices.filter(vertex -> vertex.getLabel().equals("3")), "3"));
    graphs.add(createLogicalGraph(importVertices.filter(vertex -> vertex.getLabel().equals("4")), "4"));
    graphs.add(createLogicalGraph(importVertices.filter(vertex -> vertex.getLabel().equals("5")), "5"));

    return GraphToGraphCollection.execute(graphs);
  }

  /**
   * Create a logical graph fo the given vertices and source label. The graph contains empty edges.
   *
   * @param vertices The vertices of the graph.
   * @param sourceLabel The label of the graph.
   *
   * @return A logical graph with the given vertices, label and empty edges.
   */
  private LogicalGraph createLogicalGraph(DataSet<ImportVertex<Long>> vertices, String sourceLabel) {
    DataSet<ImportEdge<Long>> edges = getEnv().fromElements(
      new ImportEdge<>(0L, 0L, 1L)).filter(new False<>());

    LogicalGraph graph = new GraphDataSource<>(vertices, edges, "id", getConfig()).getLogicalGraph();
    graph = graph.transformGraphHead((current, transformed) -> {
      current.setLabel(sourceLabel);
      return current;
    });
    return graph;
  }
}
