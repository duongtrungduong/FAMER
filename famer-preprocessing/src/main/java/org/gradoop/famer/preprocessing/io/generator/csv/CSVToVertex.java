/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.generator.csv;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.pojo.EPGMVertexFactory;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.common.model.impl.properties.PropertyValue;

import java.util.List;

/**
 * Converts a line of a csv file into a {@link EPGMVertex}.
 */
public class CSVToVertex implements FlatMapFunction<String, EPGMVertex> {

  /**
   * Factory to create vertex POJOs
   */
  private EPGMVertexFactory vertexFactory;

  /**
   * The property names
   */
  private List<String> propertyNames;

  /**
   * Reduce object instantiations
   */
  private Properties reuseProperties;

  /**
   * Construct the basic settings for the transfer from csv to vertex.
   *
   * @param vertexFactory Factory for creating vertex POJOs.
   * @param propertyNames List of the property descriptions.
   */
  public CSVToVertex(EPGMVertexFactory vertexFactory, List<String> propertyNames) {
    this.vertexFactory = vertexFactory;
    this.propertyNames = propertyNames;
    this.reuseProperties = new Properties();
  }

  @Override
  public void flatMap(String input, Collector<EPGMVertex> output) throws Exception {

    // Check if the line is an empty line
    if (input.isEmpty()) {
      return;
    }

    EPGMVertex v = vertexFactory.createVertex();

    // Splits the line by comma. The regex avoids the split on commas within a String property
    String[] properties = input.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);

    if (properties.length != propertyNames.size()) {
      return;
    }

    for (int i = 0; i < properties.length; i++) {
      // if a value is empty, set an empty String as property value
      if (!properties[i].isEmpty()) {
        reuseProperties.set(propertyNames.get(i), PropertyValue.create(properties[i].replaceAll("\"", "")));
      } else {
        reuseProperties.set(propertyNames.get(i), "");
      }
    }

    v.setProperties(reuseProperties);

    output.collect(v);
  }
}
