/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.benchmarks.ncvoters;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.core.fs.FileStatus;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.core.fs.Path;
import org.apache.flink.hadoop.shaded.com.google.common.collect.Lists;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.famer.preprocessing.io.benchmarks.BenchmarkSetReaderBase;
import org.gradoop.famer.preprocessing.io.common.GraphToGraphCollection;
import org.gradoop.flink.io.impl.graph.GraphDataSource;
import org.gradoop.flink.io.impl.graph.tuples.ImportEdge;
import org.gradoop.flink.io.impl.graph.tuples.ImportVertex;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.bool.False;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class to read the North Carolina Voters benchmark data.
 * {@see https://dbs.uni-leipzig.de/research/projects/object_matching/benchmark_datasets_for_entity_resolution}
 */
public class NorthCarolinaVotersReader extends BenchmarkSetReaderBase<DataSet<Tuple2<String, String>>> {

  @Override
  public DataSet<Tuple2<String, String>> getPerfectMapping(String folderPath) {
    try {
      DataSet<Tuple2<String, String>> sourceIdTuples = null;

      Path fsPath = new Path(folderPath);
      FileSystem fileSystem = fsPath.getFileSystem();
      FileStatus[] fileStatuses = fileSystem.listStatus(fsPath);

      for (FileStatus status : fileStatuses) {
        String filePath = status.getPath().getPath();
        String fileName = status.getPath().getName();
        final String sourceLabel = fileName.substring(0, fileName.lastIndexOf("."));

        DataSet<Tuple2<String, String>> tmpTuples = getEnv().readCsvFile(filePath)
          .ignoreFirstLine()
          .parseQuotedStrings('"')
          .fieldDelimiter(",")
          .types(String.class, String.class, String.class, String.class, String.class)
          .map(t -> Tuple2.of(sourceLabel, t.f0))
          .returns(new TypeHint<Tuple2<String, String>>() { });

        if (sourceIdTuples == null) {
          sourceIdTuples = tmpTuples;
        } else {
          sourceIdTuples = sourceIdTuples.union(tmpTuples);
        }
      }

      assert sourceIdTuples != null;
      return sourceIdTuples.groupBy(1)
        .reduceGroup((GroupReduceFunction<Tuple2<String, String>, Tuple2<String, String>>) (group, out) -> {
          ArrayList<Tuple2<String, String>> groupList = Lists.newArrayList(group);
          for (int i = 0; i < groupList.size(); i++) {
            for (int j = i + 1; j < groupList.size(); j++) {
              out.collect(Tuple2.of(
                groupList.get(i).f1 + "_" + groupList.get(i).f0,
                groupList.get(j).f1 + "_" + groupList.get(j).f0));
            }
          }
        }).returns(new TypeHint<Tuple2<String, String>>() { });
    } catch (Exception ex) {
      throw new RuntimeException("Could not read benchmark files", ex);
    }
  }

  @Override
  public GraphCollection getBenchmarkDataAsGraphCollection(String folderPath) {
    try {
      List<LogicalGraph> graphs = new ArrayList<>();

      Path fsPath = new Path(folderPath);
      FileSystem fileSystem = fsPath.getFileSystem();
      FileStatus[] fileStatuses = fileSystem.listStatus(fsPath);

      for (FileStatus status : fileStatuses) {
        String filePath = status.getPath().getPath();

        DataSet<Tuple5<String, String, String, String, String>> entryTuples = getEnv().readCsvFile(filePath)
          .ignoreFirstLine()
          .parseQuotedStrings('"')
          .fieldDelimiter(",")
          .types(String.class, String.class, String.class, String.class, String.class);

        String fileName = status.getPath().getName();
        final String sourceLabel = fileName.substring(0, fileName.lastIndexOf("."));

        DataSet<ImportVertex<String>> vertices = entryTuples
          .map((MapFunction<Tuple5<String, String, String, String, String>, ImportVertex<String>>) tuple -> {
            Map<String, Object> properties = new HashMap<>();
            properties.put("givenname", tuple.f1);
            properties.put("surname", tuple.f2);
            properties.put("suburb", tuple.f3);
            properties.put("postcode", tuple.f4);
            properties.put(GRAPH_LABEL_PROPERTY, sourceLabel);
            return new ImportVertex<>(tuple.f0, sourceLabel, Properties.createFromMap(properties));
          }).returns(new TypeHint<ImportVertex<String>>() { });

        DataSet<ImportEdge<String>> edges =
          getEnv().fromElements(new ImportEdge<>("0", "0", "1")).filter(new False<>());

        LogicalGraph graph = new GraphDataSource<>(vertices, edges, "id", getConfig()).getLogicalGraph();
        graph = graph.transformGraphHead((current, transformed) -> {
          current.setLabel(sourceLabel);
          return current;
        });
        graphs.add(graph);
      }

      assert !graphs.isEmpty();
      return GraphToGraphCollection.execute(graphs);
    } catch (Exception ex) {
      throw new RuntimeException("Could not read benchmark files", ex);
    }
  }
}
