/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io;

import org.apache.flink.api.java.ExecutionEnvironment;
import org.gradoop.flink.util.GradoopFlinkConfig;

/**
 * Base class for all data readers. Provides the Flink execution environment and the Gradoop Flink
 * configuration.
 */
public abstract class ReaderBase {

  /**
   * The Flink execution environment
   */
  private final ExecutionEnvironment env;

  /**
   * The Gradoop Flink configuration
   */
  private final GradoopFlinkConfig config;

  /**
   * Creates an instance of ReaderBase and initializes the Flink execution environment and the Gradoop
   * Flink configuration
   */
  protected ReaderBase() {
    this.env = ExecutionEnvironment.getExecutionEnvironment();
    this.config = GradoopFlinkConfig.createConfig(env);
  }

  public ExecutionEnvironment getEnv() {
    return env;
  }

  public GradoopFlinkConfig getConfig() {
    return config;
  }
}
