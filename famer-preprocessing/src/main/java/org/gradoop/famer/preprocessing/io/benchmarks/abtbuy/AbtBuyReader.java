/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.benchmarks.abtbuy;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple5;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.famer.preprocessing.io.benchmarks.BenchmarkSetReaderBase;
import org.gradoop.famer.preprocessing.io.common.GraphToGraphCollection;
import org.gradoop.flink.io.impl.graph.GraphDataSource;
import org.gradoop.flink.io.impl.graph.tuples.ImportEdge;
import org.gradoop.flink.io.impl.graph.tuples.ImportVertex;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.bool.False;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Class to read the Abt-Buy benchmark data.
 * {@see https://dbs.uni-leipzig.de/research/projects/object_matching/benchmark_datasets_for_entity_resolution}
 */
public class AbtBuyReader extends BenchmarkSetReaderBase<DataSet<Tuple2<Long, Long>>> {

  @Override
  public DataSet<Tuple2<Long, Long>> getPerfectMapping(String folderPath) {
    return getEnv().readCsvFile(folderPath + File.separator + "PerfectMapping.csv")
      .ignoreFirstLine()
      .parseQuotedStrings('"')
      .fieldDelimiter(",")
      .types(Long.class, Long.class);
  }

  @Override
  public GraphCollection getBenchmarkDataAsGraphCollection(String folderPath) {
    List<LogicalGraph> graphs = new ArrayList<>();

    LogicalGraph abtGraph = parseAbtEntriesToLogicalGraph(folderPath + File.separator + "Abt.csv");
    if (abtGraph != null) {
      graphs.add(abtGraph);
    }

    LogicalGraph buyGraph = parseBuyEntriesToLogicalGraph(folderPath + File.separator + "Buy.csv");
    if (buyGraph != null) {
      graphs.add(buyGraph);
    }

    return GraphToGraphCollection.execute(graphs);
  }

  /**
   * Reads the abt data and builds an edgeless {@link LogicalGraph} with all abt entries as vertices.
   *
   * @param abtPath Path to the abt data file
   *
   * @return {@link LogicalGraph} with all abt entries as vertices
   */
  private LogicalGraph parseAbtEntriesToLogicalGraph(String abtPath) {
    DataSet<Tuple4<Long, String, String, String>> abtEntries = getEnv().readCsvFile(abtPath)
      .ignoreFirstLine()
      .parseQuotedStrings('"')
      .fieldDelimiter(",")
      .types(Long.class, String.class, String.class, String.class);

    DataSet<ImportVertex<Long>> importVertices = abtEntries.map(
      (MapFunction<Tuple4<Long, String, String, String>, ImportVertex<Long>>) abtTuple -> {
        Map<String, Object> properties = new HashMap<>();
        properties.put("name", abtTuple.f1);
        properties.put("description", abtTuple.f2);
        if (!abtTuple.f3.isEmpty()) {
          NumberFormat format = NumberFormat.getInstance(Locale.US);
          Number number = format.parse(abtTuple.f3.replaceAll("\\$", ""));
          properties.put("price", number.doubleValue());
        } else {
          properties.put("price", abtTuple.f3);
        }
        properties.put(GRAPH_LABEL_PROPERTY, "Abt");
        return new ImportVertex<>(abtTuple.f0, "Abt", Properties.createFromMap(properties));
      }).returns(new TypeHint<ImportVertex<Long>>() { });

    DataSet<ImportEdge<Long>> importEdges = getEnv().fromElements(
      new ImportEdge<>(0L, 0L, 1L)).filter(new False<>());

    LogicalGraph graph =
      new GraphDataSource<>(importVertices, importEdges, "id", getConfig()).getLogicalGraph();
    graph = graph.transformGraphHead((current, transformed) -> {
      current.setLabel("Abt");
      return current;
    });
    return graph;
  }

  /**
   * Reads the buy data and builds an edgeless {@link LogicalGraph} with all buy entries as vertices.
   *
   * @param buyPath Path to the buy data file
   *
   * @return {@link LogicalGraph} with all buy entries as vertices
   */
  private LogicalGraph parseBuyEntriesToLogicalGraph(String buyPath) {
    DataSet<Tuple5<Long, String, String, String, String>> abtEntries = getEnv().readCsvFile(buyPath)
      .ignoreFirstLine()
      .parseQuotedStrings('"')
      .fieldDelimiter(",")
      .types(Long.class, String.class, String.class, String.class, String.class);

    DataSet<ImportVertex<Long>> importVertices = abtEntries.map(
      (MapFunction<Tuple5<Long, String, String, String, String>, ImportVertex<Long>>) buyTuple -> {
        Map<String, Object> properties = new HashMap<>();
        properties.put("name", buyTuple.f1);
        properties.put("description", buyTuple.f2);
        properties.put("manufacturer", buyTuple.f3);
        if (!buyTuple.f4.isEmpty()) {
          NumberFormat format = NumberFormat.getInstance(Locale.US);
          Number number = format.parse(buyTuple.f4.replaceAll("\\$", ""));
          properties.put("price", number.doubleValue());
        } else {
          properties.put("price", buyTuple.f4);
        }
        properties.put(GRAPH_LABEL_PROPERTY, "Buy");
        return new ImportVertex<>(buyTuple.f0, "Buy", Properties.createFromMap(properties));
      }).returns(new TypeHint<ImportVertex<Long>>() { });

    DataSet<ImportEdge<Long>> importEdges = getEnv().fromElements(
      new ImportEdge<>(0L, 0L, 1L)).filter(new False<>());

    LogicalGraph graph =
      new GraphDataSource<>(importVertices, importEdges, "id", getConfig()).getLogicalGraph();
    graph = graph.transformGraphHead((current, transformed) -> {
      current.setLabel("Buy");
      return current;
    });
    return graph;
  }
}
