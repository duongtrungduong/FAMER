/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.common;

import org.gradoop.famer.preprocessing.io.properties.GraphPropertyChecker;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import static org.gradoop.famer.preprocessing.io.properties.GraphPropertyChecker.GRAPH_LABEL_PROPERTY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * JUnit test for {@link GraphPropertyChecker}
 */
public class GraphPropertyCheckerTest extends GradoopFlinkTestBase {

  @Test
  public void testCheckLogicalGraphForGraphLabelPropertyWithOverwrite() throws Exception {
    String graphString = "input:oldLabel[" +
      "(alice:Person {id:1, name:\"Alice\", " + GRAPH_LABEL_PROPERTY + ":\"foo\"})" +
      "(bob:Person {id:2, name:\"Bob\"})" +
      "(alice)-[]->(bob)" +
      "]";
    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    LogicalGraph result =
      new GraphPropertyChecker().checkLogicalGraphForGraphLabelProperty(inputGraph, "test");

    assertNotNull(result);

    assertEquals("test", result.getGraphHead().collect().get(0).getLabel());

    assertEquals(2L, result.getVertices().count());
    assertEquals(1L, result.getEdges().count());
    result.getVertices().collect().forEach(vertex -> {
      assertTrue(vertex.hasProperty(GRAPH_LABEL_PROPERTY));
      assertEquals("test", vertex.getPropertyValue(GRAPH_LABEL_PROPERTY).getString());
    });
  }

  @Test
  public void testCheckLogicalGraphForGraphLabelPropertyWithoutOverwrite() throws Exception {
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\", " + GRAPH_LABEL_PROPERTY + ":\"foo\"})" +
      "(bob:Person {id:2, name:\"Bob\"})" +
      "(alice)-[]->(bob)" +
      "]";
    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    LogicalGraph result =
      new GraphPropertyChecker().checkLogicalGraphForGraphLabelProperty(inputGraph, "test", false);

    assertNotNull(result);

    assertEquals("test", result.getGraphHead().collect().get(0).getLabel());

    assertEquals(2L, result.getVertices().count());
    assertEquals(1L, result.getEdges().count());
    result.getVertices().collect().forEach(vertex -> {
      assertTrue(vertex.hasProperty(GRAPH_LABEL_PROPERTY));
      if (vertex.getPropertyValue("id").getInt() == 1) {
        assertEquals("foo", vertex.getPropertyValue(GRAPH_LABEL_PROPERTY).getString());
      } else {
        assertEquals("test", vertex.getPropertyValue(GRAPH_LABEL_PROPERTY).getString());
      }
    });
  }
}
