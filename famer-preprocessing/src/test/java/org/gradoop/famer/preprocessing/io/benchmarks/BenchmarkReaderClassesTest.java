/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.benchmarks;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMGraphHead;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.preprocessing.io.benchmarks.abtbuy.AbtBuyReader;
import org.gradoop.famer.preprocessing.io.benchmarks.affiliations.AffiliationsReader;
import org.gradoop.famer.preprocessing.io.benchmarks.amazon.AmazonProductsReader;
import org.gradoop.famer.preprocessing.io.benchmarks.dblp.acm.DblpAcmReader;
import org.gradoop.famer.preprocessing.io.benchmarks.dblp.scholar.DblpScholarReader;
import org.gradoop.famer.preprocessing.io.benchmarks.geographic.GeographicSettlementsReader;
import org.gradoop.famer.preprocessing.io.benchmarks.musicbrainz.MusicBrainzReader;
import org.gradoop.famer.preprocessing.io.benchmarks.ncvoters.NorthCarolinaVotersReader;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.gradoop.famer.preprocessing.io.benchmarks.BenchmarkSetReaderBase.GRAPH_LABEL_PROPERTY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * JUnit tests for benchmark reader classes
 */
public class BenchmarkReaderClassesTest extends GradoopFlinkTestBase {

  @Test
  public void testAbtBuyGetPerfectMapping() throws Exception {
    AbtBuyReader abtBuyReader = new AbtBuyReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/abtBuy").getPath();

    List<Tuple2<Long, Long>> perfectMapping = abtBuyReader.getPerfectMapping(folderPath).collect();
    assertNotNull(perfectMapping);
    assertEquals(10, perfectMapping.size());
  }

  @Test
  public void testAbtBuyGetBenchmarkDataAsGraphCollection() throws Exception {
    AbtBuyReader abtBuyReader = new AbtBuyReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/abtBuy").getPath();

    GraphCollection benchmarkDataCollection = abtBuyReader.getBenchmarkDataAsGraphCollection(folderPath);
    assertEquals(2L, benchmarkDataCollection.getGraphHeads().count());

    for (String source : Arrays.asList("Abt", "Buy")) {
      EPGMGraphHead graphHead = benchmarkDataCollection.getGraphHeadsByLabel(source).collect().get(0);
      assertNotNull(graphHead);

      LogicalGraph resultGraph = benchmarkDataCollection.getGraph(graphHead.getId());
      assertNotNull(resultGraph);

      List<EPGMVertex> resultVertices = resultGraph.getVertices().collect();
      if (source.equals("Abt")) {
        assertEquals(10, resultVertices.size());
      }
      if (source.equals("Buy")) {
        assertEquals(10, resultVertices.size());
      }

      resultVertices.forEach(vertex -> {
        assertTrue(vertex.hasProperty(AbtBuyReader.GRAPH_LABEL_PROPERTY));
        assertEquals(source, vertex.getPropertyValue(AbtBuyReader.GRAPH_LABEL_PROPERTY).getString());
      });
    }
  }

  @Test
  public void testAffiliationsGetPerfectMapping() throws Exception {
    AffiliationsReader affiliationsReader = new AffiliationsReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/affiliations").getPath();

    List<Tuple2<Long, Long>> perfectMapping = affiliationsReader.getPerfectMapping(folderPath).collect();
    assertNotNull(perfectMapping);
    assertEquals(10, perfectMapping.size());
  }

  @Test
  public void testAffiliationsGetBenchmarkDataAsGraphCollection() throws Exception {
    AffiliationsReader affiliationsReader = new AffiliationsReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/affiliations").getPath();

    GraphCollection benchmarkDataCollection =
      affiliationsReader.getBenchmarkDataAsGraphCollection(folderPath);
    assertEquals(1L, benchmarkDataCollection.getGraphHeads().count());

    String source = "Affiliations";

    EPGMGraphHead graphHead = benchmarkDataCollection.getGraphHeadsByLabel(source).collect().get(0);
    assertNotNull(graphHead);

    LogicalGraph resultGraph = benchmarkDataCollection.getGraph(graphHead.getId());
    assertNotNull(resultGraph);

    List<EPGMVertex> resultVertices = resultGraph.getVertices().collect();
    assertEquals(20, resultVertices.size());

    resultVertices.forEach(vertex -> {
      assertTrue(vertex.hasProperty(AbtBuyReader.GRAPH_LABEL_PROPERTY));
      assertEquals(source, vertex.getPropertyValue(AbtBuyReader.GRAPH_LABEL_PROPERTY).getString());
    });
  }

  @Test
  public void testAmazonProductsGetPerfectMapping() throws Exception {
    AmazonProductsReader amazonProductsReader = new AmazonProductsReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/amazonGoogle").getPath();

    List<Tuple2<String, String>> perfectMapping =
      amazonProductsReader.getPerfectMapping(folderPath).collect();
    assertNotNull(perfectMapping);
    assertEquals(10, perfectMapping.size());
  }

  @Test
  public void testAmazonProductsGetBenchmarkDataAsGraphCollection() throws Exception {
    AmazonProductsReader amazonProductsReader = new AmazonProductsReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/amazonGoogle").getPath();

    GraphCollection benchmarkDataCollection =
      amazonProductsReader.getBenchmarkDataAsGraphCollection(folderPath);
    assertEquals(2L, benchmarkDataCollection.getGraphHeads().count());

    for (String source : Arrays.asList("Amazon", "Google")) {
      EPGMGraphHead graphHead = benchmarkDataCollection.getGraphHeadsByLabel(source).collect().get(0);
      assertNotNull(graphHead);

      LogicalGraph resultGraph = benchmarkDataCollection.getGraph(graphHead.getId());
      assertNotNull(resultGraph);

      List<EPGMVertex> resultVertices = resultGraph.getVertices().collect();
      if (source.equals("Amazon")) {
        assertEquals(10, resultVertices.size());
      }
      if (source.equals("Buy")) {
        assertEquals(10, resultVertices.size());
      }

      resultVertices.forEach(vertex -> {
        assertTrue(vertex.hasProperty(AmazonProductsReader.GRAPH_LABEL_PROPERTY));
        assertEquals(source, vertex.getPropertyValue(AmazonProductsReader.GRAPH_LABEL_PROPERTY).getString());
      });
    }
  }

  @Test
  public void testDblpAcmGetPerfectMapping() throws Exception {
    DblpAcmReader dblpAcmReader = new DblpAcmReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/dblpAcm").getPath();

    List<Tuple2<String, Long>> perfectMapping = dblpAcmReader.getPerfectMapping(folderPath).collect();
    assertNotNull(perfectMapping);
    assertEquals(10, perfectMapping.size());
  }

  @Test
  public void testDblpAcmGetBenchmarkDataAsGraphCollection() throws Exception {
    DblpAcmReader dblpAcmReader = new DblpAcmReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/dblpAcm").getPath();

    GraphCollection benchmarkDataCollection = dblpAcmReader.getBenchmarkDataAsGraphCollection(folderPath);
    assertEquals(2L, benchmarkDataCollection.getGraphHeads().count());

    for (String source : Arrays.asList("DBLP", "ACM")) {
      EPGMGraphHead graphHead = benchmarkDataCollection.getGraphHeadsByLabel(source).collect().get(0);
      assertNotNull(graphHead);

      LogicalGraph resultGraph = benchmarkDataCollection.getGraph(graphHead.getId());
      assertNotNull(resultGraph);

      List<EPGMVertex> resultVertices = resultGraph.getVertices().collect();
      if (source.equals("DBLP")) {
        assertEquals(10, resultVertices.size());
      }
      if (source.equals("ACM")) {
        assertEquals(10, resultVertices.size());
      }

      resultVertices.forEach(vertex -> {
        assertTrue(vertex.hasProperty(DblpAcmReader.GRAPH_LABEL_PROPERTY));
        assertEquals(source, vertex.getPropertyValue(DblpAcmReader.GRAPH_LABEL_PROPERTY).getString());
      });
    }
  }

  @Test
  public void testDblpScholarGetPerfectMapping() throws Exception {
    DblpScholarReader dblpScholarReader = new DblpScholarReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/dblpScholar").getPath();

    List<Tuple2<String, String>> perfectMapping = dblpScholarReader.getPerfectMapping(folderPath).collect();
    assertNotNull(perfectMapping);
    assertEquals(10, perfectMapping.size());
  }

  @Test
  public void testDblpScholarGetBenchmarkDataAsGraphCollection() throws Exception {
    DblpScholarReader dblpScholarReader = new DblpScholarReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/dblpScholar").getPath();

    GraphCollection benchmarkDataCollection = dblpScholarReader.getBenchmarkDataAsGraphCollection(folderPath);
    assertEquals(2L, benchmarkDataCollection.getGraphHeads().count());

    for (String source : Arrays.asList("DBLP", "Scholar")) {
      EPGMGraphHead graphHead = benchmarkDataCollection.getGraphHeadsByLabel(source).collect().get(0);
      assertNotNull(graphHead);

      LogicalGraph resultGraph = benchmarkDataCollection.getGraph(graphHead.getId());
      assertNotNull(resultGraph);

      List<EPGMVertex> resultVertices = resultGraph.getVertices().collect();
      if (source.equals("DBLP")) {
        assertEquals(10, resultVertices.size());
      }
      if (source.equals("Scholar")) {
        assertEquals(10, resultVertices.size());
      }

      resultVertices.forEach(vertex -> {
        assertTrue(vertex.hasProperty(DblpScholarReader.GRAPH_LABEL_PROPERTY));
        assertEquals(source, vertex.getPropertyValue(DblpScholarReader.GRAPH_LABEL_PROPERTY).getString());
      });
    }
  }

  @Test
  public void testGeographicSettlementsGetPerfectMapping() throws Exception {
    GeographicSettlementsReader geoSettlementsReader = new GeographicSettlementsReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/geographicSettlements").getPath();

    DataSet<Tuple2<Long, Long>> perfectMapping = geoSettlementsReader.getPerfectMapping(folderPath);
    assertNotNull(perfectMapping);
    assertEquals(10L, perfectMapping.count());
  }

  @Test
  public void testGeographicSettlementsGetPerfectClustering() throws Exception {
    GeographicSettlementsReader geoSettlementsReader = new GeographicSettlementsReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/geographicSettlements").getPath();

    DataSet<List<Long>> perfectClustering = geoSettlementsReader.getPerfectClustering(folderPath);
    assertNotNull(perfectClustering);
    assertEquals(3L, perfectClustering.count());
    perfectClustering.collect().forEach(cluster -> {
      assertFalse(cluster.isEmpty());
      assertTrue(cluster.size() <= 4);
    });
  }

  @Test
  public void testGeographicSettlementsGetBenchmarkDataAsGraphCollection() throws Exception {
    GeographicSettlementsReader geoSettlementsReader = new GeographicSettlementsReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/geographicSettlements").getPath();

    GraphCollection benchmarkDataCollection =
      geoSettlementsReader.getBenchmarkDataAsGraphCollection(folderPath);
    assertEquals(4L, benchmarkDataCollection.getGraphHeads().count());

    int totalNumberOfEntities = 0;

    for (String source : Arrays.asList("dbpedia", "nytimes", "freebase", "geonames")) {
      EPGMGraphHead graphHead = benchmarkDataCollection.getGraphHeadsByLabel(source).collect().get(0);
      assertNotNull(graphHead);

      LogicalGraph resultGraph = benchmarkDataCollection.getGraph(graphHead.getId());
      assertNotNull(resultGraph);

      List<EPGMVertex> resultVertices = resultGraph.getVertices().collect();
      if (source.equals("dbpedia")) {
        totalNumberOfEntities += resultVertices.size();
        assertEquals(3, resultVertices.size());
      } else if (source.equals("nytimes")) {
        totalNumberOfEntities += resultVertices.size();
        assertEquals(2, resultVertices.size());
      } else if (source.equals("freebase")) {
        totalNumberOfEntities += resultVertices.size();
        assertEquals(2, resultVertices.size());
      } else if (source.equals("geonames")) {
        totalNumberOfEntities += resultVertices.size();
        assertEquals(2, resultVertices.size());
      } else {
        fail();
      }
      resultVertices.forEach(vertex -> {
        assertTrue(vertex.hasProperty(GRAPH_LABEL_PROPERTY));
        assertEquals(source, vertex.getPropertyValue(GRAPH_LABEL_PROPERTY).getString());
      });
    }
    assertEquals(9, totalNumberOfEntities);
  }

  @Test
  public void testMusicBrainzGetPerfectMapping() throws Exception {
    MusicBrainzReader musicBrainzReader = new MusicBrainzReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/musicbrainz").getPath();

    DataSet<Tuple2<Long, Long>> perfectMapping = musicBrainzReader.getPerfectMapping(folderPath);
    assertNotNull(perfectMapping);
    assertEquals(16, perfectMapping.count());
  }

  @Test
  public void getMusicBrainzBenchmarkDataAsGraphCollection() throws Exception {
    MusicBrainzReader musicBrainzReader = new MusicBrainzReader();
    String folderPath = getClass().getClassLoader().getResource("benchmarks/musicbrainz").getPath();

    GraphCollection benchmarkDataCollection =
      musicBrainzReader.getBenchmarkDataAsGraphCollection(folderPath);

    assertEquals(5L, benchmarkDataCollection.getGraphHeads().count());

    long totalNumber = 0;

    for (String source : Arrays.asList("1", "2", "3", "4", "5")) {
      EPGMGraphHead graphHead = benchmarkDataCollection.getGraphHeadsByLabel(source).collect().get(0);
      assertNotNull(graphHead);

      LogicalGraph resultGraph = benchmarkDataCollection.getGraph(graphHead.getId());
      assertNotNull(resultGraph);

      List<EPGMVertex> resultVertices = resultGraph.getVertices().collect();

      resultVertices.forEach(v -> {
        assertTrue(v.hasProperty(MusicBrainzReader.GRAPH_LABEL_PROPERTY));
        assertEquals(source, v.getPropertyValue(GRAPH_LABEL_PROPERTY).getString());
      });

      if (source.equals("2")) {
        assertEquals(3, resultVertices.size());
      } else {
        assertEquals(2, resultVertices.size());
      }

      long count = resultGraph.getVertices().count();
      totalNumber += count;
    }

    assertEquals(11L, totalNumber);
  }

  @Test
  public void testNorthCarolinaVotersGetPerfectMapping5Party() throws Exception {
    NorthCarolinaVotersReader ncvReader = new NorthCarolinaVotersReader();
    String folderPath5Party =
      getClass().getClassLoader().getResource("benchmarks/northCarolinaVoters/5Party").getPath();

    DataSet<Tuple2<String, String>> perfectMapping = ncvReader.getPerfectMapping(folderPath5Party);
    assertNotNull(perfectMapping);
    assertEquals(23, perfectMapping.count());
  }

  @Test
  public void testNorthCarolinaVotersGetPerfectMapping10Party() throws Exception {
    NorthCarolinaVotersReader ncvReader = new NorthCarolinaVotersReader();
    String folderPath10Party =
      getClass().getClassLoader().getResource("benchmarks/northCarolinaVoters/10Party").getPath();

    DataSet<Tuple2<String, String>> perfectMapping = ncvReader.getPerfectMapping(folderPath10Party);
    assertNotNull(perfectMapping);
    assertEquals(76, perfectMapping.count());
  }

  @Test
  public void testNorthCarolinaVotersGetBenchmarkDataAsGraphCollection5Party() throws Exception {
    NorthCarolinaVotersReader ncvReader = new NorthCarolinaVotersReader();
    String folderPath5Party =
      getClass().getClassLoader().getResource("benchmarks/northCarolinaVoters/5Party").getPath();

    GraphCollection benchmarkDataCollection = ncvReader.getBenchmarkDataAsGraphCollection(folderPath5Party);
    assertEquals(5L, benchmarkDataCollection.getGraphHeads().count());

    for (String source : Arrays.asList("ncv_5_0", "ncv_5_1", "ncv_5_2", "ncv_5_3", "ncv_5_4")) {
      EPGMGraphHead head = benchmarkDataCollection.getGraphHeadsByLabel(source).collect().get(0);
      assertNotNull(head);

      LogicalGraph graph = benchmarkDataCollection.getGraph(head.getId());
      assertNotNull(graph);

      List<EPGMVertex> vertices = graph.getVertices().collect();
      if (source.equals("ncv_5_2") || source.equals("ncv_5_3")) {
        assertEquals(2, vertices.size());
      } else {
        assertEquals(3, vertices.size());
      }

      vertices.forEach(vertex -> {
        assertTrue(vertex.hasProperty(GRAPH_LABEL_PROPERTY));
        assertEquals(source, vertex.getPropertyValue(GRAPH_LABEL_PROPERTY).getString());
      });
    }
  }

  @Test
  public void testNorthCarolinaVotersGetBenchmarkDataAsGraphCollection10Party() throws Exception {
    NorthCarolinaVotersReader ncvReader = new NorthCarolinaVotersReader();
    String folderPath10Party =
      getClass().getClassLoader().getResource("benchmarks/northCarolinaVoters/10Party").getPath();

    GraphCollection benchmarkDataCollection = ncvReader.getBenchmarkDataAsGraphCollection(folderPath10Party);
    assertEquals(10L, benchmarkDataCollection.getGraphHeads().count());

    for (String source : Arrays
      .asList("ncv_10_0", "ncv_10_1", "ncv_10_2", "ncv_10_3", "ncv_10_4", "ncv_10_5", "ncv_10_6", "ncv_10_7",
        "ncv_10_8", "ncv_10_9")) {
      EPGMGraphHead head = benchmarkDataCollection.getGraphHeadsByLabel(source).collect().get(0);
      assertNotNull(head);

      LogicalGraph graph = benchmarkDataCollection.getGraph(head.getId());
      assertNotNull(graph);

      List<EPGMVertex> vertices = graph.getVertices().collect();
      switch (source) {
      case "ncv_10_1":
        assertEquals(1, vertices.size());
        break;
      case "ncv_10_0":
      case "ncv_10_2":
      case "ncv_10_3":
      case "ncv_10_5":
      case "ncv_10_6":
      case "ncv_10_9":
        assertEquals(2, vertices.size());
        break;
      case "ncv_10_4":
      case "ncv_10_7":
      case "ncv_10_8":
        assertEquals(3, vertices.size());
        break;
      default:
        fail();
      }

      vertices.forEach(vertex -> {
        assertTrue(vertex.hasProperty(GRAPH_LABEL_PROPERTY));
        assertEquals(source, vertex.getPropertyValue(GRAPH_LABEL_PROPERTY).getString());
      });
    }
  }
}
