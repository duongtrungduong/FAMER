/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.generator.csv;

import org.gradoop.famer.preprocessing.io.generator.GraphGenerator;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * JUnit test of the {@link CSVGraphGenerator}
 */
public class CSVGraphGeneratorTest extends GradoopFlinkTestBase {

  @Test
  public void generateFromFile() throws Exception {

    String acmPath =
      CSVGraphGeneratorTest.class.getResource("/generator/acm_short_without_header.csv").getFile();

    List<String> propertyNames = Arrays.asList("id", "title", "authors", "venue", "year");

    GraphGenerator acmGenerator = new CSVGraphGenerator(acmPath, "acm", getConfig(), propertyNames);

    LogicalGraph acmGraph = acmGenerator.generateGraph();

    assertEquals(5, acmGraph.getVertices().count());
    assertEquals(0, acmGraph.getEdges().count());
  }
}
