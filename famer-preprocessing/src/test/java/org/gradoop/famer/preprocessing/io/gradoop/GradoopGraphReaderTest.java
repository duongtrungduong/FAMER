/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.gradoop;

import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.util.FlinkAsciiGraphLoader;
import org.junit.Test;

/**
 * JUnit test for {@link GradoopGraphReader}
 */
public class GradoopGraphReaderTest extends GradoopFlinkTestBase {

  private final GradoopGraphReader gradoopGraphReader = new GradoopGraphReader();

  @Test
  public void testReadGraphCollectionFromCSV() throws Exception {
    String expectedString = "expected1:g1 {a:\"graph1\",b:2.75d} [\n" +
      "(v2:B {a:1234L,b:true,c:0.123d})-[e3:b {a:2718L}]->(v3:B {a:5678L,b:false,c:4.123d})\n" +
      "]\n" +
      "expected2:g2 {a:\"graph2\",b:4} [\n" +
      "(v0:A {a:\"foo\",b:42,c:13.37f,d:NULL})-[e0:a {a:1234,b:13.37f}]->(v1:A {a:\"bar\",b:23,c:19.84f}),\n" +
      "(v1)-[e1:a {a:5678,b:23.42f}]->(v0),\n" +
      "(v1)-[e2:b {a:3141L}]->(v2:B {a:1234L,b:true,c:0.123d}),\n" +
      "(v2)-[e3]->(v3),\n" +
      "(v4:B {a:2342L,c:19.84d})-[e4:a {b:19.84f}]->(v0),\n" +
      "(v4)-[e5:b]->(v0)\n" +
      "]";
    FlinkAsciiGraphLoader loader = new FlinkAsciiGraphLoader(gradoopGraphReader.getConfig());
    loader.initDatabaseFromString(expectedString);
    GraphCollection expected =  loader.getGraphCollectionByVariables("expected1", "expected2");

    String csvPath = getClass().getResource("/gradoop/csv/input_graph_collection").getFile();

    GraphCollection input = gradoopGraphReader.readGraphCollectionFromCSV(csvPath);

    collectAndAssertTrue(input.equalsByGraphElementData(expected));
  }

  @Test
  public void testReadGraphCollectionFromEdgeList() throws Exception {
    String expectedString = "expected[\n" +
      "(a)-->(b)\n" +
      "(c)-->(d)\n" +
      "(e)-->(f)\n" +
      "(e)-->(g)\n" +
      "(h)-->(i)\n" +
      "(j)-->(i)\n" +
      "(k)-->(l)\n" +
      "(m)-->(n)\n" +
      "(m)-->(o)\n" +
      "(p)-->(q)\n" +
      "(r)-->(s)\n" +
      "(r)-->(t)\n" +
      "]";
    FlinkAsciiGraphLoader loader = new FlinkAsciiGraphLoader(gradoopGraphReader.getConfig());
    loader.initDatabaseFromString(expectedString);
    GraphCollection expected =  loader.getGraphCollectionByVariables("expected");

    String edgelistPath = getClass().getResource("/gradoop/edgelist/input").getFile();

    GraphCollection input = gradoopGraphReader.readGraphCollectionFromEdgeList(edgelistPath, ",");

    collectAndAssertTrue(input.equalsByGraphElementData(expected));
  }
}
