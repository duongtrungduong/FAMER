/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.sequentialIncremental;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.incremental.sequentialIncremental.common.dataStructures.SequentialVertexComponent;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

/**
 * The abstract class all incremental sequential repair algorithms inheriting from.
 */
public abstract class AbstractSequentialIncremental implements SequentialIncremental {

  /**
   * File path to read the base graph
   */
  protected final String baseInputPath;

  /**
   * File path to read the new graph
   */
  protected final String newInputPath;

  /**
   * File path for writing the output results
   */
  protected final String outputPath;

  /**
   * Maps a vertex id to its {@link SequentialVertexComponent}
   */
  protected HashMap<String, SequentialVertexComponent> sequentialVertexComponentMap;

  /**
   * Creates an instance of AbstractSequentialIncremental
   *
   * @param baseInputPath File path to read the base graph
   * @param newInputPath  File path to read the new graph
   * @param outputPath    File path for writing the output results
   */
  protected AbstractSequentialIncremental(String baseInputPath, String newInputPath, String outputPath) {
    this.baseInputPath = baseInputPath;
    this.newInputPath = newInputPath;
    this.outputPath = outputPath;
    this.sequentialVertexComponentMap = new HashMap<>();
  }

  /**
   * Creates an instance of AbstractSequentialIncremental from json object
   *
   * @param jsonConfig The configuration in json format
   */
  public AbstractSequentialIncremental(JSONObject jsonConfig) {
    this.baseInputPath = getBaseInputPathFromJsonConfig(jsonConfig);
    this.newInputPath = getNewPathFromJsonConfig(jsonConfig);
    this.outputPath = getOutputPathFromJsonConfig(jsonConfig);
    this.sequentialVertexComponentMap = new HashMap<>();
  }

  public String getBaseInputPath() {
    return baseInputPath;
  }

  public String getNewInputPath() {
    return newInputPath;
  }

  public String getOutputPath() {
    return outputPath;
  }

  /**
   * Reads the vertices and edges from files and initializes the graph components. Needs to be
   * implemented by inheriting classes.
   *
   * @throws IOException Exception thrown on errors while reading a file
   */
  protected abstract void initializeSequentialDataStructures() throws IOException;

  /**
   * Executes the sequential incremental repair algorithm. Needs to be implemented by inheriting classes.
   */
  protected abstract void doSequentialIncrementalRepair();

  /**
   * Writes the output result. Needs to be implemented by inheriting classes.
   *
   * @throws IOException Exception thrown on errors while writing a file
   */
  protected abstract void writeIncrementalResultsToFile() throws IOException;

  /**
   * Reads a file from the given path and returns the lines as {@link Stream <String>}
   *
   * @param filePath Path to file
   * @return Lines as {@link Stream<String>}
   *
   * @throws IOException Exception thrown on read errors
   */
  protected Stream<String> getLinesFromFilePath(String filePath) throws IOException {
    return Files.lines(Paths.get(filePath), StandardCharsets.UTF_8);
  }

  /**
   * Writes the lines to the given file path. If the file doesn't exist, creates and writes to it. If the
   * file exists, deletes the content and writes to it.
   *
   * @param filePath Path to file
   * @param lines    Lines to write
   *
   * @throws IOException Exception thrown on write errors
   *                     <p>
   *                     Note: suppressed findbugs warning for ignored result of {@code mkdirs()}
   */
  protected void writeLinesToFilePath(String filePath, List<String> lines) throws IOException {
    File path = new File(filePath);
    if (!path.exists()) {
      path.getParentFile().mkdirs();
    }
    Files.write(Paths.get(filePath), lines, StandardCharsets.UTF_8);
  }

  /**
   * Gets the path for the base input graph from the json configuration object.
   *
   * @param jsonConfig The json configuration object
   *
   * @return The path for the base input graph
   */
  protected String getBaseInputPathFromJsonConfig(JSONObject jsonConfig) {
    try {
      return jsonConfig.getString("basePath");
    } catch (Exception ex) {
      throw new UnsupportedOperationException(
        "Sequential incremental repair: String value for base input path could not be found or parsed", ex);
    }
  }

  /**
   * Gets the path for the new graph from the json configuration object.
   *
   * @param jsonConfig The json configuration object
   *
   * @return The path for the new graph
   */
  protected String getNewPathFromJsonConfig(JSONObject jsonConfig) {
    try {
      return jsonConfig.getString("newPath");
    } catch (Exception ex) {
      throw new UnsupportedOperationException(
        "Sequential incremental repair: String value for new path could not be found or parsed", ex);
    }
  }

  /**
   * Gets the path for the output graph from the json configuration object.
   *
   * @param jsonConfig The clustering json configuration
   *
   * @return The path for the output graph
   */
  protected String getOutputPathFromJsonConfig(JSONObject jsonConfig) {
    try {
      return jsonConfig.getString("outputPath");
    } catch (Exception ex) {
      throw new UnsupportedOperationException(
        "Sequential incremental repair: String value for output path could not be found or parsed", ex);
    }
  }

  @Override
  public void execute() throws IOException {
    initializeSequentialDataStructures();
    doSequentialIncrementalRepair();
    writeIncrementalResultsToFile();
  }
}
