/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

import static org.gradoop.famer.clustering.common.PropertyNames.GRAPH_LABEL;

/**
 * The implementation of flatMap function that appends the id of one end and the source of the other end to
 * the input tuple.
 */
public class V1IDV2SourceGetter implements FlatMapFunction<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>,
  Tuple5<EPGMEdge, EPGMVertex, EPGMVertex, String, String>> {

  @Override
  public void flatMap(Tuple3<EPGMEdge, EPGMVertex, EPGMVertex> in,
    Collector<Tuple5<EPGMEdge, EPGMVertex, EPGMVertex, String, String>> out) throws Exception {

    // collect <Edge,Source,Target,SourceId,TargetGraphLabel>
    out.collect(Tuple5
      .of(in.f0, in.f1, in.f2, in.f1.getId().toString(), in.f2.getPropertyValue(GRAPH_LABEL).toString()));
    // collect <Edge,Target,Source,TargetId,SourceGraphLabel>
    out.collect(Tuple5
      .of(in.f0, in.f2, in.f1, in.f2.getId().toString(), in.f1.getPropertyValue(GRAPH_LABEL).toString()));
  }
}
