/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.incremental.parallelIncremental.common.utils.IncrementalUtils;

import java.util.List;

import static org.gradoop.famer.incremental.common.PropertyNames.SOURCE_LIST;

/**
 * The implementation of flatMap function that finds the vertices which corresponding clusters can be
 * merged with keeping the constraint of source consistency.
 */
public class Remover implements
  FlatMapFunction<Tuple3<Double, EPGMVertex, EPGMVertex>, Tuple3<Double, EPGMVertex, EPGMVertex>> {

  @Override
  public void flatMap(Tuple3<Double, EPGMVertex, EPGMVertex> input,
    Collector<Tuple3<Double, EPGMVertex, EPGMVertex>> out) throws Exception {
    List<PropertyValue> sourceList1 = input.f1.getPropertyValue(SOURCE_LIST).getList();
    List<PropertyValue> sourceList2 = input.f2.getPropertyValue(SOURCE_LIST).getList();
    if (IncrementalUtils.isConsistent(sourceList1, sourceList2)) {
      out.collect(input);
    }
  }
}

