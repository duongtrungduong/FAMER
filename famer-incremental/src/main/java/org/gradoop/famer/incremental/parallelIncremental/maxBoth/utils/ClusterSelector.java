/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions.CusterIdAdder;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions.Selector;

/**
 * In case of having multiple max-both (and source-consistent) links, select one or more of them with
 * keeping source consistency constraint. It does selection in two phases: First, select from
 * existing entities - second, select from new entities.
 */
public class ClusterSelector {

  /**
   * Executes the clusterSelector
   *
   * @param simVertex1Vertex2 The paired vertices
   *
   * @return The selected clusters
   */
  public DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> execute(
    DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> simVertex1Vertex2) {
    /* Phase 1: selection from existing entities */
    DataSet<Tuple4<Double, EPGMVertex, EPGMVertex, String>> simVExistVClsId =
      simVertex1Vertex2.flatMap(new CusterIdAdder(CusterIdAdder.FLAG_OLD));

    DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> selectedFromExisting =
      simVExistVClsId.groupBy(3).reduceGroup(new Selector());

    /* Phase 2: selection from new entities */
    DataSet<Tuple4<Double, EPGMVertex, EPGMVertex, String>> linkVNewVClsId =
      selectedFromExisting.flatMap(new CusterIdAdder(CusterIdAdder.FLAG_NEW));

    return linkVNewVClsId.groupBy(3).reduceGroup(new Selector());
  }
}
