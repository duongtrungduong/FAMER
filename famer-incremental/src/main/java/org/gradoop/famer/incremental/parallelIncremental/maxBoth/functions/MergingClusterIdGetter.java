/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.incremental.parallelIncremental.common.utils.IncrementalUtils;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;

/**
 * The implementation of flatMap function to find the clusterIds that are going to be merged. The final
 * clusterId is always the second element of the output tuple2.
 */
public class MergingClusterIdGetter implements
  FlatMapFunction<Tuple3<Double, EPGMVertex, EPGMVertex>, Tuple2<String, String>> {

  @Override
  public void flatMap(Tuple3<Double, EPGMVertex, EPGMVertex> simVertex1Vertex2,
    Collector<Tuple2<String, String>> out) throws Exception {
    String clusterId1 = simVertex1Vertex2.f1.getPropertyValue(CLUSTER_ID).toString();
    String clusterId2 = simVertex1Vertex2.f2.getPropertyValue(CLUSTER_ID).toString();
    if (IncrementalUtils.isNew(clusterId1) && !IncrementalUtils.isNew(clusterId2)) {
      out.collect(Tuple2.of(clusterId1, clusterId2));
    } else if (!IncrementalUtils.isNew(clusterId1) && IncrementalUtils.isNew(clusterId2)) {
      out.collect(Tuple2.of(clusterId2, clusterId1));
    }
  }
}
