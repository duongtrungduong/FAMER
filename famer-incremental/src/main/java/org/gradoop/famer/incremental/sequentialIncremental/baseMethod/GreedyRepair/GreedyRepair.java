/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.sequentialIncremental.baseMethod.GreedyRepair;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.incremental.sequentialIncremental.AbstractSequentialIncremental;
import org.gradoop.famer.incremental.sequentialIncremental.common.dataStructures.NeighborInfo;
import org.gradoop.famer.incremental.sequentialIncremental.common.dataStructures.SequentialEdgeComponent;
import org.gradoop.famer.incremental.sequentialIncremental.common.dataStructures.SequentialVertexComponent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import static org.gradoop.famer.incremental.common.PropertyNames.NEW_PREFIX;
import static org.gradoop.famer.incremental.sequentialIncremental.common.PropertyNames.EDGES_PATH;
import static org.gradoop.famer.incremental.sequentialIncremental.common.PropertyNames.VERTICES_PATH;

/**
 * The serial implementation of Greedy algorithm from Anja Gruenheid.
 * {@see "http://www.vldb.org/pvldb/vol7/p697-gruenheid.pdf"}
 */
public abstract class GreedyRepair extends AbstractSequentialIncremental {

  /**
   * Maps a vertex id to a list of its neighbors
   */
  protected HashMap<String, List<NeighborInfo>> neighbors;

  /**
   * Maps a cluster id to a list of its member (vertex) ids
   */
  protected HashMap<String, List<String>> clusterList;

  /**
   * The queue of the Greedy algorithm
   */
  protected Queue<String> queue;

  /**
   * Maps an edge id to its {@link SequentialEdgeComponent}
   */
  protected HashMap<String, SequentialEdgeComponent> sequentialEdgeComponentMap;

  /**
   * Creates an instance of GreedyRepair
   *
   * @param baseInputPath File path to read the base graph
   * @param newInputPath  File path to read the new graph
   * @param outputPath    File path for writing the output results
   */
  public GreedyRepair(String baseInputPath, String newInputPath, String outputPath) {
    super(baseInputPath, newInputPath, outputPath);
    sequentialEdgeComponentMap = new HashMap<>();
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The clustering algorithm json config part
   */
  public GreedyRepair(JSONObject jsonConfig) {
    super(jsonConfig);
    sequentialEdgeComponentMap = new HashMap<>();
  }

  @Override
  protected void initializeSequentialDataStructures() throws IOException {
    // read vertices from base graph
    getLinesFromFilePath(baseInputPath + File.separator + VERTICES_PATH).forEach(line -> {
      String[] lineData = line.split(",");
      SequentialVertexComponent svc = new SequentialVertexComponent(lineData[1], lineData[2]);
      svc.setId(lineData[0]);
      sequentialVertexComponentMap.put(lineData[0], svc);
    });
    // read new vertices
    getLinesFromFilePath(newInputPath + File.separator + VERTICES_PATH).forEach(line -> {
      String[] lineData = line.split(",");
      String clusterId = "";
      if (lineData.length == 2) {
        clusterId = NEW_PREFIX + lineData[0];
      } else if (lineData.length == 3) {
        clusterId = lineData[2];
      }
      SequentialVertexComponent svc = new SequentialVertexComponent(lineData[1], clusterId);
      svc.setId(lineData[0]);
      sequentialVertexComponentMap.put(lineData[0], svc);
    });
    // read edges from base graph
    getLinesFromFilePath(baseInputPath + File.separator + EDGES_PATH).forEach(line -> {
      String[] lineData = line.split(",");
      SequentialEdgeComponent sec =
        new SequentialEdgeComponent(lineData[0], lineData[1], Double.parseDouble(lineData[2]));
      sequentialEdgeComponentMap.put(lineData[0] + lineData[1], sec);
    });
    // read new edges
    getLinesFromFilePath(newInputPath + File.separator + EDGES_PATH).forEach(line -> {
      String[] lineData = line.split(",");
      SequentialEdgeComponent sec =
        new SequentialEdgeComponent(lineData[0], lineData[1], Double.parseDouble(lineData[2]));
      sequentialEdgeComponentMap.put(lineData[0] + lineData[1], sec);
    });

    setAlgorithmSpecificInfo();
    initializeGreedyRepairDataStructures();
  }

  @Override
  protected void doSequentialIncrementalRepair() {
    while (!queue.isEmpty()) {
      String clusterId = queue.remove();
      change(clusterId);
    }
  }

  @Override
  protected void writeIncrementalResultsToFile() throws IOException {
    // prepare and write vertices
    List<String> vertices = sequentialVertexComponentMap.entrySet().stream().map(
      entry -> entry.getKey() + "," + entry.getValue().getSource() + "," +
        entry.getValue().getClusterId().replaceAll(NEW_PREFIX, "")).collect(Collectors.toList());
    writeLinesToFilePath(outputPath + File.separator + VERTICES_PATH, vertices);
    // prepare and write edges
    List<String> edges = getEdgeLines();
    writeLinesToFilePath(outputPath + File.separator + EDGES_PATH, edges);
  }

  /**
   * Gets the data fields of the edges. Needs to be implemented by inheriting classes.
   *
   * @return The list of edges. Each edge is represented in one line
   */
  protected abstract List<String> getEdgeLines();

  /**
   * Sets the special information every clustering algorithms needs for repairing. The additional
   * information is stored in {@code additionalInfo} list of the sequentialEdgeComponent. Needs to be
   * implemented by inheriting classes.
   */
  protected abstract void setAlgorithmSpecificInfo();

  /**
   * Initializes the data structures of the greedy algorithm - clusterList, queue and neighbors.
   */
  private void initializeGreedyRepairDataStructures() {
    initializeClusterList();
    initializeQueue();
    initializeNeighbors();
  }

  /**
   * Initializes the clusterList using the sequentialVertexComponentMap
   */
  private void initializeClusterList() {
    clusterList = new HashMap<>();
    sequentialVertexComponentMap.forEach((vId, vertex) -> {
      List<String> clusterVertexIds = new ArrayList<>();
      if (clusterList.containsKey(vertex.getClusterId())) {
        clusterVertexIds = clusterList.get(vertex.getClusterId());
        clusterList.remove(vertex.getClusterId());
      }
      clusterVertexIds.add(vertex.getId());
      clusterList.put(vertex.getClusterId(), clusterVertexIds);
    });
  }

  /**
   * Initializes the queue using the sequentialVertexComponentMap
   */
  private void initializeQueue() {
    queue = new ArrayDeque<>();
    sequentialVertexComponentMap.forEach((vId, vertex) -> {
      if (vertex.getClusterId().contains(NEW_PREFIX) && !queue.contains(vertex.getClusterId())) {
        queue.add(vertex.getClusterId());
      }
    });
  }

  /**
   * Initializes the neighbor information using the sequentialEdgeComponentMap
   */
  private void initializeNeighbors() {
    neighbors = new HashMap<>();
//    sequentialEdgeComponentMap.entrySet().stream().forEach(entry -> {
    sequentialEdgeComponentMap.forEach((secId, sec) -> {
      String sourceId = sec.getSourceId();
      String targetId = sec.getTargetId();
      List<String> info = sec.getInfo();
      double simValue = sec.getSimValue();
      // build neighbors for sec-source
      List<NeighborInfo> neighborInfos1 = new ArrayList<>();
      if (neighbors.containsKey(sourceId)) {
        neighborInfos1 = neighbors.get(sourceId);
        neighbors.remove(sourceId);
      }
      neighborInfos1.add(new NeighborInfo(targetId, simValue, info));
      neighbors.put(sourceId, neighborInfos1);
      // build neighbors for sec-target
      List<NeighborInfo> neighborInfos2 = new ArrayList<>();
      if (neighbors.containsKey(targetId)) {
        neighborInfos2 = neighbors.get(targetId);
        neighbors.remove(targetId);
      }
      neighborInfos2.add(new NeighborInfo(sourceId, simValue, info));
      neighbors.put(targetId, neighborInfos2);
    });
  }

  /**
   * Try to change the cluster specified by the given clusterId, run "MERGE", "SPLIT" and "MOVE" actions
   *
   * @param clusterId The clusterId of the cluster that is going to be changed
   */
  private void change(String clusterId) {
    boolean isChanged = greedyMerge(clusterId);
    if (!isChanged) {
      isChanged = greedySplit(clusterId);
    }
    if (!isChanged) {
      greedyMove(clusterId);
    }
  }

  /**
   * Try merging the cluster specified by the given clusterId with the neighbor clusters
   *
   * @param clusterId The clusterId of the cluster that is going to be merged
   *
   * @return Whether the "MERGE" action is successfully done
   */
  private boolean greedyMerge(String clusterId) {
    Set<String> neighborClusterIdList = getNeighborClusters(clusterId);
    if (!neighborClusterIdList.isEmpty()) {
      for (String neighborClusterId : neighborClusterIdList) {
        if (isMergeable(clusterId, neighborClusterId)) {
          updateListsByMerge(clusterId, neighborClusterId);
          updateQueueByMerge(clusterId, neighborClusterId);
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Checks whether merging two clusters specified by the given clusterIds is possible and an optimal
   * change. Needs to be implemented by inheriting classes.
   *
   * @param clusterId1 The clusterId of the first cluster
   * @param clusterId2 The clusterId of the second cluster
   *
   * @return Whether the "MERGE" action is possible.
   */
  protected abstract boolean isMergeable(String clusterId1, String clusterId2);

  /**
   * Updates the lists by changes made by the "MERGE" action
   *
   * @param clusterId1 The clusterId of the first cluster
   * @param clusterId2 The clusterId of the second cluster
   */
  private void updateListsByMerge(String clusterId1, String clusterId2) {
    updateSequentialVertexComponentMapByMerge(clusterId1, clusterId2);
    updateClusterListByMerge(clusterId1, clusterId2);
  }

  /**
   * Updates the {@link #sequentialVertexComponentMap} with changes done by the "MERGE" action
   *
   * @param clusterId1 The clusterId of the first cluster
   * @param clusterId2 The clusterId of the second cluster
   */
  private void updateSequentialVertexComponentMapByMerge(String clusterId1, String clusterId2) {
    String finalClusterId = getFinalClusterId(clusterId1, clusterId2);
    String changedCluster = clusterId1;
    if (finalClusterId.equals(changedCluster)) {
      changedCluster = clusterId2;
    }
    List<String> vertexList = clusterList.get(changedCluster);
    for (String vertexId : vertexList) {
      String src = sequentialVertexComponentMap.get(vertexId).getSource();
      sequentialVertexComponentMap.remove(vertexId);
      sequentialVertexComponentMap.put(vertexId, new SequentialVertexComponent(src, finalClusterId));
    }
  }

  /**
   * Updates the {@link #clusterList} with changes done by the "MERGE" action
   *
   * @param clusterId1 The clusterId of the first cluster
   * @param clusterId2 The clusterId of the second cluster
   */
  private void updateClusterListByMerge(String clusterId1, String clusterId2) {
    String finalClusterId = getFinalClusterId(clusterId1, clusterId2);
    List<String> vertexList1 = clusterList.get(clusterId1);
    List<String> vertexList2 = clusterList.get(clusterId2);
    vertexList1.addAll(vertexList2);
    clusterList.remove(clusterId1);
    clusterList.remove(clusterId2);
    clusterList.put(finalClusterId, vertexList1);
  }

  /**
   * Updates the queue with changes done by the "MERGE" action
   *
   * @param clusterId1 The clusterId of the first cluster
   * @param clusterId2 The clusterId of the second cluster
   */
  private void updateQueueByMerge(String clusterId1, String clusterId2) {
    queue.remove(clusterId1);
    queue.remove(clusterId2);
    queue.add(getFinalClusterId(clusterId1, clusterId2));
  }

  /**
   * Chooses one the "final cluster id" out of the two given cluster ids by comparing the id strings.
   *
   * @param clusterId1 The clusterId of the first cluster
   * @param clusterId2 The clusterId of the second cluster
   *
   * @return The selected clusterId
   */
  private String getFinalClusterId(String clusterId1, String clusterId2) {
    String finalClusterId = clusterId1;
    if (clusterId1.compareTo(clusterId2) > 0) {
      finalClusterId = clusterId2;
    }
    return finalClusterId;
  }

  /**
   * Try splitting the cluster specified by the input clusterId into two clusters. For CLIP there is not
   * split done, so just return false.
   *
   * @param clusterId The clusterId of the cluster that is going to be split
   *
   * @return Whether the "SPLIT" action is done successfully
   */
  private boolean greedySplit(String clusterId) {
    return false;
  }

  /**
   * Try moving the members of the cluster specified by the given clusterId to the neighbor clusters
   *
   * @param clusterId The clusterId of the cluster which members are being moved
   */
  private void greedyMove(String clusterId) {
    Set<String> neighborClusterIdList = getNeighborClusters(clusterId);
    if (!neighborClusterIdList.isEmpty()) {
      for (String neighborClusterId : neighborClusterIdList) {
        List<String> connectingVList = getConnectingVertices(clusterId, neighborClusterId);
        boolean isMoved = false;
        for (String vertexId : connectingVList) {
          if (isAddable(vertexId, neighborClusterId)) {
            updateListsByMove(vertexId, clusterId, neighborClusterId);
            isMoved = true;
          }
        }
        if (isMoved) {
          updateQueueByMove(clusterId, neighborClusterId);
        }
      }
    }
  }

  /**
   * Get the ids of vertices that connect two clusters
   *
   * @param clusterId1 The clusterId of the first cluster
   * @param clusterId2 The clusterId of the second cluster
   *
   * @return The list of connecting vertex ids
   */
  private List<String> getConnectingVertices(String clusterId1, String clusterId2) {
    List<String> connectingVertices = new ArrayList<>();
    List<String> verticesCluster1 = clusterList.get(clusterId1);
    List<String> verticesCluster2 = clusterList.get(clusterId2);
    for (String vId1 : verticesCluster1) {
      if (neighbors.containsKey(vId1)) {
        List<NeighborInfo> neighborInfos = neighbors.get(vId1);
        for (NeighborInfo neighborInfo : neighborInfos) {
          String neighborVId = neighborInfo.getVertexId();
          if (verticesCluster2.contains(neighborVId) && !connectingVertices.contains(vId1)) {
            connectingVertices.add(vId1);
          }
        }
      }
    }
    return connectingVertices;
  }

  /**
   * Checks whether moving the vertex specified by the given vertexId to the neighbor cluster is possible and
   * an optimal change. Needs to be implemented by inheriting classes.
   *
   * @param vertexId  The vertexId of the vertex to be moved
   * @param clusterId The neighbor clusterId
   *
   * @return Whether the "MOVE (adding)" action is possible.
   */
  protected abstract boolean isAddable(String vertexId, String clusterId);

  /**
   * Updates the lists with the changes done by the "MOVE" action
   *
   * @param vertexId        The vertexId of the moved vertex
   * @param sourceClusterId The clusterId of the moved vertex
   * @param targetClusterId The new clusterId of the moved vertex
   */
  private void updateListsByMove(String vertexId, String sourceClusterId, String targetClusterId) {
    updateSequentialVertexComponentMapByMove(vertexId, targetClusterId);
    updateClusterListByMove(vertexId, sourceClusterId, targetClusterId);
  }

  /**
   * Updates the sequentialVertexComponentMap with the changes done by the "MOVE" action
   *
   * @param vertexId        The vertexId of the moved vertex
   * @param targetClusterId The new clusterId of the moved vertex
   */
  private void updateSequentialVertexComponentMapByMove(String vertexId, String targetClusterId) {
    String src = sequentialVertexComponentMap.get(vertexId).getSource();
    sequentialVertexComponentMap.remove(vertexId);
    sequentialVertexComponentMap.put(vertexId, new SequentialVertexComponent(src, targetClusterId));
  }

  /**
   * Updates the clusterList with the changes done by the "MOVE" action
   *
   * @param vertexId        The vertexId of the moved vertex
   * @param sourceClusterId The clusterId of the moved vertex
   * @param targetClusterId The new clusterId of the moved vertex
   */
  private void updateClusterListByMove(String vertexId, String sourceClusterId, String targetClusterId) {
    List<String> theOtherIds = clusterList.get(targetClusterId);
    theOtherIds.add(vertexId);
    clusterList.remove(targetClusterId);
    clusterList.put(targetClusterId, theOtherIds);

    List<String> vIds = clusterList.get(sourceClusterId);
    vIds.remove(vertexId);
    clusterList.remove(sourceClusterId);
    clusterList.put(sourceClusterId, vIds);
  }

  /**
   * Updates the queue by the changes made by the "MOVE" action
   *
   * @param sourceClusterId The clusterId of the moved vertex
   * @param targetClusterId The new clusterId of the moved vertex
   */
  private void updateQueueByMove(String sourceClusterId, String targetClusterId) {
    queue.remove(sourceClusterId);
    queue.remove(targetClusterId);
    // put source and cluster id to the end of the queue
    queue.add(sourceClusterId);
    queue.add(targetClusterId);
  }

  /**
   * Retrieves the neighbors of the cluster specified by the given clusterId
   *
   * @param clusterId The id of the desired cluster
   *
   * @return The list of neighbor clusterIds for the given clusterId
   */
  private Set<String> getNeighborClusters(String clusterId) {
    List<String> vertexList;
    Set<String> output = new HashSet<>();

    if (clusterList.containsKey(clusterId)) {
      vertexList = clusterList.get(clusterId);
      for (String vertexId : vertexList) {
        if (neighbors.containsKey(vertexId)) {
          List<NeighborInfo> neighborInfos = neighbors.get(vertexId);
          for (NeighborInfo neighbor : neighborInfos) {
            String neighborId = neighbor.getVertexId();
            if (!vertexList.contains(neighborId)) {
              String neighborClusterId = sequentialVertexComponentMap.get(neighborId).getClusterId();
              output.add(neighborClusterId);
            }
          }
        }
      }
    }
    return output;
  }

  /**
   * Abstract method for building the repairing execution that must be implemented by all sequential
   * incremental repairing methods
   *
   * @return The repairing method object
   */
  public abstract AbstractSequentialIncremental buildRepairingExecutor();
}



