/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.common.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.incremental.common.PropertyNames.NEW_PREFIX;

/**
 * Map function to assign a new-prefixed and unique cluster id to each unclustered vertex, thus make this
 * vertex a singleton cluster.
 */
public class SingletonMaker implements MapFunction<EPGMVertex, EPGMVertex> {

  @Override
  public EPGMVertex map(EPGMVertex vertex) throws Exception {
    if (!vertex.hasProperty(CLUSTER_ID)) {
      vertex.setProperty(CLUSTER_ID, NEW_PREFIX + vertex.getId().toString());
    }
    return vertex;
  }
}
