/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions.Remover;

/**
 * Remove the paired vertices which corresponding clusters can not be merged due to the source consistency
 * constraint
 */
public class SourceInconsistentRemover {

  /**
   * Whether the source consistency constraint should be applied.
   */
  private final boolean isSCRemoving;

  /**
   * Creates an instance of SourceInconsistentRemover
   *
   * @param isSCRemoving Whether the source consistency constraint should be applied.
   */
  public SourceInconsistentRemover(boolean isSCRemoving) {
    this.isSCRemoving = isSCRemoving;
  }

  /**
   * Executes the SourceInconsistentRemover
   *
   * @param simVertex1Vertex2 The paired vertices
   *
   * @return The source consistent pairs
   */
  public DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> execute(
    DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> simVertex1Vertex2) {
    if (isSCRemoving) {
      simVertex1Vertex2 = simVertex1Vertex2.flatMap(new Remover());
    }
    return simVertex1Vertex2;
  }
}
