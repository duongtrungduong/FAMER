/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

import static org.gradoop.famer.clustering.common.PropertyNames.SIM_VALUE;

/**
 * The reduce implementation for finding max-both links
 */
public class MaxBothVNewVGetter implements
  GroupReduceFunction<Tuple4<EPGMEdge, EPGMVertex, EPGMVertex, String>,
    Tuple3<Double, EPGMVertex, EPGMVertex>> {

  @Override
  public void reduce(Iterable<Tuple4<EPGMEdge, EPGMVertex, EPGMVertex, String>> group,
    Collector<Tuple3<Double, EPGMVertex, EPGMVertex>> out) {
    EPGMVertex v1 = null;
    EPGMVertex v2 = null;
    double sim = 0d;

    for (Tuple4<EPGMEdge, EPGMVertex, EPGMVertex, String> item : group) {
      if (v1 == null) {
        v1 = item.f1;
      } else {
        v2 = item.f1;
      }
      sim = Double.parseDouble(item.f0.getPropertyValue(SIM_VALUE).toString());
    }

    if (v2 != null) {
      out.collect(Tuple3.of(sim, v1, v2));
    }
  }
}
