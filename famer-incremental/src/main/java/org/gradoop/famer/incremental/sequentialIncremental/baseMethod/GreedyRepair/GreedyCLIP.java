/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.sequentialIncremental.baseMethod.GreedyRepair;

import org.apache.commons.lang3.tuple.Pair;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.incremental.sequentialIncremental.AbstractSequentialIncremental;
import org.gradoop.famer.incremental.sequentialIncremental.common.dataStructures.NeighborInfo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Implements the greedy repair algorithm with objective function of the CLIP
 */
public class GreedyCLIP extends GreedyRepair {

  /**
   * The similarity value coefficient
   */
  private final double simValueCoef;

  /**
   * The change coefficient
   */
  private final double strengthCoef;

  /**
   * The degree coefficient
   */
  private final double degreeCoef;

  /**
   * Creates an instance of GreedyCLIP
   *
   * @param baseInputPath File path to read the base graph
   * @param newInputPath  File path to read the new graph
   * @param outputPath    File path for writing the output results
   * @param simValueCoef  The similarity value coefficient
   * @param strengthCoef  The strength coefficient
   * @param degreeCoef  The degree coefficient
   */
  public GreedyCLIP(String baseInputPath, String newInputPath, String outputPath, double simValueCoef,
    double strengthCoef, double degreeCoef) {
    super(baseInputPath, newInputPath, outputPath);
    this.simValueCoef = simValueCoef;
    this.strengthCoef = strengthCoef;
    this.degreeCoef = degreeCoef;
  }

  /**
   * Creates an instance of GreedyCLIP with default coefficient values
   *
   * @param baseInputPath File path to read the base graph
   * @param newInputPath  File path to read the new graph
   * @param outputPath    File path for writing the output results
   */
  public GreedyCLIP(String baseInputPath, String newInputPath, String outputPath) throws Exception {
    super(baseInputPath, newInputPath, outputPath);
    this.simValueCoef = 0.5;
    this.strengthCoef = 0.3;
    this.degreeCoef = 0.2;
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The json configuration object
   */
  public GreedyCLIP(JSONObject jsonConfig) {
    super(jsonConfig);
    try {
      this.simValueCoef = jsonConfig.getDouble("simValueCoef");
    } catch (Exception ex) {
      throw new UnsupportedOperationException(
        "Sequential incremental repairing: Double value for simValueCoef could not be found or parsed", ex);
    }
    try {
      this.strengthCoef = jsonConfig.getDouble("strengthCoef");
    } catch (Exception ex) {
      throw new UnsupportedOperationException(
        "Sequential incremental repairing: Double value for strengthCoef could not be found or parsed", ex);
    }
    try {
      this.degreeCoef = jsonConfig.getDouble("degreeCoef");
    } catch (Exception ex) {
      throw new UnsupportedOperationException(
        "Sequential incremental repairing: Double value for degreeCoef could not be found or parsed", ex);
    }
  }

  public double getSimValueCoef() {
    return simValueCoef;
  }

  public double getStrengthCoef() {
    return strengthCoef;
  }

  public double getDegreeCoef() {
    return degreeCoef;
  }

  @Override
  protected List<String> getEdgeLines() {
    return sequentialEdgeComponentMap.values().stream().map(sec ->
      sec.getSourceId() + "," + sec.getTargetId() + "," + sec.getSimValue() + "," + sec.getInfoFromIndex(0))
      .collect(Collectors.toList());
  }

  /**
   * {@inheritDoc}
   *
   * Sets the extra information for the objective function of the CLIP algorithm. This implementation adds
   * "change" to each edge.
   */
  @Override
  protected void setAlgorithmSpecificInfo() {
    Map<String, List<Pair<String, Double>>> vertexSourceMap = new HashMap<>();
    sequentialEdgeComponentMap.forEach((secKey, sec) -> {
      double simValue = sec.getSimValue();
      String sourceId = sec.getSourceId();
      String targetId = sec.getTargetId();

      String src1 = sequentialVertexComponentMap.get(sourceId).getSource();
      String src2 = sequentialVertexComponentMap.get(targetId).getSource();

      String key1 = sec.getSourceId() + src2;
      String key2 = sec.getTargetId() + src1;

      int order1 = 1;
      int order2 = 1;

      if (vertexSourceMap.containsKey(key1)) {
        double topSimValue = vertexSourceMap.get(key1).get(0).getValue();
        if (simValue < topSimValue) {
          order1 = 0;
        } else {
          List<Pair<String, Double>> pairs = vertexSourceMap.get(key1);
          for (Pair<String, Double> pair : pairs) {
            if (pair.getValue() < simValue) {
              int strengthDegree =
                Integer.parseInt(sequentialEdgeComponentMap.get(pair.getKey()).getInfoFromIndex(0));
              strengthDegree--;
              if (strengthDegree < 0) {
                strengthDegree = 0;
              }
              sequentialEdgeComponentMap.get(pair.getKey()).setInfoAtIndex(0,
                String.valueOf(strengthDegree));
            }
          }
        }
        List<Pair<String, Double>> pairs = vertexSourceMap.get(key1);
        pairs.add(Pair.of(secKey, simValue));
        pairs.sort(Comparator.comparing(p -> -p.getValue()));
        vertexSourceMap.remove(key1);
        vertexSourceMap.put(key1, pairs);
      } else {
        List<Pair<String, Double>> pairs = new ArrayList<>();
        pairs.add(Pair.of(secKey, simValue));
        vertexSourceMap.put(key1, pairs);
      }

      if (vertexSourceMap.containsKey(key2)) {
        double topSimValue = vertexSourceMap.get(key2).get(0).getValue();
        if (simValue < topSimValue) {
          order2 = 0;
        } else {
          List<Pair<String, Double>> pairs = vertexSourceMap.get(key2);
          for (Pair<String, Double> pair : pairs) {
            if (pair.getValue() < simValue) {
              int strengthDegree =
                Integer.parseInt(sequentialEdgeComponentMap.get(pair.getKey()).getInfoFromIndex(0));
              strengthDegree--;
              if (strengthDegree < 0) {
                strengthDegree = 0;
              }
              sequentialEdgeComponentMap.get(pair.getKey()).setInfoAtIndex(0,
                String.valueOf(strengthDegree));
            }
          }
        }
        List<Pair<String, Double>> pairs = vertexSourceMap.get(key2);
        pairs.add(Pair.of(secKey, simValue));
        pairs.sort(Comparator.comparing(p -> -p.getValue()));
        vertexSourceMap.remove(key2);
        vertexSourceMap.put(key2, pairs);
      } else {
        List<Pair<String, Double>> pairs = new ArrayList<>();
        pairs.add(Pair.of(secKey, simValue));
        vertexSourceMap.put(key2, pairs);
      }

      int order = order1 + order2;
      sequentialEdgeComponentMap.get(secKey).setInfoAtIndex(0, String.valueOf(order));
    });
  }

  @Override
  protected boolean isMergeable(String clusterId1, String clusterId2) {
    List<String> vList1 = clusterList.get(clusterId1);
    List<String> vList2 = clusterList.get(clusterId2);
    List<String> srcList1 = new ArrayList<>();
    for (String v : vList1) {
      if (sequentialVertexComponentMap.containsKey(v)) {
        srcList1.add(sequentialVertexComponentMap.get(v).getSource());
      }
    }
    for (String v : vList2) {
      if (sequentialVertexComponentMap.containsKey(v) &&
        srcList1.contains(sequentialVertexComponentMap.get(v).getSource())) {
        return false;
      }
    }
    return true;
  }

  @Override
  protected boolean isAddable(String vertexId, String clusterId) {
    return isSourceConsistent(vertexId, clusterId) && isOptimal(vertexId, clusterId);
  }

  @Override
  public AbstractSequentialIncremental buildRepairingExecutor() {
    return this;
  }

  /**
   * Checks whether adding the vertex with the specified vertexId to the cluster with the specified
   * clusterId keeps the source consistency constraint.
   *
   * @param vertexId  The vertexId of the vertex that is going to be changed
   * @param clusterId The clusterId of the cluster that is going to be changed
   *
   * @return Whether source consistency is kept
   */
  private boolean isSourceConsistent(String vertexId, String clusterId) {
    List<String> theOtherVertices = clusterList.get(clusterId);
    String vertexSource = sequentialVertexComponentMap.get(vertexId).getSource();
    for (String otherVertexId : theOtherVertices) {
      if (sequentialVertexComponentMap.get(otherVertexId).getSource().equals(vertexSource)) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks whether adding the vertex with the specified vertexId to the cluster with the specified
   * clusterId is optimal.
   *
   * @param vertexId  The vertexId of the vertex that is going to be changed
   * @param clusterId The clusterId of the cluster that is going to be changed
   *
   * @return Whether the adding is optimal
   */
  private boolean isOptimal(String vertexId, String clusterId) {
    String vertexClusterId = sequentialVertexComponentMap.get(vertexId).getClusterId();
    List<NeighborInfo> neighborInfos = neighbors.get(vertexId);
    boolean isMoveStrength = false;
    boolean isStayStrength = false;

    double minMovePriority = Integer.MAX_VALUE;
    double maxStayPriority = 0.0;

    for (NeighborInfo neighborInfo : neighborInfos) {
      int strengthDegree = Integer.parseInt(neighborInfo.getOtherInfo(0));
      double priority = computePriority(neighborInfo);

      if (sequentialVertexComponentMap.get(neighborInfo.getVertexId()).getClusterId()
        .equals(vertexClusterId)) {
        if (strengthDegree == 2) {
          isStayStrength = true;
        }
        if (priority > maxStayPriority) {
          maxStayPriority = priority;
        }
      } else if (sequentialVertexComponentMap.get(neighborInfo.getVertexId()).getClusterId()
        .equals(clusterId)) {
        if (strengthDegree == 2) {
          isMoveStrength = true;
        }
        if (priority < minMovePriority) {
          minMovePriority = priority;
        }
      }
    }
    if (isMoveStrength && !isStayStrength) {
      return true;
    } else if (!isMoveStrength && isStayStrength) {
      return false;
    }
    if (minMovePriority > maxStayPriority) {
      return true;
    }
    return false;
  }

  /**
   * Computes the priority of one edge
   *
   * @param info The information of a neighbor (the connecting link information) is included
   *
   * @return The computed priority
   */
  public double computePriority(NeighborInfo info) {
    int strengthDegree = Integer.parseInt(info.getOtherInfo().get(0));
    return strengthCoef * strengthDegree + simValueCoef * info.getSimValue();
  }
}



