/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.sequentialIncremental.common;

/**
 * All common property names used for sequential incremental repairing
 */
public class PropertyNames {

  /**
   * The file name of edges path
   */
  public static final String EDGES_PATH = "edges.csv";

  /**
   * The file name of vertices path
   */
  public static final String VERTICES_PATH = "vertices.csv";
}
