/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils;

import org.apache.flink.api.java.DataSet;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.incremental.parallelIncremental.common.functions.EPGMGraphIdResetter;
import org.gradoop.famer.incremental.parallelIncremental.common.functions.SingletonMaker;
import org.gradoop.famer.incremental.parallelIncremental.common.utils.Clusterer;
import org.gradoop.flink.model.api.operators.UnaryGraphToGraphOperator;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.epgm.LogicalGraphFactory;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.incremental.common.PropertyNames.NEW_LINK;
import static org.gradoop.famer.incremental.common.PropertyNames.NEW_PREFIX;

/**
 * Run clustering with the desired clustering algorithm among unclustered vertices. If no clustering
 * algorithm is set (null), it just makes singletons out of unclustered vertices.
 */
public class PreClusterer implements UnaryGraphToGraphOperator {

  /**
   * The used clustering algorithm
   */
  private final AbstractParallelClustering clusteringAlgorithm;

  /**
   * The prefix string for clusterIds of pre-clustering
   */
  private final String clusterIdPrefix;

  /**
   * Creates an instance of PreClusterer
   *
   * @param clusteringAlgorithm The used clustering algorithm
   * @param preClusteringPrefix The prefix string for clusterIds of pre-clustering
   */
  public PreClusterer(AbstractParallelClustering clusteringAlgorithm, String preClusteringPrefix) {
    this.clusteringAlgorithm = clusteringAlgorithm;
    this.clusterIdPrefix = NEW_PREFIX + preClusteringPrefix;
  }

  @Override
  public LogicalGraph execute(LogicalGraph inputGraph) {
    LogicalGraphFactory logicalGraphFactory = inputGraph.getConfig().getLogicalGraphFactory();

    DataSet<EPGMVertex> unclusteredVertices =
      inputGraph.getVertices().filter(v -> !v.hasProperty(CLUSTER_ID));
    DataSet<EPGMVertex> oldVertices = inputGraph.getVertices().filter(v -> v.hasProperty(CLUSTER_ID));

    /* If no clustering algorithm is set make singletons or run clustering with the given algorithm */
    if (clusteringAlgorithm == null) {
      unclusteredVertices = unclusteredVertices.map(new SingletonMaker());
    } else {
      DataSet<EPGMEdge> newLinks = inputGraph.getEdges().filter(e -> e.hasProperty(NEW_LINK));

      unclusteredVertices = logicalGraphFactory.fromDataSets(unclusteredVertices, newLinks)
        .callForGraph(new Clusterer(clusteringAlgorithm, clusterIdPrefix)).getVertices();
    }
    DataSet<EPGMVertex> vertices = unclusteredVertices.union(oldVertices).map(new EPGMGraphIdResetter<>());
    DataSet<EPGMEdge> edges = inputGraph.getEdges().map(new EPGMGraphIdResetter<>());

    return logicalGraphFactory.fromDataSets(vertices, edges);
  }
}
