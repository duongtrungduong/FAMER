/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.sequentialIncremental.common.dataStructures;

import java.util.ArrayList;
import java.util.List;

/**
 * Edge structure for sequential incremental repair algorithms
 */
public class SequentialEdgeComponent {

  /**
   * The source id of the edge component
   */
  private String sourceId;

  /**
   * The target id of the edge component
   */
  private String targetId;

  /**
   * The degree for the edge component
   */
  private double simValue;

  /**
   * A list for storing other required information by different algorithms
   */
  private List<String> additionalInfo;

  /**
   * Creates an instance of SequentialEdgeComponent
   *
   * @param sourceId The source id of the edge component
   * @param targetId The target id of the edge component
   * @param simValue The degree for the edge component
   */
  public SequentialEdgeComponent(String sourceId, String targetId, double simValue) {
    this.sourceId = sourceId;
    this.targetId = targetId;
    this.simValue = simValue;
    this.additionalInfo = new ArrayList<>();
  }

  public String getSourceId() {
    return sourceId;
  }

  public String getTargetId() {
    return targetId;
  }

  public double getSimValue() {
    return simValue;
  }

  public List<String> getInfo() {
    return additionalInfo;
  }

  /**
   * Stores information at the specified index
   *
   * @param index The list index to store the information at
   * @param info The information to be stored
   */
  public void setInfoAtIndex(int index, String info) {
    this.additionalInfo.listIterator(index).add(info);
  }

  /**
   * Retrieves the information from the specified index
   *
   * @param index The list index to retrieve the information from
   *
   * @return The retrieved information from the given index
   */
  public String getInfoFromIndex(int index) {
    return additionalInfo.get(index);
  }
}
