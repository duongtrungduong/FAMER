/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.sequentialIncremental.common.dataStructures;

import java.util.List;

/**
 * The information of a neighbor vertex and its connecting edge
 */
public class NeighborInfo {

  /**
   * Id of the neighbor vertex
   */
  private String vertexId;

  /**
   * The similarity value of the connecting edge
   */
  private double simValue;

  /**
   * A list for storing other required information by different algorithms
   */
  private List<String> otherInfo;

  /**
   * Creates an instance of NeighborInfo
   *
   * @param vertexId  Id of the neighbor vertex
   * @param simValue  The similarity value of the connecting edge
   * @param otherInfo A list for storing other required information by different algorithms
   */
  public NeighborInfo(String vertexId, double simValue, List<String> otherInfo) {
    this.vertexId = vertexId;
    this.simValue = simValue;
    this.otherInfo = otherInfo;
  }

  public String getVertexId() {
    return vertexId;
  }

  public double getSimValue() {
    return simValue;
  }

  public List<String> getOtherInfo() {
    return otherInfo;
  }

  /**
   * Retrieves the information from the specified index
   *
   * @param index The list index to retrieve the information from
   *
   * @return The retrieved information from the given index
   */
  public String getOtherInfo(int index) {
    return otherInfo.get(index);
  }
}
