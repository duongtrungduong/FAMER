/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.sequentialIncremental.common.dataStructures;

/**
 * Enumeration for the available incremental repairing algorithms. Each enum value holds the full class name
 * for the incremental repairing algorithm, which is used in the famer configuration to instantiate the
 * classes with Java reflections.
 */
public enum SequentialIncrementalRepairingMethod {
  /**
   * GreedyCLIP algorithm
   */
  GREEDY_CLIP("org.gradoop.famer.incremental.sequentialIncremental.baseMethod.GreedyRepair.GreedyCLIP");

  /**
   * The String representation for the class name used for reflections
   */
  private final String fullClassName;

  /**
   * Creates an instance of IncrementalRepairingMethod
   *
   * @param fullClassName The String representation for the class name used for reflections
   */
  SequentialIncrementalRepairingMethod(String fullClassName) {
    this.fullClassName = fullClassName;
  }

  /**
   * Returns the package name for the incremental repairing algorithm
   *
   * @return The package name
   */
  public String getFullClassName() {
    return this.fullClassName;
  }
}
