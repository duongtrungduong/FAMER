/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.functions.Minus;
import org.gradoop.famer.clustering.common.functions.MinusVertexGroupReducer;
import org.gradoop.famer.clustering.common.functions.VertexToVertexClusterId;
import org.gradoop.famer.clustering.parallelClustering.clip.functions.VertexToVertexVertexId;
import org.gradoop.famer.incremental.parallelIncremental.common.utils.IncrementalUtils;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions.Merger;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions.MergingClusterIdGetter;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.incremental.common.PropertyNames.NEW_PREFIX;

/**
 * Merge clusters by assigning the existing clusterIds to the new vertices.
 */
public class ClusterMerger {

  /**
   * Executes the ClusterMerger
   *
   * @param simVertex1Vertex2 The paired vertices with sim value
   * @param vertices all vertices
   *
   * @return The output vertices with the final clusterIds
   */
  public DataSet<EPGMVertex> execute(DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> simVertex1Vertex2,
    DataSet<EPGMVertex> vertices) {
    DataSet<Tuple2<String, String>> newClsIdOldClsId =
      simVertex1Vertex2.flatMap(new MergingClusterIdGetter()).distinct(0, 1);
    // filter for new and old vertices by checking the cluster id prefix
    DataSet<EPGMVertex> vNew =
      vertices.filter(v -> IncrementalUtils.isNew(v.getPropertyValue(CLUSTER_ID).toString()));
    DataSet<EPGMVertex> vOld =
      vertices.filter(v -> !IncrementalUtils.isNew(v.getPropertyValue(CLUSTER_ID).toString()));

    DataSet<Tuple2<EPGMVertex, String>> vNewClusterId =
      vNew.flatMap(new VertexToVertexClusterId(CLUSTER_ID, false));

    DataSet<EPGMVertex> vNewMerged = vNewClusterId.join(newClsIdOldClsId)
      .where(1).equalTo(0)
      .with(new Merger());

    DataSet<EPGMVertex> vSingle = new Minus<>(new MinusVertexGroupReducer())
      .execute(vNew.map(new VertexToVertexVertexId()), vNewMerged.map(new VertexToVertexVertexId()));

    /* Removes the NEW_PREFIX from clusterIds so that the new vertices of this increment are not mixed
    up with the new vertices of the next increment*/
    vSingle = vSingle.map((MapFunction<EPGMVertex, EPGMVertex>) epgmVertex -> {
      String clusterId = epgmVertex.getPropertyValue(CLUSTER_ID).toString().replaceFirst(NEW_PREFIX, "");
      epgmVertex.setProperty(CLUSTER_ID, clusterId);
      return epgmVertex;
    });

    vNew = vNewMerged.union(vSingle);

    return vNew.union(vOld);
  }
}
