/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.common.utils;

import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.pojo.EPGMVertexFactory;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.incremental.common.PropertyNames.NEW_PREFIX;
import static org.junit.Assert.*;

/**
 * JUnit tests for {@link IncrementalUtils}
 */
public class IncrementalUtilsTest extends GradoopFlinkTestBase {

  @Test
  public void testIsNew() throws Exception {
    assertTrue(IncrementalUtils.isNew(NEW_PREFIX + "test"));
    assertFalse(IncrementalUtils.isNew("test"));
  }

  @Test
  public void testIsConsistent() throws Exception  {
    List<PropertyValue> srcList1 = Arrays.asList(PropertyValue.create("src1"), PropertyValue.create("src2"));
    List<PropertyValue> srcList2 = Arrays.asList(PropertyValue.create("src3"), PropertyValue.create("src4"));
    List<PropertyValue> srcList3 = Collections.singletonList(PropertyValue.create("src1"));

    assertTrue(IncrementalUtils.isConsistent(srcList1, srcList2));
    assertFalse(IncrementalUtils.isConsistent(srcList1, srcList3));
  }

  @Test
  public void testGetLinkStatus()  throws Exception {
    EPGMVertexFactory factory = new EPGMVertexFactory();
    EPGMVertex vertex1 = factory.createVertex();
    vertex1.setProperty(CLUSTER_ID, "c1");
    EPGMVertex vertex2 = factory.createVertex();
    vertex2.setProperty(CLUSTER_ID, "c2");
    EPGMVertex vertex3 = factory.createVertex();
    EPGMVertex vertex4 = factory.createVertex();

    assertEquals(0, IncrementalUtils.getLinkStatus(vertex3, vertex4));
    assertEquals(2, IncrementalUtils.getLinkStatus(vertex1, vertex2));
    assertEquals(1, IncrementalUtils.getLinkStatus(vertex1, vertex3));
  }

  @Test
  public void testGetNeighborClusterId()  throws Exception {
    EPGMVertexFactory factory = new EPGMVertexFactory();
    EPGMVertex vertex1 = factory.createVertex();
    vertex1.setProperty(CLUSTER_ID, "c1");
    EPGMVertex vertex2 = factory.createVertex();
    vertex2.setProperty(CLUSTER_ID, "c2");
    EPGMVertex vertex3 = factory.createVertex();
    EPGMVertex vertex4 = factory.createVertex();

    assertEquals("", IncrementalUtils.getNeighborClusterId(vertex3, vertex4));
    assertEquals("c1", IncrementalUtils.getNeighborClusterId(vertex1, vertex2));
    assertEquals("c1", IncrementalUtils.getNeighborClusterId(vertex1, vertex3));
  }
}
