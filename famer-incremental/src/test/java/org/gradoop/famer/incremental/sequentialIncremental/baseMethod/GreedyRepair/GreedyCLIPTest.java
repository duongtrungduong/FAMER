/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.sequentialIncremental.baseMethod.GreedyRepair;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.incremental.TestUtils;
import org.junit.Test;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.gradoop.famer.incremental.sequentialIncremental.common.PropertyNames.VERTICES_PATH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * JUnit tests for {@link GreedyCLIP}
 */
public class GreedyCLIPTest {

  @Test
  public void testInstantiationWithJSON() throws Exception {
    String configString = "{\n" +
      " \"repairingMethod\":\"GREEDY_CLIP\",\n" +
      " \"basePath\":\"basePath\",\n" +
      " \"newPath\":\"newPath\",\n" +
      " \"outputPath\":\"outputPath\",\n" +
      " \"strengthCoef\":0.3,\n" +
      " \"simValueCoef\":0.5,\n" +
      " \"degreeCoef\":0.2 \n" +
      "}";
    GreedyCLIP greedyCLIP = new GreedyCLIP(new JSONObject(configString));

    assertEquals("basePath", greedyCLIP.getBaseInputPath());
    assertEquals("newPath", greedyCLIP.getNewInputPath());
    assertEquals("outputPath", greedyCLIP.getOutputPath());
    assertEquals(0.3, greedyCLIP.getStrengthCoef(), 0.0);
    assertEquals(0.5, greedyCLIP.getSimValueCoef(), 0.0);
    assertEquals(0.2, greedyCLIP.getDegreeCoef(), 0.0);
  }

  @Test(expected = RuntimeException.class)
  public void testInstantiationWithJSONMissingStrengthCoeff() throws Exception {
    String configString = "{\n" +
      " \"repairingMethod\":\"GREEDY_CLIP\",\n" +
      " \"basePath\":\"basePath\",\n" +
      " \"newPath\":\"newPath\",\n" +
      " \"outputPath\":\"outputPath\",\n" +
      " \"simValueCoef\":0.5,\n" +
      " \"degreeCoef\":0.2 \n" +
      "}";
    new GreedyCLIP(new JSONObject(configString));
  }

  @Test(expected = RuntimeException.class)
  public void testInstantiationWithJSONMissingSimValueCoeff() throws Exception {
    String configString = "{\n" +
      " \"repairingMethod\":\"GREEDY_CLIP\",\n" +
      " \"basePath\":\"basePath\",\n" +
      " \"newPath\":\"newPath\",\n" +
      " \"outputPath\":\"outputPath\",\n" +
      " \"strengthCoef\":0.3,\n" +
      " \"degreeCoef\":0.2 \n" +
      "}";
    new GreedyCLIP(new JSONObject(configString));
  }

  @Test(expected = RuntimeException.class)
  public void testInstantiationWithJSONMissingDegreeCoeff() throws Exception {
    String configString = "{\n" +
      " \"repairingMethod\":\"GREEDY_CLIP\",\n" +
      " \"basePath\":\"basePath\",\n" +
      " \"newPath\":\"newPath\",\n" +
      " \"outputPath\":\"outputPath\",\n" +
      " \"strengthCoef\":0.3,\n" +
      " \"simValueCoef\":0.5,\n" +
      "}";
    new GreedyCLIP(new JSONObject(configString));
  }

  @Test
  public void testExecutionWithMerge() throws Exception {
    String testGraphPath = new File(GreedyCLIP.class.getResource(
      "/sequentialIncremental/baseMethods/GreedyRepair/mergeTest").getPath()).getAbsolutePath();
    String outputPath = Files.createTempDirectory("famerSeqIncrGreedyMergeTestResult").toString();

    new GreedyCLIP(testGraphPath, testGraphPath, outputPath, 0.5, 0.3, 0.2).execute();

    assertTrue(new File(outputPath + File.separator + VERTICES_PATH).exists());

    List<Set<String>> result = TestUtils.createSortedClusterSets(outputPath);

    List<Set<String>> expectedResult =
      Collections.singletonList(new HashSet<>(Arrays.asList("v1", "v2", "v3")));

    assertFalse(result.isEmpty());
    assertEquals(1, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testExecutionWithSplit() throws Exception {
    String testGraphPath = new File(GreedyCLIP.class.getResource(
      "/sequentialIncremental/baseMethods/GreedyRepair/splitTest").getPath()).getAbsolutePath();
    String outputPath = Files.createTempDirectory("famerSeqIncrGreedySplitTestResult").toString();

    new GreedyCLIP(testGraphPath, testGraphPath, outputPath, 0.5, 0.3, 0.2).execute();

    assertTrue(new File(outputPath + File.separator + VERTICES_PATH).exists());

    List<Set<String>> result = TestUtils.createSortedClusterSets(outputPath);

    List<Set<String>> expectedResult = Arrays.asList(
      new HashSet<>(Arrays.asList("v1", "v2")),
      new HashSet<>(Collections.singletonList("v3")));

    assertFalse(result.isEmpty());
    assertEquals(2, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testExecutionWithMoveChange() throws Exception {
    String testGraphPath = new File(GreedyCLIP.class.getResource(
      "/sequentialIncremental/baseMethods/GreedyRepair/moveTest/change").getPath()).getAbsolutePath();
    String outputPath = Files.createTempDirectory("famerSeqIncrGreedyMoveChangeTestResult").toString();

    new GreedyCLIP(testGraphPath, testGraphPath, outputPath, 0.5, 0.3, 0.2).execute();

    assertTrue(new File(outputPath + File.separator + VERTICES_PATH).exists());

    List<Set<String>> result = TestUtils.createSortedClusterSets(outputPath);

    List<Set<String>> expectedResult = Arrays.asList(
      new HashSet<>(Arrays.asList("v1", "v2", "v3")),
      new HashSet<>(Collections.singletonList("v4")));

    assertFalse(result.isEmpty());
    assertEquals(2, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testExecutionWithMoveNoChange() throws Exception {
    String testGraphPath = new File(GreedyCLIP.class.getResource(
      "/sequentialIncremental/baseMethods/GreedyRepair/moveTest/noChange").getPath()).getAbsolutePath();
    String outputPath = Files.createTempDirectory("famerSeqIncrGreedyMoveNoChangeTestResult").toString();

    new GreedyCLIP(testGraphPath, testGraphPath, outputPath, 0.5, 0.3, 0.2).execute();

    assertTrue(new File(outputPath + File.separator + VERTICES_PATH).exists());

    List<Set<String>> result = TestUtils.createSortedClusterSets(outputPath);

    List<Set<String>> expectedResult = Arrays.asList(
      new HashSet<>(Arrays.asList("v1", "v2")),
      new HashSet<>(Arrays.asList("v3", "v4")));

    assertFalse(result.isEmpty());
    assertEquals(2, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testExecutionWithMove2Points() throws Exception {
    String testGraphPath = new File(GreedyCLIP.class.getResource(
      "/sequentialIncremental/baseMethods/GreedyRepair/moveTest/2points").getPath()).getAbsolutePath();
    String outputPath = Files.createTempDirectory("famerSeqIncrGreedyMove2PointsTestResult").toString();

    new GreedyCLIP(testGraphPath, testGraphPath, outputPath, 0.5, 0.3, 0.2).execute();

    assertTrue(new File(outputPath + File.separator + VERTICES_PATH).exists());

    List<Set<String>> result = TestUtils.createSortedClusterSets(outputPath);

    List<Set<String>> expectedResult = Arrays.asList(
      new HashSet<>(Arrays.asList("v1", "v2", "v3")),
      new HashSet<>(Arrays.asList("v4", "v5")));

    assertFalse(result.isEmpty());
    assertEquals(2, result.size());
    assertEquals(expectedResult, result);
  }
}
