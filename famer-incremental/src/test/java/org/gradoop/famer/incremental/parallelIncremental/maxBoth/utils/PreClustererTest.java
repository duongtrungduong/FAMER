/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils;

import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.ConnectedComponents;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.incremental.common.PropertyNames.NEW_LINK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * JUnit tests for {@link PreClusterer}
 */
public class PreClustererTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  private final String prefix = "preClusterPrefix";

  @Before
  public void setUp() throws Exception {
    String graphString = "input[" +
      "(v1:Node {id:1, " + CLUSTER_ID + ":\"c1\"})" +
      "(v2:Node {id:2, " + CLUSTER_ID + ":\"c1\"})" +
      "(v3:Node {id:3})" +
      "(v4:Node {id:4})" +
      "(v5:Node {id:5})" +
      "(v1)-[e1 {id:\"e1\"}]->(v2)" +
      "(v3)-[e2 {id:\"e2\", " + NEW_LINK + ":true}]->(v4)" +
      "]";

    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");
  }

  @Test
  public void testPreClusteringWithConnComp() throws Exception {
    LogicalGraph clusteredGraph =
      new PreClusterer(new ConnectedComponents(20), prefix).execute(inputGraph);

    List<EPGMVertex> vertices = clusteredGraph.getVertices().collect();
    vertices.forEach(vertex -> assertTrue(vertex.hasProperty(CLUSTER_ID)));

    assertEquals(3, clusteredGraph.splitBy(CLUSTER_ID).getGraphHeads().collect().size());
  }

  @Test
  public void testPreClusteringWithNoClusterAlgo() throws Exception {
    LogicalGraph clusteredGraph = new PreClusterer(null, prefix).execute(inputGraph);

    List<EPGMVertex> vertices = clusteredGraph.getVertices().collect();
    vertices.forEach(vertex -> assertTrue(vertex.hasProperty(CLUSTER_ID)));

    assertEquals(4, clusteredGraph.splitBy(CLUSTER_ID).getGraphHeads().collect().size());
  }
}
