/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.flink.api.java.DataSet;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.incremental.sequentialIncremental.common.PropertyNames.VERTICES_PATH;

/**
 * Commonly used methods for testing
 */
public class TestUtils {

  /**
   * Creates the list of clusters from the given DataSet of vertices. Each cluster is represented as a set of
   * members (the vertex ids of the members)
   */
  public static List<Set<String>> createSortedClusterSets(DataSet<EPGMVertex> vertices) throws Exception {
    // create pairs: vertexId-clusterId
    List<Pair<String, String>> vertexIdClusterId = vertices.collect().stream().map(vertex -> Pair.of(
      vertex.getPropertyValue("id").toString(),
      vertex.getPropertyValue(CLUSTER_ID).getString()))
      .collect(Collectors.toList());

    return groupAndSortResultPairs(vertexIdClusterId);
  }

  /**
   * Creates the list of clusters from the vertex file written to the the given path. Each cluster is
   * represented as a set of members (the vertex ids of the members)
   *
   * @throws IOException Thrown on errors while reading the clustering result file.
   */
  public static List<Set<String>> createSortedClusterSets(String path) throws IOException {
    // create pairs: vertexId-clusterId
    List<Pair<String, String>> vertexIdClusterId =
      Files.lines(Paths.get(path + File.separator + VERTICES_PATH), StandardCharsets.UTF_8).map(line -> {
        String[] lineData = line.split(",");
        return Pair.of(lineData[0], lineData[lineData.length - 1]);
      }).collect(Collectors.toList());

    return groupAndSortResultPairs(vertexIdClusterId);
  }

  private static List<Set<String>> groupAndSortResultPairs(List<Pair<String, String>> vertexIdClusterId) {
    Collection<List<Pair<String, String>>> pairsGroupedByClusterId =
      vertexIdClusterId.stream().collect(Collectors.groupingBy(Pair::getValue)).values();

    List<Set<String>> clusters = new ArrayList<>();
    for (List<Pair<String, String>> pairs : pairsGroupedByClusterId) {
      clusters.add(pairs.stream().map(Pair::getKey).collect(Collectors.toSet()));
    }

    clusters = clusters.stream().sorted(Comparator.comparing(set -> set.iterator().next()))
      .collect(Collectors.toList());

    return clusters;
  }
}
