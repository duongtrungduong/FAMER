/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMEdgeFactory;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.pojo.EPGMVertexFactory;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.clustering.common.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.clustering.common.PropertyNames.SIM_VALUE;
import static org.gradoop.famer.incremental.common.PropertyNames.SOURCE_LIST;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * JUnit tests for {@link ClusterSelector}.
 */
public class ClusterSelectorTest extends GradoopFlinkTestBase {

  private final EPGMVertexFactory vertexFactory = new EPGMVertexFactory();
  private final EPGMEdgeFactory edgeFactory = new EPGMEdgeFactory();

  private EPGMVertex v1;
  private EPGMVertex v2;
  private EPGMVertex v3;
  private EPGMVertex v4;
  private EPGMVertex v5;
  private EPGMVertex v6;
  private EPGMVertex v7;

  @Before
  public void setUp() {
    v1 = vertexFactory.createVertex("v1");
    v1.setProperty(SOURCE_LIST,
      PropertyValue.create(Arrays.asList(PropertyValue.create("src1"), PropertyValue.create("src2"))));
    v1.setProperty(CLUSTER_ID, "c1");

    v2 = vertexFactory.createVertex("v2");
    v2.setProperty(SOURCE_LIST,
      PropertyValue.create(Arrays.asList(PropertyValue.create("src1"), PropertyValue.create("src2"))));
    v2.setProperty(CLUSTER_ID, "c1");

    v3 = vertexFactory.createVertex("v3");
    v3.setProperty(SOURCE_LIST,
      PropertyValue.create(Collections.singletonList(PropertyValue.create("src2"))));
    v3.setProperty(CLUSTER_ID, "c2");

    v4 = vertexFactory.createVertex("v4");
    v4.setProperty(SOURCE_LIST,
      PropertyValue.create(Arrays.asList(PropertyValue.create("src3"), PropertyValue.create("src4"))));
    v4.setProperty(CLUSTER_ID, "new1");

    v5 = vertexFactory.createVertex("v5");
    v5.setProperty(SOURCE_LIST,
      PropertyValue.create(Arrays.asList(PropertyValue.create("src3"), PropertyValue.create("src4"))));
    v5.setProperty(CLUSTER_ID, "new1");

    v6 = vertexFactory.createVertex("v6");
    v6.setProperty(SOURCE_LIST,
      PropertyValue.create(Collections.singletonList(PropertyValue.create("src3"))));
    v6.setProperty(CLUSTER_ID, "new2");

    v7 = vertexFactory.createVertex("v7");
    v7.setProperty(SOURCE_LIST,
      PropertyValue.create(Collections.singletonList(PropertyValue.create("src4"))));
    v7.setProperty(CLUSTER_ID, "new3");
  }

  @Test
  public void testWithNoMultipleMaxBothLinks() throws Exception {
    EPGMEdge e1 = edgeFactory.createEdge("e1", v1.getId(), v4.getId());
    e1.setProperty(SIM_VALUE, 1.0);

    EPGMEdge e2 = edgeFactory.createEdge("e2", v3.getId(), v7.getId());
    e2.setProperty(SIM_VALUE, 1.0);

    LogicalGraph inputGraph = getConfig().getLogicalGraphFactory()
      .fromCollections(Arrays.asList(v1, v2, v3, v4, v5, v6, v7), Arrays.asList(e1, e2));

    DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> simVertex1Vertex2 = transformGraphRunSelector(inputGraph);

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(simVertex1Vertex2);
    List<Tuple2<String, String>> expectedResult = Arrays.asList(Tuple2.of("v1", "v4"), Tuple2.of("v3", "v7"));

    assertFalse(result.isEmpty());
    assertEquals(2, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testPrioritySelectionOnMaxBothLinks() throws Exception {
    EPGMEdge e1 = edgeFactory.createEdge("e1", v1.getId(), v4.getId());
    e1.setProperty(SIM_VALUE, 1.0);

    EPGMEdge e2 = edgeFactory.createEdge("e2", v1.getId(), v6.getId());
    e2.setProperty(SIM_VALUE, 0.9);

    EPGMEdge e3 = edgeFactory.createEdge("e3", v2.getId(), v7.getId());
    e3.setProperty(SIM_VALUE, 1.0);

    EPGMEdge e4 = edgeFactory.createEdge("e4", v3.getId(), v7.getId());
    e4.setProperty(SIM_VALUE, 1.0);

    LogicalGraph inputGraph = getConfig().getLogicalGraphFactory()
      .fromCollections(Arrays.asList(v1, v2, v3, v4, v5, v6, v7), Arrays.asList(e1, e2, e3, e4));

    DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> simVertex1Vertex2 = transformGraphRunSelector(inputGraph);

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(simVertex1Vertex2);
    List<Tuple2<String, String>> expectedResult = Arrays.asList(Tuple2.of("v1", "v4"), Tuple2.of("v3", "v7"));

    assertFalse(result.isEmpty());
    assertEquals(2, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testConflictForExistingClustersMergedToNewOne() throws Exception {
    EPGMEdge e1 = edgeFactory.createEdge("e1", v1.getId(), v4.getId());
    e1.setProperty(SIM_VALUE, 0.9);

    EPGMEdge e2 = edgeFactory.createEdge("e2", v1.getId(), v6.getId());
    e2.setProperty(SIM_VALUE, 0.9);

    EPGMEdge e3 = edgeFactory.createEdge("e3", v2.getId(), v7.getId());
    e3.setProperty(SIM_VALUE, 1.0);

    EPGMEdge e4 = edgeFactory.createEdge("e4", v3.getId(), v7.getId());
    e4.setProperty(SIM_VALUE, 1.0);

    LogicalGraph inputGraph = getConfig().getLogicalGraphFactory()
      .fromCollections(Arrays.asList(v1, v2, v3, v4, v5, v6, v7), Arrays.asList(e1, e2, e3, e4));

    DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> simVertex1Vertex2 = transformGraphRunSelector(inputGraph);

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(simVertex1Vertex2);
    List<Tuple2<String, String>> expectedResult = Arrays.asList(Tuple2.of("v1", "v6"), Tuple2.of("v2", "v7"));

    assertFalse(result.isEmpty());
    assertEquals(2, result.size());
    assertEquals(expectedResult, result);
  }

  private DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> transformGraphRunSelector(LogicalGraph inputGraph) {
    DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> simValueSourceVertexTargetVertex =
      new EdgeToEdgeSourceVertexTargetVertex(inputGraph).execute().map(
        (MapFunction<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>, Tuple3<Double, EPGMVertex, EPGMVertex>>) in ->
          Tuple3.of(in.f0.getPropertyValue(SIM_VALUE).getDouble(), in.f1, in.f2))
        .returns(new TypeHint<Tuple3<Double, EPGMVertex, EPGMVertex>>() { });

    return new ClusterSelector().execute(simValueSourceVertexTargetVertex);
  }

  private List<Tuple2<String, String>> collectAndTransformResultTuples(
    DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> simValueVertex1Vertex2) throws Exception {

    return simValueVertex1Vertex2.map(
      (MapFunction<Tuple3<Double, EPGMVertex, EPGMVertex>, Tuple2<String, String>>) input -> {
        String label1 = input.f1.getLabel();
        String label2 = input.f2.getLabel();
        if (label1.compareTo(label2) < 0) {
          return Tuple2.of(label1, label2);
        }
        return Tuple2.of(label2, label1);
      }).returns(new TypeHint<Tuple2<String, String>>() { })
      .collect().stream().sorted(Comparator.comparing(input -> input.f0 + input.f1))
      .collect(Collectors.toList());
  }
}
