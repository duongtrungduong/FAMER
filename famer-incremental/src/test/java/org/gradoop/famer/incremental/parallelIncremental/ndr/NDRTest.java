/*
 * Copyright © 2016 - 2020 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.ndr;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.ConnectedComponents;
import org.gradoop.famer.incremental.TestUtils;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.clustering.common.PropertyNames.GRAPH_LABEL;
import static org.gradoop.famer.clustering.common.PropertyNames.SIM_VALUE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * JUnit tests {@link NDR}
 */
public class NDRTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  @Before
  public void setUp() throws Exception {
    String graphString = "input[" +
      "(v1:Node {id:1, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v2:Node {id:2, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v3:Node {id:3, " + CLUSTER_ID + ":\"c2\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v4:Node {id:4, " + CLUSTER_ID + ":\"c2\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v5:Node {id:5, " + CLUSTER_ID + ":\"c3\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v6:Node {id:6, " + CLUSTER_ID + ":\"c4\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v7:Node {id:7, " + CLUSTER_ID + ":\"c4\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v8:Node {id:8, " + GRAPH_LABEL + ":\"src2\"})" +
      "(v9:Node {id:9, " + GRAPH_LABEL + ":\"src1\"})" +
      "(v10:Node {id:10," + GRAPH_LABEL + ":\"src2\"})" +
      "(v1)-[e0]->(v2)" +
      "(v1)-[e1 {" + SIM_VALUE + ":1.0}]->(v2)" +
      "(v1)-[e2 {" + SIM_VALUE + ":0.8}]->(v4)" +
      "(v2)-[e3 {" + SIM_VALUE + ":0.8}]->(v3)" +
      "(v1)-[e4 {" + SIM_VALUE + ":0.3}]->(v5)" +
      "(v3)-[e5 {" + SIM_VALUE + ":1.0}]->(v4)" +
      "(v3)-[e6 {" + SIM_VALUE + ":0.2}]->(v5)" +
      "(v4)-[e7 {" + SIM_VALUE + ":0.75}]->(v7)" +
      "(v5)-[e8 {" + SIM_VALUE + ":0.7}]->(v7)" +
      "(v6)-[e9 {" + SIM_VALUE + ":0.8}]->(v7)" +
      "(v6)-[e10 {" + SIM_VALUE + ":1.0}]->(v8)" +
      "(v6)-[e11 {" + SIM_VALUE + ":0.7}]->(v10)" +
      "(v8)-[e12 {" + SIM_VALUE + ":0.8}]->(v9)" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");
  }

  @Test
  public void testNDRWithDepth1() throws Exception {
    String json = "{" +
      " \"repairingMethod\":\"NDR\",\n" +
      " \"clustering\":{" +
      "   \"clusteringMethod\":\"CLIP\", " +
      "   \"clusteringOutputType\":\"GRAPH\", " +
      "   \"maxIteration\": \"MAX_VALUE\"," +
      "   \"clipConfig\":{" +
      "     \"delta\":\"0.0\"," +
      "     \"sourceNumber\":\"2\"," +
      "     \"removeSourceConsistentVertices\":false," +
      "     \"simValueCoef\":\"0.5\"," +
      "     \"degreeCoef\":\"0.2\"," +
      "     \"strengthCoef\":\"0.3\"" +
      "   }" +
      " }," +
      " \"clusterIdPrefix\":\"n1\"," +
      " \"depth\":1" +
      "}";
    JSONObject incrementalConfig = new JSONObject(json);

    LogicalGraph outputGraph = inputGraph.callForGraph(new NDR(incrementalConfig));

    assertEquals(10, outputGraph.getVertices().collect().size());
    assertEquals(13, outputGraph.getEdges().collect().size());

    List<Set<String>> result = TestUtils.createSortedClusterSets(outputGraph.getVertices());

    List<Set<String>> expectedResult = Arrays.asList(
      new HashSet<>(Arrays.asList("1", "2")),
      new HashSet<>(Collections.singletonList("10")),
      new HashSet<>(Arrays.asList("3", "4")),
      new HashSet<>(Collections.singletonList("5")),
      new HashSet<>(Arrays.asList("6", "8")),
      new HashSet<>(Collections.singletonList("7")),
      new HashSet<>(Collections.singletonList("9")));

    assertFalse(result.isEmpty());
    assertEquals(7, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testNDRWithDepth2() throws Exception {
    String json = "{" +
      " \"repairingMethod\":\"NDR\",\n" +
      " \"clustering\":{" +
      "   \"clusteringMethod\":\"CLIP\", " +
      "   \"clusteringOutputType\":\"GRAPH\", " +
      "   \"maxIteration\": \"MAX_VALUE\"," +
      "   \"clipConfig\":{" +
      "     \"delta\":\"0.0\"," +
      "     \"sourceNumber\":\"2\"," +
      "     \"removeSourceConsistentVertices\":false," +
      "     \"simValueCoef\":\"0.5\"," +
      "     \"degreeCoef\":\"0.2\"," +
      "     \"strengthCoef\":\"0.3\"" +
      "   }" +
      " }," +
      " \"clusterIdPrefix\":\"n2\"," +
      " \"depth\":2" +
      "}";
    JSONObject incrementalConfig = new JSONObject(json);

    LogicalGraph outputGraph = inputGraph.callForGraph(new NDR(incrementalConfig));

    assertEquals(10, outputGraph.getVertices().collect().size());
    assertEquals(13, outputGraph.getEdges().collect().size());

    List<Set<String>> result = TestUtils.createSortedClusterSets(outputGraph.getVertices());

    List<Set<String>> expectedResult = Arrays.asList(
      new HashSet<>(Arrays.asList("1", "2")),
      new HashSet<>(Collections.singletonList("10")),
      new HashSet<>(Arrays.asList("3", "4")),
      new HashSet<>(Arrays.asList("5", "7")),
      new HashSet<>(Arrays.asList("6", "8")),
      new HashSet<>(Collections.singletonList("9")));

    assertFalse(result.isEmpty());
    assertEquals(6, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testNDRWithDepth3() throws Exception {
    String json = "{" +
      " \"repairingMethod\":\"NDR\",\n" +
      " \"clustering\":{" +
      "   \"clusteringMethod\":\"CLIP\", " +
      "   \"clusteringOutputType\":\"GRAPH\", " +
      "   \"maxIteration\": \"MAX_VALUE\"," +
      "   \"clipConfig\":{" +
      "     \"delta\":\"0.0\"," +
      "     \"sourceNumber\":\"2\"," +
      "     \"removeSourceConsistentVertices\":false," +
      "     \"simValueCoef\":\"0.5\"," +
      "     \"degreeCoef\":\"0.2\"," +
      "     \"strengthCoef\":\"0.3\"" +
      "   }" +
      " }," +
      " \"clusterIdPrefix\":\"n3\"," +
      " \"depth\":3" +
      "}";
    JSONObject incrementalConfig = new JSONObject(json);

    LogicalGraph outputGraph = inputGraph.callForGraph(new NDR(incrementalConfig));

    assertEquals(10, outputGraph.getVertices().collect().size());
    assertEquals(13, outputGraph.getEdges().collect().size());

    List<Set<String>> result = TestUtils.createSortedClusterSets(outputGraph.getVertices());

    List<Set<String>> expectedResult = Arrays.asList(
      new HashSet<>(Arrays.asList("1", "2")),
      new HashSet<>(Collections.singletonList("10")),
      new HashSet<>(Arrays.asList("3", "4")),
      new HashSet<>(Arrays.asList("5", "7")),
      new HashSet<>(Arrays.asList("6", "8")),
      new HashSet<>(Collections.singletonList("9")));

    assertFalse(result.isEmpty());
    assertEquals(6, result.size());
    assertEquals(expectedResult, result);
  }

  @Test(expected = RuntimeException.class)
  public void testInstantiationWithJSONConfigExpectException1() throws Exception {
    String json = "{" +
      " \"repairingMethod\":\"NDR\",\n" +
      " \"clustering\":{" +
      "   \"clusteringMethod\":\"CONNECTED_COMPONENTS\"," +
      "   \"clusteringOutputType\":\"GRAPH\"," +
      "   \"maxIteration\":\"MAX_VALUE\"" +
      " },\n" +
      " \"depth\":3" +
      "}";
    JSONObject incrementalConfig = new JSONObject(json);

    new NDR(incrementalConfig);
  }

  @Test(expected = RuntimeException.class)
  public void testInstantiationWithJSONConfigExpectException2() throws Exception {
    String json = "{" +
      " \"repairingMethod\":\"NDR\",\n" +
      " \"clustering\":{" +
      "   \"clusteringMethod\":\"CONNECTED_COMPONENTS\"," +
      "   \"clusteringOutputType\":\"GRAPH\"," +
      "   \"maxIteration\":\"MAX_VALUE\"" +
      " },\n" +
      " \"clusterIdPrefix\":\"n3\"" +
      "}";
    JSONObject incrementalConfig = new JSONObject(json);

    new NDR(incrementalConfig);
  }

  @Test
  public void testForBaseGraphExpectNoChange() throws Exception {
    String graphString = "input[" +
      "(v1:Node {id:1, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v2:Node {id:2, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v3:Node {id:3, " + CLUSTER_ID + ":\"c2\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v4:Node {id:4, " + CLUSTER_ID + ":\"c2\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v5:Node {id:5, " + CLUSTER_ID + ":\"c3\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v6:Node {id:6, " + CLUSTER_ID + ":\"c4\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v7:Node {id:7, " + CLUSTER_ID + ":\"c4\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v1)-[e0]->(v2)" +
      "(v1)-[e1 {" + SIM_VALUE + ":1.0}]->(v2)" +
      "(v1)-[e2 {" + SIM_VALUE + ":0.8}]->(v4)" +
      "(v2)-[e3 {" + SIM_VALUE + ":0.8}]->(v3)" +
      "(v1)-[e4 {" + SIM_VALUE + ":0.3}]->(v5)" +
      "(v3)-[e5 {" + SIM_VALUE + ":1.0}]->(v4)" +
      "(v3)-[e6 {" + SIM_VALUE + ":0.2}]->(v5)" +
      "(v4)-[e7 {" + SIM_VALUE + ":0.75}]->(v7)" +
      "(v5)-[e8 {" + SIM_VALUE + ":0.7}]->(v7)" +
      "(v6)-[e9 {" + SIM_VALUE + ":0.8}]->(v7)" +
      "]";
    LogicalGraph baseGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    LogicalGraph resultGraph =
      new NDR(new ConnectedComponents(20), "1dr-cc", 1).execute(baseGraph);

    collectAndAssertTrue(resultGraph.equalsByData(baseGraph));
  }
}
